package com.ajwcc.epinurse.studenthealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_student_health_assessment_page3)
public class Page3Fragment extends BaseEpinurseFragment {


    public Page3Fragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup bhSuspension;

	@ViewById
	@MapToModelField
	protected EditText bhSuspensionSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup bhAntiSocialBehavior;

	@ViewById
	@MapToModelField
	protected RadioGroup bhDelinquency;

	@ViewById
	@MapToModelField
	protected RadioGroup bhViolence;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSmoking;

	@ViewById
	@MapToModelField
	protected RadioGroup bhAlcohol;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSubstanceAbuse;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSuicidalThoughts;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSuicidalAttempts;

	@ViewById
	@MapToModelField
	protected EditText breakfastPlace;

	@ViewById
	@MapToModelField
	protected EditText breakfastTime;

	@ViewById
	@MapToModelField
	protected EditText breakfastTypeOfFood;

	@ViewById
	@MapToModelField
	protected EditText lunchPlace;

	@ViewById
	@MapToModelField
	protected EditText lunchTime;

	@ViewById
	@MapToModelField
	protected EditText lunchTypeOfFood;

	@ViewById
	@MapToModelField
	protected EditText snackPlace;

	@ViewById
	@MapToModelField
	protected EditText snackTime;

	@ViewById
	@MapToModelField
	protected EditText snackTypeOfFood;

	@ViewById
	@MapToModelField
	protected EditText dinnerPlace;

	@ViewById
	@MapToModelField
	protected EditText dinnerTime;

	@ViewById
	@MapToModelField
	protected EditText dinnerTypeOfFood;

	@ViewById
	@MapToModelField
	protected RadioGroup malnutrition;

	@ViewById
	@MapToModelField
	protected EditText malnutritionSpecify;

	@ViewById
	@MapToModelField
	protected EditText idleHours;

	@ViewById
	@MapToModelField
	protected EditText activeHours;

	@ViewById
	@MapToModelField
	protected EditText ageOfMenarche;

	@ViewById
	@MapToModelField
	protected EditText menstruationMaterials;

	@ViewById
	@MapToModelField
	protected RadioGroup familyInteraction;

	@ViewById
	@MapToModelField
	protected EditText familyInteractionSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup communityInvolvement;

	@ViewById
	@MapToModelField
	protected EditText communityInvolvementSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup childDrrKnowledge;

	@ViewById
	@MapToModelField
	protected EditText childComplaint;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from StudentHealthAssessment");
                	StudentHealthAssessment pojo = (StudentHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(bhSuspension,pojo.getBhSuspension());
					PojoToViewMapper.setViewValue(bhSuspensionSpecify,pojo.getBhSuspensionSpecify());
					PojoToViewMapper.setViewValue(bhAntiSocialBehavior,pojo.getBhAntiSocialBehavior());
					PojoToViewMapper.setViewValue(bhDelinquency,pojo.getBhDelinquency());
					PojoToViewMapper.setViewValue(bhViolence,pojo.getBhViolence());
					PojoToViewMapper.setViewValue(bhSmoking,pojo.getBhSmoking());
					PojoToViewMapper.setViewValue(bhAlcohol,pojo.getBhAlcohol());
					PojoToViewMapper.setViewValue(bhSubstanceAbuse,pojo.getBhSubstanceAbuse());
					PojoToViewMapper.setViewValue(bhSuicidalThoughts,pojo.getBhSuicidalThoughts());
					PojoToViewMapper.setViewValue(bhSuicidalAttempts,pojo.getBhSuicidalAttempts());
					PojoToViewMapper.setViewValue(breakfastPlace,pojo.getBreakfastPlace());
					PojoToViewMapper.setViewValue(breakfastTime,pojo.getBreakfastTime());
					PojoToViewMapper.setViewValue(breakfastTypeOfFood,pojo.getBreakfastTypeOfFood());
					PojoToViewMapper.setViewValue(lunchPlace,pojo.getLunchPlace());
					PojoToViewMapper.setViewValue(lunchTime,pojo.getLunchTime());
					PojoToViewMapper.setViewValue(lunchTypeOfFood,pojo.getLunchTypeOfFood());
					PojoToViewMapper.setViewValue(snackPlace,pojo.getSnackPlace());
					PojoToViewMapper.setViewValue(snackTime,pojo.getSnackTime());
					PojoToViewMapper.setViewValue(snackTypeOfFood,pojo.getSnackTypeOfFood());
					PojoToViewMapper.setViewValue(dinnerPlace,pojo.getDinnerPlace());
					PojoToViewMapper.setViewValue(dinnerTime,pojo.getDinnerTime());
					PojoToViewMapper.setViewValue(dinnerTypeOfFood,pojo.getDinnerTypeOfFood());
					PojoToViewMapper.setViewValue(malnutrition,pojo.getMalnutrition());
					PojoToViewMapper.setViewValue(malnutritionSpecify,pojo.getMalnutritionSpecify());
					PojoToViewMapper.setViewValue(idleHours,pojo.getIdleHours());
					PojoToViewMapper.setViewValue(activeHours,pojo.getActiveHours());
					PojoToViewMapper.setViewValue(ageOfMenarche,pojo.getAgeOfMenarche());
					PojoToViewMapper.setViewValue(menstruationMaterials,pojo.getMenstruationMaterials());
					PojoToViewMapper.setViewValue(familyInteraction,pojo.getFamilyInteraction());
					PojoToViewMapper.setViewValue(familyInteractionSpecify,pojo.getFamilyInteractionSpecify());
					PojoToViewMapper.setViewValue(communityInvolvement,pojo.getCommunityInvolvement());
					PojoToViewMapper.setViewValue(communityInvolvementSpecify,pojo.getCommunityInvolvementSpecify());
					PojoToViewMapper.setViewValue(childDrrKnowledge,pojo.getChildDrrKnowledge());
					PojoToViewMapper.setViewValue(childComplaint,pojo.getChildComplaint());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: StudentHealthAssessment");
                	StudentHealthAssessment pojo = (StudentHealthAssessment) getModel();
                	
					pojo.setBhSuspension((Integer) ViewToPojoMapper.getValueByView(bhSuspension));
					pojo.setBhSuspensionSpecify((String) ViewToPojoMapper.getValueByView(bhSuspensionSpecify));
					pojo.setBhAntiSocialBehavior((Integer) ViewToPojoMapper.getValueByView(bhAntiSocialBehavior));
					pojo.setBhDelinquency((Integer) ViewToPojoMapper.getValueByView(bhDelinquency));
					pojo.setBhViolence((Integer) ViewToPojoMapper.getValueByView(bhViolence));
					pojo.setBhSmoking((Integer) ViewToPojoMapper.getValueByView(bhSmoking));
					pojo.setBhAlcohol((Integer) ViewToPojoMapper.getValueByView(bhAlcohol));
					pojo.setBhSubstanceAbuse((Integer) ViewToPojoMapper.getValueByView(bhSubstanceAbuse));
					pojo.setBhSuicidalThoughts((Integer) ViewToPojoMapper.getValueByView(bhSuicidalThoughts));
					pojo.setBhSuicidalAttempts((Integer) ViewToPojoMapper.getValueByView(bhSuicidalAttempts));
					pojo.setBreakfastPlace((String) ViewToPojoMapper.getValueByView(breakfastPlace));
					pojo.setBreakfastTime((String) ViewToPojoMapper.getValueByView(breakfastTime));
					pojo.setBreakfastTypeOfFood((String) ViewToPojoMapper.getValueByView(breakfastTypeOfFood));
					pojo.setLunchPlace((String) ViewToPojoMapper.getValueByView(lunchPlace));
					pojo.setLunchTime((String) ViewToPojoMapper.getValueByView(lunchTime));
					pojo.setLunchTypeOfFood((String) ViewToPojoMapper.getValueByView(lunchTypeOfFood));
					pojo.setSnackPlace((String) ViewToPojoMapper.getValueByView(snackPlace));
					pojo.setSnackTime((String) ViewToPojoMapper.getValueByView(snackTime));
					pojo.setSnackTypeOfFood((String) ViewToPojoMapper.getValueByView(snackTypeOfFood));
					pojo.setDinnerPlace((String) ViewToPojoMapper.getValueByView(dinnerPlace));
					pojo.setDinnerTime((String) ViewToPojoMapper.getValueByView(dinnerTime));
					pojo.setDinnerTypeOfFood((String) ViewToPojoMapper.getValueByView(dinnerTypeOfFood));
					pojo.setMalnutrition((Integer) ViewToPojoMapper.getValueByView(malnutrition));
					pojo.setMalnutritionSpecify((String) ViewToPojoMapper.getValueByView(malnutritionSpecify));
					pojo.setIdleHours((String) ViewToPojoMapper.getValueByView(idleHours));
					pojo.setActiveHours((String) ViewToPojoMapper.getValueByView(activeHours));
					pojo.setAgeOfMenarche((Integer) ViewToPojoMapper.getValueByView(ageOfMenarche));
					pojo.setMenstruationMaterials((String) ViewToPojoMapper.getValueByView(menstruationMaterials));
					pojo.setFamilyInteraction((Integer) ViewToPojoMapper.getValueByView(familyInteraction));
					pojo.setFamilyInteractionSpecify((String) ViewToPojoMapper.getValueByView(familyInteractionSpecify));
					pojo.setCommunityInvolvement((Integer) ViewToPojoMapper.getValueByView(communityInvolvement));
					pojo.setCommunityInvolvementSpecify((String) ViewToPojoMapper.getValueByView(communityInvolvementSpecify));
					pojo.setChildDrrKnowledge((Integer) ViewToPojoMapper.getValueByView(childDrrKnowledge));
					pojo.setChildComplaint((String) ViewToPojoMapper.getValueByView(childComplaint));

					if (save)
					{
                		System.out.println("Save to realm: StudentHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.bhSuspension1,R.id.malnutrition1,R.id.familyInteraction0,R.id.communityInvolvement0})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.bhSuspension1:
				 UiUtils.toggleSpecify(view, bhSuspensionSpecify);
				break;
			case R.id.malnutrition1:
				 UiUtils.toggleSpecify(view, malnutritionSpecify);
				break;
			case R.id.familyInteraction0:
				 UiUtils.toggleSpecify(view, familyInteractionSpecify);
				break;
			case R.id.communityInvolvement0:
				 UiUtils.toggleSpecify(view, communityInvolvementSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			StudentHealthAssessment model = (StudentHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(StudentHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
