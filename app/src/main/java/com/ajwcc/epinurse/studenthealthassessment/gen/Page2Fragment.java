package com.ajwcc.epinurse.studenthealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_student_health_assessment_page2)
public class Page2Fragment extends BaseEpinurseFragment {


    public Page2Fragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup traumaMajorHead;

	@ViewById
	@MapToModelField
	protected EditText traumaMajorHeadSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaMajorSpinal;

	@ViewById
	@MapToModelField
	protected EditText traumaMajorSpinalSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaMajorTorso;

	@ViewById
	@MapToModelField
	protected EditText traumaMajorTorsoSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaLeftLeg;

	@ViewById
	@MapToModelField
	protected EditText traumaLeftLegSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaRightLeg;

	@ViewById
	@MapToModelField
	protected EditText traumaRightLegSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaLeftArm;

	@ViewById
	@MapToModelField
	protected EditText traumaLeftArmSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup traumaRightArm;

	@ViewById
	@MapToModelField
	protected EditText traumaRightArmSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup skinDiseases;

	@ViewById
	@MapToModelField
	protected EditText skinDiseasesSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup allegies;

	@ViewById
	@MapToModelField
	protected EditText allegiesSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup respiratoryDisease;

	@ViewById
	@MapToModelField
	protected EditText respiratoryDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup cardiovascularDisease;

	@ViewById
	@MapToModelField
	protected EditText cardiovascularDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup endocrineDisease;

	@ViewById
	@MapToModelField
	protected EditText endocrineDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup urinarySystemDisease;

	@ViewById
	@MapToModelField
	protected EditText urinarySystemDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup reproductivSystemDisease;

	@ViewById
	@MapToModelField
	protected EditText reproductivSystemDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup communicationHearingProblem;

	@ViewById
	@MapToModelField
	protected EditText communicationHearingProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup visionProblem;

	@ViewById
	@MapToModelField
	protected EditText visionProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup oralDentalHealthProblem;

	@ViewById
	@MapToModelField
	protected EditText oralDentalHealthProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup psychologicalProblem;

	@ViewById
	@MapToModelField
	protected EditText psychologicalProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup physicalStructuralProblems;

	@ViewById
	@MapToModelField
	protected EditText physicalStructuralProblemsSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup cognativePatterns;

	@ViewById
	@MapToModelField
	protected EditText cognativePatternsSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup bowelHabit;

	@ViewById
	@MapToModelField
	protected EditText bowelHabitSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup bladderHabit;

	@ViewById
	@MapToModelField
	protected EditText bladderHabitSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup underMedication;

	@ViewById
	@MapToModelField
	protected EditText underMedicationSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup recentTreatment;

	@ViewById
	@MapToModelField
	protected EditText recentTreatmentSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup recentProcedure;

	@ViewById
	@MapToModelField
	protected EditText recentProcedureSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup recentCounseling;

	@ViewById
	@MapToModelField
	protected EditText recentCounselingSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from StudentHealthAssessment");
                	StudentHealthAssessment pojo = (StudentHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(traumaMajorHead,pojo.getTraumaMajorHead());
					PojoToViewMapper.setViewValue(traumaMajorHeadSpecify,pojo.getTraumaMajorHeadSpecify());
					PojoToViewMapper.setViewValue(traumaMajorSpinal,pojo.getTraumaMajorSpinal());
					PojoToViewMapper.setViewValue(traumaMajorSpinalSpecify,pojo.getTraumaMajorSpinalSpecify());
					PojoToViewMapper.setViewValue(traumaMajorTorso,pojo.getTraumaMajorTorso());
					PojoToViewMapper.setViewValue(traumaMajorTorsoSpecify,pojo.getTraumaMajorTorsoSpecify());
					PojoToViewMapper.setViewValue(traumaLeftLeg,pojo.getTraumaLeftLeg());
					PojoToViewMapper.setViewValue(traumaLeftLegSpecify,pojo.getTraumaLeftLegSpecify());
					PojoToViewMapper.setViewValue(traumaRightLeg,pojo.getTraumaRightLeg());
					PojoToViewMapper.setViewValue(traumaRightLegSpecify,pojo.getTraumaRightLegSpecify());
					PojoToViewMapper.setViewValue(traumaLeftArm,pojo.getTraumaLeftArm());
					PojoToViewMapper.setViewValue(traumaLeftArmSpecify,pojo.getTraumaLeftArmSpecify());
					PojoToViewMapper.setViewValue(traumaRightArm,pojo.getTraumaRightArm());
					PojoToViewMapper.setViewValue(traumaRightArmSpecify,pojo.getTraumaRightArmSpecify());
					PojoToViewMapper.setViewValue(skinDiseases,pojo.getSkinDiseases());
					PojoToViewMapper.setViewValue(skinDiseasesSpecify,pojo.getSkinDiseasesSpecify());
					PojoToViewMapper.setViewValue(allegies,pojo.getAllegies());
					PojoToViewMapper.setViewValue(allegiesSpecify,pojo.getAllegiesSpecify());
					PojoToViewMapper.setViewValue(respiratoryDisease,pojo.getRespiratoryDisease());
					PojoToViewMapper.setViewValue(respiratoryDiseaseSpecify,pojo.getRespiratoryDiseaseSpecify());
					PojoToViewMapper.setViewValue(cardiovascularDisease,pojo.getCardiovascularDisease());
					PojoToViewMapper.setViewValue(cardiovascularDiseaseSpecify,pojo.getCardiovascularDiseaseSpecify());
					PojoToViewMapper.setViewValue(endocrineDisease,pojo.getEndocrineDisease());
					PojoToViewMapper.setViewValue(endocrineDiseaseSpecify,pojo.getEndocrineDiseaseSpecify());
					PojoToViewMapper.setViewValue(urinarySystemDisease,pojo.getUrinarySystemDisease());
					PojoToViewMapper.setViewValue(urinarySystemDiseaseSpecify,pojo.getUrinarySystemDiseaseSpecify());
					PojoToViewMapper.setViewValue(reproductivSystemDisease,pojo.getReproductivSystemDisease());
					PojoToViewMapper.setViewValue(reproductivSystemDiseaseSpecify,pojo.getReproductivSystemDiseaseSpecify());
					PojoToViewMapper.setViewValue(communicationHearingProblem,pojo.getCommunicationHearingProblem());
					PojoToViewMapper.setViewValue(communicationHearingProblemSpecify,pojo.getCommunicationHearingProblemSpecify());
					PojoToViewMapper.setViewValue(visionProblem,pojo.getVisionProblem());
					PojoToViewMapper.setViewValue(visionProblemSpecify,pojo.getVisionProblemSpecify());
					PojoToViewMapper.setViewValue(oralDentalHealthProblem,pojo.getOralDentalHealthProblem());
					PojoToViewMapper.setViewValue(oralDentalHealthProblemSpecify,pojo.getOralDentalHealthProblemSpecify());
					PojoToViewMapper.setViewValue(psychologicalProblem,pojo.getPsychologicalProblem());
					PojoToViewMapper.setViewValue(psychologicalProblemSpecify,pojo.getPsychologicalProblemSpecify());
					PojoToViewMapper.setViewValue(physicalStructuralProblems,pojo.getPhysicalStructuralProblems());
					PojoToViewMapper.setViewValue(physicalStructuralProblemsSpecify,pojo.getPhysicalStructuralProblemsSpecify());
					PojoToViewMapper.setViewValue(cognativePatterns,pojo.getCognativePatterns());
					PojoToViewMapper.setViewValue(cognativePatternsSpecify,pojo.getCognativePatternsSpecify());
					PojoToViewMapper.setViewValue(bowelHabit,pojo.getBowelHabit());
					PojoToViewMapper.setViewValue(bowelHabitSpecify,pojo.getBowelHabitSpecify());
					PojoToViewMapper.setViewValue(bladderHabit,pojo.getBladderHabit());
					PojoToViewMapper.setViewValue(bladderHabitSpecify,pojo.getBladderHabitSpecify());
					PojoToViewMapper.setViewValue(underMedication,pojo.getUnderMedication());
					PojoToViewMapper.setViewValue(underMedicationSpecify,pojo.getUnderMedicationSpecify());
					PojoToViewMapper.setViewValue(recentTreatment,pojo.getRecentTreatment());
					PojoToViewMapper.setViewValue(recentTreatmentSpecify,pojo.getRecentTreatmentSpecify());
					PojoToViewMapper.setViewValue(recentProcedure,pojo.getRecentProcedure());
					PojoToViewMapper.setViewValue(recentProcedureSpecify,pojo.getRecentProcedureSpecify());
					PojoToViewMapper.setViewValue(recentCounseling,pojo.getRecentCounseling());
					PojoToViewMapper.setViewValue(recentCounselingSpecify,pojo.getRecentCounselingSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: StudentHealthAssessment");
                	StudentHealthAssessment pojo = (StudentHealthAssessment) getModel();
                	
					pojo.setTraumaMajorHead((Integer) ViewToPojoMapper.getValueByView(traumaMajorHead));
					pojo.setTraumaMajorHeadSpecify((String) ViewToPojoMapper.getValueByView(traumaMajorHeadSpecify));
					pojo.setTraumaMajorSpinal((Integer) ViewToPojoMapper.getValueByView(traumaMajorSpinal));
					pojo.setTraumaMajorSpinalSpecify((String) ViewToPojoMapper.getValueByView(traumaMajorSpinalSpecify));
					pojo.setTraumaMajorTorso((Integer) ViewToPojoMapper.getValueByView(traumaMajorTorso));
					pojo.setTraumaMajorTorsoSpecify((String) ViewToPojoMapper.getValueByView(traumaMajorTorsoSpecify));
					pojo.setTraumaLeftLeg((Integer) ViewToPojoMapper.getValueByView(traumaLeftLeg));
					pojo.setTraumaLeftLegSpecify((String) ViewToPojoMapper.getValueByView(traumaLeftLegSpecify));
					pojo.setTraumaRightLeg((Integer) ViewToPojoMapper.getValueByView(traumaRightLeg));
					pojo.setTraumaRightLegSpecify((String) ViewToPojoMapper.getValueByView(traumaRightLegSpecify));
					pojo.setTraumaLeftArm((Integer) ViewToPojoMapper.getValueByView(traumaLeftArm));
					pojo.setTraumaLeftArmSpecify((String) ViewToPojoMapper.getValueByView(traumaLeftArmSpecify));
					pojo.setTraumaRightArm((Integer) ViewToPojoMapper.getValueByView(traumaRightArm));
					pojo.setTraumaRightArmSpecify((String) ViewToPojoMapper.getValueByView(traumaRightArmSpecify));
					pojo.setSkinDiseases((Integer) ViewToPojoMapper.getValueByView(skinDiseases));
					pojo.setSkinDiseasesSpecify((String) ViewToPojoMapper.getValueByView(skinDiseasesSpecify));
					pojo.setAllegies((Integer) ViewToPojoMapper.getValueByView(allegies));
					pojo.setAllegiesSpecify((String) ViewToPojoMapper.getValueByView(allegiesSpecify));
					pojo.setRespiratoryDisease((Integer) ViewToPojoMapper.getValueByView(respiratoryDisease));
					pojo.setRespiratoryDiseaseSpecify((String) ViewToPojoMapper.getValueByView(respiratoryDiseaseSpecify));
					pojo.setCardiovascularDisease((Integer) ViewToPojoMapper.getValueByView(cardiovascularDisease));
					pojo.setCardiovascularDiseaseSpecify((String) ViewToPojoMapper.getValueByView(cardiovascularDiseaseSpecify));
					pojo.setEndocrineDisease((Integer) ViewToPojoMapper.getValueByView(endocrineDisease));
					pojo.setEndocrineDiseaseSpecify((String) ViewToPojoMapper.getValueByView(endocrineDiseaseSpecify));
					pojo.setUrinarySystemDisease((Integer) ViewToPojoMapper.getValueByView(urinarySystemDisease));
					pojo.setUrinarySystemDiseaseSpecify((String) ViewToPojoMapper.getValueByView(urinarySystemDiseaseSpecify));
					pojo.setReproductivSystemDisease((Integer) ViewToPojoMapper.getValueByView(reproductivSystemDisease));
					pojo.setReproductivSystemDiseaseSpecify((String) ViewToPojoMapper.getValueByView(reproductivSystemDiseaseSpecify));
					pojo.setCommunicationHearingProblem((Integer) ViewToPojoMapper.getValueByView(communicationHearingProblem));
					pojo.setCommunicationHearingProblemSpecify((String) ViewToPojoMapper.getValueByView(communicationHearingProblemSpecify));
					pojo.setVisionProblem((Integer) ViewToPojoMapper.getValueByView(visionProblem));
					pojo.setVisionProblemSpecify((String) ViewToPojoMapper.getValueByView(visionProblemSpecify));
					pojo.setOralDentalHealthProblem((Integer) ViewToPojoMapper.getValueByView(oralDentalHealthProblem));
					pojo.setOralDentalHealthProblemSpecify((String) ViewToPojoMapper.getValueByView(oralDentalHealthProblemSpecify));
					pojo.setPsychologicalProblem((Integer) ViewToPojoMapper.getValueByView(psychologicalProblem));
					pojo.setPsychologicalProblemSpecify((String) ViewToPojoMapper.getValueByView(psychologicalProblemSpecify));
					pojo.setPhysicalStructuralProblems((Integer) ViewToPojoMapper.getValueByView(physicalStructuralProblems));
					pojo.setPhysicalStructuralProblemsSpecify((String) ViewToPojoMapper.getValueByView(physicalStructuralProblemsSpecify));
					pojo.setCognativePatterns((Integer) ViewToPojoMapper.getValueByView(cognativePatterns));
					pojo.setCognativePatternsSpecify((String) ViewToPojoMapper.getValueByView(cognativePatternsSpecify));
					pojo.setBowelHabit((Integer) ViewToPojoMapper.getValueByView(bowelHabit));
					pojo.setBowelHabitSpecify((String) ViewToPojoMapper.getValueByView(bowelHabitSpecify));
					pojo.setBladderHabit((Integer) ViewToPojoMapper.getValueByView(bladderHabit));
					pojo.setBladderHabitSpecify((String) ViewToPojoMapper.getValueByView(bladderHabitSpecify));
					pojo.setUnderMedication((Integer) ViewToPojoMapper.getValueByView(underMedication));
					pojo.setUnderMedicationSpecify((String) ViewToPojoMapper.getValueByView(underMedicationSpecify));
					pojo.setRecentTreatment((Integer) ViewToPojoMapper.getValueByView(recentTreatment));
					pojo.setRecentTreatmentSpecify((String) ViewToPojoMapper.getValueByView(recentTreatmentSpecify));
					pojo.setRecentProcedure((Integer) ViewToPojoMapper.getValueByView(recentProcedure));
					pojo.setRecentProcedureSpecify((String) ViewToPojoMapper.getValueByView(recentProcedureSpecify));
					pojo.setRecentCounseling((Integer) ViewToPojoMapper.getValueByView(recentCounseling));
					pojo.setRecentCounselingSpecify((String) ViewToPojoMapper.getValueByView(recentCounselingSpecify));

					if (save)
					{
                		System.out.println("Save to realm: StudentHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.traumaMajorHead1,R.id.traumaMajorSpinal1,R.id.traumaMajorTorso1,R.id.traumaLeftLeg1,R.id.traumaRightLeg1,R.id.traumaLeftArm1,R.id.traumaRightArm1,R.id.skinDiseases1,R.id.allegies1,R.id.respiratoryDisease1,R.id.cardiovascularDisease1,R.id.endocrineDisease1,R.id.urinarySystemDisease1,R.id.reproductivSystemDisease1,R.id.communicationHearingProblem1,R.id.visionProblem1,R.id.oralDentalHealthProblem1,R.id.psychologicalProblem1,R.id.physicalStructuralProblems1,R.id.cognativePatterns1,R.id.bowelHabit1,R.id.bladderHabit1,R.id.underMedication1,R.id.recentTreatment1,R.id.recentProcedure1,R.id.recentCounseling1})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.traumaMajorHead1:
				 UiUtils.toggleSpecify(view, traumaMajorHeadSpecify);
				break;
			case R.id.traumaMajorSpinal1:
				 UiUtils.toggleSpecify(view, traumaMajorSpinalSpecify);
				break;
			case R.id.traumaMajorTorso1:
				 UiUtils.toggleSpecify(view, traumaMajorTorsoSpecify);
				break;
			case R.id.traumaLeftLeg1:
				 UiUtils.toggleSpecify(view, traumaLeftLegSpecify);
				break;
			case R.id.traumaRightLeg1:
				 UiUtils.toggleSpecify(view, traumaRightLegSpecify);
				break;
			case R.id.traumaLeftArm1:
				 UiUtils.toggleSpecify(view, traumaLeftArmSpecify);
				break;
			case R.id.traumaRightArm1:
				 UiUtils.toggleSpecify(view, traumaRightArmSpecify);
				break;
			case R.id.skinDiseases1:
				 UiUtils.toggleSpecify(view, skinDiseasesSpecify);
				break;
			case R.id.allegies1:
				 UiUtils.toggleSpecify(view, allegiesSpecify);
				break;
			case R.id.respiratoryDisease1:
				 UiUtils.toggleSpecify(view, respiratoryDiseaseSpecify);
				break;
			case R.id.cardiovascularDisease1:
				 UiUtils.toggleSpecify(view, cardiovascularDiseaseSpecify);
				break;
			case R.id.endocrineDisease1:
				 UiUtils.toggleSpecify(view, endocrineDiseaseSpecify);
				break;
			case R.id.urinarySystemDisease1:
				 UiUtils.toggleSpecify(view, urinarySystemDiseaseSpecify);
				break;
			case R.id.reproductivSystemDisease1:
				 UiUtils.toggleSpecify(view, reproductivSystemDiseaseSpecify);
				break;
			case R.id.communicationHearingProblem1:
				 UiUtils.toggleSpecify(view, communicationHearingProblemSpecify);
				break;
			case R.id.visionProblem1:
				 UiUtils.toggleSpecify(view, visionProblemSpecify);
				break;
			case R.id.oralDentalHealthProblem1:
				 UiUtils.toggleSpecify(view, oralDentalHealthProblemSpecify);
				break;
			case R.id.psychologicalProblem1:
				 UiUtils.toggleSpecify(view, psychologicalProblemSpecify);
				break;
			case R.id.physicalStructuralProblems1:
				 UiUtils.toggleSpecify(view, physicalStructuralProblemsSpecify);
				break;
			case R.id.cognativePatterns1:
				 UiUtils.toggleSpecify(view, cognativePatternsSpecify);
				break;
			case R.id.bowelHabit1:
				 UiUtils.toggleSpecify(view, bowelHabitSpecify);
				break;
			case R.id.bladderHabit1:
				 UiUtils.toggleSpecify(view, bladderHabitSpecify);
				break;
			case R.id.underMedication1:
				 UiUtils.toggleSpecify(view, underMedicationSpecify);
				break;
			case R.id.recentTreatment1:
				 UiUtils.toggleSpecify(view, recentTreatmentSpecify);
				break;
			case R.id.recentProcedure1:
				 UiUtils.toggleSpecify(view, recentProcedureSpecify);
				break;
			case R.id.recentCounseling1:
				 UiUtils.toggleSpecify(view, recentCounselingSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			StudentHealthAssessment model = (StudentHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(StudentHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
