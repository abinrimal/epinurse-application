package com.ajwcc.epinurse.studenthealthassessment.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.OwnerSelectFragment_;
import com.ajwcc.epinurse.common.image.ImageListFragment_;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessment;
import com.ajwcc.util.ui.validation.ValidationHandler;

import org.androidannotations.annotations.EActivity;

import java.util.List;


@EActivity(R.layout.gen_activity_student_health_assessment)
public class StudentHealthAssessmentActivity extends com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessmentActivity
{
    public ValidationHandler<StudentHealthAssessment> getValidationHandler()
    {
        StudentHealthAssessmentValidator validator = new StudentHealthAssessmentValidator(this);
        validator.setModel((StudentHealthAssessment)getModel());
        return validator;
    }


    protected List<String> createTableOfContents()
    {
        List<String> names = super.createTableOfContents();

        names.add(0,"Owner");

        return names;
    }

    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = super.createFragmentList();

        // add custom Page1
        list.set(0, () -> Page1Fragment_.builder().build());

        list.add(0, () -> OwnerSelectFragment_.builder().build());

        list.add(list.size()-1, ()-> ImageListFragment_.builder().build());

        return list;
    }




    public BasicInformation getProfile(String uuid)
    {
        return getRealm().where(BasicInformation.class).equalTo("uuid", uuid).findFirst();
    }
}
