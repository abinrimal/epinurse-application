package com.ajwcc.epinurse.studenthealthassessment.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessment;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessmentActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

public class StudentHealthAssessmentValidator extends com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessmentValidator {
    public StudentHealthAssessmentValidator(StudentHealthAssessmentActivity a) {
        super(a);
    }



    @Override
    public void validateModel()
    {
        // check if owenerUuid has been set
        validateNonNullField(model.getOwnerUuid(), activity.getPage("Owner"), R.id.button, context.getResources().getString(R.string.basic_information_profile));   // TODO: USE RES


        super.validateModel();
    }


}