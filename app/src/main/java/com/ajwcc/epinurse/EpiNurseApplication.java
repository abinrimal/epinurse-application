package com.ajwcc.epinurse;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.text.TextUtils;

import com.zhuinden.realmautomigration.AutoMigration;

import org.androidannotations.annotations.EApplication;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by logan on 16/11/2017.
 */

@EApplication
public class  EpiNurseApplication extends Application
{
    public void onCreate()
    {
        super.onCreate();

        // init Realm and other stuff here
        Realm.init(this);

        // enable auto migration to simplify adding new objects and modifying RealmObjects
        // between releases
            // each time you change schema should increment schemaVersion() to trigger migration

        // 10 - Feb 21, 2019
        // 11 - Mar 16, 2019
        // 12 - Mar 21, 2019
        // 13 - Mar 26, 2019


        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
                                                            .schemaVersion(13)   // increment
                                                            .migration(new AutoMigration())
                                                            .build());



        // not sure if this will work in higher SDK levels
        SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
        String lang = prefs.getString("lang", "en");

        System.out.println("using "+lang);
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());


    }

//
//    public Locale neLocale = new Locale("ne");
//    public Locale enLocale = new Locale("en");
//
//    public void updateLanguage(Context c, Locale locale)
//    {
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        c.getResources().updateConfiguration(config,
//                c.getResources().getDisplayMetrics());
//    }
//



}
