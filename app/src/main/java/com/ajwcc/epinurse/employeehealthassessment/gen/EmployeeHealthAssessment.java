package com.ajwcc.epinurse.employeehealthassessment.gen;

import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.epinurse.common.utils.network.ExcludeFromJson;

import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import com.zhuinden.realmautomigration.AutoMigration;

public class EmployeeHealthAssessment extends RealmObject implements EpinurseModel
{
    @PrimaryKey
    private String uuid;
    
    public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public String getUuid()
	{
		return uuid;
	}

	@ExcludeFromJson
	private boolean editing;

	@Override
	public boolean isEditing() {
		return editing;
	}

	@Override
	public void setEditing(boolean editing) {
		this.editing = editing;
	}


	@ExcludeFromJson
	private String ownerUuid;

	@Override
	public String getOwnerUuid() {
		return ownerUuid;
	}

	@Override
	public void setOwnerUuid(String ownerUuid) {
		this.ownerUuid = ownerUuid;
	}


    
	@ExcludeFromJson
	private boolean synced;

	@Override
	public boolean isSynced() {
		return synced;
	}

	@Override
	public void setSynced(boolean synced) {
		this.synced = synced;
	}


	@ExcludeFromJson
	private Date createdAt;

	@Override
	public Date getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(Date date)
	{
		this.createdAt = date;
	}
    
	private String shineId;

	// Page1
	private Double temperature;
	private Integer systolic;
	private Integer diastolic;
	private Double heightCm;
	private Double weightKg;
	private Integer bmi;
	private Double bmiComputed;
	private Integer acuteRespiratoryInfection;
	private Integer acuteWateryDiarrhea;
	private Integer acuteBloodyDiarrhea;
	private Integer acuteJaundiceInfection;
	private Integer suspectedMeningitis;
	private Integer suspectedTetanus;
	private Integer fever;

	// Page2
	private Integer skinDiseases;
	private String skinDiseasesSpecify;
	private Integer allegies;
	private String allegiesSpecify;
	private Integer injuriesTrauma;
	private String injuriesTraumaSpecify;
	private Integer respiratoryDisease;
	private String respiratoryDiseaseSpecify;
	private Integer cardiovascularDisease;
	private String cardiovascularDiseaseSpecify;
	private Integer endocrineDisease;
	private String endocrineDiseaseSpecify;
	private Integer urinarySystemDisease;
	private String urinarySystemDiseaseSpecify;
	private Integer reproductiveSystemDisease;
	private String reproductiveSystemDiseaseSpecify;
	private Integer communicationHearingProblem;
	private String communicationHearingProblemSpecify;
	private Integer visionProblem;
	private String visionProblemSpecify;
	private Integer oralDentalHealthProblem;
	private String oralDentalHealthProblemSpecify;
	private String others;
	private Integer underMedication;
	private String underMedicationSpecify;
	private Integer recentTreatment;
	private String recentTreatmentSpecify;
	private Integer recentProcedure;
	private String recentProcedureSpecify;

	// Page3
	private Integer bhSmoking;
	private Integer bhAlcohol;
	private Integer bhSubstanceAbuse;
	private Integer bhSuicidalThoughts;
	private Integer bhSuicidalAttempts;
	private Integer ageOfMenarche;
	private String menstruationMaterials;
	private Integer teacherDdrKnowledge;
    
    

	public String getShineId() {
		return this.shineId;
	}

	public void setShineId(String shineId) {
	
		if (this.shineId==null || !this.shineId.equals(shineId))
		{
			editing = true;
			synced = false;
		}
	
		this.shineId = shineId;
	}

	public Double getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Double temperature) {
	
		if (this.temperature==null || !this.temperature.equals(temperature))
		{
			editing = true;
			synced = false;
		}
	
		this.temperature = temperature;
	}

	public Integer getSystolic() {
		return this.systolic;
	}

	public void setSystolic(Integer systolic) {
	
		if (this.systolic==null || !this.systolic.equals(systolic))
		{
			editing = true;
			synced = false;
		}
	
		this.systolic = systolic;
	}

	public Integer getDiastolic() {
		return this.diastolic;
	}

	public void setDiastolic(Integer diastolic) {
	
		if (this.diastolic==null || !this.diastolic.equals(diastolic))
		{
			editing = true;
			synced = false;
		}
	
		this.diastolic = diastolic;
	}

	public Double getHeightCm() {
		return this.heightCm;
	}

	public void setHeightCm(Double heightCm) {
	
		if (this.heightCm==null || !this.heightCm.equals(heightCm))
		{
			editing = true;
			synced = false;
		}
	
		this.heightCm = heightCm;
	}

	public Double getWeightKg() {
		return this.weightKg;
	}

	public void setWeightKg(Double weightKg) {
	
		if (this.weightKg==null || !this.weightKg.equals(weightKg))
		{
			editing = true;
			synced = false;
		}
	
		this.weightKg = weightKg;
	}

	public Integer getBmi() {
		return this.bmi;
	}

	public void setBmi(Integer bmi) {
	
		if (this.bmi==null || !this.bmi.equals(bmi))
		{
			editing = true;
			synced = false;
		}
	
		this.bmi = bmi;
	}

	public Double getBmiComputed() {
		return this.bmiComputed;
	}

	public void setBmiComputed(Double bmiComputed) {
	
		if (this.bmiComputed==null || !this.bmiComputed.equals(bmiComputed))
		{
			editing = true;
			synced = false;
		}
	
		this.bmiComputed = bmiComputed;
	}

	public Integer getAcuteRespiratoryInfection() {
		return this.acuteRespiratoryInfection;
	}

	public void setAcuteRespiratoryInfection(Integer acuteRespiratoryInfection) {
	
		if (this.acuteRespiratoryInfection==null || !this.acuteRespiratoryInfection.equals(acuteRespiratoryInfection))
		{
			editing = true;
			synced = false;
		}
	
		this.acuteRespiratoryInfection = acuteRespiratoryInfection;
	}

	public Integer getAcuteWateryDiarrhea() {
		return this.acuteWateryDiarrhea;
	}

	public void setAcuteWateryDiarrhea(Integer acuteWateryDiarrhea) {
	
		if (this.acuteWateryDiarrhea==null || !this.acuteWateryDiarrhea.equals(acuteWateryDiarrhea))
		{
			editing = true;
			synced = false;
		}
	
		this.acuteWateryDiarrhea = acuteWateryDiarrhea;
	}

	public Integer getAcuteBloodyDiarrhea() {
		return this.acuteBloodyDiarrhea;
	}

	public void setAcuteBloodyDiarrhea(Integer acuteBloodyDiarrhea) {
	
		if (this.acuteBloodyDiarrhea==null || !this.acuteBloodyDiarrhea.equals(acuteBloodyDiarrhea))
		{
			editing = true;
			synced = false;
		}
	
		this.acuteBloodyDiarrhea = acuteBloodyDiarrhea;
	}

	public Integer getAcuteJaundiceInfection() {
		return this.acuteJaundiceInfection;
	}

	public void setAcuteJaundiceInfection(Integer acuteJaundiceInfection) {
	
		if (this.acuteJaundiceInfection==null || !this.acuteJaundiceInfection.equals(acuteJaundiceInfection))
		{
			editing = true;
			synced = false;
		}
	
		this.acuteJaundiceInfection = acuteJaundiceInfection;
	}

	public Integer getSuspectedMeningitis() {
		return this.suspectedMeningitis;
	}

	public void setSuspectedMeningitis(Integer suspectedMeningitis) {
	
		if (this.suspectedMeningitis==null || !this.suspectedMeningitis.equals(suspectedMeningitis))
		{
			editing = true;
			synced = false;
		}
	
		this.suspectedMeningitis = suspectedMeningitis;
	}

	public Integer getSuspectedTetanus() {
		return this.suspectedTetanus;
	}

	public void setSuspectedTetanus(Integer suspectedTetanus) {
	
		if (this.suspectedTetanus==null || !this.suspectedTetanus.equals(suspectedTetanus))
		{
			editing = true;
			synced = false;
		}
	
		this.suspectedTetanus = suspectedTetanus;
	}

	public Integer getFever() {
		return this.fever;
	}

	public void setFever(Integer fever) {
	
		if (this.fever==null || !this.fever.equals(fever))
		{
			editing = true;
			synced = false;
		}
	
		this.fever = fever;
	}

	public Integer getSkinDiseases() {
		return this.skinDiseases;
	}

	public void setSkinDiseases(Integer skinDiseases) {
	
		if (this.skinDiseases==null || !this.skinDiseases.equals(skinDiseases))
		{
			editing = true;
			synced = false;
		}
	
		this.skinDiseases = skinDiseases;
	}

	public String getSkinDiseasesSpecify() {
		return this.skinDiseasesSpecify;
	}

	public void setSkinDiseasesSpecify(String skinDiseasesSpecify) {
	
		if (this.skinDiseasesSpecify==null || !this.skinDiseasesSpecify.equals(skinDiseasesSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.skinDiseasesSpecify = skinDiseasesSpecify;
	}

	public Integer getAllegies() {
		return this.allegies;
	}

	public void setAllegies(Integer allegies) {
	
		if (this.allegies==null || !this.allegies.equals(allegies))
		{
			editing = true;
			synced = false;
		}
	
		this.allegies = allegies;
	}

	public String getAllegiesSpecify() {
		return this.allegiesSpecify;
	}

	public void setAllegiesSpecify(String allegiesSpecify) {
	
		if (this.allegiesSpecify==null || !this.allegiesSpecify.equals(allegiesSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.allegiesSpecify = allegiesSpecify;
	}

	public Integer getInjuriesTrauma() {
		return this.injuriesTrauma;
	}

	public void setInjuriesTrauma(Integer injuriesTrauma) {
	
		if (this.injuriesTrauma==null || !this.injuriesTrauma.equals(injuriesTrauma))
		{
			editing = true;
			synced = false;
		}
	
		this.injuriesTrauma = injuriesTrauma;
	}

	public String getInjuriesTraumaSpecify() {
		return this.injuriesTraumaSpecify;
	}

	public void setInjuriesTraumaSpecify(String injuriesTraumaSpecify) {
	
		if (this.injuriesTraumaSpecify==null || !this.injuriesTraumaSpecify.equals(injuriesTraumaSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.injuriesTraumaSpecify = injuriesTraumaSpecify;
	}

	public Integer getRespiratoryDisease() {
		return this.respiratoryDisease;
	}

	public void setRespiratoryDisease(Integer respiratoryDisease) {
	
		if (this.respiratoryDisease==null || !this.respiratoryDisease.equals(respiratoryDisease))
		{
			editing = true;
			synced = false;
		}
	
		this.respiratoryDisease = respiratoryDisease;
	}

	public String getRespiratoryDiseaseSpecify() {
		return this.respiratoryDiseaseSpecify;
	}

	public void setRespiratoryDiseaseSpecify(String respiratoryDiseaseSpecify) {
	
		if (this.respiratoryDiseaseSpecify==null || !this.respiratoryDiseaseSpecify.equals(respiratoryDiseaseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.respiratoryDiseaseSpecify = respiratoryDiseaseSpecify;
	}

	public Integer getCardiovascularDisease() {
		return this.cardiovascularDisease;
	}

	public void setCardiovascularDisease(Integer cardiovascularDisease) {
	
		if (this.cardiovascularDisease==null || !this.cardiovascularDisease.equals(cardiovascularDisease))
		{
			editing = true;
			synced = false;
		}
	
		this.cardiovascularDisease = cardiovascularDisease;
	}

	public String getCardiovascularDiseaseSpecify() {
		return this.cardiovascularDiseaseSpecify;
	}

	public void setCardiovascularDiseaseSpecify(String cardiovascularDiseaseSpecify) {
	
		if (this.cardiovascularDiseaseSpecify==null || !this.cardiovascularDiseaseSpecify.equals(cardiovascularDiseaseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.cardiovascularDiseaseSpecify = cardiovascularDiseaseSpecify;
	}

	public Integer getEndocrineDisease() {
		return this.endocrineDisease;
	}

	public void setEndocrineDisease(Integer endocrineDisease) {
	
		if (this.endocrineDisease==null || !this.endocrineDisease.equals(endocrineDisease))
		{
			editing = true;
			synced = false;
		}
	
		this.endocrineDisease = endocrineDisease;
	}

	public String getEndocrineDiseaseSpecify() {
		return this.endocrineDiseaseSpecify;
	}

	public void setEndocrineDiseaseSpecify(String endocrineDiseaseSpecify) {
	
		if (this.endocrineDiseaseSpecify==null || !this.endocrineDiseaseSpecify.equals(endocrineDiseaseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.endocrineDiseaseSpecify = endocrineDiseaseSpecify;
	}

	public Integer getUrinarySystemDisease() {
		return this.urinarySystemDisease;
	}

	public void setUrinarySystemDisease(Integer urinarySystemDisease) {
	
		if (this.urinarySystemDisease==null || !this.urinarySystemDisease.equals(urinarySystemDisease))
		{
			editing = true;
			synced = false;
		}
	
		this.urinarySystemDisease = urinarySystemDisease;
	}

	public String getUrinarySystemDiseaseSpecify() {
		return this.urinarySystemDiseaseSpecify;
	}

	public void setUrinarySystemDiseaseSpecify(String urinarySystemDiseaseSpecify) {
	
		if (this.urinarySystemDiseaseSpecify==null || !this.urinarySystemDiseaseSpecify.equals(urinarySystemDiseaseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.urinarySystemDiseaseSpecify = urinarySystemDiseaseSpecify;
	}

	public Integer getReproductiveSystemDisease() {
		return this.reproductiveSystemDisease;
	}

	public void setReproductiveSystemDisease(Integer reproductiveSystemDisease) {
	
		if (this.reproductiveSystemDisease==null || !this.reproductiveSystemDisease.equals(reproductiveSystemDisease))
		{
			editing = true;
			synced = false;
		}
	
		this.reproductiveSystemDisease = reproductiveSystemDisease;
	}

	public String getReproductiveSystemDiseaseSpecify() {
		return this.reproductiveSystemDiseaseSpecify;
	}

	public void setReproductiveSystemDiseaseSpecify(String reproductiveSystemDiseaseSpecify) {
	
		if (this.reproductiveSystemDiseaseSpecify==null || !this.reproductiveSystemDiseaseSpecify.equals(reproductiveSystemDiseaseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.reproductiveSystemDiseaseSpecify = reproductiveSystemDiseaseSpecify;
	}

	public Integer getCommunicationHearingProblem() {
		return this.communicationHearingProblem;
	}

	public void setCommunicationHearingProblem(Integer communicationHearingProblem) {
	
		if (this.communicationHearingProblem==null || !this.communicationHearingProblem.equals(communicationHearingProblem))
		{
			editing = true;
			synced = false;
		}
	
		this.communicationHearingProblem = communicationHearingProblem;
	}

	public String getCommunicationHearingProblemSpecify() {
		return this.communicationHearingProblemSpecify;
	}

	public void setCommunicationHearingProblemSpecify(String communicationHearingProblemSpecify) {
	
		if (this.communicationHearingProblemSpecify==null || !this.communicationHearingProblemSpecify.equals(communicationHearingProblemSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.communicationHearingProblemSpecify = communicationHearingProblemSpecify;
	}

	public Integer getVisionProblem() {
		return this.visionProblem;
	}

	public void setVisionProblem(Integer visionProblem) {
	
		if (this.visionProblem==null || !this.visionProblem.equals(visionProblem))
		{
			editing = true;
			synced = false;
		}
	
		this.visionProblem = visionProblem;
	}

	public String getVisionProblemSpecify() {
		return this.visionProblemSpecify;
	}

	public void setVisionProblemSpecify(String visionProblemSpecify) {
	
		if (this.visionProblemSpecify==null || !this.visionProblemSpecify.equals(visionProblemSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.visionProblemSpecify = visionProblemSpecify;
	}

	public Integer getOralDentalHealthProblem() {
		return this.oralDentalHealthProblem;
	}

	public void setOralDentalHealthProblem(Integer oralDentalHealthProblem) {
	
		if (this.oralDentalHealthProblem==null || !this.oralDentalHealthProblem.equals(oralDentalHealthProblem))
		{
			editing = true;
			synced = false;
		}
	
		this.oralDentalHealthProblem = oralDentalHealthProblem;
	}

	public String getOralDentalHealthProblemSpecify() {
		return this.oralDentalHealthProblemSpecify;
	}

	public void setOralDentalHealthProblemSpecify(String oralDentalHealthProblemSpecify) {
	
		if (this.oralDentalHealthProblemSpecify==null || !this.oralDentalHealthProblemSpecify.equals(oralDentalHealthProblemSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.oralDentalHealthProblemSpecify = oralDentalHealthProblemSpecify;
	}

	public String getOthers() {
		return this.others;
	}

	public void setOthers(String others) {
	
		if (this.others==null || !this.others.equals(others))
		{
			editing = true;
			synced = false;
		}
	
		this.others = others;
	}

	public Integer getUnderMedication() {
		return this.underMedication;
	}

	public void setUnderMedication(Integer underMedication) {
	
		if (this.underMedication==null || !this.underMedication.equals(underMedication))
		{
			editing = true;
			synced = false;
		}
	
		this.underMedication = underMedication;
	}

	public String getUnderMedicationSpecify() {
		return this.underMedicationSpecify;
	}

	public void setUnderMedicationSpecify(String underMedicationSpecify) {
	
		if (this.underMedicationSpecify==null || !this.underMedicationSpecify.equals(underMedicationSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.underMedicationSpecify = underMedicationSpecify;
	}

	public Integer getRecentTreatment() {
		return this.recentTreatment;
	}

	public void setRecentTreatment(Integer recentTreatment) {
	
		if (this.recentTreatment==null || !this.recentTreatment.equals(recentTreatment))
		{
			editing = true;
			synced = false;
		}
	
		this.recentTreatment = recentTreatment;
	}

	public String getRecentTreatmentSpecify() {
		return this.recentTreatmentSpecify;
	}

	public void setRecentTreatmentSpecify(String recentTreatmentSpecify) {
	
		if (this.recentTreatmentSpecify==null || !this.recentTreatmentSpecify.equals(recentTreatmentSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.recentTreatmentSpecify = recentTreatmentSpecify;
	}

	public Integer getRecentProcedure() {
		return this.recentProcedure;
	}

	public void setRecentProcedure(Integer recentProcedure) {
	
		if (this.recentProcedure==null || !this.recentProcedure.equals(recentProcedure))
		{
			editing = true;
			synced = false;
		}
	
		this.recentProcedure = recentProcedure;
	}

	public String getRecentProcedureSpecify() {
		return this.recentProcedureSpecify;
	}

	public void setRecentProcedureSpecify(String recentProcedureSpecify) {
	
		if (this.recentProcedureSpecify==null || !this.recentProcedureSpecify.equals(recentProcedureSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.recentProcedureSpecify = recentProcedureSpecify;
	}

	public Integer getBhSmoking() {
		return this.bhSmoking;
	}

	public void setBhSmoking(Integer bhSmoking) {
	
		if (this.bhSmoking==null || !this.bhSmoking.equals(bhSmoking))
		{
			editing = true;
			synced = false;
		}
	
		this.bhSmoking = bhSmoking;
	}

	public Integer getBhAlcohol() {
		return this.bhAlcohol;
	}

	public void setBhAlcohol(Integer bhAlcohol) {
	
		if (this.bhAlcohol==null || !this.bhAlcohol.equals(bhAlcohol))
		{
			editing = true;
			synced = false;
		}
	
		this.bhAlcohol = bhAlcohol;
	}

	public Integer getBhSubstanceAbuse() {
		return this.bhSubstanceAbuse;
	}

	public void setBhSubstanceAbuse(Integer bhSubstanceAbuse) {
	
		if (this.bhSubstanceAbuse==null || !this.bhSubstanceAbuse.equals(bhSubstanceAbuse))
		{
			editing = true;
			synced = false;
		}
	
		this.bhSubstanceAbuse = bhSubstanceAbuse;
	}

	public Integer getBhSuicidalThoughts() {
		return this.bhSuicidalThoughts;
	}

	public void setBhSuicidalThoughts(Integer bhSuicidalThoughts) {
	
		if (this.bhSuicidalThoughts==null || !this.bhSuicidalThoughts.equals(bhSuicidalThoughts))
		{
			editing = true;
			synced = false;
		}
	
		this.bhSuicidalThoughts = bhSuicidalThoughts;
	}

	public Integer getBhSuicidalAttempts() {
		return this.bhSuicidalAttempts;
	}

	public void setBhSuicidalAttempts(Integer bhSuicidalAttempts) {
	
		if (this.bhSuicidalAttempts==null || !this.bhSuicidalAttempts.equals(bhSuicidalAttempts))
		{
			editing = true;
			synced = false;
		}
	
		this.bhSuicidalAttempts = bhSuicidalAttempts;
	}

	public Integer getAgeOfMenarche() {
		return this.ageOfMenarche;
	}

	public void setAgeOfMenarche(Integer ageOfMenarche) {
	
		if (this.ageOfMenarche==null || !this.ageOfMenarche.equals(ageOfMenarche))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOfMenarche = ageOfMenarche;
	}

	public String getMenstruationMaterials() {
		return this.menstruationMaterials;
	}

	public void setMenstruationMaterials(String menstruationMaterials) {
	
		if (this.menstruationMaterials==null || !this.menstruationMaterials.equals(menstruationMaterials))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationMaterials = menstruationMaterials;
	}

	public Integer getTeacherDdrKnowledge() {
		return this.teacherDdrKnowledge;
	}

	public void setTeacherDdrKnowledge(Integer teacherDdrKnowledge) {
	
		if (this.teacherDdrKnowledge==null || !this.teacherDdrKnowledge.equals(teacherDdrKnowledge))
		{
			editing = true;
			synced = false;
		}
	
		this.teacherDdrKnowledge = teacherDdrKnowledge;
	}

    
    
    @Override
	public String toString() {
		return "EmployeeHealthAssessment{" +
				"uuid='" + uuid + '\'' +
				", editing=" + editing +
				", synced=" + synced +
				", createdAt=" + createdAt +
				", shineId='" + shineId + '\'' +
				", temperature='" + temperature + '\'' +
				", systolic='" + systolic + '\'' +
				", diastolic='" + diastolic + '\'' +
				", heightCm='" + heightCm + '\'' +
				", weightKg='" + weightKg + '\'' +
				", bmi='" + bmi + '\'' +
				", bmiComputed='" + bmiComputed + '\'' +
				", acuteRespiratoryInfection='" + acuteRespiratoryInfection + '\'' +
				", acuteWateryDiarrhea='" + acuteWateryDiarrhea + '\'' +
				", acuteBloodyDiarrhea='" + acuteBloodyDiarrhea + '\'' +
				", acuteJaundiceInfection='" + acuteJaundiceInfection + '\'' +
				", suspectedMeningitis='" + suspectedMeningitis + '\'' +
				", suspectedTetanus='" + suspectedTetanus + '\'' +
				", fever='" + fever + '\'' +
				", skinDiseases='" + skinDiseases + '\'' +
				", skinDiseasesSpecify='" + skinDiseasesSpecify + '\'' +
				", allegies='" + allegies + '\'' +
				", allegiesSpecify='" + allegiesSpecify + '\'' +
				", injuriesTrauma='" + injuriesTrauma + '\'' +
				", injuriesTraumaSpecify='" + injuriesTraumaSpecify + '\'' +
				", respiratoryDisease='" + respiratoryDisease + '\'' +
				", respiratoryDiseaseSpecify='" + respiratoryDiseaseSpecify + '\'' +
				", cardiovascularDisease='" + cardiovascularDisease + '\'' +
				", cardiovascularDiseaseSpecify='" + cardiovascularDiseaseSpecify + '\'' +
				", endocrineDisease='" + endocrineDisease + '\'' +
				", endocrineDiseaseSpecify='" + endocrineDiseaseSpecify + '\'' +
				", urinarySystemDisease='" + urinarySystemDisease + '\'' +
				", urinarySystemDiseaseSpecify='" + urinarySystemDiseaseSpecify + '\'' +
				", reproductiveSystemDisease='" + reproductiveSystemDisease + '\'' +
				", reproductiveSystemDiseaseSpecify='" + reproductiveSystemDiseaseSpecify + '\'' +
				", communicationHearingProblem='" + communicationHearingProblem + '\'' +
				", communicationHearingProblemSpecify='" + communicationHearingProblemSpecify + '\'' +
				", visionProblem='" + visionProblem + '\'' +
				", visionProblemSpecify='" + visionProblemSpecify + '\'' +
				", oralDentalHealthProblem='" + oralDentalHealthProblem + '\'' +
				", oralDentalHealthProblemSpecify='" + oralDentalHealthProblemSpecify + '\'' +
				", others='" + others + '\'' +
				", underMedication='" + underMedication + '\'' +
				", underMedicationSpecify='" + underMedicationSpecify + '\'' +
				", recentTreatment='" + recentTreatment + '\'' +
				", recentTreatmentSpecify='" + recentTreatmentSpecify + '\'' +
				", recentProcedure='" + recentProcedure + '\'' +
				", recentProcedureSpecify='" + recentProcedureSpecify + '\'' +
				", bhSmoking='" + bhSmoking + '\'' +
				", bhAlcohol='" + bhAlcohol + '\'' +
				", bhSubstanceAbuse='" + bhSubstanceAbuse + '\'' +
				", bhSuicidalThoughts='" + bhSuicidalThoughts + '\'' +
				", bhSuicidalAttempts='" + bhSuicidalAttempts + '\'' +
				", ageOfMenarche='" + ageOfMenarche + '\'' +
				", menstruationMaterials='" + menstruationMaterials + '\'' +
				", teacherDdrKnowledge='" + teacherDdrKnowledge + '\'' +

				'}';
	}
}
