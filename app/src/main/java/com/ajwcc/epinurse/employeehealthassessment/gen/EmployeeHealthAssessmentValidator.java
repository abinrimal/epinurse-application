package com.ajwcc.epinurse.employeehealthassessment.gen;

import com.ajwcc.epinurse.common.BaseValidator;
import com.ajwcc.util.ui.validation.ValidationHandler;
import com.ajwcc.epinurse.R;

public class EmployeeHealthAssessmentValidator extends BaseValidator implements ValidationHandler<EmployeeHealthAssessment>
{
    protected EmployeeHealthAssessment model;
    protected EmployeeHealthAssessmentActivity activity;


    public EmployeeHealthAssessmentValidator(EmployeeHealthAssessmentActivity a)
    {
    	super(a);
        activity = a;
    }

    @Override
    public void setModel(EmployeeHealthAssessment model)
    {
        this.model = model;
    }

    @Override
    public void validateModel()
    {
		validateTemperature();
		validateSystolic();
		validateDiastolic();
		validateHeightCm();
		validateWeightKg();
		validateBmi();
		validateBmiComputed();
		validateAcuteRespiratoryInfection();
		validateAcuteWateryDiarrhea();
		validateAcuteBloodyDiarrhea();
		validateAcuteJaundiceInfection();
		validateSuspectedMeningitis();
		validateSuspectedTetanus();
		validateFever();
		validateSkinDiseases();
		validateSkinDiseasesSpecify();
		validateAllegies();
		validateAllegiesSpecify();
		validateInjuriesTrauma();
		validateInjuriesTraumaSpecify();
		validateRespiratoryDisease();
		validateRespiratoryDiseaseSpecify();
		validateCardiovascularDisease();
		validateCardiovascularDiseaseSpecify();
		validateEndocrineDisease();
		validateEndocrineDiseaseSpecify();
		validateUrinarySystemDisease();
		validateUrinarySystemDiseaseSpecify();
		validateReproductiveSystemDisease();
		validateReproductiveSystemDiseaseSpecify();
		validateCommunicationHearingProblem();
		validateCommunicationHearingProblemSpecify();
		validateVisionProblem();
		validateVisionProblemSpecify();
		validateOralDentalHealthProblem();
		validateOralDentalHealthProblemSpecify();
		//  others optional
		validateUnderMedication();
		validateUnderMedicationSpecify();
		validateRecentTreatment();
		validateRecentTreatmentSpecify();
		validateRecentProcedure();
		validateRecentProcedureSpecify();
		validateBhSmoking();
		validateBhAlcohol();
		validateBhSubstanceAbuse();
		validateBhSuicidalThoughts();
		validateBhSuicidalAttempts();
		//  ageOfMenarche optional
		//  menstruationMaterials optional
		validateTeacherDdrKnowledge();

    }


    public void validateTemperature()
    {
    
        validateNonNullField(model.getTemperature(), activity.getPage("Page1"), R.id.temperatureContainer, context.getResources().getString(R.string.employee_health_assessment_temperature));
    }

    public void validateSystolic()
    {
    
        validateNonNullField(model.getSystolic(), activity.getPage("Page1"), R.id.systolicContainer, context.getResources().getString(R.string.employee_health_assessment_systolic));
    }

    public void validateDiastolic()
    {
    
        validateNonNullField(model.getDiastolic(), activity.getPage("Page1"), R.id.diastolicContainer, context.getResources().getString(R.string.employee_health_assessment_diastolic));
    }

    public void validateHeightCm()
    {
    
        validateNonNullField(model.getHeightCm(), activity.getPage("Page1"), R.id.heightCmContainer, context.getResources().getString(R.string.employee_health_assessment_heightCm));
    }

    public void validateWeightKg()
    {
    
        validateNonNullField(model.getWeightKg(), activity.getPage("Page1"), R.id.weightKgContainer, context.getResources().getString(R.string.employee_health_assessment_weightKg));
    }

    public void validateBmi()
    {
    
        validateNonNullField(model.getBmi(), activity.getPage("Page1"), R.id.bmiContainer, context.getResources().getString(R.string.employee_health_assessment_bmi));
    }

    public void validateBmiComputed()
    {
    
        validateNonNullField(model.getBmiComputed(), activity.getPage("Page1"), R.id.bmiComputedContainer, context.getResources().getString(R.string.employee_health_assessment_bmiComputed));
    }

    public void validateAcuteRespiratoryInfection()
    {
    
        validateNonNullField(model.getAcuteRespiratoryInfection(), activity.getPage("Page1"), R.id.acuteRespiratoryInfectionContainer, context.getResources().getString(R.string.employee_health_assessment_acuteRespiratoryInfection));
    }

    public void validateAcuteWateryDiarrhea()
    {
    
        validateNonNullField(model.getAcuteWateryDiarrhea(), activity.getPage("Page1"), R.id.acuteWateryDiarrheaContainer, context.getResources().getString(R.string.employee_health_assessment_acuteWateryDiarrhea));
    }

    public void validateAcuteBloodyDiarrhea()
    {
    
        validateNonNullField(model.getAcuteBloodyDiarrhea(), activity.getPage("Page1"), R.id.acuteBloodyDiarrheaContainer, context.getResources().getString(R.string.employee_health_assessment_acuteBloodyDiarrhea));
    }

    public void validateAcuteJaundiceInfection()
    {
    
        validateNonNullField(model.getAcuteJaundiceInfection(), activity.getPage("Page1"), R.id.acuteJaundiceInfectionContainer, context.getResources().getString(R.string.employee_health_assessment_acuteJaundiceInfection));
    }

    public void validateSuspectedMeningitis()
    {
    
        validateNonNullField(model.getSuspectedMeningitis(), activity.getPage("Page1"), R.id.suspectedMeningitisContainer, context.getResources().getString(R.string.employee_health_assessment_suspectedMeningitis));
    }

    public void validateSuspectedTetanus()
    {
    
        validateNonNullField(model.getSuspectedTetanus(), activity.getPage("Page1"), R.id.suspectedTetanusContainer, context.getResources().getString(R.string.employee_health_assessment_suspectedTetanus));
    }

    public void validateFever()
    {
    
        validateNonNullField(model.getFever(), activity.getPage("Page1"), R.id.feverContainer, context.getResources().getString(R.string.employee_health_assessment_fever));
    }

    public void validateSkinDiseases()
    {
    
        validateNonNullField(model.getSkinDiseases(), activity.getPage("Page2"), R.id.skinDiseasesContainer, context.getResources().getString(R.string.employee_health_assessment_skinDiseases));
    }

    public void validateSkinDiseasesSpecify()
    {
  
        validateNonNullSpecifyField(model.getSkinDiseases(),1,model.getSkinDiseasesSpecify(), activity.getPage("Page2"), R.id.skinDiseasesSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_skinDiseasesSpecify));   
    }

    public void validateAllegies()
    {
    
        validateNonNullField(model.getAllegies(), activity.getPage("Page2"), R.id.allegiesContainer, context.getResources().getString(R.string.employee_health_assessment_allegies));
    }

    public void validateAllegiesSpecify()
    {
  
        validateNonNullSpecifyField(model.getAllegies(),1,model.getAllegiesSpecify(), activity.getPage("Page2"), R.id.allegiesSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_allegiesSpecify));   
    }

    public void validateInjuriesTrauma()
    {
    
        validateNonNullField(model.getInjuriesTrauma(), activity.getPage("Page2"), R.id.injuriesTraumaContainer, context.getResources().getString(R.string.employee_health_assessment_injuriesTrauma));
    }

    public void validateInjuriesTraumaSpecify()
    {
  
        validateNonNullSpecifyField(model.getInjuriesTrauma(),1,model.getInjuriesTraumaSpecify(), activity.getPage("Page2"), R.id.injuriesTraumaSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_injuriesTraumaSpecify));   
    }

    public void validateRespiratoryDisease()
    {
    
        validateNonNullField(model.getRespiratoryDisease(), activity.getPage("Page2"), R.id.respiratoryDiseaseContainer, context.getResources().getString(R.string.employee_health_assessment_respiratoryDisease));
    }

    public void validateRespiratoryDiseaseSpecify()
    {
  
        validateNonNullSpecifyField(model.getRespiratoryDisease(),1,model.getRespiratoryDiseaseSpecify(), activity.getPage("Page2"), R.id.respiratoryDiseaseSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_respiratoryDiseaseSpecify));   
    }

    public void validateCardiovascularDisease()
    {
    
        validateNonNullField(model.getCardiovascularDisease(), activity.getPage("Page2"), R.id.cardiovascularDiseaseContainer, context.getResources().getString(R.string.employee_health_assessment_cardiovascularDisease));
    }

    public void validateCardiovascularDiseaseSpecify()
    {
  
        validateNonNullSpecifyField(model.getCardiovascularDisease(),1,model.getCardiovascularDiseaseSpecify(), activity.getPage("Page2"), R.id.cardiovascularDiseaseSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_cardiovascularDiseaseSpecify));   
    }

    public void validateEndocrineDisease()
    {
    
        validateNonNullField(model.getEndocrineDisease(), activity.getPage("Page2"), R.id.endocrineDiseaseContainer, context.getResources().getString(R.string.employee_health_assessment_endocrineDisease));
    }

    public void validateEndocrineDiseaseSpecify()
    {
  
        validateNonNullSpecifyField(model.getEndocrineDisease(),1,model.getEndocrineDiseaseSpecify(), activity.getPage("Page2"), R.id.endocrineDiseaseSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_endocrineDiseaseSpecify));   
    }

    public void validateUrinarySystemDisease()
    {
    
        validateNonNullField(model.getUrinarySystemDisease(), activity.getPage("Page2"), R.id.urinarySystemDiseaseContainer, context.getResources().getString(R.string.employee_health_assessment_urinarySystemDisease));
    }

    public void validateUrinarySystemDiseaseSpecify()
    {
  
        validateNonNullSpecifyField(model.getUrinarySystemDisease(),1,model.getUrinarySystemDiseaseSpecify(), activity.getPage("Page2"), R.id.urinarySystemDiseaseSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_urinarySystemDiseaseSpecify));   
    }

    public void validateReproductiveSystemDisease()
    {
    
        validateNonNullField(model.getReproductiveSystemDisease(), activity.getPage("Page2"), R.id.reproductiveSystemDiseaseContainer, context.getResources().getString(R.string.employee_health_assessment_reproductiveSystemDisease));
    }

    public void validateReproductiveSystemDiseaseSpecify()
    {
  
        validateNonNullSpecifyField(model.getReproductiveSystemDisease(),1,model.getReproductiveSystemDiseaseSpecify(), activity.getPage("Page2"), R.id.reproductiveSystemDiseaseSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_reproductiveSystemDiseaseSpecify));   
    }

    public void validateCommunicationHearingProblem()
    {
    
        validateNonNullField(model.getCommunicationHearingProblem(), activity.getPage("Page2"), R.id.communicationHearingProblemContainer, context.getResources().getString(R.string.employee_health_assessment_communicationHearingProblem));
    }

    public void validateCommunicationHearingProblemSpecify()
    {
  
        validateNonNullSpecifyField(model.getCommunicationHearingProblem(),1,model.getCommunicationHearingProblemSpecify(), activity.getPage("Page2"), R.id.communicationHearingProblemSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_communicationHearingProblemSpecify));   
    }

    public void validateVisionProblem()
    {
    
        validateNonNullField(model.getVisionProblem(), activity.getPage("Page2"), R.id.visionProblemContainer, context.getResources().getString(R.string.employee_health_assessment_visionProblem));
    }

    public void validateVisionProblemSpecify()
    {
  
        validateNonNullSpecifyField(model.getVisionProblem(),1,model.getVisionProblemSpecify(), activity.getPage("Page2"), R.id.visionProblemSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_visionProblemSpecify));   
    }

    public void validateOralDentalHealthProblem()
    {
    
        validateNonNullField(model.getOralDentalHealthProblem(), activity.getPage("Page2"), R.id.oralDentalHealthProblemContainer, context.getResources().getString(R.string.employee_health_assessment_oralDentalHealthProblem));
    }

    public void validateOralDentalHealthProblemSpecify()
    {
  
        validateNonNullSpecifyField(model.getOralDentalHealthProblem(),1,model.getOralDentalHealthProblemSpecify(), activity.getPage("Page2"), R.id.oralDentalHealthProblemSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_oralDentalHealthProblemSpecify));   
    }

    public void validateUnderMedication()
    {
    
        validateNonNullField(model.getUnderMedication(), activity.getPage("Page2"), R.id.underMedicationContainer, context.getResources().getString(R.string.employee_health_assessment_underMedication));
    }

    public void validateUnderMedicationSpecify()
    {
  
        validateNonNullSpecifyField(model.getUnderMedication(),1,model.getUnderMedicationSpecify(), activity.getPage("Page2"), R.id.underMedicationSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_underMedicationSpecify));   
    }

    public void validateRecentTreatment()
    {
    
        validateNonNullField(model.getRecentTreatment(), activity.getPage("Page2"), R.id.recentTreatmentContainer, context.getResources().getString(R.string.employee_health_assessment_recentTreatment));
    }

    public void validateRecentTreatmentSpecify()
    {
  
        validateNonNullSpecifyField(model.getRecentTreatment(),1,model.getRecentTreatmentSpecify(), activity.getPage("Page2"), R.id.recentTreatmentSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_recentTreatmentSpecify));   
    }

    public void validateRecentProcedure()
    {
    
        validateNonNullField(model.getRecentProcedure(), activity.getPage("Page2"), R.id.recentProcedureContainer, context.getResources().getString(R.string.employee_health_assessment_recentProcedure));
    }

    public void validateRecentProcedureSpecify()
    {
  
        validateNonNullSpecifyField(model.getRecentProcedure(),1,model.getRecentProcedureSpecify(), activity.getPage("Page2"), R.id.recentProcedureSpecifyContainer, context.getResources().getString(R.string.employee_health_assessment_recentProcedureSpecify));   
    }

    public void validateBhSmoking()
    {
    
        validateNonNullField(model.getBhSmoking(), activity.getPage("Page3"), R.id.bhSmokingContainer, context.getResources().getString(R.string.employee_health_assessment_bhSmoking));
    }

    public void validateBhAlcohol()
    {
    
        validateNonNullField(model.getBhAlcohol(), activity.getPage("Page3"), R.id.bhAlcoholContainer, context.getResources().getString(R.string.employee_health_assessment_bhAlcohol));
    }

    public void validateBhSubstanceAbuse()
    {
    
        validateNonNullField(model.getBhSubstanceAbuse(), activity.getPage("Page3"), R.id.bhSubstanceAbuseContainer, context.getResources().getString(R.string.employee_health_assessment_bhSubstanceAbuse));
    }

    public void validateBhSuicidalThoughts()
    {
    
        validateNonNullField(model.getBhSuicidalThoughts(), activity.getPage("Page3"), R.id.bhSuicidalThoughtsContainer, context.getResources().getString(R.string.employee_health_assessment_bhSuicidalThoughts));
    }

    public void validateBhSuicidalAttempts()
    {
    
        validateNonNullField(model.getBhSuicidalAttempts(), activity.getPage("Page3"), R.id.bhSuicidalAttemptsContainer, context.getResources().getString(R.string.employee_health_assessment_bhSuicidalAttempts));
    }

    public void validateTeacherDdrKnowledge()
    {
    
        validateNonNullField(model.getTeacherDdrKnowledge(), activity.getPage("Page3"), R.id.teacherDdrKnowledgeContainer, context.getResources().getString(R.string.employee_health_assessment_teacherDdrKnowledge));
    }


}
