package com.ajwcc.epinurse.employeehealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_employee_health_assessment_page1)
public class Page1Fragment extends BaseEpinurseFragment {


    public Page1Fragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText temperature;

	@ViewById
	@MapToModelField
	protected EditText systolic;

	@ViewById
	@MapToModelField
	protected EditText diastolic;

	@ViewById
	@MapToModelField
	protected EditText heightCm;

	@ViewById
	@MapToModelField
	protected EditText weightKg;

	@ViewById
	@MapToModelField
	protected RadioGroup bmi;

	@ViewById
	@MapToModelField
	protected EditText bmiComputed;

	@ViewById
	@MapToModelField
	protected RadioGroup acuteRespiratoryInfection;

	@ViewById
	@MapToModelField
	protected RadioGroup acuteWateryDiarrhea;

	@ViewById
	@MapToModelField
	protected RadioGroup acuteBloodyDiarrhea;

	@ViewById
	@MapToModelField
	protected RadioGroup acuteJaundiceInfection;

	@ViewById
	@MapToModelField
	protected RadioGroup suspectedMeningitis;

	@ViewById
	@MapToModelField
	protected RadioGroup suspectedTetanus;

	@ViewById
	@MapToModelField
	protected RadioGroup fever;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(temperature,pojo.getTemperature());
					PojoToViewMapper.setViewValue(systolic,pojo.getSystolic());
					PojoToViewMapper.setViewValue(diastolic,pojo.getDiastolic());
					PojoToViewMapper.setViewValue(heightCm,pojo.getHeightCm());
					PojoToViewMapper.setViewValue(weightKg,pojo.getWeightKg());
					PojoToViewMapper.setViewValue(bmi,pojo.getBmi());
					PojoToViewMapper.setViewValue(bmiComputed,pojo.getBmiComputed());
					PojoToViewMapper.setViewValue(acuteRespiratoryInfection,pojo.getAcuteRespiratoryInfection());
					PojoToViewMapper.setViewValue(acuteWateryDiarrhea,pojo.getAcuteWateryDiarrhea());
					PojoToViewMapper.setViewValue(acuteBloodyDiarrhea,pojo.getAcuteBloodyDiarrhea());
					PojoToViewMapper.setViewValue(acuteJaundiceInfection,pojo.getAcuteJaundiceInfection());
					PojoToViewMapper.setViewValue(suspectedMeningitis,pojo.getSuspectedMeningitis());
					PojoToViewMapper.setViewValue(suspectedTetanus,pojo.getSuspectedTetanus());
					PojoToViewMapper.setViewValue(fever,pojo.getFever());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
                	
					pojo.setTemperature((Double) ViewToPojoMapper.getValueByView(temperature));
					pojo.setSystolic((Integer) ViewToPojoMapper.getValueByView(systolic));
					pojo.setDiastolic((Integer) ViewToPojoMapper.getValueByView(diastolic));
					pojo.setHeightCm((Double) ViewToPojoMapper.getValueByView(heightCm));
					pojo.setWeightKg((Double) ViewToPojoMapper.getValueByView(weightKg));
					pojo.setBmi((Integer) ViewToPojoMapper.getValueByView(bmi));
					pojo.setBmiComputed((Double) ViewToPojoMapper.getValueByView(bmiComputed));
					pojo.setAcuteRespiratoryInfection((Integer) ViewToPojoMapper.getValueByView(acuteRespiratoryInfection));
					pojo.setAcuteWateryDiarrhea((Integer) ViewToPojoMapper.getValueByView(acuteWateryDiarrhea));
					pojo.setAcuteBloodyDiarrhea((Integer) ViewToPojoMapper.getValueByView(acuteBloodyDiarrhea));
					pojo.setAcuteJaundiceInfection((Integer) ViewToPojoMapper.getValueByView(acuteJaundiceInfection));
					pojo.setSuspectedMeningitis((Integer) ViewToPojoMapper.getValueByView(suspectedMeningitis));
					pojo.setSuspectedTetanus((Integer) ViewToPojoMapper.getValueByView(suspectedTetanus));
					pojo.setFever((Integer) ViewToPojoMapper.getValueByView(fever));

					if (save)
					{
                		System.out.println("Save to realm: EmployeeHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			EmployeeHealthAssessment model = (EmployeeHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(EmployeeHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
