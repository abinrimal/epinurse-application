package com.ajwcc.epinurse.employeehealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_employee_health_assessment_page3)
public class Page3Fragment extends BaseEpinurseFragment {


    public Page3Fragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup bhSmoking;

	@ViewById
	@MapToModelField
	protected RadioGroup bhAlcohol;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSubstanceAbuse;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSuicidalThoughts;

	@ViewById
	@MapToModelField
	protected RadioGroup bhSuicidalAttempts;

	@ViewById
	@MapToModelField
	protected EditText ageOfMenarche;

	@ViewById
	@MapToModelField
	protected EditText menstruationMaterials;

	@ViewById
	@MapToModelField
	protected RadioGroup teacherDdrKnowledge;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(bhSmoking,pojo.getBhSmoking());
					PojoToViewMapper.setViewValue(bhAlcohol,pojo.getBhAlcohol());
					PojoToViewMapper.setViewValue(bhSubstanceAbuse,pojo.getBhSubstanceAbuse());
					PojoToViewMapper.setViewValue(bhSuicidalThoughts,pojo.getBhSuicidalThoughts());
					PojoToViewMapper.setViewValue(bhSuicidalAttempts,pojo.getBhSuicidalAttempts());
					PojoToViewMapper.setViewValue(ageOfMenarche,pojo.getAgeOfMenarche());
					PojoToViewMapper.setViewValue(menstruationMaterials,pojo.getMenstruationMaterials());
					PojoToViewMapper.setViewValue(teacherDdrKnowledge,pojo.getTeacherDdrKnowledge());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
                	
					pojo.setBhSmoking((Integer) ViewToPojoMapper.getValueByView(bhSmoking));
					pojo.setBhAlcohol((Integer) ViewToPojoMapper.getValueByView(bhAlcohol));
					pojo.setBhSubstanceAbuse((Integer) ViewToPojoMapper.getValueByView(bhSubstanceAbuse));
					pojo.setBhSuicidalThoughts((Integer) ViewToPojoMapper.getValueByView(bhSuicidalThoughts));
					pojo.setBhSuicidalAttempts((Integer) ViewToPojoMapper.getValueByView(bhSuicidalAttempts));
					pojo.setAgeOfMenarche((Integer) ViewToPojoMapper.getValueByView(ageOfMenarche));
					pojo.setMenstruationMaterials((String) ViewToPojoMapper.getValueByView(menstruationMaterials));
					pojo.setTeacherDdrKnowledge((Integer) ViewToPojoMapper.getValueByView(teacherDdrKnowledge));

					if (save)
					{
                		System.out.println("Save to realm: EmployeeHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			EmployeeHealthAssessment model = (EmployeeHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(EmployeeHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
