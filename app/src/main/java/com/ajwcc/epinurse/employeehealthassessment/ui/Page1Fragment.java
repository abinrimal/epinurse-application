package com.ajwcc.epinurse.employeehealthassessment.ui;


import android.widget.RadioGroup;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.gen_fragment_employee_health_assessment_page1)
public class Page1Fragment extends com.ajwcc.epinurse.employeehealthassessment.gen.Page1Fragment {


    @AfterViews
    public void init()
    {

        UiUtils.disableRadioGroup(this.bmi);
        bmiComputed.setEnabled(false);
    }

    @AfterTextChange({R.id.heightCm, R.id.weightKg})
    public void selectBmi()
    {
        UiUtils.setBmi(bmi, heightCm, weightKg, bmiComputed);
    }
}
