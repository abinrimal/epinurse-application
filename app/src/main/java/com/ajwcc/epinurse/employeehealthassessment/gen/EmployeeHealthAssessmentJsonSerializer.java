package com.ajwcc.epinurse.employeehealthassessment.gen;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;


public class EmployeeHealthAssessmentJsonSerializer implements JsonSerializer<EmployeeHealthAssessment> {

    @Override
    public JsonElement serialize(EmployeeHealthAssessment model, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        
        object.add("uuid", context.serialize(model.getUuid()));
        object.add("ownerUuid", context.serialize(model.getOwnerUuid()));
        
		serializeShineId(model, object, context); // shine_id

		// Page1
		serializeTemperature(model, object, context); // temperature
		serializeSystolic(model, object, context); // systolic
		serializeDiastolic(model, object, context); // diastolic
		serializeHeightCm(model, object, context); // height_cm
		serializeWeightKg(model, object, context); // weight_kg
		serializeBmi(model, object, context); // bmi
		serializeBmiComputed(model, object, context); // bmi_computed
		serializeAcuteRespiratoryInfection(model, object, context); // acute_respiratory_infection
		serializeAcuteWateryDiarrhea(model, object, context); // acute_watery_diarrhea
		serializeAcuteBloodyDiarrhea(model, object, context); // acute_bloody_diarrhea
		serializeAcuteJaundiceInfection(model, object, context); // acute_jaundice_infection
		serializeSuspectedMeningitis(model, object, context); // suspected_meningitis
		serializeSuspectedTetanus(model, object, context); // suspected_tetanus
		serializeFever(model, object, context); // fever

		// Page2
		serializeSkinDiseases(model, object, context); // skin_diseases
		serializeSkinDiseasesSpecify(model, object, context); // skin_diseases_specify
		serializeAllegies(model, object, context); // allegies
		serializeAllegiesSpecify(model, object, context); // allegies_specify
		serializeInjuriesTrauma(model, object, context); // injuries_trauma
		serializeInjuriesTraumaSpecify(model, object, context); // injuries_trauma_specify
		serializeRespiratoryDisease(model, object, context); // respiratory_disease
		serializeRespiratoryDiseaseSpecify(model, object, context); // respiratory_disease_specify
		serializeCardiovascularDisease(model, object, context); // cardiovascular_disease
		serializeCardiovascularDiseaseSpecify(model, object, context); // cardiovascular_disease_specify
		serializeEndocrineDisease(model, object, context); // endocrine_disease
		serializeEndocrineDiseaseSpecify(model, object, context); // endocrine_disease_specify
		serializeUrinarySystemDisease(model, object, context); // urinary_system_disease
		serializeUrinarySystemDiseaseSpecify(model, object, context); // urinary_system_disease_specify
		serializeReproductiveSystemDisease(model, object, context); // reproductive_system_disease
		serializeReproductiveSystemDiseaseSpecify(model, object, context); // reproductive_system_disease_specify
		serializeCommunicationHearingProblem(model, object, context); // communication_hearing_problem
		serializeCommunicationHearingProblemSpecify(model, object, context); // communication_hearing_problem_specify
		serializeVisionProblem(model, object, context); // vision_problem
		serializeVisionProblemSpecify(model, object, context); // vision_problem_specify
		serializeOralDentalHealthProblem(model, object, context); // oral_dental_health_problem
		serializeOralDentalHealthProblemSpecify(model, object, context); // oral_dental_health_problem_specify
		serializeOthers(model, object, context); // others
		serializeUnderMedication(model, object, context); // under_medication
		serializeUnderMedicationSpecify(model, object, context); // under_medication_specify
		serializeRecentTreatment(model, object, context); // recent_treatment
		serializeRecentTreatmentSpecify(model, object, context); // recent_treatment_specify
		serializeRecentProcedure(model, object, context); // recent_procedure
		serializeRecentProcedureSpecify(model, object, context); // recent_procedure_specify

		// Page3
		serializeBhSmoking(model, object, context); // bh_smoking
		serializeBhAlcohol(model, object, context); // bh_alcohol
		serializeBhSubstanceAbuse(model, object, context); // bh_substance_abuse
		serializeBhSuicidalThoughts(model, object, context); // bh_suicidal_thoughts
		serializeBhSuicidalAttempts(model, object, context); // bh_suicidal_attempts
		serializeAgeOfMenarche(model, object, context); // age_of_menarche
		serializeMenstruationMaterials(model, object, context); // menstruation_materials
		serializeTeacherDdrKnowledge(model, object, context); // teacher_ddr_knowledge
 
       
        return object;
    }
    

    public void serializeShineId(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("shine_id", context.serialize(model.getShineId()));
    }

    public void serializeTemperature(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temperature", context.serialize(model.getTemperature()));
    }

    public void serializeSystolic(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("systolic", context.serialize(model.getSystolic()));
    }

    public void serializeDiastolic(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("diastolic", context.serialize(model.getDiastolic()));
    }

    public void serializeHeightCm(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("height_cm", context.serialize(model.getHeightCm()));
    }

    public void serializeWeightKg(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("weight_kg", context.serialize(model.getWeightKg()));
    }

    public void serializeBmi(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bmi", context.serialize(model.getBmi()));
    }

    public void serializeBmiComputed(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bmi_computed", context.serialize(model.getBmiComputed()));
    }

    public void serializeAcuteRespiratoryInfection(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("acute_respiratory_infection", context.serialize(model.getAcuteRespiratoryInfection()));
    }

    public void serializeAcuteWateryDiarrhea(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("acute_watery_diarrhea", context.serialize(model.getAcuteWateryDiarrhea()));
    }

    public void serializeAcuteBloodyDiarrhea(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("acute_bloody_diarrhea", context.serialize(model.getAcuteBloodyDiarrhea()));
    }

    public void serializeAcuteJaundiceInfection(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("acute_jaundice_infection", context.serialize(model.getAcuteJaundiceInfection()));
    }

    public void serializeSuspectedMeningitis(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("suspected_meningitis", context.serialize(model.getSuspectedMeningitis()));
    }

    public void serializeSuspectedTetanus(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("suspected_tetanus", context.serialize(model.getSuspectedTetanus()));
    }

    public void serializeFever(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("fever", context.serialize(model.getFever()));
    }

    public void serializeSkinDiseases(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("skin_diseases", context.serialize(model.getSkinDiseases()));
    }

    public void serializeSkinDiseasesSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("skin_diseases_specify", context.serialize(model.getSkinDiseasesSpecify()));
    }

    public void serializeAllegies(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("allegies", context.serialize(model.getAllegies()));
    }

    public void serializeAllegiesSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("allegies_specify", context.serialize(model.getAllegiesSpecify()));
    }

    public void serializeInjuriesTrauma(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injuries_trauma", context.serialize(model.getInjuriesTrauma()));
    }

    public void serializeInjuriesTraumaSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injuries_trauma_specify", context.serialize(model.getInjuriesTraumaSpecify()));
    }

    public void serializeRespiratoryDisease(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("respiratory_disease", context.serialize(model.getRespiratoryDisease()));
    }

    public void serializeRespiratoryDiseaseSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("respiratory_disease_specify", context.serialize(model.getRespiratoryDiseaseSpecify()));
    }

    public void serializeCardiovascularDisease(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cardiovascular_disease", context.serialize(model.getCardiovascularDisease()));
    }

    public void serializeCardiovascularDiseaseSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cardiovascular_disease_specify", context.serialize(model.getCardiovascularDiseaseSpecify()));
    }

    public void serializeEndocrineDisease(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("endocrine_disease", context.serialize(model.getEndocrineDisease()));
    }

    public void serializeEndocrineDiseaseSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("endocrine_disease_specify", context.serialize(model.getEndocrineDiseaseSpecify()));
    }

    public void serializeUrinarySystemDisease(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("urinary_system_disease", context.serialize(model.getUrinarySystemDisease()));
    }

    public void serializeUrinarySystemDiseaseSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("urinary_system_disease_specify", context.serialize(model.getUrinarySystemDiseaseSpecify()));
    }

    public void serializeReproductiveSystemDisease(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("reproductive_system_disease", context.serialize(model.getReproductiveSystemDisease()));
    }

    public void serializeReproductiveSystemDiseaseSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("reproductive_system_disease_specify", context.serialize(model.getReproductiveSystemDiseaseSpecify()));
    }

    public void serializeCommunicationHearingProblem(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("communication_hearing_problem", context.serialize(model.getCommunicationHearingProblem()));
    }

    public void serializeCommunicationHearingProblemSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("communication_hearing_problem_specify", context.serialize(model.getCommunicationHearingProblemSpecify()));
    }

    public void serializeVisionProblem(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("vision_problem", context.serialize(model.getVisionProblem()));
    }

    public void serializeVisionProblemSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("vision_problem_specify", context.serialize(model.getVisionProblemSpecify()));
    }

    public void serializeOralDentalHealthProblem(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("oral_dental_health_problem", context.serialize(model.getOralDentalHealthProblem()));
    }

    public void serializeOralDentalHealthProblemSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("oral_dental_health_problem_specify", context.serialize(model.getOralDentalHealthProblemSpecify()));
    }

    public void serializeOthers(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("others", context.serialize(model.getOthers()));
    }

    public void serializeUnderMedication(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("under_medication", context.serialize(model.getUnderMedication()));
    }

    public void serializeUnderMedicationSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("under_medication_specify", context.serialize(model.getUnderMedicationSpecify()));
    }

    public void serializeRecentTreatment(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("recent_treatment", context.serialize(model.getRecentTreatment()));
    }

    public void serializeRecentTreatmentSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("recent_treatment_specify", context.serialize(model.getRecentTreatmentSpecify()));
    }

    public void serializeRecentProcedure(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("recent_procedure", context.serialize(model.getRecentProcedure()));
    }

    public void serializeRecentProcedureSpecify(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("recent_procedure_specify", context.serialize(model.getRecentProcedureSpecify()));
    }

    public void serializeBhSmoking(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bh_smoking", context.serialize(model.getBhSmoking()));
    }

    public void serializeBhAlcohol(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bh_alcohol", context.serialize(model.getBhAlcohol()));
    }

    public void serializeBhSubstanceAbuse(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bh_substance_abuse", context.serialize(model.getBhSubstanceAbuse()));
    }

    public void serializeBhSuicidalThoughts(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bh_suicidal_thoughts", context.serialize(model.getBhSuicidalThoughts()));
    }

    public void serializeBhSuicidalAttempts(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bh_suicidal_attempts", context.serialize(model.getBhSuicidalAttempts()));
    }

    public void serializeAgeOfMenarche(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_of_menarche", context.serialize(model.getAgeOfMenarche()));
    }

    public void serializeMenstruationMaterials(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_materials", context.serialize(model.getMenstruationMaterials()));
    }

    public void serializeTeacherDdrKnowledge(EmployeeHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("teacher_ddr_knowledge", context.serialize(model.getTeacherDdrKnowledge()));
    }
 
    

}