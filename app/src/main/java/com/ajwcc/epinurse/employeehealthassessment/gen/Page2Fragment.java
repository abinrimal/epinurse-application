package com.ajwcc.epinurse.employeehealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_employee_health_assessment_page2)
public class Page2Fragment extends BaseEpinurseFragment {


    public Page2Fragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup skinDiseases;

	@ViewById
	@MapToModelField
	protected EditText skinDiseasesSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup allegies;

	@ViewById
	@MapToModelField
	protected EditText allegiesSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup injuriesTrauma;

	@ViewById
	@MapToModelField
	protected EditText injuriesTraumaSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup respiratoryDisease;

	@ViewById
	@MapToModelField
	protected EditText respiratoryDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup cardiovascularDisease;

	@ViewById
	@MapToModelField
	protected EditText cardiovascularDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup endocrineDisease;

	@ViewById
	@MapToModelField
	protected EditText endocrineDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup urinarySystemDisease;

	@ViewById
	@MapToModelField
	protected EditText urinarySystemDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup reproductiveSystemDisease;

	@ViewById
	@MapToModelField
	protected EditText reproductiveSystemDiseaseSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup communicationHearingProblem;

	@ViewById
	@MapToModelField
	protected EditText communicationHearingProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup visionProblem;

	@ViewById
	@MapToModelField
	protected EditText visionProblemSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup oralDentalHealthProblem;

	@ViewById
	@MapToModelField
	protected EditText oralDentalHealthProblemSpecify;

	@ViewById
	@MapToModelField
	protected EditText others;

	@ViewById
	@MapToModelField
	protected RadioGroup underMedication;

	@ViewById
	@MapToModelField
	protected EditText underMedicationSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup recentTreatment;

	@ViewById
	@MapToModelField
	protected EditText recentTreatmentSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup recentProcedure;

	@ViewById
	@MapToModelField
	protected EditText recentProcedureSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(skinDiseases,pojo.getSkinDiseases());
					PojoToViewMapper.setViewValue(skinDiseasesSpecify,pojo.getSkinDiseasesSpecify());
					PojoToViewMapper.setViewValue(allegies,pojo.getAllegies());
					PojoToViewMapper.setViewValue(allegiesSpecify,pojo.getAllegiesSpecify());
					PojoToViewMapper.setViewValue(injuriesTrauma,pojo.getInjuriesTrauma());
					PojoToViewMapper.setViewValue(injuriesTraumaSpecify,pojo.getInjuriesTraumaSpecify());
					PojoToViewMapper.setViewValue(respiratoryDisease,pojo.getRespiratoryDisease());
					PojoToViewMapper.setViewValue(respiratoryDiseaseSpecify,pojo.getRespiratoryDiseaseSpecify());
					PojoToViewMapper.setViewValue(cardiovascularDisease,pojo.getCardiovascularDisease());
					PojoToViewMapper.setViewValue(cardiovascularDiseaseSpecify,pojo.getCardiovascularDiseaseSpecify());
					PojoToViewMapper.setViewValue(endocrineDisease,pojo.getEndocrineDisease());
					PojoToViewMapper.setViewValue(endocrineDiseaseSpecify,pojo.getEndocrineDiseaseSpecify());
					PojoToViewMapper.setViewValue(urinarySystemDisease,pojo.getUrinarySystemDisease());
					PojoToViewMapper.setViewValue(urinarySystemDiseaseSpecify,pojo.getUrinarySystemDiseaseSpecify());
					PojoToViewMapper.setViewValue(reproductiveSystemDisease,pojo.getReproductiveSystemDisease());
					PojoToViewMapper.setViewValue(reproductiveSystemDiseaseSpecify,pojo.getReproductiveSystemDiseaseSpecify());
					PojoToViewMapper.setViewValue(communicationHearingProblem,pojo.getCommunicationHearingProblem());
					PojoToViewMapper.setViewValue(communicationHearingProblemSpecify,pojo.getCommunicationHearingProblemSpecify());
					PojoToViewMapper.setViewValue(visionProblem,pojo.getVisionProblem());
					PojoToViewMapper.setViewValue(visionProblemSpecify,pojo.getVisionProblemSpecify());
					PojoToViewMapper.setViewValue(oralDentalHealthProblem,pojo.getOralDentalHealthProblem());
					PojoToViewMapper.setViewValue(oralDentalHealthProblemSpecify,pojo.getOralDentalHealthProblemSpecify());
					PojoToViewMapper.setViewValue(others,pojo.getOthers());
					PojoToViewMapper.setViewValue(underMedication,pojo.getUnderMedication());
					PojoToViewMapper.setViewValue(underMedicationSpecify,pojo.getUnderMedicationSpecify());
					PojoToViewMapper.setViewValue(recentTreatment,pojo.getRecentTreatment());
					PojoToViewMapper.setViewValue(recentTreatmentSpecify,pojo.getRecentTreatmentSpecify());
					PojoToViewMapper.setViewValue(recentProcedure,pojo.getRecentProcedure());
					PojoToViewMapper.setViewValue(recentProcedureSpecify,pojo.getRecentProcedureSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: EmployeeHealthAssessment");
                	EmployeeHealthAssessment pojo = (EmployeeHealthAssessment) getModel();
                	
					pojo.setSkinDiseases((Integer) ViewToPojoMapper.getValueByView(skinDiseases));
					pojo.setSkinDiseasesSpecify((String) ViewToPojoMapper.getValueByView(skinDiseasesSpecify));
					pojo.setAllegies((Integer) ViewToPojoMapper.getValueByView(allegies));
					pojo.setAllegiesSpecify((String) ViewToPojoMapper.getValueByView(allegiesSpecify));
					pojo.setInjuriesTrauma((Integer) ViewToPojoMapper.getValueByView(injuriesTrauma));
					pojo.setInjuriesTraumaSpecify((String) ViewToPojoMapper.getValueByView(injuriesTraumaSpecify));
					pojo.setRespiratoryDisease((Integer) ViewToPojoMapper.getValueByView(respiratoryDisease));
					pojo.setRespiratoryDiseaseSpecify((String) ViewToPojoMapper.getValueByView(respiratoryDiseaseSpecify));
					pojo.setCardiovascularDisease((Integer) ViewToPojoMapper.getValueByView(cardiovascularDisease));
					pojo.setCardiovascularDiseaseSpecify((String) ViewToPojoMapper.getValueByView(cardiovascularDiseaseSpecify));
					pojo.setEndocrineDisease((Integer) ViewToPojoMapper.getValueByView(endocrineDisease));
					pojo.setEndocrineDiseaseSpecify((String) ViewToPojoMapper.getValueByView(endocrineDiseaseSpecify));
					pojo.setUrinarySystemDisease((Integer) ViewToPojoMapper.getValueByView(urinarySystemDisease));
					pojo.setUrinarySystemDiseaseSpecify((String) ViewToPojoMapper.getValueByView(urinarySystemDiseaseSpecify));
					pojo.setReproductiveSystemDisease((Integer) ViewToPojoMapper.getValueByView(reproductiveSystemDisease));
					pojo.setReproductiveSystemDiseaseSpecify((String) ViewToPojoMapper.getValueByView(reproductiveSystemDiseaseSpecify));
					pojo.setCommunicationHearingProblem((Integer) ViewToPojoMapper.getValueByView(communicationHearingProblem));
					pojo.setCommunicationHearingProblemSpecify((String) ViewToPojoMapper.getValueByView(communicationHearingProblemSpecify));
					pojo.setVisionProblem((Integer) ViewToPojoMapper.getValueByView(visionProblem));
					pojo.setVisionProblemSpecify((String) ViewToPojoMapper.getValueByView(visionProblemSpecify));
					pojo.setOralDentalHealthProblem((Integer) ViewToPojoMapper.getValueByView(oralDentalHealthProblem));
					pojo.setOralDentalHealthProblemSpecify((String) ViewToPojoMapper.getValueByView(oralDentalHealthProblemSpecify));
					pojo.setOthers((String) ViewToPojoMapper.getValueByView(others));
					pojo.setUnderMedication((Integer) ViewToPojoMapper.getValueByView(underMedication));
					pojo.setUnderMedicationSpecify((String) ViewToPojoMapper.getValueByView(underMedicationSpecify));
					pojo.setRecentTreatment((Integer) ViewToPojoMapper.getValueByView(recentTreatment));
					pojo.setRecentTreatmentSpecify((String) ViewToPojoMapper.getValueByView(recentTreatmentSpecify));
					pojo.setRecentProcedure((Integer) ViewToPojoMapper.getValueByView(recentProcedure));
					pojo.setRecentProcedureSpecify((String) ViewToPojoMapper.getValueByView(recentProcedureSpecify));

					if (save)
					{
                		System.out.println("Save to realm: EmployeeHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.skinDiseases1,R.id.allegies1,R.id.injuriesTrauma1,R.id.respiratoryDisease1,R.id.cardiovascularDisease1,R.id.endocrineDisease1,R.id.urinarySystemDisease1,R.id.reproductiveSystemDisease1,R.id.communicationHearingProblem1,R.id.visionProblem1,R.id.oralDentalHealthProblem1,R.id.underMedication1,R.id.recentTreatment1,R.id.recentProcedure1})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.skinDiseases1:
				 UiUtils.toggleSpecify(view, skinDiseasesSpecify);
				break;
			case R.id.allegies1:
				 UiUtils.toggleSpecify(view, allegiesSpecify);
				break;
			case R.id.injuriesTrauma1:
				 UiUtils.toggleSpecify(view, injuriesTraumaSpecify);
				break;
			case R.id.respiratoryDisease1:
				 UiUtils.toggleSpecify(view, respiratoryDiseaseSpecify);
				break;
			case R.id.cardiovascularDisease1:
				 UiUtils.toggleSpecify(view, cardiovascularDiseaseSpecify);
				break;
			case R.id.endocrineDisease1:
				 UiUtils.toggleSpecify(view, endocrineDiseaseSpecify);
				break;
			case R.id.urinarySystemDisease1:
				 UiUtils.toggleSpecify(view, urinarySystemDiseaseSpecify);
				break;
			case R.id.reproductiveSystemDisease1:
				 UiUtils.toggleSpecify(view, reproductiveSystemDiseaseSpecify);
				break;
			case R.id.communicationHearingProblem1:
				 UiUtils.toggleSpecify(view, communicationHearingProblemSpecify);
				break;
			case R.id.visionProblem1:
				 UiUtils.toggleSpecify(view, visionProblemSpecify);
				break;
			case R.id.oralDentalHealthProblem1:
				 UiUtils.toggleSpecify(view, oralDentalHealthProblemSpecify);
				break;
			case R.id.underMedication1:
				 UiUtils.toggleSpecify(view, underMedicationSpecify);
				break;
			case R.id.recentTreatment1:
				 UiUtils.toggleSpecify(view, recentTreatmentSpecify);
				break;
			case R.id.recentProcedure1:
				 UiUtils.toggleSpecify(view, recentProcedureSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			EmployeeHealthAssessment model = (EmployeeHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(EmployeeHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
