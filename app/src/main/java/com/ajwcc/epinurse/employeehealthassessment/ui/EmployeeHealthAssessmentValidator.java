package com.ajwcc.epinurse.employeehealthassessment.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessment;
import com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessmentActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

public class EmployeeHealthAssessmentValidator extends com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessmentValidator
{

    public EmployeeHealthAssessmentValidator(EmployeeHealthAssessmentActivity a)
    {
        super(a);
    }


    @Override
    public void validateModel()
    {
        // check if owenerUuid has been set
        validateNonNullField(model.getOwnerUuid(), activity.getPage("Owner"), R.id.button, context.getResources().getString(R.string.basic_information_profile));  // TODO: USE RES


        super.validateModel();
    }

}
