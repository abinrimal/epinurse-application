package com.ajwcc.epinurse.employeehealthassessment.gen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ajwcc.epinurse.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.UiThread;


import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.RealmRecyclerViewAdapter;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;

import com.ajwcc.epinurse.common.GenericSenderActivity_;
import com.ajwcc.epinurse.common.network.NurseLogin_;
import com.ajwcc.epinurse.common.network.ShineSender;
import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.util.network.AndroidNetUtils;
import android.widget.Toast;
import java.util.HashMap;
import io.realm.RealmQuery;
import android.widget.Button;

@EActivity(R.layout.gen_activity_employee_health_assessment_select)
public class EmployeeHealthAssessmentSelectActivity extends BaseEpinurseSelectActivity
{
	@Extra
	public QueryFilter<EmployeeHealthAssessment> filter;

    @Extra
    public boolean selectMode;

    @ViewById(R.id.recyclerView)
    public RecyclerView recyclerView;

    @ViewById
    public TextView available;

    @ViewById
    public Button upload;

    @ViewById
    public Button download;


    @AfterViews
    public void init()
    {
        realm = Realm.getDefaultInstance();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(createAdapter());
        
        if (filter!=null)
        {
        	available.setText(available.getText()+"("+filter.getName()+")");
        }
        
        if (selectMode==true)
        {
        	if (upload!=null) upload.setVisibility(View.GONE);
        	if (download!=null) download.setVisibility(View.GONE);
        }
    }

    public void finish()
    {
        super.finish();
        realm.close();
    }

    @Click(R.id.newReport)
    public void newRow()
    {
        com.ajwcc.epinurse.employeehealthassessment.ui.EmployeeHealthAssessmentActivity_.intent(this).startForResult(0);
    }

    public void openEditor(View view)
    {
        // edit mode
        EmployeeHealthAssessment b = (EmployeeHealthAssessment) view.getTag();
        com.ajwcc.epinurse.employeehealthassessment.ui.EmployeeHealthAssessmentActivity_.intent(this).uuid(b.getUuid()).startForResult(0);

    }

    public boolean getSelectMode()
    {
        return selectMode;
    }

    public RealmResults<EmployeeHealthAssessment> getResults()
    {
        // load realm results
        RealmQuery<EmployeeHealthAssessment> query = realm.where(EmployeeHealthAssessment.class);

		if (filter!=null)
        {
            query = filter.adjust(query);
        }

        RealmResults<EmployeeHealthAssessment> results = query.findAll();

        results = results.sort("editing", Sort.DESCENDING, "createdAt", Sort.DESCENDING);

        //System.out.println(results);
        return results;
    }
    
    public RealmRecyclerViewAdapter createAdapter()
    {
        // make adapter
        com.ajwcc.epinurse.common.GenericSelectAdapter adapter = new com.ajwcc.epinurse.common.GenericSelectAdapter(this, getResults(), true);
        return adapter;
    }
    
    public BasicInformation getProfile(String uuid)
    {
        return realm.where(BasicInformation.class).equalTo("uuid", uuid).findFirst();
    }
 
 
    @Bean
    public ShineSender sender;

    @Click(R.id.upload)
    public void upload()
    {
        if (!AndroidNetUtils.isNetworkAvailable(this))
        {
        	Toast.makeText(this, getResources().getString(R.string.error_no_network_connection), Toast.LENGTH_LONG).show();
        	return;
        }

        // open login to login
        // on success return here, onActivityResult to trigger download processs

        if (sender.loginNeeded())
        {
            NurseLogin_.intent(this).closeOnSuccess(true).startForResult(1);
        }
        else
        {
            openSender();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // if login was successful, openSenderActivity
        if (requestCode==1)
        {
	        if (!sender.loginNeeded())
	        {
	            openSender();
	        }
	    }
    }

    public void openSender()
    {
        Class clazz =  EmployeeHealthAssessment.class;
        GenericSenderActivity_.intent(this)
                              .reportClassName(clazz.getName())
                              .reportName(clazz.getSimpleName())
                              .start();
    }
 
    
}
