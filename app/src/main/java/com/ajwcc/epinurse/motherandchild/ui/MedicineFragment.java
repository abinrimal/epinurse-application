package com.ajwcc.epinurse.motherandchild.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChild;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.ViewToPojoMapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.realm.RealmList;


@EFragment(R.layout.fragment_mother_and_child_medicines)
public class MedicineFragment extends BaseEpinurseFragment
{

    @ViewById
    LinearLayout medicines;


    @AfterViews
    public void init()
    {
    }

    public void deleteRow(View v)
    {
        medicines.removeView((View) v.getTag());
        medicines.invalidate();
    }

    @Click(R.id.addEntry)
    public void newRow()
    {
        View view = createRow("",0, 0);

        // add to layout
        medicines.addView(view);
        medicines.invalidate();
    }

    public View createRow(String medicine, int prescribed, int stock)
    {
        final View view = (View) getActivity().getLayoutInflater().inflate(R.layout.row_medicine, null);

        // getViews
        EditText medicineField = (EditText) view.findViewById(R.id.medicineName);
        RadioGroup prescribedField = (RadioGroup) view.findViewById(R.id.prescribed);
        EditText stockField = (EditText) view.findViewById(R.id.stock);

        Button button = view.findViewById(R.id.delete);
        button.setTag(view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRow(v);
            }
        });

        // fill rows
        medicineField.setText(medicine);
        ((RadioButton) prescribedField.getChildAt(prescribed)).setChecked(true);
        stockField.setText(String.valueOf(stock));

        return view;
    }

    public View createRowFromEntry(MedicineEntry entry)
    {
        return createRow(entry.getMedicine(), entry.getPrescribed(), entry.getStock());
    }

    public void mapModelToViews()
    {
        MotherAndChild model = (MotherAndChild) getModel();
        RealmList<MedicineEntry> list = model.getMedicines();

        for (MedicineEntry entry : list)
        {
            View view = createRowFromEntry(entry);
            medicines.addView(view);
        }
        medicines.invalidate();

    }

    public void mapViewsToModel()
    {
        // validate the fields first to make sure they all have values


        try {

            MotherAndChild model = (MotherAndChild) getModel();
            RealmList<MedicineEntry> list = model.getMedicines();

            list.clear();

            // create MedicineEntry from rows
            for (int i = 0; i < medicines.getChildCount(); i++) {
                View view = medicines.getChildAt(i);

                // getViews
                EditText medicineField = (EditText) view.findViewById(R.id.medicineName);
                RadioGroup prescribedField = (RadioGroup) view.findViewById(R.id.prescribed);
                EditText stockField = (EditText) view.findViewById(R.id.stock);


                MedicineEntry entry = new MedicineEntry();
                entry.setMedicine((String) ViewToPojoMapper.getValueByView(medicineField));
                entry.setPrescribed((Integer) ViewToPojoMapper.getValueByView(prescribedField));
                entry.setStock((Integer) ViewToPojoMapper.getValueByView(stockField));

                list.add(entry);

                // force these for now
                // NOTE: this means no matter what happens this will be flagged as dirty
                //       even if you just swipe through
                model.setEditing(true);
                model.setSynced(false);
            }

            // save
            saveModel();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }







}
