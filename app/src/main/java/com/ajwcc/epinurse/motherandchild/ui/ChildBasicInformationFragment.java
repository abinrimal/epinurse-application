package com.ajwcc.epinurse.motherandchild.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.gen_fragment_mother_and_child_child_basic_information)
public class ChildBasicInformationFragment extends com.ajwcc.epinurse.motherandchild.gen.ChildBasicInformationFragment
{
    @Click(R.id.dateOfBirthInAdButton)
    public void openADCalendar()
    {
        UiUtils.openGregorianCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);
    }

    @Click(R.id.dateOfBirthInBsButton)
    public void openBSCalendar()
    {
        UiUtils.openNepaliCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);
    }
}
