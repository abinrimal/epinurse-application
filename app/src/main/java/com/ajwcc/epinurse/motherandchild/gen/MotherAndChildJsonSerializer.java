package com.ajwcc.epinurse.motherandchild.gen;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;


public class MotherAndChildJsonSerializer implements JsonSerializer<MotherAndChild> {

    @Override
    public JsonElement serialize(MotherAndChild model, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        
        object.add("uuid", context.serialize(model.getUuid()));
        object.add("ownerUuid", context.serialize(model.getOwnerUuid()));
        
		serializeShineId(model, object, context); // shine_id

		// Menstruation
		serializeAgeOfMenarche(model, object, context); // age_of_menarche
		serializeWhereFirstMenarche(model, object, context); // where_first_menarche
		serializeWhereFirstMenarcheSpecify(model, object, context); // where_first_menarche_specify
		serializeSanitaryMaterialsUsedPads(model, object, context); // sanitary_materials_used_pads
		serializeSanitaryMaterialsUsedTampon(model, object, context); // sanitary_materials_used_tampon
		serializeSanitaryMaterialsUsedClothes(model, object, context); // sanitary_materials_used_clothes
		serializeSanitaryMaterialsUsedHp(model, object, context); // sanitary_materials_used_hp
		serializeSanitaryMaterialsUsedMhp(model, object, context); // sanitary_materials_used_mhp
		serializeSanitaryMaterialsUsedNone(model, object, context); // sanitary_materials_used_none
		serializeHowOftenUsed(model, object, context); // how_often_used
		serializeHowOftenUsedSpecify(model, object, context); // how_often_used_specify
		serializeReasonNotTimely(model, object, context); // reason_not_timely
		serializeHowDisposeMaterials(model, object, context); // how_dispose_materials
		serializeHowDisposeMaterialsSpecify(model, object, context); // how_dispose_materials_specify

		// Pregnancy
		serializeObstetricHistoryGravida(model, object, context); // obstetric_history_gravida
		serializeObstetricHistoryPara(model, object, context); // obstetric_history_para
		serializeObstetricHistoryAbortion(model, object, context); // obstetric_history_abortion
		serializeObstetricHistoryLiving(model, object, context); // obstetric_history_living
		serializePeriodGestation(model, object, context); // period_gestation
		serializeNumAntenatalVisits(model, object, context); // num_antenatal_visits
		serializeAntenalServiceAncCheckUp(model, object, context); // antenal_service_anc_check_up
		serializeAntenalServiceAlbendazole(model, object, context); // antenal_service_albendazole
		serializeAntenalServiceTdImmunization(model, object, context); // antenal_service_td_immunization
		serializeAntenalServiceIrontabs(model, object, context); // antenal_service_irontabs
		serializeAntenalServiceCouseling(model, object, context); // antenal_service_couseling
		serializeAntenalServicePmtct(model, object, context); // antenal_service_pmtct
		serializeAntenalServiceOthers(model, object, context); // antenal_service_others
		serializeAntenalServiceSpecify(model, object, context); // antenal_service_specify
		serializeHealthSeekingBehaviour(model, object, context); // health_seeking_behaviour
		serializeHealthSeekingBehaviourSpecify(model, object, context); // health_seeking_behaviour_specify
		serializeMedicines(model, object, context); // medicines

		// Childbirth
		serializeYoungestBabyMonths(model, object, context); // youngest_baby_months
		serializeTypeOfDelivery(model, object, context); // type_of_delivery
		serializePlaceDeliveryYoungest(model, object, context); // place_delivery_youngest
		serializePlaceDeliveryYoungestSpecify(model, object, context); // place_delivery_youngest_specify
		serializeDeliveryComplications(model, object, context); // delivery_complications
		serializeDeliveryComplicationsSpecify(model, object, context); // delivery_complications_specify
		serializeGapBetweenKids(model, object, context); // gap_between_kids

		// Post Natal
		serializePostNatalPe(model, object, context); // post_natal_pe
		serializePostNatalCounselBF(model, object, context); // post_natal_counsel_b_f
		serializePostNatalCounselFP(model, object, context); // post_natal_counsel_f_p
		serializePostNatalInvestigations(model, object, context); // post_natal_investigations
		serializePostNatalIronTablets(model, object, context); // post_natal_iron_tablets
		serializePostNatalVitaminA(model, object, context); // post_natal_vitamin_a
		serializePostNatalOthers(model, object, context); // post_natal_others
		serializePostNatalSpecify(model, object, context); // post_natal_specify
		serializePncFollowupNone(model, object, context); // pnc_followup_none
		serializePncFollowupWithin24hr(model, object, context); // pnc_followup_within24hr
		serializePncFollowupAt3rdDay(model, object, context); // pnc_followup_at3rd_day
		serializePncFollowupAt7thDay(model, object, context); // pnc_followup_at7th_day
		serializePncFollowupAt28thDay(model, object, context); // pnc_followup_at28th_day
		serializePncFollowupAt45thDay(model, object, context); // pnc_followup_at45th_day
		serializePncFollowupOthers(model, object, context); // pnc_followup_others
		serializePncFollowupSpecify(model, object, context); // pnc_followup_specify
		serializeFoodAvailableAfterDelivery(model, object, context); // food_available_after_delivery
		serializeAssessmentFoodQuality(model, object, context); // assessment_food_quality
		serializeAssessmentFoodFrequency(model, object, context); // assessment_food_frequency
		serializeAssessmentFoodDistribution(model, object, context); // assessment_food_distribution
		serializePrelactatingFeeding(model, object, context); // prelactating_feeding
		serializeDietaryRestrictions(model, object, context); // dietary_restrictions

		// Family Planning
		serializeHadCounselingFamilyPlanning(model, object, context); // had_counseling_family_planning
		serializeUsedFamilyPlanningMethods(model, object, context); // used_family_planning_methods
		serializePermanentVasectomy(model, object, context); // permanent_vasectomy
		serializePermanentMinilap(model, object, context); // permanent_minilap
		serializeTemporaryNatural(model, object, context); // temporary_natural
		serializeTemporaryCondom(model, object, context); // temporary_condom
		serializeTemporaryDepo(model, object, context); // temporary_depo
		serializeTemporaryPills(model, object, context); // temporary_pills
		serializeTemporaryIucd(model, object, context); // temporary_iucd
		serializeTemporaryImplant(model, object, context); // temporary_implant
		serializeSourceMaternityInfo(model, object, context); // source_maternity_info

		// Child Basic Information
		serializeChildUniqueId(model, object, context); // child_unique_id
		serializeChildName(model, object, context); // child_name
		serializeChildSex(model, object, context); // child_sex
		serializeDateOfBirthInAd(model, object, context); // date_of_birth_in_ad
		serializeDateOfBirthInBs(model, object, context); // date_of_birth_in_bs
		serializeWeightAtBirth(model, object, context); // weight_at_birth
		serializeWeightCurrent(model, object, context); // weight_current
		serializeHeightCm(model, object, context); // height_cm
		serializeMidArmCircumference(model, object, context); // mid_arm_circumference
		serializeHasCongentialAnomaly(model, object, context); // has_congential_anomaly
		serializeHealthCondition(model, object, context); // health_condition
		serializeHadVitKInjection(model, object, context); // had_vit_k_injection

		// Child Breast Feeding
		serializeWhenStartBreastFeed(model, object, context); // when_start_breast_feed
		serializeHowOftenBreastFeed(model, object, context); // how_often_breast_feed
		serializeDidExclusiveBreastFeed(model, object, context); // did_exclusive_breast_feed
		serializeStoppedBreastFeedingMonths(model, object, context); // stopped_breast_feeding_months
		serializeReasonDiscontinueBreastFeed(model, object, context); // reason_discontinue_breast_feed
		serializeReasonDiscontinueBreastFeedSpecify(model, object, context); // reason_discontinue_breast_feed_specify
		serializeAwareBreastFeedPreventsIllness(model, object, context); // aware_breast_feed_prevents_illness
		serializeStartedSemiSolidFood(model, object, context); // started_semi_solid_food
		serializeFoodSupplementRicePudding(model, object, context); // food_supplement_rice_pudding
		serializeFoodSupplementJaulo(model, object, context); // food_supplement_jaulo
		serializeFoodSupplementLito(model, object, context); // food_supplement_lito
		serializeFoodSupplementCerelac(model, object, context); // food_supplement_cerelac
		serializeFoodSupplementOthers(model, object, context); // food_supplement_others
		serializeFoodSupplementSpecify(model, object, context); // food_supplement_specify
		serializeWhyNoSupplement(model, object, context); // why_no_supplement
		serializeWhyNoSupplementSpecify(model, object, context); // why_no_supplement_specify
		serializeDetailFoodToBaby(model, object, context); // detail_food_to_baby
		serializeWashHandsBreastfeed(model, object, context); // wash_hands_breastfeed
		serializeHasHandwashFacility(model, object, context); // has_handwash_facility
		serializeChildFoodQuality(model, object, context); // child_food_quality
		serializeChildFoodFrequency(model, object, context); // child_food_frequency
		serializeChildFoodDistribution(model, object, context); // child_food_distribution

		// Accident and Injury
		serializeInjuryChildFacedFalls(model, object, context); // injury_child_faced_falls
		serializeInjuryChildFacedDrowning(model, object, context); // injury_child_faced_drowning
		serializeInjuryChildFacedBurnsscald(model, object, context); // injury_child_faced_burnsscald
		serializeInjuryChildFacedPoisoning(model, object, context); // injury_child_faced_poisoning
		serializeInjuryChildFacedSuffocationchokingaspiration(model, object, context); // injury_child_faced_suffocationchokingaspiration
		serializeInjuryChildFacedCutInjury(model, object, context); // injury_child_faced_cut_injury
		serializeInjuryChildFacedOthers(model, object, context); // injury_child_faced_others
		serializeInjuryChildFacedSpecify(model, object, context); // injury_child_faced_specify
		serializeInjuryCauseUnsafeHome(model, object, context); // injury_cause_unsafe_home
		serializeInjuryCauseNoSuperVision(model, object, context); // injury_cause_no_super_vision
		serializeInjuryCauseBusyMother(model, object, context); // injury_cause_busy_mother
		serializeInjuryCauseSlipperyFloor(model, object, context); // injury_cause_slippery_floor
		serializeInjuryCauseOthers(model, object, context); // injury_cause_others
		serializeInjuryCauseSpecify(model, object, context); // injury_cause_specify

		// Immunization
		serializeAgeBcgAtBirth(model, object, context); // age_bcg_at_birth
		serializeRemarksBcg(model, object, context); // remarks_bcg
		serializeAgeDptHepbHib6Weeks(model, object, context); // age_dpt_hepb_hib6_weeks
		serializeAgeDptHepbHib10Weeks(model, object, context); // age_dpt_hepb_hib10_weeks
		serializeAgeDptHepbHib14Weeks(model, object, context); // age_dpt_hepb_hib14_weeks
		serializeRemarksDptHepbHib(model, object, context); // remarks_dpt_hepb_hib
		serializeAgeOpv6Weeks(model, object, context); // age_opv6_weeks
		serializeAgeOpv10Weeks(model, object, context); // age_opv10_weeks
		serializeAgeOpv14Weeks(model, object, context); // age_opv14_weeks
		serializeRemarksOpv(model, object, context); // remarks_opv
		serializeAgePcv6Weeks(model, object, context); // age_pcv6_weeks
		serializeAgePcv10Weeks(model, object, context); // age_pcv10_weeks
		serializeAgePcv9Months(model, object, context); // age_pcv9_months
		serializeRemarksPcv(model, object, context); // remarks_pcv
		serializeAgeIpv6Weeks(model, object, context); // age_ipv6_weeks
		serializeAgeIpv14Weeks(model, object, context); // age_ipv14_weeks
		serializeRemarksIpv(model, object, context); // remarks_ipv
		serializeAgeMeaslesRubella9Months(model, object, context); // age_measles_rubella9_months
		serializeAgeMeaslesRubella15Months(model, object, context); // age_measles_rubella15_months
		serializeRemarksMeaslesRubella(model, object, context); // remarks_measles_rubella
		serializeAgeJapaneseEncephalitis12Months(model, object, context); // age_japanese_encephalitis12_months
		serializeRemarksJapaneseEncephalitis(model, object, context); // remarks_japanese_encephalitis

		// Advices
		serializeAdviceBreastFeeding(model, object, context); // advice_breast_feeding
		serializeAdviceDentalHygiene(model, object, context); // advice_dental_hygiene
		serializeAdviceToiletTraining(model, object, context); // advice_toilet_training
		serializeAdviceComplementaryFeeding(model, object, context); // advice_complementary_feeding
		serializeAdviceAccidentPrevention(model, object, context); // advice_accident_prevention
		serializeMdPhysical(model, object, context); // md_physical
		serializeMdVerbal(model, object, context); // md_verbal
		serializeMdSocial(model, object, context); // md_social
		serializeMdSpiritualReligious(model, object, context); // md_spiritual_religious
		serializeMdMotor(model, object, context); // md_motor
		serializeMdIntellectual(model, object, context); // md_intellectual
		serializeMdEmotional(model, object, context); // md_emotional
 
       
        return object;
    }
    

    public void serializeShineId(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("shine_id", context.serialize(model.getShineId()));
    }

    public void serializeAgeOfMenarche(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_of_menarche", context.serialize(model.getAgeOfMenarche()));
    }

    public void serializeWhereFirstMenarche(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_first_menarche", context.serialize(model.getWhereFirstMenarche()));
    }

    public void serializeWhereFirstMenarcheSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_first_menarche_specify", context.serialize(model.getWhereFirstMenarcheSpecify()));
    }

    public void serializeSanitaryMaterialsUsedPads(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_pads", context.serialize(model.getSanitaryMaterialsUsedPads()));
    }

    public void serializeSanitaryMaterialsUsedTampon(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_tampon", context.serialize(model.getSanitaryMaterialsUsedTampon()));
    }

    public void serializeSanitaryMaterialsUsedClothes(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_clothes", context.serialize(model.getSanitaryMaterialsUsedClothes()));
    }

    public void serializeSanitaryMaterialsUsedHp(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_hp", context.serialize(model.getSanitaryMaterialsUsedHp()));
    }

    public void serializeSanitaryMaterialsUsedMhp(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_mhp", context.serialize(model.getSanitaryMaterialsUsedMhp()));
    }

    public void serializeSanitaryMaterialsUsedNone(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("sanitary_materials_used_none", context.serialize(model.getSanitaryMaterialsUsedNone()));
    }

    public void serializeHowOftenUsed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("how_often_used", context.serialize(model.getHowOftenUsed()));
    }

    public void serializeHowOftenUsedSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("how_often_used_specify", context.serialize(model.getHowOftenUsedSpecify()));
    }

    public void serializeReasonNotTimely(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("reason_not_timely", context.serialize(model.getReasonNotTimely()));
    }

    public void serializeHowDisposeMaterials(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("how_dispose_materials", context.serialize(model.getHowDisposeMaterials()));
    }

    public void serializeHowDisposeMaterialsSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("how_dispose_materials_specify", context.serialize(model.getHowDisposeMaterialsSpecify()));
    }

    public void serializeObstetricHistoryGravida(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("obstetric_history_gravida", context.serialize(model.getObstetricHistoryGravida()));
    }

    public void serializeObstetricHistoryPara(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("obstetric_history_para", context.serialize(model.getObstetricHistoryPara()));
    }

    public void serializeObstetricHistoryAbortion(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("obstetric_history_abortion", context.serialize(model.getObstetricHistoryAbortion()));
    }

    public void serializeObstetricHistoryLiving(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("obstetric_history_living", context.serialize(model.getObstetricHistoryLiving()));
    }

    public void serializePeriodGestation(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("period_gestation", context.serialize(model.getPeriodGestation()));
    }

    public void serializeNumAntenatalVisits(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("num_antenatal_visits", context.serialize(model.getNumAntenatalVisits()));
    }

    public void serializeAntenalServiceAncCheckUp(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_anc_check_up", context.serialize(model.getAntenalServiceAncCheckUp()));
    }

    public void serializeAntenalServiceAlbendazole(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_albendazole", context.serialize(model.getAntenalServiceAlbendazole()));
    }

    public void serializeAntenalServiceTdImmunization(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_td_immunization", context.serialize(model.getAntenalServiceTdImmunization()));
    }

    public void serializeAntenalServiceIrontabs(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_irontabs", context.serialize(model.getAntenalServiceIrontabs()));
    }

    public void serializeAntenalServiceCouseling(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_couseling", context.serialize(model.getAntenalServiceCouseling()));
    }

    public void serializeAntenalServicePmtct(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_pmtct", context.serialize(model.getAntenalServicePmtct()));
    }

    public void serializeAntenalServiceOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_others", context.serialize(model.getAntenalServiceOthers()));
    }

    public void serializeAntenalServiceSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("antenal_service_specify", context.serialize(model.getAntenalServiceSpecify()));
    }

    public void serializeHealthSeekingBehaviour(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_seeking_behaviour", context.serialize(model.getHealthSeekingBehaviour()));
    }

    public void serializeHealthSeekingBehaviourSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_seeking_behaviour_specify", context.serialize(model.getHealthSeekingBehaviourSpecify()));
    }

    public void serializeMedicines(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("medicines", context.serialize(model.getMedicines()));
    }

    public void serializeYoungestBabyMonths(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("youngest_baby_months", context.serialize(model.getYoungestBabyMonths()));
    }

    public void serializeTypeOfDelivery(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_delivery", context.serialize(model.getTypeOfDelivery()));
    }

    public void serializePlaceDeliveryYoungest(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("place_delivery_youngest", context.serialize(model.getPlaceDeliveryYoungest()));
    }

    public void serializePlaceDeliveryYoungestSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("place_delivery_youngest_specify", context.serialize(model.getPlaceDeliveryYoungestSpecify()));
    }

    public void serializeDeliveryComplications(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("delivery_complications", context.serialize(model.getDeliveryComplications()));
    }

    public void serializeDeliveryComplicationsSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("delivery_complications_specify", context.serialize(model.getDeliveryComplicationsSpecify()));
    }

    public void serializeGapBetweenKids(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("gap_between_kids", context.serialize(model.getGapBetweenKids()));
    }

    public void serializePostNatalPe(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_pe", context.serialize(model.getPostNatalPe()));
    }

    public void serializePostNatalCounselBF(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_counsel_b_f", context.serialize(model.getPostNatalCounselBF()));
    }

    public void serializePostNatalCounselFP(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_counsel_f_p", context.serialize(model.getPostNatalCounselFP()));
    }

    public void serializePostNatalInvestigations(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_investigations", context.serialize(model.getPostNatalInvestigations()));
    }

    public void serializePostNatalIronTablets(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_iron_tablets", context.serialize(model.getPostNatalIronTablets()));
    }

    public void serializePostNatalVitaminA(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_vitamin_a", context.serialize(model.getPostNatalVitaminA()));
    }

    public void serializePostNatalOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_others", context.serialize(model.getPostNatalOthers()));
    }

    public void serializePostNatalSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("post_natal_specify", context.serialize(model.getPostNatalSpecify()));
    }

    public void serializePncFollowupNone(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_none", context.serialize(model.getPncFollowupNone()));
    }

    public void serializePncFollowupWithin24hr(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_within24hr", context.serialize(model.getPncFollowupWithin24hr()));
    }

    public void serializePncFollowupAt3rdDay(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_at3rd_day", context.serialize(model.getPncFollowupAt3rdDay()));
    }

    public void serializePncFollowupAt7thDay(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_at7th_day", context.serialize(model.getPncFollowupAt7thDay()));
    }

    public void serializePncFollowupAt28thDay(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_at28th_day", context.serialize(model.getPncFollowupAt28thDay()));
    }

    public void serializePncFollowupAt45thDay(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_at45th_day", context.serialize(model.getPncFollowupAt45thDay()));
    }

    public void serializePncFollowupOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_others", context.serialize(model.getPncFollowupOthers()));
    }

    public void serializePncFollowupSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pnc_followup_specify", context.serialize(model.getPncFollowupSpecify()));
    }

    public void serializeFoodAvailableAfterDelivery(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_available_after_delivery", context.serialize(model.getFoodAvailableAfterDelivery()));
    }

    public void serializeAssessmentFoodQuality(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("assessment_food_quality", context.serialize(model.getAssessmentFoodQuality()));
    }

    public void serializeAssessmentFoodFrequency(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("assessment_food_frequency", context.serialize(model.getAssessmentFoodFrequency()));
    }

    public void serializeAssessmentFoodDistribution(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("assessment_food_distribution", context.serialize(model.getAssessmentFoodDistribution()));
    }

    public void serializePrelactatingFeeding(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("prelactating_feeding", context.serialize(model.getPrelactatingFeeding()));
    }

    public void serializeDietaryRestrictions(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("dietary_restrictions", context.serialize(model.getDietaryRestrictions()));
    }

    public void serializeHadCounselingFamilyPlanning(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("had_counseling_family_planning", context.serialize(model.getHadCounselingFamilyPlanning()));
    }

    public void serializeUsedFamilyPlanningMethods(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("used_family_planning_methods", context.serialize(model.getUsedFamilyPlanningMethods()));
    }

    public void serializePermanentVasectomy(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("permanent_vasectomy", context.serialize(model.getPermanentVasectomy()));
    }

    public void serializePermanentMinilap(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("permanent_minilap", context.serialize(model.getPermanentMinilap()));
    }

    public void serializeTemporaryNatural(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_natural", context.serialize(model.getTemporaryNatural()));
    }

    public void serializeTemporaryCondom(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_condom", context.serialize(model.getTemporaryCondom()));
    }

    public void serializeTemporaryDepo(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_depo", context.serialize(model.getTemporaryDepo()));
    }

    public void serializeTemporaryPills(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_pills", context.serialize(model.getTemporaryPills()));
    }

    public void serializeTemporaryIucd(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_iucd", context.serialize(model.getTemporaryIucd()));
    }

    public void serializeTemporaryImplant(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("temporary_implant", context.serialize(model.getTemporaryImplant()));
    }

    public void serializeSourceMaternityInfo(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_maternity_info", context.serialize(model.getSourceMaternityInfo()));
    }

    public void serializeChildUniqueId(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_unique_id", context.serialize(model.getChildUniqueId()));
    }

    public void serializeChildName(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_name", context.serialize(model.getChildName()));
    }

    public void serializeChildSex(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_sex", context.serialize(model.getChildSex()));
    }

    public void serializeDateOfBirthInAd(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("date_of_birth_in_ad", context.serialize(model.getDateOfBirthInAd()));
    }

    public void serializeDateOfBirthInBs(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("date_of_birth_in_bs", context.serialize(model.getDateOfBirthInBs()));
    }

    public void serializeWeightAtBirth(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("weight_at_birth", context.serialize(model.getWeightAtBirth()));
    }

    public void serializeWeightCurrent(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("weight_current", context.serialize(model.getWeightCurrent()));
    }

    public void serializeHeightCm(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("height_cm", context.serialize(model.getHeightCm()));
    }

    public void serializeMidArmCircumference(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mid_arm_circumference", context.serialize(model.getMidArmCircumference()));
    }

    public void serializeHasCongentialAnomaly(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("has_congential_anomaly", context.serialize(model.getHasCongentialAnomaly()));
    }

    public void serializeHealthCondition(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_condition", context.serialize(model.getHealthCondition()));
    }

    public void serializeHadVitKInjection(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("had_vit_k_injection", context.serialize(model.getHadVitKInjection()));
    }

    public void serializeWhenStartBreastFeed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_start_breast_feed", context.serialize(model.getWhenStartBreastFeed()));
    }

    public void serializeHowOftenBreastFeed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("how_often_breast_feed", context.serialize(model.getHowOftenBreastFeed()));
    }

    public void serializeDidExclusiveBreastFeed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("did_exclusive_breast_feed", context.serialize(model.getDidExclusiveBreastFeed()));
    }

    public void serializeStoppedBreastFeedingMonths(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("stopped_breast_feeding_months", context.serialize(model.getStoppedBreastFeedingMonths()));
    }

    public void serializeReasonDiscontinueBreastFeed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("reason_discontinue_breast_feed", context.serialize(model.getReasonDiscontinueBreastFeed()));
    }

    public void serializeReasonDiscontinueBreastFeedSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("reason_discontinue_breast_feed_specify", context.serialize(model.getReasonDiscontinueBreastFeedSpecify()));
    }

    public void serializeAwareBreastFeedPreventsIllness(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("aware_breast_feed_prevents_illness", context.serialize(model.getAwareBreastFeedPreventsIllness()));
    }

    public void serializeStartedSemiSolidFood(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("started_semi_solid_food", context.serialize(model.getStartedSemiSolidFood()));
    }

    public void serializeFoodSupplementRicePudding(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_rice_pudding", context.serialize(model.getFoodSupplementRicePudding()));
    }

    public void serializeFoodSupplementJaulo(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_jaulo", context.serialize(model.getFoodSupplementJaulo()));
    }

    public void serializeFoodSupplementLito(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_lito", context.serialize(model.getFoodSupplementLito()));
    }

    public void serializeFoodSupplementCerelac(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_cerelac", context.serialize(model.getFoodSupplementCerelac()));
    }

    public void serializeFoodSupplementOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_others", context.serialize(model.getFoodSupplementOthers()));
    }

    public void serializeFoodSupplementSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_supplement_specify", context.serialize(model.getFoodSupplementSpecify()));
    }

    public void serializeWhyNoSupplement(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("why_no_supplement", context.serialize(model.getWhyNoSupplement()));
    }

    public void serializeWhyNoSupplementSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("why_no_supplement_specify", context.serialize(model.getWhyNoSupplementSpecify()));
    }

    public void serializeDetailFoodToBaby(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("detail_food_to_baby", context.serialize(model.getDetailFoodToBaby()));
    }

    public void serializeWashHandsBreastfeed(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("wash_hands_breastfeed", context.serialize(model.getWashHandsBreastfeed()));
    }

    public void serializeHasHandwashFacility(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("has_handwash_facility", context.serialize(model.getHasHandwashFacility()));
    }

    public void serializeChildFoodQuality(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_food_quality", context.serialize(model.getChildFoodQuality()));
    }

    public void serializeChildFoodFrequency(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_food_frequency", context.serialize(model.getChildFoodFrequency()));
    }

    public void serializeChildFoodDistribution(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("child_food_distribution", context.serialize(model.getChildFoodDistribution()));
    }

    public void serializeInjuryChildFacedFalls(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_falls", context.serialize(model.getInjuryChildFacedFalls()));
    }

    public void serializeInjuryChildFacedDrowning(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_drowning", context.serialize(model.getInjuryChildFacedDrowning()));
    }

    public void serializeInjuryChildFacedBurnsscald(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_burnsscald", context.serialize(model.getInjuryChildFacedBurnsscald()));
    }

    public void serializeInjuryChildFacedPoisoning(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_poisoning", context.serialize(model.getInjuryChildFacedPoisoning()));
    }

    public void serializeInjuryChildFacedSuffocationchokingaspiration(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_suffocationchokingaspiration", context.serialize(model.getInjuryChildFacedSuffocationchokingaspiration()));
    }

    public void serializeInjuryChildFacedCutInjury(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_cut_injury", context.serialize(model.getInjuryChildFacedCutInjury()));
    }

    public void serializeInjuryChildFacedOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_others", context.serialize(model.getInjuryChildFacedOthers()));
    }

    public void serializeInjuryChildFacedSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_child_faced_specify", context.serialize(model.getInjuryChildFacedSpecify()));
    }

    public void serializeInjuryCauseUnsafeHome(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_unsafe_home", context.serialize(model.getInjuryCauseUnsafeHome()));
    }

    public void serializeInjuryCauseNoSuperVision(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_no_super_vision", context.serialize(model.getInjuryCauseNoSuperVision()));
    }

    public void serializeInjuryCauseBusyMother(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_busy_mother", context.serialize(model.getInjuryCauseBusyMother()));
    }

    public void serializeInjuryCauseSlipperyFloor(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_slippery_floor", context.serialize(model.getInjuryCauseSlipperyFloor()));
    }

    public void serializeInjuryCauseOthers(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_others", context.serialize(model.getInjuryCauseOthers()));
    }

    public void serializeInjuryCauseSpecify(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("injury_cause_specify", context.serialize(model.getInjuryCauseSpecify()));
    }

    public void serializeAgeBcgAtBirth(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_bcg_at_birth", context.serialize(model.getAgeBcgAtBirth()));
    }

    public void serializeRemarksBcg(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_bcg", context.serialize(model.getRemarksBcg()));
    }

    public void serializeAgeDptHepbHib6Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_dpt_hepb_hib6_weeks", context.serialize(model.getAgeDptHepbHib6Weeks()));
    }

    public void serializeAgeDptHepbHib10Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_dpt_hepb_hib10_weeks", context.serialize(model.getAgeDptHepbHib10Weeks()));
    }

    public void serializeAgeDptHepbHib14Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_dpt_hepb_hib14_weeks", context.serialize(model.getAgeDptHepbHib14Weeks()));
    }

    public void serializeRemarksDptHepbHib(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_dpt_hepb_hib", context.serialize(model.getRemarksDptHepbHib()));
    }

    public void serializeAgeOpv6Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_opv6_weeks", context.serialize(model.getAgeOpv6Weeks()));
    }

    public void serializeAgeOpv10Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_opv10_weeks", context.serialize(model.getAgeOpv10Weeks()));
    }

    public void serializeAgeOpv14Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_opv14_weeks", context.serialize(model.getAgeOpv14Weeks()));
    }

    public void serializeRemarksOpv(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_opv", context.serialize(model.getRemarksOpv()));
    }

    public void serializeAgePcv6Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_pcv6_weeks", context.serialize(model.getAgePcv6Weeks()));
    }

    public void serializeAgePcv10Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_pcv10_weeks", context.serialize(model.getAgePcv10Weeks()));
    }

    public void serializeAgePcv9Months(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_pcv9_months", context.serialize(model.getAgePcv9Months()));
    }

    public void serializeRemarksPcv(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_pcv", context.serialize(model.getRemarksPcv()));
    }

    public void serializeAgeIpv6Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_ipv6_weeks", context.serialize(model.getAgeIpv6Weeks()));
    }

    public void serializeAgeIpv14Weeks(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_ipv14_weeks", context.serialize(model.getAgeIpv14Weeks()));
    }

    public void serializeRemarksIpv(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_ipv", context.serialize(model.getRemarksIpv()));
    }

    public void serializeAgeMeaslesRubella9Months(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_measles_rubella9_months", context.serialize(model.getAgeMeaslesRubella9Months()));
    }

    public void serializeAgeMeaslesRubella15Months(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_measles_rubella15_months", context.serialize(model.getAgeMeaslesRubella15Months()));
    }

    public void serializeRemarksMeaslesRubella(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_measles_rubella", context.serialize(model.getRemarksMeaslesRubella()));
    }

    public void serializeAgeJapaneseEncephalitis12Months(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_japanese_encephalitis12_months", context.serialize(model.getAgeJapaneseEncephalitis12Months()));
    }

    public void serializeRemarksJapaneseEncephalitis(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("remarks_japanese_encephalitis", context.serialize(model.getRemarksJapaneseEncephalitis()));
    }

    public void serializeAdviceBreastFeeding(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("advice_breast_feeding", context.serialize(model.getAdviceBreastFeeding()));
    }

    public void serializeAdviceDentalHygiene(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("advice_dental_hygiene", context.serialize(model.getAdviceDentalHygiene()));
    }

    public void serializeAdviceToiletTraining(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("advice_toilet_training", context.serialize(model.getAdviceToiletTraining()));
    }

    public void serializeAdviceComplementaryFeeding(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("advice_complementary_feeding", context.serialize(model.getAdviceComplementaryFeeding()));
    }

    public void serializeAdviceAccidentPrevention(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("advice_accident_prevention", context.serialize(model.getAdviceAccidentPrevention()));
    }

    public void serializeMdPhysical(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_physical", context.serialize(model.getMdPhysical()));
    }

    public void serializeMdVerbal(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_verbal", context.serialize(model.getMdVerbal()));
    }

    public void serializeMdSocial(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_social", context.serialize(model.getMdSocial()));
    }

    public void serializeMdSpiritualReligious(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_spiritual_religious", context.serialize(model.getMdSpiritualReligious()));
    }

    public void serializeMdMotor(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_motor", context.serialize(model.getMdMotor()));
    }

    public void serializeMdIntellectual(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_intellectual", context.serialize(model.getMdIntellectual()));
    }

    public void serializeMdEmotional(MotherAndChild model, JsonObject object, JsonSerializationContext context)
    {
        object.add("md_emotional", context.serialize(model.getMdEmotional()));
    }
 
    

}