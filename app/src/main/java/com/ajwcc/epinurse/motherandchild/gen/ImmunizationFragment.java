package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_immunization)
public class ImmunizationFragment extends BaseEpinurseFragment {


    public ImmunizationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected CheckBox ageBcgAtBirth;

	@ViewById
	@MapToModelField
	protected EditText remarksBcg;

	@ViewById
	@MapToModelField
	protected CheckBox ageDptHepbHib6Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox ageDptHepbHib10Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox ageDptHepbHib14Weeks;

	@ViewById
	@MapToModelField
	protected EditText remarksDptHepbHib;

	@ViewById
	@MapToModelField
	protected CheckBox ageOpv6Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox ageOpv10Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox ageOpv14Weeks;

	@ViewById
	@MapToModelField
	protected EditText remarksOpv;

	@ViewById
	@MapToModelField
	protected CheckBox agePcv6Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox agePcv10Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox agePcv9Months;

	@ViewById
	@MapToModelField
	protected EditText remarksPcv;

	@ViewById
	@MapToModelField
	protected CheckBox ageIpv6Weeks;

	@ViewById
	@MapToModelField
	protected CheckBox ageIpv14Weeks;

	@ViewById
	@MapToModelField
	protected EditText remarksIpv;

	@ViewById
	@MapToModelField
	protected CheckBox ageMeaslesRubella9Months;

	@ViewById
	@MapToModelField
	protected CheckBox ageMeaslesRubella15Months;

	@ViewById
	@MapToModelField
	protected EditText remarksMeaslesRubella;

	@ViewById
	@MapToModelField
	protected CheckBox ageJapaneseEncephalitis12Months;

	@ViewById
	@MapToModelField
	protected EditText remarksJapaneseEncephalitis;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(ageBcgAtBirth,pojo.getAgeBcgAtBirth());
					PojoToViewMapper.setViewValue(remarksBcg,pojo.getRemarksBcg());
					PojoToViewMapper.setViewValue(ageDptHepbHib6Weeks,pojo.getAgeDptHepbHib6Weeks());
					PojoToViewMapper.setViewValue(ageDptHepbHib10Weeks,pojo.getAgeDptHepbHib10Weeks());
					PojoToViewMapper.setViewValue(ageDptHepbHib14Weeks,pojo.getAgeDptHepbHib14Weeks());
					PojoToViewMapper.setViewValue(remarksDptHepbHib,pojo.getRemarksDptHepbHib());
					PojoToViewMapper.setViewValue(ageOpv6Weeks,pojo.getAgeOpv6Weeks());
					PojoToViewMapper.setViewValue(ageOpv10Weeks,pojo.getAgeOpv10Weeks());
					PojoToViewMapper.setViewValue(ageOpv14Weeks,pojo.getAgeOpv14Weeks());
					PojoToViewMapper.setViewValue(remarksOpv,pojo.getRemarksOpv());
					PojoToViewMapper.setViewValue(agePcv6Weeks,pojo.getAgePcv6Weeks());
					PojoToViewMapper.setViewValue(agePcv10Weeks,pojo.getAgePcv10Weeks());
					PojoToViewMapper.setViewValue(agePcv9Months,pojo.getAgePcv9Months());
					PojoToViewMapper.setViewValue(remarksPcv,pojo.getRemarksPcv());
					PojoToViewMapper.setViewValue(ageIpv6Weeks,pojo.getAgeIpv6Weeks());
					PojoToViewMapper.setViewValue(ageIpv14Weeks,pojo.getAgeIpv14Weeks());
					PojoToViewMapper.setViewValue(remarksIpv,pojo.getRemarksIpv());
					PojoToViewMapper.setViewValue(ageMeaslesRubella9Months,pojo.getAgeMeaslesRubella9Months());
					PojoToViewMapper.setViewValue(ageMeaslesRubella15Months,pojo.getAgeMeaslesRubella15Months());
					PojoToViewMapper.setViewValue(remarksMeaslesRubella,pojo.getRemarksMeaslesRubella());
					PojoToViewMapper.setViewValue(ageJapaneseEncephalitis12Months,pojo.getAgeJapaneseEncephalitis12Months());
					PojoToViewMapper.setViewValue(remarksJapaneseEncephalitis,pojo.getRemarksJapaneseEncephalitis());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setAgeBcgAtBirth((Integer) ViewToPojoMapper.getValueByView(ageBcgAtBirth));
					pojo.setRemarksBcg((String) ViewToPojoMapper.getValueByView(remarksBcg));
					pojo.setAgeDptHepbHib6Weeks((Integer) ViewToPojoMapper.getValueByView(ageDptHepbHib6Weeks));
					pojo.setAgeDptHepbHib10Weeks((Integer) ViewToPojoMapper.getValueByView(ageDptHepbHib10Weeks));
					pojo.setAgeDptHepbHib14Weeks((Integer) ViewToPojoMapper.getValueByView(ageDptHepbHib14Weeks));
					pojo.setRemarksDptHepbHib((String) ViewToPojoMapper.getValueByView(remarksDptHepbHib));
					pojo.setAgeOpv6Weeks((Integer) ViewToPojoMapper.getValueByView(ageOpv6Weeks));
					pojo.setAgeOpv10Weeks((Integer) ViewToPojoMapper.getValueByView(ageOpv10Weeks));
					pojo.setAgeOpv14Weeks((Integer) ViewToPojoMapper.getValueByView(ageOpv14Weeks));
					pojo.setRemarksOpv((String) ViewToPojoMapper.getValueByView(remarksOpv));
					pojo.setAgePcv6Weeks((Integer) ViewToPojoMapper.getValueByView(agePcv6Weeks));
					pojo.setAgePcv10Weeks((Integer) ViewToPojoMapper.getValueByView(agePcv10Weeks));
					pojo.setAgePcv9Months((Integer) ViewToPojoMapper.getValueByView(agePcv9Months));
					pojo.setRemarksPcv((String) ViewToPojoMapper.getValueByView(remarksPcv));
					pojo.setAgeIpv6Weeks((Integer) ViewToPojoMapper.getValueByView(ageIpv6Weeks));
					pojo.setAgeIpv14Weeks((Integer) ViewToPojoMapper.getValueByView(ageIpv14Weeks));
					pojo.setRemarksIpv((String) ViewToPojoMapper.getValueByView(remarksIpv));
					pojo.setAgeMeaslesRubella9Months((Integer) ViewToPojoMapper.getValueByView(ageMeaslesRubella9Months));
					pojo.setAgeMeaslesRubella15Months((Integer) ViewToPojoMapper.getValueByView(ageMeaslesRubella15Months));
					pojo.setRemarksMeaslesRubella((String) ViewToPojoMapper.getValueByView(remarksMeaslesRubella));
					pojo.setAgeJapaneseEncephalitis12Months((Integer) ViewToPojoMapper.getValueByView(ageJapaneseEncephalitis12Months));
					pojo.setRemarksJapaneseEncephalitis((String) ViewToPojoMapper.getValueByView(remarksJapaneseEncephalitis));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
