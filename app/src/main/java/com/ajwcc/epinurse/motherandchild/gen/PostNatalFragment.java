package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_post_natal)
public class PostNatalFragment extends BaseEpinurseFragment {


    public PostNatalFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected CheckBox postNatalPe;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalCounselBF;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalCounselFP;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalInvestigations;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalIronTablets;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalVitaminA;

	@ViewById
	@MapToModelField
	protected CheckBox postNatalOthers;

	@ViewById
	@MapToModelField
	protected EditText postNatalSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupNone;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupWithin24hr;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupAt3rdDay;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupAt7thDay;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupAt28thDay;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupAt45thDay;

	@ViewById
	@MapToModelField
	protected CheckBox pncFollowupOthers;

	@ViewById
	@MapToModelField
	protected EditText pncFollowupSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup foodAvailableAfterDelivery;

	@ViewById
	@MapToModelField
	protected EditText assessmentFoodQuality;

	@ViewById
	@MapToModelField
	protected EditText assessmentFoodFrequency;

	@ViewById
	@MapToModelField
	protected EditText assessmentFoodDistribution;

	@ViewById
	@MapToModelField
	protected RadioGroup prelactatingFeeding;

	@ViewById
	@MapToModelField
	protected RadioGroup dietaryRestrictions;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(postNatalPe,pojo.getPostNatalPe());
					PojoToViewMapper.setViewValue(postNatalCounselBF,pojo.getPostNatalCounselBF());
					PojoToViewMapper.setViewValue(postNatalCounselFP,pojo.getPostNatalCounselFP());
					PojoToViewMapper.setViewValue(postNatalInvestigations,pojo.getPostNatalInvestigations());
					PojoToViewMapper.setViewValue(postNatalIronTablets,pojo.getPostNatalIronTablets());
					PojoToViewMapper.setViewValue(postNatalVitaminA,pojo.getPostNatalVitaminA());
					PojoToViewMapper.setViewValue(postNatalOthers,pojo.getPostNatalOthers());
					PojoToViewMapper.setViewValue(postNatalSpecify,pojo.getPostNatalSpecify());
					PojoToViewMapper.setViewValue(pncFollowupNone,pojo.getPncFollowupNone());
					PojoToViewMapper.setViewValue(pncFollowupWithin24hr,pojo.getPncFollowupWithin24hr());
					PojoToViewMapper.setViewValue(pncFollowupAt3rdDay,pojo.getPncFollowupAt3rdDay());
					PojoToViewMapper.setViewValue(pncFollowupAt7thDay,pojo.getPncFollowupAt7thDay());
					PojoToViewMapper.setViewValue(pncFollowupAt28thDay,pojo.getPncFollowupAt28thDay());
					PojoToViewMapper.setViewValue(pncFollowupAt45thDay,pojo.getPncFollowupAt45thDay());
					PojoToViewMapper.setViewValue(pncFollowupOthers,pojo.getPncFollowupOthers());
					PojoToViewMapper.setViewValue(pncFollowupSpecify,pojo.getPncFollowupSpecify());
					PojoToViewMapper.setViewValue(foodAvailableAfterDelivery,pojo.getFoodAvailableAfterDelivery());
					PojoToViewMapper.setViewValue(assessmentFoodQuality,pojo.getAssessmentFoodQuality());
					PojoToViewMapper.setViewValue(assessmentFoodFrequency,pojo.getAssessmentFoodFrequency());
					PojoToViewMapper.setViewValue(assessmentFoodDistribution,pojo.getAssessmentFoodDistribution());
					PojoToViewMapper.setViewValue(prelactatingFeeding,pojo.getPrelactatingFeeding());
					PojoToViewMapper.setViewValue(dietaryRestrictions,pojo.getDietaryRestrictions());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setPostNatalPe((Integer) ViewToPojoMapper.getValueByView(postNatalPe));
					pojo.setPostNatalCounselBF((Integer) ViewToPojoMapper.getValueByView(postNatalCounselBF));
					pojo.setPostNatalCounselFP((Integer) ViewToPojoMapper.getValueByView(postNatalCounselFP));
					pojo.setPostNatalInvestigations((Integer) ViewToPojoMapper.getValueByView(postNatalInvestigations));
					pojo.setPostNatalIronTablets((Integer) ViewToPojoMapper.getValueByView(postNatalIronTablets));
					pojo.setPostNatalVitaminA((Integer) ViewToPojoMapper.getValueByView(postNatalVitaminA));
					pojo.setPostNatalOthers((Integer) ViewToPojoMapper.getValueByView(postNatalOthers));
					pojo.setPostNatalSpecify((String) ViewToPojoMapper.getValueByView(postNatalSpecify));
					pojo.setPncFollowupNone((Integer) ViewToPojoMapper.getValueByView(pncFollowupNone));
					pojo.setPncFollowupWithin24hr((Integer) ViewToPojoMapper.getValueByView(pncFollowupWithin24hr));
					pojo.setPncFollowupAt3rdDay((Integer) ViewToPojoMapper.getValueByView(pncFollowupAt3rdDay));
					pojo.setPncFollowupAt7thDay((Integer) ViewToPojoMapper.getValueByView(pncFollowupAt7thDay));
					pojo.setPncFollowupAt28thDay((Integer) ViewToPojoMapper.getValueByView(pncFollowupAt28thDay));
					pojo.setPncFollowupAt45thDay((Integer) ViewToPojoMapper.getValueByView(pncFollowupAt45thDay));
					pojo.setPncFollowupOthers((Integer) ViewToPojoMapper.getValueByView(pncFollowupOthers));
					pojo.setPncFollowupSpecify((String) ViewToPojoMapper.getValueByView(pncFollowupSpecify));
					pojo.setFoodAvailableAfterDelivery((Integer) ViewToPojoMapper.getValueByView(foodAvailableAfterDelivery));
					pojo.setAssessmentFoodQuality((String) ViewToPojoMapper.getValueByView(assessmentFoodQuality));
					pojo.setAssessmentFoodFrequency((String) ViewToPojoMapper.getValueByView(assessmentFoodFrequency));
					pojo.setAssessmentFoodDistribution((String) ViewToPojoMapper.getValueByView(assessmentFoodDistribution));
					pojo.setPrelactatingFeeding((Integer) ViewToPojoMapper.getValueByView(prelactatingFeeding));
					pojo.setDietaryRestrictions((Integer) ViewToPojoMapper.getValueByView(dietaryRestrictions));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.postNatalOthers,R.id.pncFollowupOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.postNatalOthers:
				 UiUtils.toggleSpecify(view, postNatalSpecify);
				break;
			case R.id.pncFollowupOthers:
				 UiUtils.toggleSpecify(view, pncFollowupSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.postNatalContainer).setVisibility(View.GONE);
				update = true;				
				model.setPostNatalPe(null);
				model.setPostNatalCounselBF(null);
				model.setPostNatalCounselFP(null);
				model.setPostNatalInvestigations(null);
				model.setPostNatalIronTablets(null);
				model.setPostNatalVitaminA(null);
				model.setPostNatalOthers(null);
				model.setPostNatalSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.postNatalContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.pncFollowupContainer).setVisibility(View.GONE);
				update = true;				
				model.setPncFollowupNone(null);
				model.setPncFollowupWithin24hr(null);
				model.setPncFollowupAt3rdDay(null);
				model.setPncFollowupAt7thDay(null);
				model.setPncFollowupAt28thDay(null);
				model.setPncFollowupAt45thDay(null);
				model.setPncFollowupOthers(null);
				model.setPncFollowupSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.pncFollowupContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.foodAvailableAfterDeliveryContainer).setVisibility(View.GONE);
				update = true;				
				model.setFoodAvailableAfterDelivery(null);

			}
			else
			{
				getView().findViewById(R.id.foodAvailableAfterDeliveryContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.assessmentFoodQualityContainer).setVisibility(View.GONE);
				update = true;				
				model.setAssessmentFoodQuality(null);

			}
			else
			{
				getView().findViewById(R.id.assessmentFoodQualityContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.assessmentFoodFrequencyContainer).setVisibility(View.GONE);
				update = true;				
				model.setAssessmentFoodFrequency(null);

			}
			else
			{
				getView().findViewById(R.id.assessmentFoodFrequencyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.assessmentFoodDistributionContainer).setVisibility(View.GONE);
				update = true;				
				model.setAssessmentFoodDistribution(null);

			}
			else
			{
				getView().findViewById(R.id.assessmentFoodDistributionContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.prelactatingFeedingContainer).setVisibility(View.GONE);
				update = true;				
				model.setPrelactatingFeeding(null);

			}
			else
			{
				getView().findViewById(R.id.prelactatingFeedingContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.dietaryRestrictionsContainer).setVisibility(View.GONE);
				update = true;				
				model.setDietaryRestrictions(null);

			}
			else
			{
				getView().findViewById(R.id.dietaryRestrictionsContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
