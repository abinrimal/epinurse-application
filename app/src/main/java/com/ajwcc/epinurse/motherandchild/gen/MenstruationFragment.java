package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_menstruation)
public class MenstruationFragment extends BaseEpinurseFragment {


    public MenstruationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText ageOfMenarche;

	@ViewById
	@MapToModelField
	protected RadioGroup whereFirstMenarche;

	@ViewById
	@MapToModelField
	protected EditText whereFirstMenarcheSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedPads;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedTampon;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedClothes;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedHp;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedMhp;

	@ViewById
	@MapToModelField
	protected CheckBox sanitaryMaterialsUsedNone;

	@ViewById
	@MapToModelField
	protected RadioGroup howOftenUsed;

	@ViewById
	@MapToModelField
	protected EditText howOftenUsedSpecify;

	@ViewById
	@MapToModelField
	protected EditText reasonNotTimely;

	@ViewById
	@MapToModelField
	protected RadioGroup howDisposeMaterials;

	@ViewById
	@MapToModelField
	protected EditText howDisposeMaterialsSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(ageOfMenarche,pojo.getAgeOfMenarche());
					PojoToViewMapper.setViewValue(whereFirstMenarche,pojo.getWhereFirstMenarche());
					PojoToViewMapper.setViewValue(whereFirstMenarcheSpecify,pojo.getWhereFirstMenarcheSpecify());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedPads,pojo.getSanitaryMaterialsUsedPads());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedTampon,pojo.getSanitaryMaterialsUsedTampon());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedClothes,pojo.getSanitaryMaterialsUsedClothes());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedHp,pojo.getSanitaryMaterialsUsedHp());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedMhp,pojo.getSanitaryMaterialsUsedMhp());
					PojoToViewMapper.setViewValue(sanitaryMaterialsUsedNone,pojo.getSanitaryMaterialsUsedNone());
					PojoToViewMapper.setViewValue(howOftenUsed,pojo.getHowOftenUsed());
					PojoToViewMapper.setViewValue(howOftenUsedSpecify,pojo.getHowOftenUsedSpecify());
					PojoToViewMapper.setViewValue(reasonNotTimely,pojo.getReasonNotTimely());
					PojoToViewMapper.setViewValue(howDisposeMaterials,pojo.getHowDisposeMaterials());
					PojoToViewMapper.setViewValue(howDisposeMaterialsSpecify,pojo.getHowDisposeMaterialsSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setAgeOfMenarche((Integer) ViewToPojoMapper.getValueByView(ageOfMenarche));
					pojo.setWhereFirstMenarche((Integer) ViewToPojoMapper.getValueByView(whereFirstMenarche));
					pojo.setWhereFirstMenarcheSpecify((String) ViewToPojoMapper.getValueByView(whereFirstMenarcheSpecify));
					pojo.setSanitaryMaterialsUsedPads((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedPads));
					pojo.setSanitaryMaterialsUsedTampon((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedTampon));
					pojo.setSanitaryMaterialsUsedClothes((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedClothes));
					pojo.setSanitaryMaterialsUsedHp((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedHp));
					pojo.setSanitaryMaterialsUsedMhp((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedMhp));
					pojo.setSanitaryMaterialsUsedNone((Integer) ViewToPojoMapper.getValueByView(sanitaryMaterialsUsedNone));
					pojo.setHowOftenUsed((Integer) ViewToPojoMapper.getValueByView(howOftenUsed));
					pojo.setHowOftenUsedSpecify((String) ViewToPojoMapper.getValueByView(howOftenUsedSpecify));
					pojo.setReasonNotTimely((String) ViewToPojoMapper.getValueByView(reasonNotTimely));
					pojo.setHowDisposeMaterials((Integer) ViewToPojoMapper.getValueByView(howDisposeMaterials));
					pojo.setHowDisposeMaterialsSpecify((String) ViewToPojoMapper.getValueByView(howDisposeMaterialsSpecify));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.whereFirstMenarche3,R.id.howOftenUsed4,R.id.howDisposeMaterials4})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.whereFirstMenarche3:
				 UiUtils.toggleSpecify(view, whereFirstMenarcheSpecify);
				break;
			case R.id.howOftenUsed4:
				 UiUtils.toggleSpecify(view, howOftenUsedSpecify);
				break;
			case R.id.howDisposeMaterials4:
				 UiUtils.toggleSpecify(view, howDisposeMaterialsSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getHowOftenUsed()==null || !(model.getHowOftenUsed()==2 || model.getHowOftenUsed()==3 || model.getHowOftenUsed()==4)))
			{
				getView().findViewById(R.id.reasonNotTimelyContainer).setVisibility(View.GONE);
				update = true;				
				model.setReasonNotTimely(null);

			}
			else
			{
				getView().findViewById(R.id.reasonNotTimelyContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.howOftenUsed0,R.id.howOftenUsed1,R.id.howOftenUsed2,R.id.howOftenUsed3,R.id.howOftenUsed4})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
