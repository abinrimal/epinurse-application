package com.ajwcc.epinurse.motherandchild.gen;

import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.epinurse.common.utils.network.ExcludeFromJson;

import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import com.zhuinden.realmautomigration.AutoMigration;

public class MotherAndChild extends RealmObject implements EpinurseModel
{
    @PrimaryKey
    private String uuid;
    
    public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public String getUuid()
	{
		return uuid;
	}

	@ExcludeFromJson
	private boolean editing;

	@Override
	public boolean isEditing() {
		return editing;
	}

	@Override
	public void setEditing(boolean editing) {
		this.editing = editing;
	}


	@ExcludeFromJson
	private String ownerUuid;

	@Override
	public String getOwnerUuid() {
		return ownerUuid;
	}

	@Override
	public void setOwnerUuid(String ownerUuid) {
		this.ownerUuid = ownerUuid;
	}


    
	@ExcludeFromJson
	private boolean synced;

	@Override
	public boolean isSynced() {
		return synced;
	}

	@Override
	public void setSynced(boolean synced) {
		this.synced = synced;
	}


	@ExcludeFromJson
	private Date createdAt;

	@Override
	public Date getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(Date date)
	{
		this.createdAt = date;
	}
    
	private String shineId;

	// Menstruation
	private Integer ageOfMenarche;
	private Integer whereFirstMenarche;
	private String whereFirstMenarcheSpecify;
	private Integer sanitaryMaterialsUsedPads;
	private Integer sanitaryMaterialsUsedTampon;
	private Integer sanitaryMaterialsUsedClothes;
	private Integer sanitaryMaterialsUsedHp;
	private Integer sanitaryMaterialsUsedMhp;
	private Integer sanitaryMaterialsUsedNone;
	private Integer howOftenUsed;
	private String howOftenUsedSpecify;
	private String reasonNotTimely;
	private Integer howDisposeMaterials;
	private String howDisposeMaterialsSpecify;

	// Pregnancy
	private Integer obstetricHistoryGravida;
	private Integer obstetricHistoryPara;
	private Integer obstetricHistoryAbortion;
	private Integer obstetricHistoryLiving;
	private Integer periodGestation;
	private Integer numAntenatalVisits;
	private Integer antenalServiceAncCheckUp;
	private Integer antenalServiceAlbendazole;
	private Integer antenalServiceTdImmunization;
	private Integer antenalServiceIrontabs;
	private Integer antenalServiceCouseling;
	private Integer antenalServicePmtct;
	private Integer antenalServiceOthers;
	private String antenalServiceSpecify;
	private Integer healthSeekingBehaviour;
	private String healthSeekingBehaviourSpecify;

	@AutoMigration.MigratedList(listType = com.ajwcc.epinurse.motherandchild.ui.MedicineEntry.class)
	private io.realm.RealmList<com.ajwcc.epinurse.motherandchild.ui.MedicineEntry> medicines = new io.realm.RealmList<com.ajwcc.epinurse.motherandchild.ui.MedicineEntry>();

	// Childbirth
	private Integer youngestBabyMonths;
	private Integer typeOfDelivery;
	private Integer placeDeliveryYoungest;
	private String placeDeliveryYoungestSpecify;
	private Integer deliveryComplications;
	private String deliveryComplicationsSpecify;
	private Integer gapBetweenKids;

	// Post Natal
	private Integer postNatalPe;
	private Integer postNatalCounselBF;
	private Integer postNatalCounselFP;
	private Integer postNatalInvestigations;
	private Integer postNatalIronTablets;
	private Integer postNatalVitaminA;
	private Integer postNatalOthers;
	private String postNatalSpecify;
	private Integer pncFollowupNone;
	private Integer pncFollowupWithin24hr;
	private Integer pncFollowupAt3rdDay;
	private Integer pncFollowupAt7thDay;
	private Integer pncFollowupAt28thDay;
	private Integer pncFollowupAt45thDay;
	private Integer pncFollowupOthers;
	private String pncFollowupSpecify;
	private Integer foodAvailableAfterDelivery;
	private String assessmentFoodQuality;
	private String assessmentFoodFrequency;
	private String assessmentFoodDistribution;
	private Integer prelactatingFeeding;
	private Integer dietaryRestrictions;

	// Family Planning
	private Integer hadCounselingFamilyPlanning;
	private Integer usedFamilyPlanningMethods;
	private Integer permanentVasectomy;
	private Integer permanentMinilap;
	private Integer temporaryNatural;
	private Integer temporaryCondom;
	private Integer temporaryDepo;
	private Integer temporaryPills;
	private Integer temporaryIucd;
	private Integer temporaryImplant;
	private Integer sourceMaternityInfo;

	// Child Basic Information
	private String childUniqueId;
	private String childName;
	private Integer childSex;
	private String dateOfBirthInAd;
	private String dateOfBirthInBs;
	private Double weightAtBirth;
	private Double weightCurrent;
	private Integer heightCm;
	private Double midArmCircumference;
	private Integer hasCongentialAnomaly;
	private Integer healthCondition;
	private Integer hadVitKInjection;

	// Child Breast Feeding
	private Integer whenStartBreastFeed;
	private Integer howOftenBreastFeed;
	private Integer didExclusiveBreastFeed;
	private Integer stoppedBreastFeedingMonths;
	private Integer reasonDiscontinueBreastFeed;
	private String reasonDiscontinueBreastFeedSpecify;
	private Integer awareBreastFeedPreventsIllness;
	private Integer startedSemiSolidFood;
	private Integer foodSupplementRicePudding;
	private Integer foodSupplementJaulo;
	private Integer foodSupplementLito;
	private Integer foodSupplementCerelac;
	private Integer foodSupplementOthers;
	private String foodSupplementSpecify;
	private Integer whyNoSupplement;
	private String whyNoSupplementSpecify;
	private String detailFoodToBaby;
	private Integer washHandsBreastfeed;
	private Integer hasHandwashFacility;
	private String childFoodQuality;
	private String childFoodFrequency;
	private String childFoodDistribution;

	// Accident and Injury
	private Integer injuryChildFacedFalls;
	private Integer injuryChildFacedDrowning;
	private Integer injuryChildFacedBurnsscald;
	private Integer injuryChildFacedPoisoning;
	private Integer injuryChildFacedSuffocationchokingaspiration;
	private Integer injuryChildFacedCutInjury;
	private Integer injuryChildFacedOthers;
	private String injuryChildFacedSpecify;
	private Integer injuryCauseUnsafeHome;
	private Integer injuryCauseNoSuperVision;
	private Integer injuryCauseBusyMother;
	private Integer injuryCauseSlipperyFloor;
	private Integer injuryCauseOthers;
	private String injuryCauseSpecify;

	// Immunization
	private Integer ageBcgAtBirth;
	private String remarksBcg;
	private Integer ageDptHepbHib6Weeks;
	private Integer ageDptHepbHib10Weeks;
	private Integer ageDptHepbHib14Weeks;
	private String remarksDptHepbHib;
	private Integer ageOpv6Weeks;
	private Integer ageOpv10Weeks;
	private Integer ageOpv14Weeks;
	private String remarksOpv;
	private Integer agePcv6Weeks;
	private Integer agePcv10Weeks;
	private Integer agePcv9Months;
	private String remarksPcv;
	private Integer ageIpv6Weeks;
	private Integer ageIpv14Weeks;
	private String remarksIpv;
	private Integer ageMeaslesRubella9Months;
	private Integer ageMeaslesRubella15Months;
	private String remarksMeaslesRubella;
	private Integer ageJapaneseEncephalitis12Months;
	private String remarksJapaneseEncephalitis;

	// Advices
	private String adviceBreastFeeding;
	private String adviceDentalHygiene;
	private String adviceToiletTraining;
	private String adviceComplementaryFeeding;
	private String adviceAccidentPrevention;
	private String mdPhysical;
	private String mdVerbal;
	private String mdSocial;
	private String mdSpiritualReligious;
	private String mdMotor;
	private String mdIntellectual;
	private String mdEmotional;
    
    

	public String getShineId() {
		return this.shineId;
	}

	public void setShineId(String shineId) {
	
		if (this.shineId==null || !this.shineId.equals(shineId))
		{
			editing = true;
			synced = false;
		}
	
		this.shineId = shineId;
	}

	public Integer getAgeOfMenarche() {
		return this.ageOfMenarche;
	}

	public void setAgeOfMenarche(Integer ageOfMenarche) {
	
		if (this.ageOfMenarche==null || !this.ageOfMenarche.equals(ageOfMenarche))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOfMenarche = ageOfMenarche;
	}

	public Integer getWhereFirstMenarche() {
		return this.whereFirstMenarche;
	}

	public void setWhereFirstMenarche(Integer whereFirstMenarche) {
	
		if (this.whereFirstMenarche==null || !this.whereFirstMenarche.equals(whereFirstMenarche))
		{
			editing = true;
			synced = false;
		}
	
		this.whereFirstMenarche = whereFirstMenarche;
	}

	public String getWhereFirstMenarcheSpecify() {
		return this.whereFirstMenarcheSpecify;
	}

	public void setWhereFirstMenarcheSpecify(String whereFirstMenarcheSpecify) {
	
		if (this.whereFirstMenarcheSpecify==null || !this.whereFirstMenarcheSpecify.equals(whereFirstMenarcheSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.whereFirstMenarcheSpecify = whereFirstMenarcheSpecify;
	}

	public Integer getSanitaryMaterialsUsedPads() {
		return this.sanitaryMaterialsUsedPads;
	}

	public void setSanitaryMaterialsUsedPads(Integer sanitaryMaterialsUsedPads) {
	
		if (this.sanitaryMaterialsUsedPads==null || !this.sanitaryMaterialsUsedPads.equals(sanitaryMaterialsUsedPads))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedPads = sanitaryMaterialsUsedPads;
	}

	public Integer getSanitaryMaterialsUsedTampon() {
		return this.sanitaryMaterialsUsedTampon;
	}

	public void setSanitaryMaterialsUsedTampon(Integer sanitaryMaterialsUsedTampon) {
	
		if (this.sanitaryMaterialsUsedTampon==null || !this.sanitaryMaterialsUsedTampon.equals(sanitaryMaterialsUsedTampon))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedTampon = sanitaryMaterialsUsedTampon;
	}

	public Integer getSanitaryMaterialsUsedClothes() {
		return this.sanitaryMaterialsUsedClothes;
	}

	public void setSanitaryMaterialsUsedClothes(Integer sanitaryMaterialsUsedClothes) {
	
		if (this.sanitaryMaterialsUsedClothes==null || !this.sanitaryMaterialsUsedClothes.equals(sanitaryMaterialsUsedClothes))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedClothes = sanitaryMaterialsUsedClothes;
	}

	public Integer getSanitaryMaterialsUsedHp() {
		return this.sanitaryMaterialsUsedHp;
	}

	public void setSanitaryMaterialsUsedHp(Integer sanitaryMaterialsUsedHp) {
	
		if (this.sanitaryMaterialsUsedHp==null || !this.sanitaryMaterialsUsedHp.equals(sanitaryMaterialsUsedHp))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedHp = sanitaryMaterialsUsedHp;
	}

	public Integer getSanitaryMaterialsUsedMhp() {
		return this.sanitaryMaterialsUsedMhp;
	}

	public void setSanitaryMaterialsUsedMhp(Integer sanitaryMaterialsUsedMhp) {
	
		if (this.sanitaryMaterialsUsedMhp==null || !this.sanitaryMaterialsUsedMhp.equals(sanitaryMaterialsUsedMhp))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedMhp = sanitaryMaterialsUsedMhp;
	}

	public Integer getSanitaryMaterialsUsedNone() {
		return this.sanitaryMaterialsUsedNone;
	}

	public void setSanitaryMaterialsUsedNone(Integer sanitaryMaterialsUsedNone) {
	
		if (this.sanitaryMaterialsUsedNone==null || !this.sanitaryMaterialsUsedNone.equals(sanitaryMaterialsUsedNone))
		{
			editing = true;
			synced = false;
		}
	
		this.sanitaryMaterialsUsedNone = sanitaryMaterialsUsedNone;
	}

	public Integer getHowOftenUsed() {
		return this.howOftenUsed;
	}

	public void setHowOftenUsed(Integer howOftenUsed) {
	
		if (this.howOftenUsed==null || !this.howOftenUsed.equals(howOftenUsed))
		{
			editing = true;
			synced = false;
		}
	
		this.howOftenUsed = howOftenUsed;
	}

	public String getHowOftenUsedSpecify() {
		return this.howOftenUsedSpecify;
	}

	public void setHowOftenUsedSpecify(String howOftenUsedSpecify) {
	
		if (this.howOftenUsedSpecify==null || !this.howOftenUsedSpecify.equals(howOftenUsedSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.howOftenUsedSpecify = howOftenUsedSpecify;
	}

	public String getReasonNotTimely() {
		return this.reasonNotTimely;
	}

	public void setReasonNotTimely(String reasonNotTimely) {
	
		if (this.reasonNotTimely==null || !this.reasonNotTimely.equals(reasonNotTimely))
		{
			editing = true;
			synced = false;
		}
	
		this.reasonNotTimely = reasonNotTimely;
	}

	public Integer getHowDisposeMaterials() {
		return this.howDisposeMaterials;
	}

	public void setHowDisposeMaterials(Integer howDisposeMaterials) {
	
		if (this.howDisposeMaterials==null || !this.howDisposeMaterials.equals(howDisposeMaterials))
		{
			editing = true;
			synced = false;
		}
	
		this.howDisposeMaterials = howDisposeMaterials;
	}

	public String getHowDisposeMaterialsSpecify() {
		return this.howDisposeMaterialsSpecify;
	}

	public void setHowDisposeMaterialsSpecify(String howDisposeMaterialsSpecify) {
	
		if (this.howDisposeMaterialsSpecify==null || !this.howDisposeMaterialsSpecify.equals(howDisposeMaterialsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.howDisposeMaterialsSpecify = howDisposeMaterialsSpecify;
	}

	public Integer getObstetricHistoryGravida() {
		return this.obstetricHistoryGravida;
	}

	public void setObstetricHistoryGravida(Integer obstetricHistoryGravida) {
	
		if (this.obstetricHistoryGravida==null || !this.obstetricHistoryGravida.equals(obstetricHistoryGravida))
		{
			editing = true;
			synced = false;
		}
	
		this.obstetricHistoryGravida = obstetricHistoryGravida;
	}

	public Integer getObstetricHistoryPara() {
		return this.obstetricHistoryPara;
	}

	public void setObstetricHistoryPara(Integer obstetricHistoryPara) {
	
		if (this.obstetricHistoryPara==null || !this.obstetricHistoryPara.equals(obstetricHistoryPara))
		{
			editing = true;
			synced = false;
		}
	
		this.obstetricHistoryPara = obstetricHistoryPara;
	}

	public Integer getObstetricHistoryAbortion() {
		return this.obstetricHistoryAbortion;
	}

	public void setObstetricHistoryAbortion(Integer obstetricHistoryAbortion) {
	
		if (this.obstetricHistoryAbortion==null || !this.obstetricHistoryAbortion.equals(obstetricHistoryAbortion))
		{
			editing = true;
			synced = false;
		}
	
		this.obstetricHistoryAbortion = obstetricHistoryAbortion;
	}

	public Integer getObstetricHistoryLiving() {
		return this.obstetricHistoryLiving;
	}

	public void setObstetricHistoryLiving(Integer obstetricHistoryLiving) {
	
		if (this.obstetricHistoryLiving==null || !this.obstetricHistoryLiving.equals(obstetricHistoryLiving))
		{
			editing = true;
			synced = false;
		}
	
		this.obstetricHistoryLiving = obstetricHistoryLiving;
	}

	public Integer getPeriodGestation() {
		return this.periodGestation;
	}

	public void setPeriodGestation(Integer periodGestation) {
	
		if (this.periodGestation==null || !this.periodGestation.equals(periodGestation))
		{
			editing = true;
			synced = false;
		}
	
		this.periodGestation = periodGestation;
	}

	public Integer getNumAntenatalVisits() {
		return this.numAntenatalVisits;
	}

	public void setNumAntenatalVisits(Integer numAntenatalVisits) {
	
		if (this.numAntenatalVisits==null || !this.numAntenatalVisits.equals(numAntenatalVisits))
		{
			editing = true;
			synced = false;
		}
	
		this.numAntenatalVisits = numAntenatalVisits;
	}

	public Integer getAntenalServiceAncCheckUp() {
		return this.antenalServiceAncCheckUp;
	}

	public void setAntenalServiceAncCheckUp(Integer antenalServiceAncCheckUp) {
	
		if (this.antenalServiceAncCheckUp==null || !this.antenalServiceAncCheckUp.equals(antenalServiceAncCheckUp))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceAncCheckUp = antenalServiceAncCheckUp;
	}

	public Integer getAntenalServiceAlbendazole() {
		return this.antenalServiceAlbendazole;
	}

	public void setAntenalServiceAlbendazole(Integer antenalServiceAlbendazole) {
	
		if (this.antenalServiceAlbendazole==null || !this.antenalServiceAlbendazole.equals(antenalServiceAlbendazole))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceAlbendazole = antenalServiceAlbendazole;
	}

	public Integer getAntenalServiceTdImmunization() {
		return this.antenalServiceTdImmunization;
	}

	public void setAntenalServiceTdImmunization(Integer antenalServiceTdImmunization) {
	
		if (this.antenalServiceTdImmunization==null || !this.antenalServiceTdImmunization.equals(antenalServiceTdImmunization))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceTdImmunization = antenalServiceTdImmunization;
	}

	public Integer getAntenalServiceIrontabs() {
		return this.antenalServiceIrontabs;
	}

	public void setAntenalServiceIrontabs(Integer antenalServiceIrontabs) {
	
		if (this.antenalServiceIrontabs==null || !this.antenalServiceIrontabs.equals(antenalServiceIrontabs))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceIrontabs = antenalServiceIrontabs;
	}

	public Integer getAntenalServiceCouseling() {
		return this.antenalServiceCouseling;
	}

	public void setAntenalServiceCouseling(Integer antenalServiceCouseling) {
	
		if (this.antenalServiceCouseling==null || !this.antenalServiceCouseling.equals(antenalServiceCouseling))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceCouseling = antenalServiceCouseling;
	}

	public Integer getAntenalServicePmtct() {
		return this.antenalServicePmtct;
	}

	public void setAntenalServicePmtct(Integer antenalServicePmtct) {
	
		if (this.antenalServicePmtct==null || !this.antenalServicePmtct.equals(antenalServicePmtct))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServicePmtct = antenalServicePmtct;
	}

	public Integer getAntenalServiceOthers() {
		return this.antenalServiceOthers;
	}

	public void setAntenalServiceOthers(Integer antenalServiceOthers) {
	
		if (this.antenalServiceOthers==null || !this.antenalServiceOthers.equals(antenalServiceOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceOthers = antenalServiceOthers;
	}

	public String getAntenalServiceSpecify() {
		return this.antenalServiceSpecify;
	}

	public void setAntenalServiceSpecify(String antenalServiceSpecify) {
	
		if (this.antenalServiceSpecify==null || !this.antenalServiceSpecify.equals(antenalServiceSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.antenalServiceSpecify = antenalServiceSpecify;
	}

	public Integer getHealthSeekingBehaviour() {
		return this.healthSeekingBehaviour;
	}

	public void setHealthSeekingBehaviour(Integer healthSeekingBehaviour) {
	
		if (this.healthSeekingBehaviour==null || !this.healthSeekingBehaviour.equals(healthSeekingBehaviour))
		{
			editing = true;
			synced = false;
		}
	
		this.healthSeekingBehaviour = healthSeekingBehaviour;
	}

	public String getHealthSeekingBehaviourSpecify() {
		return this.healthSeekingBehaviourSpecify;
	}

	public void setHealthSeekingBehaviourSpecify(String healthSeekingBehaviourSpecify) {
	
		if (this.healthSeekingBehaviourSpecify==null || !this.healthSeekingBehaviourSpecify.equals(healthSeekingBehaviourSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.healthSeekingBehaviourSpecify = healthSeekingBehaviourSpecify;
	}

	public io.realm.RealmList<com.ajwcc.epinurse.motherandchild.ui.MedicineEntry> getMedicines() {
		return this.medicines;
	}

	public void setMedicines(io.realm.RealmList<com.ajwcc.epinurse.motherandchild.ui.MedicineEntry> medicines) {
	
		if (this.medicines==null || !this.medicines.equals(medicines))
		{
			editing = true;
			synced = false;
		}
	
		this.medicines = medicines;
	}

	public Integer getYoungestBabyMonths() {
		return this.youngestBabyMonths;
	}

	public void setYoungestBabyMonths(Integer youngestBabyMonths) {
	
		if (this.youngestBabyMonths==null || !this.youngestBabyMonths.equals(youngestBabyMonths))
		{
			editing = true;
			synced = false;
		}
	
		this.youngestBabyMonths = youngestBabyMonths;
	}

	public Integer getTypeOfDelivery() {
		return this.typeOfDelivery;
	}

	public void setTypeOfDelivery(Integer typeOfDelivery) {
	
		if (this.typeOfDelivery==null || !this.typeOfDelivery.equals(typeOfDelivery))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfDelivery = typeOfDelivery;
	}

	public Integer getPlaceDeliveryYoungest() {
		return this.placeDeliveryYoungest;
	}

	public void setPlaceDeliveryYoungest(Integer placeDeliveryYoungest) {
	
		if (this.placeDeliveryYoungest==null || !this.placeDeliveryYoungest.equals(placeDeliveryYoungest))
		{
			editing = true;
			synced = false;
		}
	
		this.placeDeliveryYoungest = placeDeliveryYoungest;
	}

	public String getPlaceDeliveryYoungestSpecify() {
		return this.placeDeliveryYoungestSpecify;
	}

	public void setPlaceDeliveryYoungestSpecify(String placeDeliveryYoungestSpecify) {
	
		if (this.placeDeliveryYoungestSpecify==null || !this.placeDeliveryYoungestSpecify.equals(placeDeliveryYoungestSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.placeDeliveryYoungestSpecify = placeDeliveryYoungestSpecify;
	}

	public Integer getDeliveryComplications() {
		return this.deliveryComplications;
	}

	public void setDeliveryComplications(Integer deliveryComplications) {
	
		if (this.deliveryComplications==null || !this.deliveryComplications.equals(deliveryComplications))
		{
			editing = true;
			synced = false;
		}
	
		this.deliveryComplications = deliveryComplications;
	}

	public String getDeliveryComplicationsSpecify() {
		return this.deliveryComplicationsSpecify;
	}

	public void setDeliveryComplicationsSpecify(String deliveryComplicationsSpecify) {
	
		if (this.deliveryComplicationsSpecify==null || !this.deliveryComplicationsSpecify.equals(deliveryComplicationsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.deliveryComplicationsSpecify = deliveryComplicationsSpecify;
	}

	public Integer getGapBetweenKids() {
		return this.gapBetweenKids;
	}

	public void setGapBetweenKids(Integer gapBetweenKids) {
	
		if (this.gapBetweenKids==null || !this.gapBetweenKids.equals(gapBetweenKids))
		{
			editing = true;
			synced = false;
		}
	
		this.gapBetweenKids = gapBetweenKids;
	}

	public Integer getPostNatalPe() {
		return this.postNatalPe;
	}

	public void setPostNatalPe(Integer postNatalPe) {
	
		if (this.postNatalPe==null || !this.postNatalPe.equals(postNatalPe))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalPe = postNatalPe;
	}

	public Integer getPostNatalCounselBF() {
		return this.postNatalCounselBF;
	}

	public void setPostNatalCounselBF(Integer postNatalCounselBF) {
	
		if (this.postNatalCounselBF==null || !this.postNatalCounselBF.equals(postNatalCounselBF))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalCounselBF = postNatalCounselBF;
	}

	public Integer getPostNatalCounselFP() {
		return this.postNatalCounselFP;
	}

	public void setPostNatalCounselFP(Integer postNatalCounselFP) {
	
		if (this.postNatalCounselFP==null || !this.postNatalCounselFP.equals(postNatalCounselFP))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalCounselFP = postNatalCounselFP;
	}

	public Integer getPostNatalInvestigations() {
		return this.postNatalInvestigations;
	}

	public void setPostNatalInvestigations(Integer postNatalInvestigations) {
	
		if (this.postNatalInvestigations==null || !this.postNatalInvestigations.equals(postNatalInvestigations))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalInvestigations = postNatalInvestigations;
	}

	public Integer getPostNatalIronTablets() {
		return this.postNatalIronTablets;
	}

	public void setPostNatalIronTablets(Integer postNatalIronTablets) {
	
		if (this.postNatalIronTablets==null || !this.postNatalIronTablets.equals(postNatalIronTablets))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalIronTablets = postNatalIronTablets;
	}

	public Integer getPostNatalVitaminA() {
		return this.postNatalVitaminA;
	}

	public void setPostNatalVitaminA(Integer postNatalVitaminA) {
	
		if (this.postNatalVitaminA==null || !this.postNatalVitaminA.equals(postNatalVitaminA))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalVitaminA = postNatalVitaminA;
	}

	public Integer getPostNatalOthers() {
		return this.postNatalOthers;
	}

	public void setPostNatalOthers(Integer postNatalOthers) {
	
		if (this.postNatalOthers==null || !this.postNatalOthers.equals(postNatalOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalOthers = postNatalOthers;
	}

	public String getPostNatalSpecify() {
		return this.postNatalSpecify;
	}

	public void setPostNatalSpecify(String postNatalSpecify) {
	
		if (this.postNatalSpecify==null || !this.postNatalSpecify.equals(postNatalSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.postNatalSpecify = postNatalSpecify;
	}

	public Integer getPncFollowupNone() {
		return this.pncFollowupNone;
	}

	public void setPncFollowupNone(Integer pncFollowupNone) {
	
		if (this.pncFollowupNone==null || !this.pncFollowupNone.equals(pncFollowupNone))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupNone = pncFollowupNone;
	}

	public Integer getPncFollowupWithin24hr() {
		return this.pncFollowupWithin24hr;
	}

	public void setPncFollowupWithin24hr(Integer pncFollowupWithin24hr) {
	
		if (this.pncFollowupWithin24hr==null || !this.pncFollowupWithin24hr.equals(pncFollowupWithin24hr))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupWithin24hr = pncFollowupWithin24hr;
	}

	public Integer getPncFollowupAt3rdDay() {
		return this.pncFollowupAt3rdDay;
	}

	public void setPncFollowupAt3rdDay(Integer pncFollowupAt3rdDay) {
	
		if (this.pncFollowupAt3rdDay==null || !this.pncFollowupAt3rdDay.equals(pncFollowupAt3rdDay))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupAt3rdDay = pncFollowupAt3rdDay;
	}

	public Integer getPncFollowupAt7thDay() {
		return this.pncFollowupAt7thDay;
	}

	public void setPncFollowupAt7thDay(Integer pncFollowupAt7thDay) {
	
		if (this.pncFollowupAt7thDay==null || !this.pncFollowupAt7thDay.equals(pncFollowupAt7thDay))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupAt7thDay = pncFollowupAt7thDay;
	}

	public Integer getPncFollowupAt28thDay() {
		return this.pncFollowupAt28thDay;
	}

	public void setPncFollowupAt28thDay(Integer pncFollowupAt28thDay) {
	
		if (this.pncFollowupAt28thDay==null || !this.pncFollowupAt28thDay.equals(pncFollowupAt28thDay))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupAt28thDay = pncFollowupAt28thDay;
	}

	public Integer getPncFollowupAt45thDay() {
		return this.pncFollowupAt45thDay;
	}

	public void setPncFollowupAt45thDay(Integer pncFollowupAt45thDay) {
	
		if (this.pncFollowupAt45thDay==null || !this.pncFollowupAt45thDay.equals(pncFollowupAt45thDay))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupAt45thDay = pncFollowupAt45thDay;
	}

	public Integer getPncFollowupOthers() {
		return this.pncFollowupOthers;
	}

	public void setPncFollowupOthers(Integer pncFollowupOthers) {
	
		if (this.pncFollowupOthers==null || !this.pncFollowupOthers.equals(pncFollowupOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupOthers = pncFollowupOthers;
	}

	public String getPncFollowupSpecify() {
		return this.pncFollowupSpecify;
	}

	public void setPncFollowupSpecify(String pncFollowupSpecify) {
	
		if (this.pncFollowupSpecify==null || !this.pncFollowupSpecify.equals(pncFollowupSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.pncFollowupSpecify = pncFollowupSpecify;
	}

	public Integer getFoodAvailableAfterDelivery() {
		return this.foodAvailableAfterDelivery;
	}

	public void setFoodAvailableAfterDelivery(Integer foodAvailableAfterDelivery) {
	
		if (this.foodAvailableAfterDelivery==null || !this.foodAvailableAfterDelivery.equals(foodAvailableAfterDelivery))
		{
			editing = true;
			synced = false;
		}
	
		this.foodAvailableAfterDelivery = foodAvailableAfterDelivery;
	}

	public String getAssessmentFoodQuality() {
		return this.assessmentFoodQuality;
	}

	public void setAssessmentFoodQuality(String assessmentFoodQuality) {
	
		if (this.assessmentFoodQuality==null || !this.assessmentFoodQuality.equals(assessmentFoodQuality))
		{
			editing = true;
			synced = false;
		}
	
		this.assessmentFoodQuality = assessmentFoodQuality;
	}

	public String getAssessmentFoodFrequency() {
		return this.assessmentFoodFrequency;
	}

	public void setAssessmentFoodFrequency(String assessmentFoodFrequency) {
	
		if (this.assessmentFoodFrequency==null || !this.assessmentFoodFrequency.equals(assessmentFoodFrequency))
		{
			editing = true;
			synced = false;
		}
	
		this.assessmentFoodFrequency = assessmentFoodFrequency;
	}

	public String getAssessmentFoodDistribution() {
		return this.assessmentFoodDistribution;
	}

	public void setAssessmentFoodDistribution(String assessmentFoodDistribution) {
	
		if (this.assessmentFoodDistribution==null || !this.assessmentFoodDistribution.equals(assessmentFoodDistribution))
		{
			editing = true;
			synced = false;
		}
	
		this.assessmentFoodDistribution = assessmentFoodDistribution;
	}

	public Integer getPrelactatingFeeding() {
		return this.prelactatingFeeding;
	}

	public void setPrelactatingFeeding(Integer prelactatingFeeding) {
	
		if (this.prelactatingFeeding==null || !this.prelactatingFeeding.equals(prelactatingFeeding))
		{
			editing = true;
			synced = false;
		}
	
		this.prelactatingFeeding = prelactatingFeeding;
	}

	public Integer getDietaryRestrictions() {
		return this.dietaryRestrictions;
	}

	public void setDietaryRestrictions(Integer dietaryRestrictions) {
	
		if (this.dietaryRestrictions==null || !this.dietaryRestrictions.equals(dietaryRestrictions))
		{
			editing = true;
			synced = false;
		}
	
		this.dietaryRestrictions = dietaryRestrictions;
	}

	public Integer getHadCounselingFamilyPlanning() {
		return this.hadCounselingFamilyPlanning;
	}

	public void setHadCounselingFamilyPlanning(Integer hadCounselingFamilyPlanning) {
	
		if (this.hadCounselingFamilyPlanning==null || !this.hadCounselingFamilyPlanning.equals(hadCounselingFamilyPlanning))
		{
			editing = true;
			synced = false;
		}
	
		this.hadCounselingFamilyPlanning = hadCounselingFamilyPlanning;
	}

	public Integer getUsedFamilyPlanningMethods() {
		return this.usedFamilyPlanningMethods;
	}

	public void setUsedFamilyPlanningMethods(Integer usedFamilyPlanningMethods) {
	
		if (this.usedFamilyPlanningMethods==null || !this.usedFamilyPlanningMethods.equals(usedFamilyPlanningMethods))
		{
			editing = true;
			synced = false;
		}
	
		this.usedFamilyPlanningMethods = usedFamilyPlanningMethods;
	}

	public Integer getPermanentVasectomy() {
		return this.permanentVasectomy;
	}

	public void setPermanentVasectomy(Integer permanentVasectomy) {
	
		if (this.permanentVasectomy==null || !this.permanentVasectomy.equals(permanentVasectomy))
		{
			editing = true;
			synced = false;
		}
	
		this.permanentVasectomy = permanentVasectomy;
	}

	public Integer getPermanentMinilap() {
		return this.permanentMinilap;
	}

	public void setPermanentMinilap(Integer permanentMinilap) {
	
		if (this.permanentMinilap==null || !this.permanentMinilap.equals(permanentMinilap))
		{
			editing = true;
			synced = false;
		}
	
		this.permanentMinilap = permanentMinilap;
	}

	public Integer getTemporaryNatural() {
		return this.temporaryNatural;
	}

	public void setTemporaryNatural(Integer temporaryNatural) {
	
		if (this.temporaryNatural==null || !this.temporaryNatural.equals(temporaryNatural))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryNatural = temporaryNatural;
	}

	public Integer getTemporaryCondom() {
		return this.temporaryCondom;
	}

	public void setTemporaryCondom(Integer temporaryCondom) {
	
		if (this.temporaryCondom==null || !this.temporaryCondom.equals(temporaryCondom))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryCondom = temporaryCondom;
	}

	public Integer getTemporaryDepo() {
		return this.temporaryDepo;
	}

	public void setTemporaryDepo(Integer temporaryDepo) {
	
		if (this.temporaryDepo==null || !this.temporaryDepo.equals(temporaryDepo))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryDepo = temporaryDepo;
	}

	public Integer getTemporaryPills() {
		return this.temporaryPills;
	}

	public void setTemporaryPills(Integer temporaryPills) {
	
		if (this.temporaryPills==null || !this.temporaryPills.equals(temporaryPills))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryPills = temporaryPills;
	}

	public Integer getTemporaryIucd() {
		return this.temporaryIucd;
	}

	public void setTemporaryIucd(Integer temporaryIucd) {
	
		if (this.temporaryIucd==null || !this.temporaryIucd.equals(temporaryIucd))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryIucd = temporaryIucd;
	}

	public Integer getTemporaryImplant() {
		return this.temporaryImplant;
	}

	public void setTemporaryImplant(Integer temporaryImplant) {
	
		if (this.temporaryImplant==null || !this.temporaryImplant.equals(temporaryImplant))
		{
			editing = true;
			synced = false;
		}
	
		this.temporaryImplant = temporaryImplant;
	}

	public Integer getSourceMaternityInfo() {
		return this.sourceMaternityInfo;
	}

	public void setSourceMaternityInfo(Integer sourceMaternityInfo) {
	
		if (this.sourceMaternityInfo==null || !this.sourceMaternityInfo.equals(sourceMaternityInfo))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceMaternityInfo = sourceMaternityInfo;
	}

	public String getChildUniqueId() {
		return this.childUniqueId;
	}

	public void setChildUniqueId(String childUniqueId) {
	
		if (this.childUniqueId==null || !this.childUniqueId.equals(childUniqueId))
		{
			editing = true;
			synced = false;
		}
	
		this.childUniqueId = childUniqueId;
	}

	public String getChildName() {
		return this.childName;
	}

	public void setChildName(String childName) {
	
		if (this.childName==null || !this.childName.equals(childName))
		{
			editing = true;
			synced = false;
		}
	
		this.childName = childName;
	}

	public Integer getChildSex() {
		return this.childSex;
	}

	public void setChildSex(Integer childSex) {
	
		if (this.childSex==null || !this.childSex.equals(childSex))
		{
			editing = true;
			synced = false;
		}
	
		this.childSex = childSex;
	}

	public String getDateOfBirthInAd() {
		return this.dateOfBirthInAd;
	}

	public void setDateOfBirthInAd(String dateOfBirthInAd) {
	
		if (this.dateOfBirthInAd==null || !this.dateOfBirthInAd.equals(dateOfBirthInAd))
		{
			editing = true;
			synced = false;
		}
	
		this.dateOfBirthInAd = dateOfBirthInAd;
	}

	public String getDateOfBirthInBs() {
		return this.dateOfBirthInBs;
	}

	public void setDateOfBirthInBs(String dateOfBirthInBs) {
	
		if (this.dateOfBirthInBs==null || !this.dateOfBirthInBs.equals(dateOfBirthInBs))
		{
			editing = true;
			synced = false;
		}
	
		this.dateOfBirthInBs = dateOfBirthInBs;
	}

	public Double getWeightAtBirth() {
		return this.weightAtBirth;
	}

	public void setWeightAtBirth(Double weightAtBirth) {
	
		if (this.weightAtBirth==null || !this.weightAtBirth.equals(weightAtBirth))
		{
			editing = true;
			synced = false;
		}
	
		this.weightAtBirth = weightAtBirth;
	}

	public Double getWeightCurrent() {
		return this.weightCurrent;
	}

	public void setWeightCurrent(Double weightCurrent) {
	
		if (this.weightCurrent==null || !this.weightCurrent.equals(weightCurrent))
		{
			editing = true;
			synced = false;
		}
	
		this.weightCurrent = weightCurrent;
	}

	public Integer getHeightCm() {
		return this.heightCm;
	}

	public void setHeightCm(Integer heightCm) {
	
		if (this.heightCm==null || !this.heightCm.equals(heightCm))
		{
			editing = true;
			synced = false;
		}
	
		this.heightCm = heightCm;
	}

	public Double getMidArmCircumference() {
		return this.midArmCircumference;
	}

	public void setMidArmCircumference(Double midArmCircumference) {
	
		if (this.midArmCircumference==null || !this.midArmCircumference.equals(midArmCircumference))
		{
			editing = true;
			synced = false;
		}
	
		this.midArmCircumference = midArmCircumference;
	}

	public Integer getHasCongentialAnomaly() {
		return this.hasCongentialAnomaly;
	}

	public void setHasCongentialAnomaly(Integer hasCongentialAnomaly) {
	
		if (this.hasCongentialAnomaly==null || !this.hasCongentialAnomaly.equals(hasCongentialAnomaly))
		{
			editing = true;
			synced = false;
		}
	
		this.hasCongentialAnomaly = hasCongentialAnomaly;
	}

	public Integer getHealthCondition() {
		return this.healthCondition;
	}

	public void setHealthCondition(Integer healthCondition) {
	
		if (this.healthCondition==null || !this.healthCondition.equals(healthCondition))
		{
			editing = true;
			synced = false;
		}
	
		this.healthCondition = healthCondition;
	}

	public Integer getHadVitKInjection() {
		return this.hadVitKInjection;
	}

	public void setHadVitKInjection(Integer hadVitKInjection) {
	
		if (this.hadVitKInjection==null || !this.hadVitKInjection.equals(hadVitKInjection))
		{
			editing = true;
			synced = false;
		}
	
		this.hadVitKInjection = hadVitKInjection;
	}

	public Integer getWhenStartBreastFeed() {
		return this.whenStartBreastFeed;
	}

	public void setWhenStartBreastFeed(Integer whenStartBreastFeed) {
	
		if (this.whenStartBreastFeed==null || !this.whenStartBreastFeed.equals(whenStartBreastFeed))
		{
			editing = true;
			synced = false;
		}
	
		this.whenStartBreastFeed = whenStartBreastFeed;
	}

	public Integer getHowOftenBreastFeed() {
		return this.howOftenBreastFeed;
	}

	public void setHowOftenBreastFeed(Integer howOftenBreastFeed) {
	
		if (this.howOftenBreastFeed==null || !this.howOftenBreastFeed.equals(howOftenBreastFeed))
		{
			editing = true;
			synced = false;
		}
	
		this.howOftenBreastFeed = howOftenBreastFeed;
	}

	public Integer getDidExclusiveBreastFeed() {
		return this.didExclusiveBreastFeed;
	}

	public void setDidExclusiveBreastFeed(Integer didExclusiveBreastFeed) {
	
		if (this.didExclusiveBreastFeed==null || !this.didExclusiveBreastFeed.equals(didExclusiveBreastFeed))
		{
			editing = true;
			synced = false;
		}
	
		this.didExclusiveBreastFeed = didExclusiveBreastFeed;
	}

	public Integer getStoppedBreastFeedingMonths() {
		return this.stoppedBreastFeedingMonths;
	}

	public void setStoppedBreastFeedingMonths(Integer stoppedBreastFeedingMonths) {
	
		if (this.stoppedBreastFeedingMonths==null || !this.stoppedBreastFeedingMonths.equals(stoppedBreastFeedingMonths))
		{
			editing = true;
			synced = false;
		}
	
		this.stoppedBreastFeedingMonths = stoppedBreastFeedingMonths;
	}

	public Integer getReasonDiscontinueBreastFeed() {
		return this.reasonDiscontinueBreastFeed;
	}

	public void setReasonDiscontinueBreastFeed(Integer reasonDiscontinueBreastFeed) {
	
		if (this.reasonDiscontinueBreastFeed==null || !this.reasonDiscontinueBreastFeed.equals(reasonDiscontinueBreastFeed))
		{
			editing = true;
			synced = false;
		}
	
		this.reasonDiscontinueBreastFeed = reasonDiscontinueBreastFeed;
	}

	public String getReasonDiscontinueBreastFeedSpecify() {
		return this.reasonDiscontinueBreastFeedSpecify;
	}

	public void setReasonDiscontinueBreastFeedSpecify(String reasonDiscontinueBreastFeedSpecify) {
	
		if (this.reasonDiscontinueBreastFeedSpecify==null || !this.reasonDiscontinueBreastFeedSpecify.equals(reasonDiscontinueBreastFeedSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.reasonDiscontinueBreastFeedSpecify = reasonDiscontinueBreastFeedSpecify;
	}

	public Integer getAwareBreastFeedPreventsIllness() {
		return this.awareBreastFeedPreventsIllness;
	}

	public void setAwareBreastFeedPreventsIllness(Integer awareBreastFeedPreventsIllness) {
	
		if (this.awareBreastFeedPreventsIllness==null || !this.awareBreastFeedPreventsIllness.equals(awareBreastFeedPreventsIllness))
		{
			editing = true;
			synced = false;
		}
	
		this.awareBreastFeedPreventsIllness = awareBreastFeedPreventsIllness;
	}

	public Integer getStartedSemiSolidFood() {
		return this.startedSemiSolidFood;
	}

	public void setStartedSemiSolidFood(Integer startedSemiSolidFood) {
	
		if (this.startedSemiSolidFood==null || !this.startedSemiSolidFood.equals(startedSemiSolidFood))
		{
			editing = true;
			synced = false;
		}
	
		this.startedSemiSolidFood = startedSemiSolidFood;
	}

	public Integer getFoodSupplementRicePudding() {
		return this.foodSupplementRicePudding;
	}

	public void setFoodSupplementRicePudding(Integer foodSupplementRicePudding) {
	
		if (this.foodSupplementRicePudding==null || !this.foodSupplementRicePudding.equals(foodSupplementRicePudding))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementRicePudding = foodSupplementRicePudding;
	}

	public Integer getFoodSupplementJaulo() {
		return this.foodSupplementJaulo;
	}

	public void setFoodSupplementJaulo(Integer foodSupplementJaulo) {
	
		if (this.foodSupplementJaulo==null || !this.foodSupplementJaulo.equals(foodSupplementJaulo))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementJaulo = foodSupplementJaulo;
	}

	public Integer getFoodSupplementLito() {
		return this.foodSupplementLito;
	}

	public void setFoodSupplementLito(Integer foodSupplementLito) {
	
		if (this.foodSupplementLito==null || !this.foodSupplementLito.equals(foodSupplementLito))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementLito = foodSupplementLito;
	}

	public Integer getFoodSupplementCerelac() {
		return this.foodSupplementCerelac;
	}

	public void setFoodSupplementCerelac(Integer foodSupplementCerelac) {
	
		if (this.foodSupplementCerelac==null || !this.foodSupplementCerelac.equals(foodSupplementCerelac))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementCerelac = foodSupplementCerelac;
	}

	public Integer getFoodSupplementOthers() {
		return this.foodSupplementOthers;
	}

	public void setFoodSupplementOthers(Integer foodSupplementOthers) {
	
		if (this.foodSupplementOthers==null || !this.foodSupplementOthers.equals(foodSupplementOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementOthers = foodSupplementOthers;
	}

	public String getFoodSupplementSpecify() {
		return this.foodSupplementSpecify;
	}

	public void setFoodSupplementSpecify(String foodSupplementSpecify) {
	
		if (this.foodSupplementSpecify==null || !this.foodSupplementSpecify.equals(foodSupplementSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.foodSupplementSpecify = foodSupplementSpecify;
	}

	public Integer getWhyNoSupplement() {
		return this.whyNoSupplement;
	}

	public void setWhyNoSupplement(Integer whyNoSupplement) {
	
		if (this.whyNoSupplement==null || !this.whyNoSupplement.equals(whyNoSupplement))
		{
			editing = true;
			synced = false;
		}
	
		this.whyNoSupplement = whyNoSupplement;
	}

	public String getWhyNoSupplementSpecify() {
		return this.whyNoSupplementSpecify;
	}

	public void setWhyNoSupplementSpecify(String whyNoSupplementSpecify) {
	
		if (this.whyNoSupplementSpecify==null || !this.whyNoSupplementSpecify.equals(whyNoSupplementSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.whyNoSupplementSpecify = whyNoSupplementSpecify;
	}

	public String getDetailFoodToBaby() {
		return this.detailFoodToBaby;
	}

	public void setDetailFoodToBaby(String detailFoodToBaby) {
	
		if (this.detailFoodToBaby==null || !this.detailFoodToBaby.equals(detailFoodToBaby))
		{
			editing = true;
			synced = false;
		}
	
		this.detailFoodToBaby = detailFoodToBaby;
	}

	public Integer getWashHandsBreastfeed() {
		return this.washHandsBreastfeed;
	}

	public void setWashHandsBreastfeed(Integer washHandsBreastfeed) {
	
		if (this.washHandsBreastfeed==null || !this.washHandsBreastfeed.equals(washHandsBreastfeed))
		{
			editing = true;
			synced = false;
		}
	
		this.washHandsBreastfeed = washHandsBreastfeed;
	}

	public Integer getHasHandwashFacility() {
		return this.hasHandwashFacility;
	}

	public void setHasHandwashFacility(Integer hasHandwashFacility) {
	
		if (this.hasHandwashFacility==null || !this.hasHandwashFacility.equals(hasHandwashFacility))
		{
			editing = true;
			synced = false;
		}
	
		this.hasHandwashFacility = hasHandwashFacility;
	}

	public String getChildFoodQuality() {
		return this.childFoodQuality;
	}

	public void setChildFoodQuality(String childFoodQuality) {
	
		if (this.childFoodQuality==null || !this.childFoodQuality.equals(childFoodQuality))
		{
			editing = true;
			synced = false;
		}
	
		this.childFoodQuality = childFoodQuality;
	}

	public String getChildFoodFrequency() {
		return this.childFoodFrequency;
	}

	public void setChildFoodFrequency(String childFoodFrequency) {
	
		if (this.childFoodFrequency==null || !this.childFoodFrequency.equals(childFoodFrequency))
		{
			editing = true;
			synced = false;
		}
	
		this.childFoodFrequency = childFoodFrequency;
	}

	public String getChildFoodDistribution() {
		return this.childFoodDistribution;
	}

	public void setChildFoodDistribution(String childFoodDistribution) {
	
		if (this.childFoodDistribution==null || !this.childFoodDistribution.equals(childFoodDistribution))
		{
			editing = true;
			synced = false;
		}
	
		this.childFoodDistribution = childFoodDistribution;
	}

	public Integer getInjuryChildFacedFalls() {
		return this.injuryChildFacedFalls;
	}

	public void setInjuryChildFacedFalls(Integer injuryChildFacedFalls) {
	
		if (this.injuryChildFacedFalls==null || !this.injuryChildFacedFalls.equals(injuryChildFacedFalls))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedFalls = injuryChildFacedFalls;
	}

	public Integer getInjuryChildFacedDrowning() {
		return this.injuryChildFacedDrowning;
	}

	public void setInjuryChildFacedDrowning(Integer injuryChildFacedDrowning) {
	
		if (this.injuryChildFacedDrowning==null || !this.injuryChildFacedDrowning.equals(injuryChildFacedDrowning))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedDrowning = injuryChildFacedDrowning;
	}

	public Integer getInjuryChildFacedBurnsscald() {
		return this.injuryChildFacedBurnsscald;
	}

	public void setInjuryChildFacedBurnsscald(Integer injuryChildFacedBurnsscald) {
	
		if (this.injuryChildFacedBurnsscald==null || !this.injuryChildFacedBurnsscald.equals(injuryChildFacedBurnsscald))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedBurnsscald = injuryChildFacedBurnsscald;
	}

	public Integer getInjuryChildFacedPoisoning() {
		return this.injuryChildFacedPoisoning;
	}

	public void setInjuryChildFacedPoisoning(Integer injuryChildFacedPoisoning) {
	
		if (this.injuryChildFacedPoisoning==null || !this.injuryChildFacedPoisoning.equals(injuryChildFacedPoisoning))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedPoisoning = injuryChildFacedPoisoning;
	}

	public Integer getInjuryChildFacedSuffocationchokingaspiration() {
		return this.injuryChildFacedSuffocationchokingaspiration;
	}

	public void setInjuryChildFacedSuffocationchokingaspiration(Integer injuryChildFacedSuffocationchokingaspiration) {
	
		if (this.injuryChildFacedSuffocationchokingaspiration==null || !this.injuryChildFacedSuffocationchokingaspiration.equals(injuryChildFacedSuffocationchokingaspiration))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedSuffocationchokingaspiration = injuryChildFacedSuffocationchokingaspiration;
	}

	public Integer getInjuryChildFacedCutInjury() {
		return this.injuryChildFacedCutInjury;
	}

	public void setInjuryChildFacedCutInjury(Integer injuryChildFacedCutInjury) {
	
		if (this.injuryChildFacedCutInjury==null || !this.injuryChildFacedCutInjury.equals(injuryChildFacedCutInjury))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedCutInjury = injuryChildFacedCutInjury;
	}

	public Integer getInjuryChildFacedOthers() {
		return this.injuryChildFacedOthers;
	}

	public void setInjuryChildFacedOthers(Integer injuryChildFacedOthers) {
	
		if (this.injuryChildFacedOthers==null || !this.injuryChildFacedOthers.equals(injuryChildFacedOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedOthers = injuryChildFacedOthers;
	}

	public String getInjuryChildFacedSpecify() {
		return this.injuryChildFacedSpecify;
	}

	public void setInjuryChildFacedSpecify(String injuryChildFacedSpecify) {
	
		if (this.injuryChildFacedSpecify==null || !this.injuryChildFacedSpecify.equals(injuryChildFacedSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryChildFacedSpecify = injuryChildFacedSpecify;
	}

	public Integer getInjuryCauseUnsafeHome() {
		return this.injuryCauseUnsafeHome;
	}

	public void setInjuryCauseUnsafeHome(Integer injuryCauseUnsafeHome) {
	
		if (this.injuryCauseUnsafeHome==null || !this.injuryCauseUnsafeHome.equals(injuryCauseUnsafeHome))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseUnsafeHome = injuryCauseUnsafeHome;
	}

	public Integer getInjuryCauseNoSuperVision() {
		return this.injuryCauseNoSuperVision;
	}

	public void setInjuryCauseNoSuperVision(Integer injuryCauseNoSuperVision) {
	
		if (this.injuryCauseNoSuperVision==null || !this.injuryCauseNoSuperVision.equals(injuryCauseNoSuperVision))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseNoSuperVision = injuryCauseNoSuperVision;
	}

	public Integer getInjuryCauseBusyMother() {
		return this.injuryCauseBusyMother;
	}

	public void setInjuryCauseBusyMother(Integer injuryCauseBusyMother) {
	
		if (this.injuryCauseBusyMother==null || !this.injuryCauseBusyMother.equals(injuryCauseBusyMother))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseBusyMother = injuryCauseBusyMother;
	}

	public Integer getInjuryCauseSlipperyFloor() {
		return this.injuryCauseSlipperyFloor;
	}

	public void setInjuryCauseSlipperyFloor(Integer injuryCauseSlipperyFloor) {
	
		if (this.injuryCauseSlipperyFloor==null || !this.injuryCauseSlipperyFloor.equals(injuryCauseSlipperyFloor))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseSlipperyFloor = injuryCauseSlipperyFloor;
	}

	public Integer getInjuryCauseOthers() {
		return this.injuryCauseOthers;
	}

	public void setInjuryCauseOthers(Integer injuryCauseOthers) {
	
		if (this.injuryCauseOthers==null || !this.injuryCauseOthers.equals(injuryCauseOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseOthers = injuryCauseOthers;
	}

	public String getInjuryCauseSpecify() {
		return this.injuryCauseSpecify;
	}

	public void setInjuryCauseSpecify(String injuryCauseSpecify) {
	
		if (this.injuryCauseSpecify==null || !this.injuryCauseSpecify.equals(injuryCauseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.injuryCauseSpecify = injuryCauseSpecify;
	}

	public Integer getAgeBcgAtBirth() {
		return this.ageBcgAtBirth;
	}

	public void setAgeBcgAtBirth(Integer ageBcgAtBirth) {
	
		if (this.ageBcgAtBirth==null || !this.ageBcgAtBirth.equals(ageBcgAtBirth))
		{
			editing = true;
			synced = false;
		}
	
		this.ageBcgAtBirth = ageBcgAtBirth;
	}

	public String getRemarksBcg() {
		return this.remarksBcg;
	}

	public void setRemarksBcg(String remarksBcg) {
	
		if (this.remarksBcg==null || !this.remarksBcg.equals(remarksBcg))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksBcg = remarksBcg;
	}

	public Integer getAgeDptHepbHib6Weeks() {
		return this.ageDptHepbHib6Weeks;
	}

	public void setAgeDptHepbHib6Weeks(Integer ageDptHepbHib6Weeks) {
	
		if (this.ageDptHepbHib6Weeks==null || !this.ageDptHepbHib6Weeks.equals(ageDptHepbHib6Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageDptHepbHib6Weeks = ageDptHepbHib6Weeks;
	}

	public Integer getAgeDptHepbHib10Weeks() {
		return this.ageDptHepbHib10Weeks;
	}

	public void setAgeDptHepbHib10Weeks(Integer ageDptHepbHib10Weeks) {
	
		if (this.ageDptHepbHib10Weeks==null || !this.ageDptHepbHib10Weeks.equals(ageDptHepbHib10Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageDptHepbHib10Weeks = ageDptHepbHib10Weeks;
	}

	public Integer getAgeDptHepbHib14Weeks() {
		return this.ageDptHepbHib14Weeks;
	}

	public void setAgeDptHepbHib14Weeks(Integer ageDptHepbHib14Weeks) {
	
		if (this.ageDptHepbHib14Weeks==null || !this.ageDptHepbHib14Weeks.equals(ageDptHepbHib14Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageDptHepbHib14Weeks = ageDptHepbHib14Weeks;
	}

	public String getRemarksDptHepbHib() {
		return this.remarksDptHepbHib;
	}

	public void setRemarksDptHepbHib(String remarksDptHepbHib) {
	
		if (this.remarksDptHepbHib==null || !this.remarksDptHepbHib.equals(remarksDptHepbHib))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksDptHepbHib = remarksDptHepbHib;
	}

	public Integer getAgeOpv6Weeks() {
		return this.ageOpv6Weeks;
	}

	public void setAgeOpv6Weeks(Integer ageOpv6Weeks) {
	
		if (this.ageOpv6Weeks==null || !this.ageOpv6Weeks.equals(ageOpv6Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOpv6Weeks = ageOpv6Weeks;
	}

	public Integer getAgeOpv10Weeks() {
		return this.ageOpv10Weeks;
	}

	public void setAgeOpv10Weeks(Integer ageOpv10Weeks) {
	
		if (this.ageOpv10Weeks==null || !this.ageOpv10Weeks.equals(ageOpv10Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOpv10Weeks = ageOpv10Weeks;
	}

	public Integer getAgeOpv14Weeks() {
		return this.ageOpv14Weeks;
	}

	public void setAgeOpv14Weeks(Integer ageOpv14Weeks) {
	
		if (this.ageOpv14Weeks==null || !this.ageOpv14Weeks.equals(ageOpv14Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOpv14Weeks = ageOpv14Weeks;
	}

	public String getRemarksOpv() {
		return this.remarksOpv;
	}

	public void setRemarksOpv(String remarksOpv) {
	
		if (this.remarksOpv==null || !this.remarksOpv.equals(remarksOpv))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksOpv = remarksOpv;
	}

	public Integer getAgePcv6Weeks() {
		return this.agePcv6Weeks;
	}

	public void setAgePcv6Weeks(Integer agePcv6Weeks) {
	
		if (this.agePcv6Weeks==null || !this.agePcv6Weeks.equals(agePcv6Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.agePcv6Weeks = agePcv6Weeks;
	}

	public Integer getAgePcv10Weeks() {
		return this.agePcv10Weeks;
	}

	public void setAgePcv10Weeks(Integer agePcv10Weeks) {
	
		if (this.agePcv10Weeks==null || !this.agePcv10Weeks.equals(agePcv10Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.agePcv10Weeks = agePcv10Weeks;
	}

	public Integer getAgePcv9Months() {
		return this.agePcv9Months;
	}

	public void setAgePcv9Months(Integer agePcv9Months) {
	
		if (this.agePcv9Months==null || !this.agePcv9Months.equals(agePcv9Months))
		{
			editing = true;
			synced = false;
		}
	
		this.agePcv9Months = agePcv9Months;
	}

	public String getRemarksPcv() {
		return this.remarksPcv;
	}

	public void setRemarksPcv(String remarksPcv) {
	
		if (this.remarksPcv==null || !this.remarksPcv.equals(remarksPcv))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksPcv = remarksPcv;
	}

	public Integer getAgeIpv6Weeks() {
		return this.ageIpv6Weeks;
	}

	public void setAgeIpv6Weeks(Integer ageIpv6Weeks) {
	
		if (this.ageIpv6Weeks==null || !this.ageIpv6Weeks.equals(ageIpv6Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageIpv6Weeks = ageIpv6Weeks;
	}

	public Integer getAgeIpv14Weeks() {
		return this.ageIpv14Weeks;
	}

	public void setAgeIpv14Weeks(Integer ageIpv14Weeks) {
	
		if (this.ageIpv14Weeks==null || !this.ageIpv14Weeks.equals(ageIpv14Weeks))
		{
			editing = true;
			synced = false;
		}
	
		this.ageIpv14Weeks = ageIpv14Weeks;
	}

	public String getRemarksIpv() {
		return this.remarksIpv;
	}

	public void setRemarksIpv(String remarksIpv) {
	
		if (this.remarksIpv==null || !this.remarksIpv.equals(remarksIpv))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksIpv = remarksIpv;
	}

	public Integer getAgeMeaslesRubella9Months() {
		return this.ageMeaslesRubella9Months;
	}

	public void setAgeMeaslesRubella9Months(Integer ageMeaslesRubella9Months) {
	
		if (this.ageMeaslesRubella9Months==null || !this.ageMeaslesRubella9Months.equals(ageMeaslesRubella9Months))
		{
			editing = true;
			synced = false;
		}
	
		this.ageMeaslesRubella9Months = ageMeaslesRubella9Months;
	}

	public Integer getAgeMeaslesRubella15Months() {
		return this.ageMeaslesRubella15Months;
	}

	public void setAgeMeaslesRubella15Months(Integer ageMeaslesRubella15Months) {
	
		if (this.ageMeaslesRubella15Months==null || !this.ageMeaslesRubella15Months.equals(ageMeaslesRubella15Months))
		{
			editing = true;
			synced = false;
		}
	
		this.ageMeaslesRubella15Months = ageMeaslesRubella15Months;
	}

	public String getRemarksMeaslesRubella() {
		return this.remarksMeaslesRubella;
	}

	public void setRemarksMeaslesRubella(String remarksMeaslesRubella) {
	
		if (this.remarksMeaslesRubella==null || !this.remarksMeaslesRubella.equals(remarksMeaslesRubella))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksMeaslesRubella = remarksMeaslesRubella;
	}

	public Integer getAgeJapaneseEncephalitis12Months() {
		return this.ageJapaneseEncephalitis12Months;
	}

	public void setAgeJapaneseEncephalitis12Months(Integer ageJapaneseEncephalitis12Months) {
	
		if (this.ageJapaneseEncephalitis12Months==null || !this.ageJapaneseEncephalitis12Months.equals(ageJapaneseEncephalitis12Months))
		{
			editing = true;
			synced = false;
		}
	
		this.ageJapaneseEncephalitis12Months = ageJapaneseEncephalitis12Months;
	}

	public String getRemarksJapaneseEncephalitis() {
		return this.remarksJapaneseEncephalitis;
	}

	public void setRemarksJapaneseEncephalitis(String remarksJapaneseEncephalitis) {
	
		if (this.remarksJapaneseEncephalitis==null || !this.remarksJapaneseEncephalitis.equals(remarksJapaneseEncephalitis))
		{
			editing = true;
			synced = false;
		}
	
		this.remarksJapaneseEncephalitis = remarksJapaneseEncephalitis;
	}

	public String getAdviceBreastFeeding() {
		return this.adviceBreastFeeding;
	}

	public void setAdviceBreastFeeding(String adviceBreastFeeding) {
	
		if (this.adviceBreastFeeding==null || !this.adviceBreastFeeding.equals(adviceBreastFeeding))
		{
			editing = true;
			synced = false;
		}
	
		this.adviceBreastFeeding = adviceBreastFeeding;
	}

	public String getAdviceDentalHygiene() {
		return this.adviceDentalHygiene;
	}

	public void setAdviceDentalHygiene(String adviceDentalHygiene) {
	
		if (this.adviceDentalHygiene==null || !this.adviceDentalHygiene.equals(adviceDentalHygiene))
		{
			editing = true;
			synced = false;
		}
	
		this.adviceDentalHygiene = adviceDentalHygiene;
	}

	public String getAdviceToiletTraining() {
		return this.adviceToiletTraining;
	}

	public void setAdviceToiletTraining(String adviceToiletTraining) {
	
		if (this.adviceToiletTraining==null || !this.adviceToiletTraining.equals(adviceToiletTraining))
		{
			editing = true;
			synced = false;
		}
	
		this.adviceToiletTraining = adviceToiletTraining;
	}

	public String getAdviceComplementaryFeeding() {
		return this.adviceComplementaryFeeding;
	}

	public void setAdviceComplementaryFeeding(String adviceComplementaryFeeding) {
	
		if (this.adviceComplementaryFeeding==null || !this.adviceComplementaryFeeding.equals(adviceComplementaryFeeding))
		{
			editing = true;
			synced = false;
		}
	
		this.adviceComplementaryFeeding = adviceComplementaryFeeding;
	}

	public String getAdviceAccidentPrevention() {
		return this.adviceAccidentPrevention;
	}

	public void setAdviceAccidentPrevention(String adviceAccidentPrevention) {
	
		if (this.adviceAccidentPrevention==null || !this.adviceAccidentPrevention.equals(adviceAccidentPrevention))
		{
			editing = true;
			synced = false;
		}
	
		this.adviceAccidentPrevention = adviceAccidentPrevention;
	}

	public String getMdPhysical() {
		return this.mdPhysical;
	}

	public void setMdPhysical(String mdPhysical) {
	
		if (this.mdPhysical==null || !this.mdPhysical.equals(mdPhysical))
		{
			editing = true;
			synced = false;
		}
	
		this.mdPhysical = mdPhysical;
	}

	public String getMdVerbal() {
		return this.mdVerbal;
	}

	public void setMdVerbal(String mdVerbal) {
	
		if (this.mdVerbal==null || !this.mdVerbal.equals(mdVerbal))
		{
			editing = true;
			synced = false;
		}
	
		this.mdVerbal = mdVerbal;
	}

	public String getMdSocial() {
		return this.mdSocial;
	}

	public void setMdSocial(String mdSocial) {
	
		if (this.mdSocial==null || !this.mdSocial.equals(mdSocial))
		{
			editing = true;
			synced = false;
		}
	
		this.mdSocial = mdSocial;
	}

	public String getMdSpiritualReligious() {
		return this.mdSpiritualReligious;
	}

	public void setMdSpiritualReligious(String mdSpiritualReligious) {
	
		if (this.mdSpiritualReligious==null || !this.mdSpiritualReligious.equals(mdSpiritualReligious))
		{
			editing = true;
			synced = false;
		}
	
		this.mdSpiritualReligious = mdSpiritualReligious;
	}

	public String getMdMotor() {
		return this.mdMotor;
	}

	public void setMdMotor(String mdMotor) {
	
		if (this.mdMotor==null || !this.mdMotor.equals(mdMotor))
		{
			editing = true;
			synced = false;
		}
	
		this.mdMotor = mdMotor;
	}

	public String getMdIntellectual() {
		return this.mdIntellectual;
	}

	public void setMdIntellectual(String mdIntellectual) {
	
		if (this.mdIntellectual==null || !this.mdIntellectual.equals(mdIntellectual))
		{
			editing = true;
			synced = false;
		}
	
		this.mdIntellectual = mdIntellectual;
	}

	public String getMdEmotional() {
		return this.mdEmotional;
	}

	public void setMdEmotional(String mdEmotional) {
	
		if (this.mdEmotional==null || !this.mdEmotional.equals(mdEmotional))
		{
			editing = true;
			synced = false;
		}
	
		this.mdEmotional = mdEmotional;
	}

    
    
    @Override
	public String toString() {
		return "MotherAndChild{" +
				"uuid='" + uuid + '\'' +
				", editing=" + editing +
				", synced=" + synced +
				", createdAt=" + createdAt +
				", shineId='" + shineId + '\'' +
				", ageOfMenarche='" + ageOfMenarche + '\'' +
				", whereFirstMenarche='" + whereFirstMenarche + '\'' +
				", whereFirstMenarcheSpecify='" + whereFirstMenarcheSpecify + '\'' +
				", sanitaryMaterialsUsedPads='" + sanitaryMaterialsUsedPads + '\'' +
				", sanitaryMaterialsUsedTampon='" + sanitaryMaterialsUsedTampon + '\'' +
				", sanitaryMaterialsUsedClothes='" + sanitaryMaterialsUsedClothes + '\'' +
				", sanitaryMaterialsUsedHp='" + sanitaryMaterialsUsedHp + '\'' +
				", sanitaryMaterialsUsedMhp='" + sanitaryMaterialsUsedMhp + '\'' +
				", sanitaryMaterialsUsedNone='" + sanitaryMaterialsUsedNone + '\'' +
				", howOftenUsed='" + howOftenUsed + '\'' +
				", howOftenUsedSpecify='" + howOftenUsedSpecify + '\'' +
				", reasonNotTimely='" + reasonNotTimely + '\'' +
				", howDisposeMaterials='" + howDisposeMaterials + '\'' +
				", howDisposeMaterialsSpecify='" + howDisposeMaterialsSpecify + '\'' +
				", obstetricHistoryGravida='" + obstetricHistoryGravida + '\'' +
				", obstetricHistoryPara='" + obstetricHistoryPara + '\'' +
				", obstetricHistoryAbortion='" + obstetricHistoryAbortion + '\'' +
				", obstetricHistoryLiving='" + obstetricHistoryLiving + '\'' +
				", periodGestation='" + periodGestation + '\'' +
				", numAntenatalVisits='" + numAntenatalVisits + '\'' +
				", antenalServiceAncCheckUp='" + antenalServiceAncCheckUp + '\'' +
				", antenalServiceAlbendazole='" + antenalServiceAlbendazole + '\'' +
				", antenalServiceTdImmunization='" + antenalServiceTdImmunization + '\'' +
				", antenalServiceIrontabs='" + antenalServiceIrontabs + '\'' +
				", antenalServiceCouseling='" + antenalServiceCouseling + '\'' +
				", antenalServicePmtct='" + antenalServicePmtct + '\'' +
				", antenalServiceOthers='" + antenalServiceOthers + '\'' +
				", antenalServiceSpecify='" + antenalServiceSpecify + '\'' +
				", healthSeekingBehaviour='" + healthSeekingBehaviour + '\'' +
				", healthSeekingBehaviourSpecify='" + healthSeekingBehaviourSpecify + '\'' +
				", medicines='" + medicines + '\'' +
				", youngestBabyMonths='" + youngestBabyMonths + '\'' +
				", typeOfDelivery='" + typeOfDelivery + '\'' +
				", placeDeliveryYoungest='" + placeDeliveryYoungest + '\'' +
				", placeDeliveryYoungestSpecify='" + placeDeliveryYoungestSpecify + '\'' +
				", deliveryComplications='" + deliveryComplications + '\'' +
				", deliveryComplicationsSpecify='" + deliveryComplicationsSpecify + '\'' +
				", gapBetweenKids='" + gapBetweenKids + '\'' +
				", postNatalPe='" + postNatalPe + '\'' +
				", postNatalCounselBF='" + postNatalCounselBF + '\'' +
				", postNatalCounselFP='" + postNatalCounselFP + '\'' +
				", postNatalInvestigations='" + postNatalInvestigations + '\'' +
				", postNatalIronTablets='" + postNatalIronTablets + '\'' +
				", postNatalVitaminA='" + postNatalVitaminA + '\'' +
				", postNatalOthers='" + postNatalOthers + '\'' +
				", postNatalSpecify='" + postNatalSpecify + '\'' +
				", pncFollowupNone='" + pncFollowupNone + '\'' +
				", pncFollowupWithin24hr='" + pncFollowupWithin24hr + '\'' +
				", pncFollowupAt3rdDay='" + pncFollowupAt3rdDay + '\'' +
				", pncFollowupAt7thDay='" + pncFollowupAt7thDay + '\'' +
				", pncFollowupAt28thDay='" + pncFollowupAt28thDay + '\'' +
				", pncFollowupAt45thDay='" + pncFollowupAt45thDay + '\'' +
				", pncFollowupOthers='" + pncFollowupOthers + '\'' +
				", pncFollowupSpecify='" + pncFollowupSpecify + '\'' +
				", foodAvailableAfterDelivery='" + foodAvailableAfterDelivery + '\'' +
				", assessmentFoodQuality='" + assessmentFoodQuality + '\'' +
				", assessmentFoodFrequency='" + assessmentFoodFrequency + '\'' +
				", assessmentFoodDistribution='" + assessmentFoodDistribution + '\'' +
				", prelactatingFeeding='" + prelactatingFeeding + '\'' +
				", dietaryRestrictions='" + dietaryRestrictions + '\'' +
				", hadCounselingFamilyPlanning='" + hadCounselingFamilyPlanning + '\'' +
				", usedFamilyPlanningMethods='" + usedFamilyPlanningMethods + '\'' +
				", permanentVasectomy='" + permanentVasectomy + '\'' +
				", permanentMinilap='" + permanentMinilap + '\'' +
				", temporaryNatural='" + temporaryNatural + '\'' +
				", temporaryCondom='" + temporaryCondom + '\'' +
				", temporaryDepo='" + temporaryDepo + '\'' +
				", temporaryPills='" + temporaryPills + '\'' +
				", temporaryIucd='" + temporaryIucd + '\'' +
				", temporaryImplant='" + temporaryImplant + '\'' +
				", sourceMaternityInfo='" + sourceMaternityInfo + '\'' +
				", childUniqueId='" + childUniqueId + '\'' +
				", childName='" + childName + '\'' +
				", childSex='" + childSex + '\'' +
				", dateOfBirthInAd='" + dateOfBirthInAd + '\'' +
				", dateOfBirthInBs='" + dateOfBirthInBs + '\'' +
				", weightAtBirth='" + weightAtBirth + '\'' +
				", weightCurrent='" + weightCurrent + '\'' +
				", heightCm='" + heightCm + '\'' +
				", midArmCircumference='" + midArmCircumference + '\'' +
				", hasCongentialAnomaly='" + hasCongentialAnomaly + '\'' +
				", healthCondition='" + healthCondition + '\'' +
				", hadVitKInjection='" + hadVitKInjection + '\'' +
				", whenStartBreastFeed='" + whenStartBreastFeed + '\'' +
				", howOftenBreastFeed='" + howOftenBreastFeed + '\'' +
				", didExclusiveBreastFeed='" + didExclusiveBreastFeed + '\'' +
				", stoppedBreastFeedingMonths='" + stoppedBreastFeedingMonths + '\'' +
				", reasonDiscontinueBreastFeed='" + reasonDiscontinueBreastFeed + '\'' +
				", reasonDiscontinueBreastFeedSpecify='" + reasonDiscontinueBreastFeedSpecify + '\'' +
				", awareBreastFeedPreventsIllness='" + awareBreastFeedPreventsIllness + '\'' +
				", startedSemiSolidFood='" + startedSemiSolidFood + '\'' +
				", foodSupplementRicePudding='" + foodSupplementRicePudding + '\'' +
				", foodSupplementJaulo='" + foodSupplementJaulo + '\'' +
				", foodSupplementLito='" + foodSupplementLito + '\'' +
				", foodSupplementCerelac='" + foodSupplementCerelac + '\'' +
				", foodSupplementOthers='" + foodSupplementOthers + '\'' +
				", foodSupplementSpecify='" + foodSupplementSpecify + '\'' +
				", whyNoSupplement='" + whyNoSupplement + '\'' +
				", whyNoSupplementSpecify='" + whyNoSupplementSpecify + '\'' +
				", detailFoodToBaby='" + detailFoodToBaby + '\'' +
				", washHandsBreastfeed='" + washHandsBreastfeed + '\'' +
				", hasHandwashFacility='" + hasHandwashFacility + '\'' +
				", childFoodQuality='" + childFoodQuality + '\'' +
				", childFoodFrequency='" + childFoodFrequency + '\'' +
				", childFoodDistribution='" + childFoodDistribution + '\'' +
				", injuryChildFacedFalls='" + injuryChildFacedFalls + '\'' +
				", injuryChildFacedDrowning='" + injuryChildFacedDrowning + '\'' +
				", injuryChildFacedBurnsscald='" + injuryChildFacedBurnsscald + '\'' +
				", injuryChildFacedPoisoning='" + injuryChildFacedPoisoning + '\'' +
				", injuryChildFacedSuffocationchokingaspiration='" + injuryChildFacedSuffocationchokingaspiration + '\'' +
				", injuryChildFacedCutInjury='" + injuryChildFacedCutInjury + '\'' +
				", injuryChildFacedOthers='" + injuryChildFacedOthers + '\'' +
				", injuryChildFacedSpecify='" + injuryChildFacedSpecify + '\'' +
				", injuryCauseUnsafeHome='" + injuryCauseUnsafeHome + '\'' +
				", injuryCauseNoSuperVision='" + injuryCauseNoSuperVision + '\'' +
				", injuryCauseBusyMother='" + injuryCauseBusyMother + '\'' +
				", injuryCauseSlipperyFloor='" + injuryCauseSlipperyFloor + '\'' +
				", injuryCauseOthers='" + injuryCauseOthers + '\'' +
				", injuryCauseSpecify='" + injuryCauseSpecify + '\'' +
				", ageBcgAtBirth='" + ageBcgAtBirth + '\'' +
				", remarksBcg='" + remarksBcg + '\'' +
				", ageDptHepbHib6Weeks='" + ageDptHepbHib6Weeks + '\'' +
				", ageDptHepbHib10Weeks='" + ageDptHepbHib10Weeks + '\'' +
				", ageDptHepbHib14Weeks='" + ageDptHepbHib14Weeks + '\'' +
				", remarksDptHepbHib='" + remarksDptHepbHib + '\'' +
				", ageOpv6Weeks='" + ageOpv6Weeks + '\'' +
				", ageOpv10Weeks='" + ageOpv10Weeks + '\'' +
				", ageOpv14Weeks='" + ageOpv14Weeks + '\'' +
				", remarksOpv='" + remarksOpv + '\'' +
				", agePcv6Weeks='" + agePcv6Weeks + '\'' +
				", agePcv10Weeks='" + agePcv10Weeks + '\'' +
				", agePcv9Months='" + agePcv9Months + '\'' +
				", remarksPcv='" + remarksPcv + '\'' +
				", ageIpv6Weeks='" + ageIpv6Weeks + '\'' +
				", ageIpv14Weeks='" + ageIpv14Weeks + '\'' +
				", remarksIpv='" + remarksIpv + '\'' +
				", ageMeaslesRubella9Months='" + ageMeaslesRubella9Months + '\'' +
				", ageMeaslesRubella15Months='" + ageMeaslesRubella15Months + '\'' +
				", remarksMeaslesRubella='" + remarksMeaslesRubella + '\'' +
				", ageJapaneseEncephalitis12Months='" + ageJapaneseEncephalitis12Months + '\'' +
				", remarksJapaneseEncephalitis='" + remarksJapaneseEncephalitis + '\'' +
				", adviceBreastFeeding='" + adviceBreastFeeding + '\'' +
				", adviceDentalHygiene='" + adviceDentalHygiene + '\'' +
				", adviceToiletTraining='" + adviceToiletTraining + '\'' +
				", adviceComplementaryFeeding='" + adviceComplementaryFeeding + '\'' +
				", adviceAccidentPrevention='" + adviceAccidentPrevention + '\'' +
				", mdPhysical='" + mdPhysical + '\'' +
				", mdVerbal='" + mdVerbal + '\'' +
				", mdSocial='" + mdSocial + '\'' +
				", mdSpiritualReligious='" + mdSpiritualReligious + '\'' +
				", mdMotor='" + mdMotor + '\'' +
				", mdIntellectual='" + mdIntellectual + '\'' +
				", mdEmotional='" + mdEmotional + '\'' +

				'}';
	}
}
