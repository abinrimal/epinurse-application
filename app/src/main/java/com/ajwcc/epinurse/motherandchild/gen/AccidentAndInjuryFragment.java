package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_accident_and_injury)
public class AccidentAndInjuryFragment extends BaseEpinurseFragment {


    public AccidentAndInjuryFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedFalls;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedDrowning;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedBurnsscald;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedPoisoning;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedSuffocationchokingaspiration;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedCutInjury;

	@ViewById
	@MapToModelField
	protected CheckBox injuryChildFacedOthers;

	@ViewById
	@MapToModelField
	protected EditText injuryChildFacedSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox injuryCauseUnsafeHome;

	@ViewById
	@MapToModelField
	protected CheckBox injuryCauseNoSuperVision;

	@ViewById
	@MapToModelField
	protected CheckBox injuryCauseBusyMother;

	@ViewById
	@MapToModelField
	protected CheckBox injuryCauseSlipperyFloor;

	@ViewById
	@MapToModelField
	protected CheckBox injuryCauseOthers;

	@ViewById
	@MapToModelField
	protected EditText injuryCauseSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(injuryChildFacedFalls,pojo.getInjuryChildFacedFalls());
					PojoToViewMapper.setViewValue(injuryChildFacedDrowning,pojo.getInjuryChildFacedDrowning());
					PojoToViewMapper.setViewValue(injuryChildFacedBurnsscald,pojo.getInjuryChildFacedBurnsscald());
					PojoToViewMapper.setViewValue(injuryChildFacedPoisoning,pojo.getInjuryChildFacedPoisoning());
					PojoToViewMapper.setViewValue(injuryChildFacedSuffocationchokingaspiration,pojo.getInjuryChildFacedSuffocationchokingaspiration());
					PojoToViewMapper.setViewValue(injuryChildFacedCutInjury,pojo.getInjuryChildFacedCutInjury());
					PojoToViewMapper.setViewValue(injuryChildFacedOthers,pojo.getInjuryChildFacedOthers());
					PojoToViewMapper.setViewValue(injuryChildFacedSpecify,pojo.getInjuryChildFacedSpecify());
					PojoToViewMapper.setViewValue(injuryCauseUnsafeHome,pojo.getInjuryCauseUnsafeHome());
					PojoToViewMapper.setViewValue(injuryCauseNoSuperVision,pojo.getInjuryCauseNoSuperVision());
					PojoToViewMapper.setViewValue(injuryCauseBusyMother,pojo.getInjuryCauseBusyMother());
					PojoToViewMapper.setViewValue(injuryCauseSlipperyFloor,pojo.getInjuryCauseSlipperyFloor());
					PojoToViewMapper.setViewValue(injuryCauseOthers,pojo.getInjuryCauseOthers());
					PojoToViewMapper.setViewValue(injuryCauseSpecify,pojo.getInjuryCauseSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setInjuryChildFacedFalls((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedFalls));
					pojo.setInjuryChildFacedDrowning((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedDrowning));
					pojo.setInjuryChildFacedBurnsscald((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedBurnsscald));
					pojo.setInjuryChildFacedPoisoning((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedPoisoning));
					pojo.setInjuryChildFacedSuffocationchokingaspiration((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedSuffocationchokingaspiration));
					pojo.setInjuryChildFacedCutInjury((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedCutInjury));
					pojo.setInjuryChildFacedOthers((Integer) ViewToPojoMapper.getValueByView(injuryChildFacedOthers));
					pojo.setInjuryChildFacedSpecify((String) ViewToPojoMapper.getValueByView(injuryChildFacedSpecify));
					pojo.setInjuryCauseUnsafeHome((Integer) ViewToPojoMapper.getValueByView(injuryCauseUnsafeHome));
					pojo.setInjuryCauseNoSuperVision((Integer) ViewToPojoMapper.getValueByView(injuryCauseNoSuperVision));
					pojo.setInjuryCauseBusyMother((Integer) ViewToPojoMapper.getValueByView(injuryCauseBusyMother));
					pojo.setInjuryCauseSlipperyFloor((Integer) ViewToPojoMapper.getValueByView(injuryCauseSlipperyFloor));
					pojo.setInjuryCauseOthers((Integer) ViewToPojoMapper.getValueByView(injuryCauseOthers));
					pojo.setInjuryCauseSpecify((String) ViewToPojoMapper.getValueByView(injuryCauseSpecify));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.injuryChildFacedOthers,R.id.injuryCauseOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.injuryChildFacedOthers:
				 UiUtils.toggleSpecify(view, injuryChildFacedSpecify);
				break;
			case R.id.injuryCauseOthers:
				 UiUtils.toggleSpecify(view, injuryCauseSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
