package com.ajwcc.epinurse.motherandchild.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChild;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChildActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import java.util.List;

public class MotherAndChildValidator extends com.ajwcc.epinurse.motherandchild.gen.MotherAndChildValidator
{

    public MotherAndChildValidator(MotherAndChildActivity a)
    {
        super(a);
    }

    @Override
    public void validateModel()
    {
        // check if owenerUuid has been set
        validateNonNullField(model.getOwnerUuid(), activity.getPage("Owner"), R.id.button, context.getResources().getString(R.string.basic_information_profile));  // TODO: USE RES


        super.validateModel();
    }

    public boolean isAnyFoodSupplementSet(MotherAndChild model)
    {
        return (model.getFoodSupplementRicePudding()==1
                || model.getFoodSupplementJaulo()==1
                || model.getFoodSupplementLito()==1
                || model.getFoodSupplementCerelac()==1
                || model.getFoodSupplementOthers()==1);
    }

    public void validateWhyNoSupplement()
    {
        if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25) || isAnyFoodSupplementSet(model)))
        {
            // optional -- validation not required
            return;
        }
        validateNonNullField(model.getWhyNoSupplement(), activity.getPage("Child Breast Feeding"), R.id.whyNoSupplementContainer, context.getResources().getString(R.string.mother_and_child_whyNoSupplement));
    }

    public void validateWhyNoSupplementSpecify()
    {
        if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25) || isAnyFoodSupplementSet(model)))
        {
            // optional -- validation not required
            return;
        }
        validateNonNullSpecifyField(model.getWhyNoSupplement(),3,model.getWhyNoSupplementSpecify(), activity.getPage("Child Breast Feeding"), R.id.whyNoSupplementSpecifyContainer, context.getResources().getString(R.string.mother_and_child_whyNoSupplementSpecify));
    }






    public void validateMedicines()
    {
        // need to check if all fields are non-empty
        List<MedicineEntry> medicineEntries = model.getMedicines();

        int page = activity.getPage("Medicines");
        int viewId = R.id.medicinesContainer;

        for (MedicineEntry me : medicineEntries)
        {
            if (me.getMedicine().equals(""))
            {
//                throw new ValidationHandler.ValidationException("Medicine must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_medicine_medicine)),
                                                                              page, viewId);  // TODO: USE RES
            }

            if (me.getPrescribed()==null)
            {
//                throw new ValidationHandler.ValidationException("Prescribed must have a value", page, viewId);   // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_medicine_prescribed)),
                                                                              page, viewId);  // TODO: USE RES
            }

            if (me.getStock()==null)
            {
//                throw new ValidationHandler.ValidationException("Stock must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_medicine_stock)),
                                                                              page, viewId);  // TODO: USE RES
            }

        }
    }




}
