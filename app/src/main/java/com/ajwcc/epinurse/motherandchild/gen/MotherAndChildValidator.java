package com.ajwcc.epinurse.motherandchild.gen;

import com.ajwcc.epinurse.common.BaseValidator;
import com.ajwcc.util.ui.validation.ValidationHandler;
import com.ajwcc.epinurse.R;

public class MotherAndChildValidator extends BaseValidator implements ValidationHandler<MotherAndChild>
{
    protected MotherAndChild model;
    protected MotherAndChildActivity activity;


    public MotherAndChildValidator(MotherAndChildActivity a)
    {
    	super(a);
        activity = a;
    }

    @Override
    public void setModel(MotherAndChild model)
    {
        this.model = model;
    }

    @Override
    public void validateModel()
    {
		validateAgeOfMenarche();
		validateWhereFirstMenarche();
		validateWhereFirstMenarcheSpecify();
		validateSanitaryMaterialsUsedPads();
		validateSanitaryMaterialsUsedTampon();
		validateSanitaryMaterialsUsedClothes();
		validateSanitaryMaterialsUsedHp();
		validateSanitaryMaterialsUsedMhp();
		validateSanitaryMaterialsUsedNone();
		validateHowOftenUsed();
		validateHowOftenUsedSpecify();
		validateReasonNotTimely();
		validateHowDisposeMaterials();
		validateHowDisposeMaterialsSpecify();
		validateObstetricHistoryGravida();
		validateObstetricHistoryPara();
		validateObstetricHistoryAbortion();
		validateObstetricHistoryLiving();
		//  periodGestation optional
		validateNumAntenatalVisits();
		validateAntenalServiceAncCheckUp();
		validateAntenalServiceAlbendazole();
		validateAntenalServiceTdImmunization();
		validateAntenalServiceIrontabs();
		validateAntenalServiceCouseling();
		validateAntenalServicePmtct();
		validateAntenalServiceOthers();
		validateAntenalServiceSpecify();
		validateHealthSeekingBehaviour();
		validateHealthSeekingBehaviourSpecify();
		validateMedicines();
		validateYoungestBabyMonths();
		validateTypeOfDelivery();
		validatePlaceDeliveryYoungest();
		validatePlaceDeliveryYoungestSpecify();
		validateDeliveryComplications();
		validateDeliveryComplicationsSpecify();
		validateGapBetweenKids();
		validatePostNatalPe();
		validatePostNatalCounselBF();
		validatePostNatalCounselFP();
		validatePostNatalInvestigations();
		validatePostNatalIronTablets();
		validatePostNatalVitaminA();
		validatePostNatalOthers();
		validatePostNatalSpecify();
		validatePncFollowupNone();
		validatePncFollowupWithin24hr();
		validatePncFollowupAt3rdDay();
		validatePncFollowupAt7thDay();
		validatePncFollowupAt28thDay();
		validatePncFollowupAt45thDay();
		validatePncFollowupOthers();
		validatePncFollowupSpecify();
		validateFoodAvailableAfterDelivery();
		validateAssessmentFoodQuality();
		validateAssessmentFoodFrequency();
		validateAssessmentFoodDistribution();
		validatePrelactatingFeeding();
		validateDietaryRestrictions();
		validateHadCounselingFamilyPlanning();
		validateUsedFamilyPlanningMethods();
		validatePermanentVasectomy();
		validatePermanentMinilap();
		validateTemporaryNatural();
		validateTemporaryCondom();
		validateTemporaryDepo();
		validateTemporaryPills();
		validateTemporaryIucd();
		validateTemporaryImplant();
		validateSourceMaternityInfo();
		validateChildUniqueId();
		validateChildName();
		validateChildSex();
		validateDateOfBirthInAd();
		validateDateOfBirthInBs();
		validateWeightAtBirth();
		validateWeightCurrent();
		validateHeightCm();
		validateMidArmCircumference();
		validateHasCongentialAnomaly();
		validateHealthCondition();
		validateHadVitKInjection();
		validateWhenStartBreastFeed();
		validateHowOftenBreastFeed();
		validateDidExclusiveBreastFeed();
		validateStoppedBreastFeedingMonths();
		validateReasonDiscontinueBreastFeed();
		validateReasonDiscontinueBreastFeedSpecify();
		validateAwareBreastFeedPreventsIllness();
		validateStartedSemiSolidFood();
		validateFoodSupplementRicePudding();
		validateFoodSupplementJaulo();
		validateFoodSupplementLito();
		validateFoodSupplementCerelac();
		validateFoodSupplementOthers();
		validateFoodSupplementSpecify();
		validateWhyNoSupplement();
		validateWhyNoSupplementSpecify();
		validateDetailFoodToBaby();
		validateWashHandsBreastfeed();
		validateHasHandwashFacility();
		validateChildFoodQuality();
		validateChildFoodFrequency();
		validateChildFoodDistribution();
		validateInjuryChildFacedFalls();
		validateInjuryChildFacedDrowning();
		validateInjuryChildFacedBurnsscald();
		validateInjuryChildFacedPoisoning();
		validateInjuryChildFacedSuffocationchokingaspiration();
		validateInjuryChildFacedCutInjury();
		validateInjuryChildFacedOthers();
		validateInjuryChildFacedSpecify();
		validateInjuryCauseUnsafeHome();
		validateInjuryCauseNoSuperVision();
		validateInjuryCauseBusyMother();
		validateInjuryCauseSlipperyFloor();
		validateInjuryCauseOthers();
		validateInjuryCauseSpecify();
		validateAgeBcgAtBirth();
		//  remarksBcg optional
		validateAgeDptHepbHib6Weeks();
		validateAgeDptHepbHib10Weeks();
		validateAgeDptHepbHib14Weeks();
		//  remarksDptHepbHib optional
		validateAgeOpv6Weeks();
		validateAgeOpv10Weeks();
		validateAgeOpv14Weeks();
		//  remarksOpv optional
		validateAgePcv6Weeks();
		validateAgePcv10Weeks();
		validateAgePcv9Months();
		//  remarksPcv optional
		validateAgeIpv6Weeks();
		validateAgeIpv14Weeks();
		//  remarksIpv optional
		validateAgeMeaslesRubella9Months();
		validateAgeMeaslesRubella15Months();
		//  remarksMeaslesRubella optional
		validateAgeJapaneseEncephalitis12Months();
		//  remarksJapaneseEncephalitis optional
		validateAdviceBreastFeeding();
		validateAdviceDentalHygiene();
		validateAdviceToiletTraining();
		validateAdviceComplementaryFeeding();
		validateAdviceAccidentPrevention();
		validateMdPhysical();
		validateMdVerbal();
		validateMdSocial();
		validateMdSpiritualReligious();
		validateMdMotor();
		validateMdIntellectual();
		validateMdEmotional();

    }


    public void validateAgeOfMenarche()
    {
    
        validateNonNullField(model.getAgeOfMenarche(), activity.getPage("Menstruation"), R.id.ageOfMenarcheContainer, context.getResources().getString(R.string.mother_and_child_ageOfMenarche));
    }

    public void validateWhereFirstMenarche()
    {
    
        validateNonNullField(model.getWhereFirstMenarche(), activity.getPage("Menstruation"), R.id.whereFirstMenarcheContainer, context.getResources().getString(R.string.mother_and_child_whereFirstMenarche));
    }

    public void validateWhereFirstMenarcheSpecify()
    {
  
        validateNonNullSpecifyField(model.getWhereFirstMenarche(),3,model.getWhereFirstMenarcheSpecify(), activity.getPage("Menstruation"), R.id.whereFirstMenarcheSpecifyContainer, context.getResources().getString(R.string.mother_and_child_whereFirstMenarcheSpecify));   
    }

    public void validateSanitaryMaterialsUsedPads()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedPads(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedPadsContainer, "@drawable/pads");
    }

    public void validateSanitaryMaterialsUsedTampon()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedTampon(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedTamponContainer, "@drawable/tampon");
    }

    public void validateSanitaryMaterialsUsedClothes()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedClothes(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedClothesContainer, context.getResources().getString(R.string.mother_and_child_sanitaryMaterialsUsedClothes));
    }

    public void validateSanitaryMaterialsUsedHp()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedHp(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedHpContainer, context.getResources().getString(R.string.mother_and_child_sanitaryMaterialsUsedHp));
    }

    public void validateSanitaryMaterialsUsedMhp()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedMhp(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedMhpContainer, context.getResources().getString(R.string.mother_and_child_sanitaryMaterialsUsedMhp));
    }

    public void validateSanitaryMaterialsUsedNone()
    {
    
        validateNonNullField(model.getSanitaryMaterialsUsedNone(), activity.getPage("Menstruation"), R.id.sanitaryMaterialsUsedNoneContainer, context.getResources().getString(R.string.mother_and_child_sanitaryMaterialsUsedNone));
    }

    public void validateHowOftenUsed()
    {
    
        validateNonNullField(model.getHowOftenUsed(), activity.getPage("Menstruation"), R.id.howOftenUsedContainer, context.getResources().getString(R.string.mother_and_child_howOftenUsed));
    }

    public void validateHowOftenUsedSpecify()
    {
  
        validateNonNullSpecifyField(model.getHowOftenUsed(),4,model.getHowOftenUsedSpecify(), activity.getPage("Menstruation"), R.id.howOftenUsedSpecifyContainer, context.getResources().getString(R.string.mother_and_child_howOftenUsedSpecify));   
    }

    public void validateReasonNotTimely()
    {
		if((model.getHowOftenUsed()==null || !(model.getHowOftenUsed()==2 || model.getHowOftenUsed()==3 || model.getHowOftenUsed()==4)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getReasonNotTimely(), activity.getPage("Menstruation"), R.id.reasonNotTimelyContainer, context.getResources().getString(R.string.mother_and_child_reasonNotTimely));
    }

    public void validateHowDisposeMaterials()
    {
    
        validateNonNullField(model.getHowDisposeMaterials(), activity.getPage("Menstruation"), R.id.howDisposeMaterialsContainer, context.getResources().getString(R.string.mother_and_child_howDisposeMaterials));
    }

    public void validateHowDisposeMaterialsSpecify()
    {
  
        validateNonNullSpecifyField(model.getHowDisposeMaterials(),4,model.getHowDisposeMaterialsSpecify(), activity.getPage("Menstruation"), R.id.howDisposeMaterialsSpecifyContainer, context.getResources().getString(R.string.mother_and_child_howDisposeMaterialsSpecify));   
    }

    public void validateObstetricHistoryGravida()
    {
    
        validateNonNullField(model.getObstetricHistoryGravida(), activity.getPage("Pregnancy"), R.id.obstetricHistoryGravidaContainer, context.getResources().getString(R.string.mother_and_child_obstetricHistoryGravida));
    }

    public void validateObstetricHistoryPara()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>0)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getObstetricHistoryPara(), activity.getPage("Pregnancy"), R.id.obstetricHistoryParaContainer, context.getResources().getString(R.string.mother_and_child_obstetricHistoryPara));
    }

    public void validateObstetricHistoryAbortion()
    {
    
        validateNonNullField(model.getObstetricHistoryAbortion(), activity.getPage("Pregnancy"), R.id.obstetricHistoryAbortionContainer, context.getResources().getString(R.string.mother_and_child_obstetricHistoryAbortion));
    }

    public void validateObstetricHistoryLiving()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>0)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getObstetricHistoryLiving(), activity.getPage("Pregnancy"), R.id.obstetricHistoryLivingContainer, context.getResources().getString(R.string.mother_and_child_obstetricHistoryLiving));
    }

    public void validateNumAntenatalVisits()
    {
    
        validateNonNullField(model.getNumAntenatalVisits(), activity.getPage("Pregnancy"), R.id.numAntenatalVisitsContainer, context.getResources().getString(R.string.mother_and_child_numAntenatalVisits));
    }

    public void validateAntenalServiceAncCheckUp()
    {
    
        validateNonNullField(model.getAntenalServiceAncCheckUp(), activity.getPage("Pregnancy"), R.id.antenalServiceAncCheckUpContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceAncCheckUp));
    }

    public void validateAntenalServiceAlbendazole()
    {
    
        validateNonNullField(model.getAntenalServiceAlbendazole(), activity.getPage("Pregnancy"), R.id.antenalServiceAlbendazoleContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceAlbendazole));
    }

    public void validateAntenalServiceTdImmunization()
    {
    
        validateNonNullField(model.getAntenalServiceTdImmunization(), activity.getPage("Pregnancy"), R.id.antenalServiceTdImmunizationContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceTdImmunization));
    }

    public void validateAntenalServiceIrontabs()
    {
    
        validateNonNullField(model.getAntenalServiceIrontabs(), activity.getPage("Pregnancy"), R.id.antenalServiceIrontabsContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceIrontabs));
    }

    public void validateAntenalServiceCouseling()
    {
    
        validateNonNullField(model.getAntenalServiceCouseling(), activity.getPage("Pregnancy"), R.id.antenalServiceCouselingContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceCouseling));
    }

    public void validateAntenalServicePmtct()
    {
    
        validateNonNullField(model.getAntenalServicePmtct(), activity.getPage("Pregnancy"), R.id.antenalServicePmtctContainer, context.getResources().getString(R.string.mother_and_child_antenalServicePmtct));
    }

    public void validateAntenalServiceOthers()
    {
    
        validateNonNullField(model.getAntenalServiceOthers(), activity.getPage("Pregnancy"), R.id.antenalServiceOthersContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceOthers));
    }

    public void validateAntenalServiceSpecify()
    {
  
        validateNonNullSpecifyField(model.getAntenalServiceOthers(),1,model.getAntenalServiceSpecify(), activity.getPage("Pregnancy"), R.id.antenalServiceSpecifyContainer, context.getResources().getString(R.string.mother_and_child_antenalServiceSpecify));   
    }

    public void validateHealthSeekingBehaviour()
    {
    
        validateNonNullField(model.getHealthSeekingBehaviour(), activity.getPage("Pregnancy"), R.id.healthSeekingBehaviourContainer, context.getResources().getString(R.string.mother_and_child_healthSeekingBehaviour));
    }

    public void validateHealthSeekingBehaviourSpecify()
    {
  
        validateNonNullSpecifyField(model.getHealthSeekingBehaviour(),4,model.getHealthSeekingBehaviourSpecify(), activity.getPage("Pregnancy"), R.id.healthSeekingBehaviourSpecifyContainer, context.getResources().getString(R.string.mother_and_child_healthSeekingBehaviourSpecify));   
    }

    public void validateMedicines()
    {
    
        validateNonNullField(model.getMedicines(), activity.getPage("Pregnancy"), R.id.medicinesContainer, context.getResources().getString(R.string.mother_and_child_medicines));
    }

    public void validateYoungestBabyMonths()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getYoungestBabyMonths(), activity.getPage("Childbirth"), R.id.youngestBabyMonthsContainer, context.getResources().getString(R.string.mother_and_child_youngestBabyMonths));
    }

    public void validateTypeOfDelivery()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getTypeOfDelivery(), activity.getPage("Childbirth"), R.id.typeOfDeliveryContainer, context.getResources().getString(R.string.mother_and_child_typeOfDelivery));
    }

    public void validatePlaceDeliveryYoungest()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPlaceDeliveryYoungest(), activity.getPage("Childbirth"), R.id.placeDeliveryYoungestContainer, context.getResources().getString(R.string.mother_and_child_placeDeliveryYoungest));
    }

    public void validatePlaceDeliveryYoungestSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getPlaceDeliveryYoungest(),3,model.getPlaceDeliveryYoungestSpecify(), activity.getPage("Childbirth"), R.id.placeDeliveryYoungestSpecifyContainer, context.getResources().getString(R.string.mother_and_child_placeDeliveryYoungestSpecify));   
    }

    public void validateDeliveryComplications()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getDeliveryComplications(), activity.getPage("Childbirth"), R.id.deliveryComplicationsContainer, context.getResources().getString(R.string.mother_and_child_deliveryComplications));
    }

    public void validateDeliveryComplicationsSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getDeliveryComplications(),1,model.getDeliveryComplicationsSpecify(), activity.getPage("Childbirth"), R.id.deliveryComplicationsSpecifyContainer, context.getResources().getString(R.string.mother_and_child_deliveryComplicationsSpecify));   
    }

    public void validateGapBetweenKids()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getGapBetweenKids(), activity.getPage("Childbirth"), R.id.gapBetweenKidsContainer, context.getResources().getString(R.string.mother_and_child_gapBetweenKids));
    }

    public void validatePostNatalPe()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalPe(), activity.getPage("Post Natal"), R.id.postNatalPeContainer, context.getResources().getString(R.string.mother_and_child_postNatalPe));
    }

    public void validatePostNatalCounselBF()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalCounselBF(), activity.getPage("Post Natal"), R.id.postNatalCounselBFContainer, context.getResources().getString(R.string.mother_and_child_postNatalCounselBF));
    }

    public void validatePostNatalCounselFP()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalCounselFP(), activity.getPage("Post Natal"), R.id.postNatalCounselFPContainer, context.getResources().getString(R.string.mother_and_child_postNatalCounselFP));
    }

    public void validatePostNatalInvestigations()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalInvestigations(), activity.getPage("Post Natal"), R.id.postNatalInvestigationsContainer, context.getResources().getString(R.string.mother_and_child_postNatalInvestigations));
    }

    public void validatePostNatalIronTablets()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalIronTablets(), activity.getPage("Post Natal"), R.id.postNatalIronTabletsContainer, context.getResources().getString(R.string.mother_and_child_postNatalIronTablets));
    }

    public void validatePostNatalVitaminA()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalVitaminA(), activity.getPage("Post Natal"), R.id.postNatalVitaminAContainer, context.getResources().getString(R.string.mother_and_child_postNatalVitaminA));
    }

    public void validatePostNatalOthers()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPostNatalOthers(), activity.getPage("Post Natal"), R.id.postNatalOthersContainer, context.getResources().getString(R.string.mother_and_child_postNatalOthers));
    }

    public void validatePostNatalSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getPostNatalOthers(),1,model.getPostNatalSpecify(), activity.getPage("Post Natal"), R.id.postNatalSpecifyContainer, context.getResources().getString(R.string.mother_and_child_postNatalSpecify));   
    }

    public void validatePncFollowupNone()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupNone(), activity.getPage("Post Natal"), R.id.pncFollowupNoneContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupNone));
    }

    public void validatePncFollowupWithin24hr()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupWithin24hr(), activity.getPage("Post Natal"), R.id.pncFollowupWithin24hrContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupWithin24hr));
    }

    public void validatePncFollowupAt3rdDay()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupAt3rdDay(), activity.getPage("Post Natal"), R.id.pncFollowupAt3rdDayContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupAt3rdDay));
    }

    public void validatePncFollowupAt7thDay()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupAt7thDay(), activity.getPage("Post Natal"), R.id.pncFollowupAt7thDayContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupAt7thDay));
    }

    public void validatePncFollowupAt28thDay()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupAt28thDay(), activity.getPage("Post Natal"), R.id.pncFollowupAt28thDayContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupAt28thDay));
    }

    public void validatePncFollowupAt45thDay()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupAt45thDay(), activity.getPage("Post Natal"), R.id.pncFollowupAt45thDayContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupAt45thDay));
    }

    public void validatePncFollowupOthers()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPncFollowupOthers(), activity.getPage("Post Natal"), R.id.pncFollowupOthersContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupOthers));
    }

    public void validatePncFollowupSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getPncFollowupOthers(),1,model.getPncFollowupSpecify(), activity.getPage("Post Natal"), R.id.pncFollowupSpecifyContainer, context.getResources().getString(R.string.mother_and_child_pncFollowupSpecify));   
    }

    public void validateFoodAvailableAfterDelivery()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodAvailableAfterDelivery(), activity.getPage("Post Natal"), R.id.foodAvailableAfterDeliveryContainer, context.getResources().getString(R.string.mother_and_child_foodAvailableAfterDelivery));
    }

    public void validateAssessmentFoodQuality()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getAssessmentFoodQuality(), activity.getPage("Post Natal"), R.id.assessmentFoodQualityContainer, context.getResources().getString(R.string.mother_and_child_assessmentFoodQuality));
    }

    public void validateAssessmentFoodFrequency()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getAssessmentFoodFrequency(), activity.getPage("Post Natal"), R.id.assessmentFoodFrequencyContainer, context.getResources().getString(R.string.mother_and_child_assessmentFoodFrequency));
    }

    public void validateAssessmentFoodDistribution()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getAssessmentFoodDistribution(), activity.getPage("Post Natal"), R.id.assessmentFoodDistributionContainer, context.getResources().getString(R.string.mother_and_child_assessmentFoodDistribution));
    }

    public void validatePrelactatingFeeding()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getPrelactatingFeeding(), activity.getPage("Post Natal"), R.id.prelactatingFeedingContainer, context.getResources().getString(R.string.mother_and_child_prelactatingFeeding));
    }

    public void validateDietaryRestrictions()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getDietaryRestrictions(), activity.getPage("Post Natal"), R.id.dietaryRestrictionsContainer, context.getResources().getString(R.string.mother_and_child_dietaryRestrictions));
    }

    public void validateHadCounselingFamilyPlanning()
    {
    
        validateNonNullField(model.getHadCounselingFamilyPlanning(), activity.getPage("Family Planning"), R.id.hadCounselingFamilyPlanningContainer, context.getResources().getString(R.string.mother_and_child_hadCounselingFamilyPlanning));
    }

    public void validateUsedFamilyPlanningMethods()
    {
    
        validateNonNullField(model.getUsedFamilyPlanningMethods(), activity.getPage("Family Planning"), R.id.usedFamilyPlanningMethodsContainer, context.getResources().getString(R.string.mother_and_child_usedFamilyPlanningMethods));
    }

    public void validatePermanentVasectomy()
    {
    
        validateNonNullField(model.getPermanentVasectomy(), activity.getPage("Family Planning"), R.id.permanentVasectomyContainer, context.getResources().getString(R.string.mother_and_child_permanentVasectomy));
    }

    public void validatePermanentMinilap()
    {
    
        validateNonNullField(model.getPermanentMinilap(), activity.getPage("Family Planning"), R.id.permanentMinilapContainer, context.getResources().getString(R.string.mother_and_child_permanentMinilap));
    }

    public void validateTemporaryNatural()
    {
    
        validateNonNullField(model.getTemporaryNatural(), activity.getPage("Family Planning"), R.id.temporaryNaturalContainer, context.getResources().getString(R.string.mother_and_child_temporaryNatural));
    }

    public void validateTemporaryCondom()
    {
    
        validateNonNullField(model.getTemporaryCondom(), activity.getPage("Family Planning"), R.id.temporaryCondomContainer, context.getResources().getString(R.string.mother_and_child_temporaryCondom));
    }

    public void validateTemporaryDepo()
    {
    
        validateNonNullField(model.getTemporaryDepo(), activity.getPage("Family Planning"), R.id.temporaryDepoContainer, context.getResources().getString(R.string.mother_and_child_temporaryDepo));
    }

    public void validateTemporaryPills()
    {
    
        validateNonNullField(model.getTemporaryPills(), activity.getPage("Family Planning"), R.id.temporaryPillsContainer, context.getResources().getString(R.string.mother_and_child_temporaryPills));
    }

    public void validateTemporaryIucd()
    {
    
        validateNonNullField(model.getTemporaryIucd(), activity.getPage("Family Planning"), R.id.temporaryIucdContainer, context.getResources().getString(R.string.mother_and_child_temporaryIucd));
    }

    public void validateTemporaryImplant()
    {
    
        validateNonNullField(model.getTemporaryImplant(), activity.getPage("Family Planning"), R.id.temporaryImplantContainer, context.getResources().getString(R.string.mother_and_child_temporaryImplant));
    }

    public void validateSourceMaternityInfo()
    {
    
        validateNonNullField(model.getSourceMaternityInfo(), activity.getPage("Family Planning"), R.id.sourceMaternityInfoContainer, context.getResources().getString(R.string.mother_and_child_sourceMaternityInfo));
    }

    public void validateChildUniqueId()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getChildUniqueId(), activity.getPage("Child Basic Information"), R.id.childUniqueIdContainer, context.getResources().getString(R.string.mother_and_child_childUniqueId));
    }

    public void validateChildName()
    {
    
        validateNonNullField(model.getChildName(), activity.getPage("Child Basic Information"), R.id.childNameContainer, context.getResources().getString(R.string.mother_and_child_childName));
    }

    public void validateChildSex()
    {
    
        validateNonNullField(model.getChildSex(), activity.getPage("Child Basic Information"), R.id.childSexContainer, context.getResources().getString(R.string.mother_and_child_childSex));
    }

    public void validateDateOfBirthInAd()
    {
    
        validateNonNullField(model.getDateOfBirthInAd(), activity.getPage("Child Basic Information"), R.id.dateOfBirthInAdContainer, context.getResources().getString(R.string.mother_and_child_dateOfBirthInAd));
    }

    public void validateDateOfBirthInBs()
    {
    
        validateNonNullField(model.getDateOfBirthInBs(), activity.getPage("Child Basic Information"), R.id.dateOfBirthInBsContainer, context.getResources().getString(R.string.mother_and_child_dateOfBirthInBs));
    }

    public void validateWeightAtBirth()
    {
    
        validateNonNullField(model.getWeightAtBirth(), activity.getPage("Child Basic Information"), R.id.weightAtBirthContainer, context.getResources().getString(R.string.mother_and_child_weightAtBirth));
    }

    public void validateWeightCurrent()
    {
    
        validateNonNullField(model.getWeightCurrent(), activity.getPage("Child Basic Information"), R.id.weightCurrentContainer, context.getResources().getString(R.string.mother_and_child_weightCurrent));
    }

    public void validateHeightCm()
    {
    
        validateNonNullField(model.getHeightCm(), activity.getPage("Child Basic Information"), R.id.heightCmContainer, context.getResources().getString(R.string.mother_and_child_heightCm));
    }

    public void validateMidArmCircumference()
    {
    
        validateNonNullField(model.getMidArmCircumference(), activity.getPage("Child Basic Information"), R.id.midArmCircumferenceContainer, context.getResources().getString(R.string.mother_and_child_midArmCircumference));
    }

    public void validateHasCongentialAnomaly()
    {
    
        validateNonNullField(model.getHasCongentialAnomaly(), activity.getPage("Child Basic Information"), R.id.hasCongentialAnomalyContainer, context.getResources().getString(R.string.mother_and_child_hasCongentialAnomaly));
    }

    public void validateHealthCondition()
    {
    
        validateNonNullField(model.getHealthCondition(), activity.getPage("Child Basic Information"), R.id.healthConditionContainer, context.getResources().getString(R.string.mother_and_child_healthCondition));
    }

    public void validateHadVitKInjection()
    {
    
        validateNonNullField(model.getHadVitKInjection(), activity.getPage("Child Basic Information"), R.id.hadVitKInjectionContainer, context.getResources().getString(R.string.mother_and_child_hadVitKInjection));
    }

    public void validateWhenStartBreastFeed()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWhenStartBreastFeed(), activity.getPage("Child Breast Feeding"), R.id.whenStartBreastFeedContainer, context.getResources().getString(R.string.mother_and_child_whenStartBreastFeed));
    }

    public void validateHowOftenBreastFeed()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getHowOftenBreastFeed(), activity.getPage("Child Breast Feeding"), R.id.howOftenBreastFeedContainer, context.getResources().getString(R.string.mother_and_child_howOftenBreastFeed));
    }

    public void validateDidExclusiveBreastFeed()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getDidExclusiveBreastFeed(), activity.getPage("Child Breast Feeding"), R.id.didExclusiveBreastFeedContainer, context.getResources().getString(R.string.mother_and_child_didExclusiveBreastFeed));
    }

    public void validateStoppedBreastFeedingMonths()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getStoppedBreastFeedingMonths(), activity.getPage("Child Breast Feeding"), R.id.stoppedBreastFeedingMonthsContainer, context.getResources().getString(R.string.mother_and_child_stoppedBreastFeedingMonths));
    }

    public void validateReasonDiscontinueBreastFeed()
    {
		if((model.getStoppedBreastFeedingMonths()==null || !(model.getStoppedBreastFeedingMonths()<24)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getReasonDiscontinueBreastFeed(), activity.getPage("Child Breast Feeding"), R.id.reasonDiscontinueBreastFeedContainer, context.getResources().getString(R.string.mother_and_child_reasonDiscontinueBreastFeed));
    }

    public void validateReasonDiscontinueBreastFeedSpecify()
    {
		if((model.getStoppedBreastFeedingMonths()==null || !(model.getStoppedBreastFeedingMonths()<24)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getReasonDiscontinueBreastFeed(),5,model.getReasonDiscontinueBreastFeedSpecify(), activity.getPage("Child Breast Feeding"), R.id.reasonDiscontinueBreastFeedSpecifyContainer, context.getResources().getString(R.string.mother_and_child_reasonDiscontinueBreastFeedSpecify));   
    }

    public void validateAwareBreastFeedPreventsIllness()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getAwareBreastFeedPreventsIllness(), activity.getPage("Child Breast Feeding"), R.id.awareBreastFeedPreventsIllnessContainer, context.getResources().getString(R.string.mother_and_child_awareBreastFeedPreventsIllness));
    }

    public void validateStartedSemiSolidFood()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getStartedSemiSolidFood(), activity.getPage("Child Breast Feeding"), R.id.startedSemiSolidFoodContainer, context.getResources().getString(R.string.mother_and_child_startedSemiSolidFood));
    }

    public void validateFoodSupplementRicePudding()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodSupplementRicePudding(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementRicePuddingContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementRicePudding));
    }

    public void validateFoodSupplementJaulo()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodSupplementJaulo(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementJauloContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementJaulo));
    }

    public void validateFoodSupplementLito()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodSupplementLito(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementLitoContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementLito));
    }

    public void validateFoodSupplementCerelac()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodSupplementCerelac(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementCerelacContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementCerelac));
    }

    public void validateFoodSupplementOthers()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodSupplementOthers(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementOthersContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementOthers));
    }

    public void validateFoodSupplementSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getFoodSupplementOthers(),1,model.getFoodSupplementSpecify(), activity.getPage("Child Breast Feeding"), R.id.foodSupplementSpecifyContainer, context.getResources().getString(R.string.mother_and_child_foodSupplementSpecify));   
    }

    public void validateWhyNoSupplement()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWhyNoSupplement(), activity.getPage("Child Breast Feeding"), R.id.whyNoSupplementContainer, context.getResources().getString(R.string.mother_and_child_whyNoSupplement));
    }

    public void validateWhyNoSupplementSpecify()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getWhyNoSupplement(),3,model.getWhyNoSupplementSpecify(), activity.getPage("Child Breast Feeding"), R.id.whyNoSupplementSpecifyContainer, context.getResources().getString(R.string.mother_and_child_whyNoSupplementSpecify));   
    }

    public void validateDetailFoodToBaby()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getDetailFoodToBaby(), activity.getPage("Child Breast Feeding"), R.id.detailFoodToBabyContainer, context.getResources().getString(R.string.mother_and_child_detailFoodToBaby));
    }

    public void validateWashHandsBreastfeed()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWashHandsBreastfeed(), activity.getPage("Child Breast Feeding"), R.id.washHandsBreastfeedContainer, context.getResources().getString(R.string.mother_and_child_washHandsBreastfeed));
    }

    public void validateHasHandwashFacility()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getHasHandwashFacility(), activity.getPage("Child Breast Feeding"), R.id.hasHandwashFacilityContainer, context.getResources().getString(R.string.mother_and_child_hasHandwashFacility));
    }

    public void validateChildFoodQuality()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getChildFoodQuality(), activity.getPage("Child Breast Feeding"), R.id.childFoodQualityContainer, context.getResources().getString(R.string.mother_and_child_childFoodQuality));
    }

    public void validateChildFoodFrequency()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getChildFoodFrequency(), activity.getPage("Child Breast Feeding"), R.id.childFoodFrequencyContainer, context.getResources().getString(R.string.mother_and_child_childFoodFrequency));
    }

    public void validateChildFoodDistribution()
    {
		if((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getChildFoodDistribution(), activity.getPage("Child Breast Feeding"), R.id.childFoodDistributionContainer, context.getResources().getString(R.string.mother_and_child_childFoodDistribution));
    }

    public void validateInjuryChildFacedFalls()
    {
    
        validateNonNullField(model.getInjuryChildFacedFalls(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedFallsContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedFalls));
    }

    public void validateInjuryChildFacedDrowning()
    {
    
        validateNonNullField(model.getInjuryChildFacedDrowning(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedDrowningContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedDrowning));
    }

    public void validateInjuryChildFacedBurnsscald()
    {
    
        validateNonNullField(model.getInjuryChildFacedBurnsscald(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedBurnsscaldContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedBurnsscald));
    }

    public void validateInjuryChildFacedPoisoning()
    {
    
        validateNonNullField(model.getInjuryChildFacedPoisoning(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedPoisoningContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedPoisoning));
    }

    public void validateInjuryChildFacedSuffocationchokingaspiration()
    {
    
        validateNonNullField(model.getInjuryChildFacedSuffocationchokingaspiration(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedSuffocationchokingaspirationContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedSuffocationchokingaspiration));
    }

    public void validateInjuryChildFacedCutInjury()
    {
    
        validateNonNullField(model.getInjuryChildFacedCutInjury(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedCutInjuryContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedCutInjury));
    }

    public void validateInjuryChildFacedOthers()
    {
    
        validateNonNullField(model.getInjuryChildFacedOthers(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedOthersContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedOthers));
    }

    public void validateInjuryChildFacedSpecify()
    {
  
        validateNonNullSpecifyField(model.getInjuryChildFacedOthers(),1,model.getInjuryChildFacedSpecify(), activity.getPage("Accident and Injury"), R.id.injuryChildFacedSpecifyContainer, context.getResources().getString(R.string.mother_and_child_injuryChildFacedSpecify));   
    }

    public void validateInjuryCauseUnsafeHome()
    {
    
        validateNonNullField(model.getInjuryCauseUnsafeHome(), activity.getPage("Accident and Injury"), R.id.injuryCauseUnsafeHomeContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseUnsafeHome));
    }

    public void validateInjuryCauseNoSuperVision()
    {
    
        validateNonNullField(model.getInjuryCauseNoSuperVision(), activity.getPage("Accident and Injury"), R.id.injuryCauseNoSuperVisionContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseNoSuperVision));
    }

    public void validateInjuryCauseBusyMother()
    {
    
        validateNonNullField(model.getInjuryCauseBusyMother(), activity.getPage("Accident and Injury"), R.id.injuryCauseBusyMotherContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseBusyMother));
    }

    public void validateInjuryCauseSlipperyFloor()
    {
    
        validateNonNullField(model.getInjuryCauseSlipperyFloor(), activity.getPage("Accident and Injury"), R.id.injuryCauseSlipperyFloorContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseSlipperyFloor));
    }

    public void validateInjuryCauseOthers()
    {
    
        validateNonNullField(model.getInjuryCauseOthers(), activity.getPage("Accident and Injury"), R.id.injuryCauseOthersContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseOthers));
    }

    public void validateInjuryCauseSpecify()
    {
  
        validateNonNullSpecifyField(model.getInjuryCauseOthers(),1,model.getInjuryCauseSpecify(), activity.getPage("Accident and Injury"), R.id.injuryCauseSpecifyContainer, context.getResources().getString(R.string.mother_and_child_injuryCauseSpecify));   
    }

    public void validateAgeBcgAtBirth()
    {
    
        validateNonNullField(model.getAgeBcgAtBirth(), activity.getPage("Immunization"), R.id.ageBcgAtBirthContainer, context.getResources().getString(R.string.mother_and_child_ageBcgAtBirth));
    }

    public void validateAgeDptHepbHib6Weeks()
    {
    
        validateNonNullField(model.getAgeDptHepbHib6Weeks(), activity.getPage("Immunization"), R.id.ageDptHepbHib6WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageDptHepbHib6Weeks));
    }

    public void validateAgeDptHepbHib10Weeks()
    {
    
        validateNonNullField(model.getAgeDptHepbHib10Weeks(), activity.getPage("Immunization"), R.id.ageDptHepbHib10WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageDptHepbHib10Weeks));
    }

    public void validateAgeDptHepbHib14Weeks()
    {
    
        validateNonNullField(model.getAgeDptHepbHib14Weeks(), activity.getPage("Immunization"), R.id.ageDptHepbHib14WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageDptHepbHib14Weeks));
    }

    public void validateAgeOpv6Weeks()
    {
    
        validateNonNullField(model.getAgeOpv6Weeks(), activity.getPage("Immunization"), R.id.ageOpv6WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageOpv6Weeks));
    }

    public void validateAgeOpv10Weeks()
    {
    
        validateNonNullField(model.getAgeOpv10Weeks(), activity.getPage("Immunization"), R.id.ageOpv10WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageOpv10Weeks));
    }

    public void validateAgeOpv14Weeks()
    {
    
        validateNonNullField(model.getAgeOpv14Weeks(), activity.getPage("Immunization"), R.id.ageOpv14WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageOpv14Weeks));
    }

    public void validateAgePcv6Weeks()
    {
    
        validateNonNullField(model.getAgePcv6Weeks(), activity.getPage("Immunization"), R.id.agePcv6WeeksContainer, context.getResources().getString(R.string.mother_and_child_agePcv6Weeks));
    }

    public void validateAgePcv10Weeks()
    {
    
        validateNonNullField(model.getAgePcv10Weeks(), activity.getPage("Immunization"), R.id.agePcv10WeeksContainer, context.getResources().getString(R.string.mother_and_child_agePcv10Weeks));
    }

    public void validateAgePcv9Months()
    {
    
        validateNonNullField(model.getAgePcv9Months(), activity.getPage("Immunization"), R.id.agePcv9MonthsContainer, context.getResources().getString(R.string.mother_and_child_agePcv9Months));
    }

    public void validateAgeIpv6Weeks()
    {
    
        validateNonNullField(model.getAgeIpv6Weeks(), activity.getPage("Immunization"), R.id.ageIpv6WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageIpv6Weeks));
    }

    public void validateAgeIpv14Weeks()
    {
    
        validateNonNullField(model.getAgeIpv14Weeks(), activity.getPage("Immunization"), R.id.ageIpv14WeeksContainer, context.getResources().getString(R.string.mother_and_child_ageIpv14Weeks));
    }

    public void validateAgeMeaslesRubella9Months()
    {
    
        validateNonNullField(model.getAgeMeaslesRubella9Months(), activity.getPage("Immunization"), R.id.ageMeaslesRubella9MonthsContainer, context.getResources().getString(R.string.mother_and_child_ageMeaslesRubella9Months));
    }

    public void validateAgeMeaslesRubella15Months()
    {
    
        validateNonNullField(model.getAgeMeaslesRubella15Months(), activity.getPage("Immunization"), R.id.ageMeaslesRubella15MonthsContainer, context.getResources().getString(R.string.mother_and_child_ageMeaslesRubella15Months));
    }

    public void validateAgeJapaneseEncephalitis12Months()
    {
    
        validateNonNullField(model.getAgeJapaneseEncephalitis12Months(), activity.getPage("Immunization"), R.id.ageJapaneseEncephalitis12MonthsContainer, context.getResources().getString(R.string.mother_and_child_ageJapaneseEncephalitis12Months));
    }

    public void validateAdviceBreastFeeding()
    {
		if((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getAdviceBreastFeeding(), activity.getPage("Advices"), R.id.adviceBreastFeedingContainer, context.getResources().getString(R.string.mother_and_child_adviceBreastFeeding));
    }

    public void validateAdviceDentalHygiene()
    {
    
        validateNonNullField(model.getAdviceDentalHygiene(), activity.getPage("Advices"), R.id.adviceDentalHygieneContainer, context.getResources().getString(R.string.mother_and_child_adviceDentalHygiene));
    }

    public void validateAdviceToiletTraining()
    {
    
        validateNonNullField(model.getAdviceToiletTraining(), activity.getPage("Advices"), R.id.adviceToiletTrainingContainer, context.getResources().getString(R.string.mother_and_child_adviceToiletTraining));
    }

    public void validateAdviceComplementaryFeeding()
    {
    
        validateNonNullField(model.getAdviceComplementaryFeeding(), activity.getPage("Advices"), R.id.adviceComplementaryFeedingContainer, context.getResources().getString(R.string.mother_and_child_adviceComplementaryFeeding));
    }

    public void validateAdviceAccidentPrevention()
    {
    
        validateNonNullField(model.getAdviceAccidentPrevention(), activity.getPage("Advices"), R.id.adviceAccidentPreventionContainer, context.getResources().getString(R.string.mother_and_child_adviceAccidentPrevention));
    }

    public void validateMdPhysical()
    {
    
        validateNonNullField(model.getMdPhysical(), activity.getPage("Advices"), R.id.mdPhysicalContainer, context.getResources().getString(R.string.mother_and_child_mdPhysical));
    }

    public void validateMdVerbal()
    {
    
        validateNonNullField(model.getMdVerbal(), activity.getPage("Advices"), R.id.mdVerbalContainer, context.getResources().getString(R.string.mother_and_child_mdVerbal));
    }

    public void validateMdSocial()
    {
    
        validateNonNullField(model.getMdSocial(), activity.getPage("Advices"), R.id.mdSocialContainer, context.getResources().getString(R.string.mother_and_child_mdSocial));
    }

    public void validateMdSpiritualReligious()
    {
    
        validateNonNullField(model.getMdSpiritualReligious(), activity.getPage("Advices"), R.id.mdSpiritualReligiousContainer, context.getResources().getString(R.string.mother_and_child_mdSpiritualReligious));
    }

    public void validateMdMotor()
    {
    
        validateNonNullField(model.getMdMotor(), activity.getPage("Advices"), R.id.mdMotorContainer, context.getResources().getString(R.string.mother_and_child_mdMotor));
    }

    public void validateMdIntellectual()
    {
    
        validateNonNullField(model.getMdIntellectual(), activity.getPage("Advices"), R.id.mdIntellectualContainer, context.getResources().getString(R.string.mother_and_child_mdIntellectual));
    }

    public void validateMdEmotional()
    {
    
        validateNonNullField(model.getMdEmotional(), activity.getPage("Advices"), R.id.mdEmotionalContainer, context.getResources().getString(R.string.mother_and_child_mdEmotional));
    }


}
