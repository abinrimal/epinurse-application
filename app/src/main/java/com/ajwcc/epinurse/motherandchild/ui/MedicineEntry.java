package com.ajwcc.epinurse.motherandchild.ui;

import io.realm.RealmObject;

public class MedicineEntry extends RealmObject
{
    private String medicine;
    private Integer prescribed;  // 0 not prescribed
    private Integer stock;

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public Integer getPrescribed() {
        return prescribed;
    }

    public void setPrescribed(Integer prescribed) {
        this.prescribed = prescribed;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "MedicineEntry{" +
                "medicine='" + medicine + '\'' +
                ", prescribed=" + prescribed +
                ", stock=" + stock +
                '}';
    }
}
