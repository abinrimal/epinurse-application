package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_child_breast_feeding)
public class ChildBreastFeedingFragment extends BaseEpinurseFragment {


    public ChildBreastFeedingFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup whenStartBreastFeed;

	@ViewById
	@MapToModelField
	protected RadioGroup howOftenBreastFeed;

	@ViewById
	@MapToModelField
	protected RadioGroup didExclusiveBreastFeed;

	@ViewById
	@MapToModelField
	protected EditText stoppedBreastFeedingMonths;

	@ViewById
	@MapToModelField
	protected RadioGroup reasonDiscontinueBreastFeed;

	@ViewById
	@MapToModelField
	protected EditText reasonDiscontinueBreastFeedSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup awareBreastFeedPreventsIllness;

	@ViewById
	@MapToModelField
	protected RadioGroup startedSemiSolidFood;

	@ViewById
	@MapToModelField
	protected CheckBox foodSupplementRicePudding;

	@ViewById
	@MapToModelField
	protected CheckBox foodSupplementJaulo;

	@ViewById
	@MapToModelField
	protected CheckBox foodSupplementLito;

	@ViewById
	@MapToModelField
	protected CheckBox foodSupplementCerelac;

	@ViewById
	@MapToModelField
	protected CheckBox foodSupplementOthers;

	@ViewById
	@MapToModelField
	protected EditText foodSupplementSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup whyNoSupplement;

	@ViewById
	@MapToModelField
	protected EditText whyNoSupplementSpecify;

	@ViewById
	@MapToModelField
	protected EditText detailFoodToBaby;

	@ViewById
	@MapToModelField
	protected RadioGroup washHandsBreastfeed;

	@ViewById
	@MapToModelField
	protected RadioGroup hasHandwashFacility;

	@ViewById
	@MapToModelField
	protected EditText childFoodQuality;

	@ViewById
	@MapToModelField
	protected EditText childFoodFrequency;

	@ViewById
	@MapToModelField
	protected EditText childFoodDistribution;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(whenStartBreastFeed,pojo.getWhenStartBreastFeed());
					PojoToViewMapper.setViewValue(howOftenBreastFeed,pojo.getHowOftenBreastFeed());
					PojoToViewMapper.setViewValue(didExclusiveBreastFeed,pojo.getDidExclusiveBreastFeed());
					PojoToViewMapper.setViewValue(stoppedBreastFeedingMonths,pojo.getStoppedBreastFeedingMonths());
					PojoToViewMapper.setViewValue(reasonDiscontinueBreastFeed,pojo.getReasonDiscontinueBreastFeed());
					PojoToViewMapper.setViewValue(reasonDiscontinueBreastFeedSpecify,pojo.getReasonDiscontinueBreastFeedSpecify());
					PojoToViewMapper.setViewValue(awareBreastFeedPreventsIllness,pojo.getAwareBreastFeedPreventsIllness());
					PojoToViewMapper.setViewValue(startedSemiSolidFood,pojo.getStartedSemiSolidFood());
					PojoToViewMapper.setViewValue(foodSupplementRicePudding,pojo.getFoodSupplementRicePudding());
					PojoToViewMapper.setViewValue(foodSupplementJaulo,pojo.getFoodSupplementJaulo());
					PojoToViewMapper.setViewValue(foodSupplementLito,pojo.getFoodSupplementLito());
					PojoToViewMapper.setViewValue(foodSupplementCerelac,pojo.getFoodSupplementCerelac());
					PojoToViewMapper.setViewValue(foodSupplementOthers,pojo.getFoodSupplementOthers());
					PojoToViewMapper.setViewValue(foodSupplementSpecify,pojo.getFoodSupplementSpecify());
					PojoToViewMapper.setViewValue(whyNoSupplement,pojo.getWhyNoSupplement());
					PojoToViewMapper.setViewValue(whyNoSupplementSpecify,pojo.getWhyNoSupplementSpecify());
					PojoToViewMapper.setViewValue(detailFoodToBaby,pojo.getDetailFoodToBaby());
					PojoToViewMapper.setViewValue(washHandsBreastfeed,pojo.getWashHandsBreastfeed());
					PojoToViewMapper.setViewValue(hasHandwashFacility,pojo.getHasHandwashFacility());
					PojoToViewMapper.setViewValue(childFoodQuality,pojo.getChildFoodQuality());
					PojoToViewMapper.setViewValue(childFoodFrequency,pojo.getChildFoodFrequency());
					PojoToViewMapper.setViewValue(childFoodDistribution,pojo.getChildFoodDistribution());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setWhenStartBreastFeed((Integer) ViewToPojoMapper.getValueByView(whenStartBreastFeed));
					pojo.setHowOftenBreastFeed((Integer) ViewToPojoMapper.getValueByView(howOftenBreastFeed));
					pojo.setDidExclusiveBreastFeed((Integer) ViewToPojoMapper.getValueByView(didExclusiveBreastFeed));
					pojo.setStoppedBreastFeedingMonths((Integer) ViewToPojoMapper.getValueByView(stoppedBreastFeedingMonths));
					pojo.setReasonDiscontinueBreastFeed((Integer) ViewToPojoMapper.getValueByView(reasonDiscontinueBreastFeed));
					pojo.setReasonDiscontinueBreastFeedSpecify((String) ViewToPojoMapper.getValueByView(reasonDiscontinueBreastFeedSpecify));
					pojo.setAwareBreastFeedPreventsIllness((Integer) ViewToPojoMapper.getValueByView(awareBreastFeedPreventsIllness));
					pojo.setStartedSemiSolidFood((Integer) ViewToPojoMapper.getValueByView(startedSemiSolidFood));
					pojo.setFoodSupplementRicePudding((Integer) ViewToPojoMapper.getValueByView(foodSupplementRicePudding));
					pojo.setFoodSupplementJaulo((Integer) ViewToPojoMapper.getValueByView(foodSupplementJaulo));
					pojo.setFoodSupplementLito((Integer) ViewToPojoMapper.getValueByView(foodSupplementLito));
					pojo.setFoodSupplementCerelac((Integer) ViewToPojoMapper.getValueByView(foodSupplementCerelac));
					pojo.setFoodSupplementOthers((Integer) ViewToPojoMapper.getValueByView(foodSupplementOthers));
					pojo.setFoodSupplementSpecify((String) ViewToPojoMapper.getValueByView(foodSupplementSpecify));
					pojo.setWhyNoSupplement((Integer) ViewToPojoMapper.getValueByView(whyNoSupplement));
					pojo.setWhyNoSupplementSpecify((String) ViewToPojoMapper.getValueByView(whyNoSupplementSpecify));
					pojo.setDetailFoodToBaby((String) ViewToPojoMapper.getValueByView(detailFoodToBaby));
					pojo.setWashHandsBreastfeed((Integer) ViewToPojoMapper.getValueByView(washHandsBreastfeed));
					pojo.setHasHandwashFacility((Integer) ViewToPojoMapper.getValueByView(hasHandwashFacility));
					pojo.setChildFoodQuality((String) ViewToPojoMapper.getValueByView(childFoodQuality));
					pojo.setChildFoodFrequency((String) ViewToPojoMapper.getValueByView(childFoodFrequency));
					pojo.setChildFoodDistribution((String) ViewToPojoMapper.getValueByView(childFoodDistribution));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.reasonDiscontinueBreastFeed5,R.id.foodSupplementOthers,R.id.whyNoSupplement3})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.reasonDiscontinueBreastFeed5:
				 UiUtils.toggleSpecify(view, reasonDiscontinueBreastFeedSpecify);
				break;
			case R.id.foodSupplementOthers:
				 UiUtils.toggleSpecify(view, foodSupplementSpecify);
				break;
			case R.id.whyNoSupplement3:
				 UiUtils.toggleSpecify(view, whyNoSupplementSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.whenStartBreastFeedContainer).setVisibility(View.GONE);
				update = true;				
				model.setWhenStartBreastFeed(null);

			}
			else
			{
				getView().findViewById(R.id.whenStartBreastFeedContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.howOftenBreastFeedContainer).setVisibility(View.GONE);
				update = true;				
				model.setHowOftenBreastFeed(null);

			}
			else
			{
				getView().findViewById(R.id.howOftenBreastFeedContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.didExclusiveBreastFeedContainer).setVisibility(View.GONE);
				update = true;				
				model.setDidExclusiveBreastFeed(null);

			}
			else
			{
				getView().findViewById(R.id.didExclusiveBreastFeedContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.stoppedBreastFeedingMonthsContainer).setVisibility(View.GONE);
				update = true;				
				model.setStoppedBreastFeedingMonths(null);

			}
			else
			{
				getView().findViewById(R.id.stoppedBreastFeedingMonthsContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getStoppedBreastFeedingMonths()==null || !(model.getStoppedBreastFeedingMonths()<24)))
			{
				getView().findViewById(R.id.reasonDiscontinueBreastFeedContainer).setVisibility(View.GONE);
				update = true;				
				model.setReasonDiscontinueBreastFeed(null);

			}
			else
			{
				getView().findViewById(R.id.reasonDiscontinueBreastFeedContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getStoppedBreastFeedingMonths()==null || !(model.getStoppedBreastFeedingMonths()<24)))
			{
				getView().findViewById(R.id.reasonDiscontinueBreastFeedSpecifyContainer).setVisibility(View.GONE);
				update = true;				
				model.setReasonDiscontinueBreastFeedSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.reasonDiscontinueBreastFeedSpecifyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.awareBreastFeedPreventsIllnessContainer).setVisibility(View.GONE);
				update = true;				
				model.setAwareBreastFeedPreventsIllness(null);

			}
			else
			{
				getView().findViewById(R.id.awareBreastFeedPreventsIllnessContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.startedSemiSolidFoodContainer).setVisibility(View.GONE);
				update = true;				
				model.setStartedSemiSolidFood(null);

			}
			else
			{
				getView().findViewById(R.id.startedSemiSolidFoodContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.foodSupplementContainer).setVisibility(View.GONE);
				update = true;				
				model.setFoodSupplementRicePudding(null);
				model.setFoodSupplementJaulo(null);
				model.setFoodSupplementLito(null);
				model.setFoodSupplementCerelac(null);
				model.setFoodSupplementOthers(null);
				model.setFoodSupplementSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.foodSupplementContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.whyNoSupplementContainer).setVisibility(View.GONE);
				update = true;				
				model.setWhyNoSupplement(null);

			}
			else
			{
				getView().findViewById(R.id.whyNoSupplementContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.whyNoSupplementSpecifyContainer).setVisibility(View.GONE);
				update = true;				
				model.setWhyNoSupplementSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.whyNoSupplementSpecifyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.detailFoodToBabyContainer).setVisibility(View.GONE);
				update = true;				
				model.setDetailFoodToBaby(null);

			}
			else
			{
				getView().findViewById(R.id.detailFoodToBabyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.washHandsBreastfeedContainer).setVisibility(View.GONE);
				update = true;				
				model.setWashHandsBreastfeed(null);

			}
			else
			{
				getView().findViewById(R.id.washHandsBreastfeedContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.hasHandwashFacilityContainer).setVisibility(View.GONE);
				update = true;				
				model.setHasHandwashFacility(null);

			}
			else
			{
				getView().findViewById(R.id.hasHandwashFacilityContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.childFoodQualityContainer).setVisibility(View.GONE);
				update = true;				
				model.setChildFoodQuality(null);

			}
			else
			{
				getView().findViewById(R.id.childFoodQualityContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.childFoodFrequencyContainer).setVisibility(View.GONE);
				update = true;				
				model.setChildFoodFrequency(null);

			}
			else
			{
				getView().findViewById(R.id.childFoodFrequencyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.childFoodDistributionContainer).setVisibility(View.GONE);
				update = true;				
				model.setChildFoodDistribution(null);

			}
			else
			{
				getView().findViewById(R.id.childFoodDistributionContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



	
	@TextChange({R.id.stoppedBreastFeedingMonths})
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
