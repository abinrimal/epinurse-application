package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_childbirth)
public class ChildbirthFragment extends BaseEpinurseFragment {


    public ChildbirthFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText youngestBabyMonths;

	@ViewById
	@MapToModelField
	protected RadioGroup typeOfDelivery;

	@ViewById
	@MapToModelField
	protected RadioGroup placeDeliveryYoungest;

	@ViewById
	@MapToModelField
	protected EditText placeDeliveryYoungestSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup deliveryComplications;

	@ViewById
	@MapToModelField
	protected EditText deliveryComplicationsSpecify;

	@ViewById
	@MapToModelField
	protected EditText gapBetweenKids;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(youngestBabyMonths,pojo.getYoungestBabyMonths());
					PojoToViewMapper.setViewValue(typeOfDelivery,pojo.getTypeOfDelivery());
					PojoToViewMapper.setViewValue(placeDeliveryYoungest,pojo.getPlaceDeliveryYoungest());
					PojoToViewMapper.setViewValue(placeDeliveryYoungestSpecify,pojo.getPlaceDeliveryYoungestSpecify());
					PojoToViewMapper.setViewValue(deliveryComplications,pojo.getDeliveryComplications());
					PojoToViewMapper.setViewValue(deliveryComplicationsSpecify,pojo.getDeliveryComplicationsSpecify());
					PojoToViewMapper.setViewValue(gapBetweenKids,pojo.getGapBetweenKids());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setYoungestBabyMonths((Integer) ViewToPojoMapper.getValueByView(youngestBabyMonths));
					pojo.setTypeOfDelivery((Integer) ViewToPojoMapper.getValueByView(typeOfDelivery));
					pojo.setPlaceDeliveryYoungest((Integer) ViewToPojoMapper.getValueByView(placeDeliveryYoungest));
					pojo.setPlaceDeliveryYoungestSpecify((String) ViewToPojoMapper.getValueByView(placeDeliveryYoungestSpecify));
					pojo.setDeliveryComplications((Integer) ViewToPojoMapper.getValueByView(deliveryComplications));
					pojo.setDeliveryComplicationsSpecify((String) ViewToPojoMapper.getValueByView(deliveryComplicationsSpecify));
					pojo.setGapBetweenKids((Integer) ViewToPojoMapper.getValueByView(gapBetweenKids));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.placeDeliveryYoungest3,R.id.deliveryComplications1})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.placeDeliveryYoungest3:
				 UiUtils.toggleSpecify(view, placeDeliveryYoungestSpecify);
				break;
			case R.id.deliveryComplications1:
				 UiUtils.toggleSpecify(view, deliveryComplicationsSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
			{
				getView().findViewById(R.id.youngestBabyMonthsContainer).setVisibility(View.GONE);
				update = true;				
				model.setYoungestBabyMonths(null);

			}
			else
			{
				getView().findViewById(R.id.youngestBabyMonthsContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.typeOfDeliveryContainer).setVisibility(View.GONE);
				update = true;				
				model.setTypeOfDelivery(null);

			}
			else
			{
				getView().findViewById(R.id.typeOfDeliveryContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.placeDeliveryYoungestContainer).setVisibility(View.GONE);
				update = true;				
				model.setPlaceDeliveryYoungest(null);

			}
			else
			{
				getView().findViewById(R.id.placeDeliveryYoungestContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.placeDeliveryYoungestSpecifyContainer).setVisibility(View.GONE);
				update = true;				
				model.setPlaceDeliveryYoungestSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.placeDeliveryYoungestSpecifyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.deliveryComplicationsContainer).setVisibility(View.GONE);
				update = true;				
				model.setDeliveryComplications(null);

			}
			else
			{
				getView().findViewById(R.id.deliveryComplicationsContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getYoungestBabyMonths()==null || !(model.getYoungestBabyMonths()<25)))
			{
				getView().findViewById(R.id.deliveryComplicationsSpecifyContainer).setVisibility(View.GONE);
				update = true;				
				model.setDeliveryComplicationsSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.deliveryComplicationsSpecifyContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
			{
				getView().findViewById(R.id.gapBetweenKidsContainer).setVisibility(View.GONE);
				update = true;				
				model.setGapBetweenKids(null);

			}
			else
			{
				getView().findViewById(R.id.gapBetweenKidsContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



	
	@TextChange({R.id.youngestBabyMonths})
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
