package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_advices)
public class AdvicesFragment extends BaseEpinurseFragment {


    public AdvicesFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText adviceBreastFeeding;

	@ViewById
	@MapToModelField
	protected EditText adviceDentalHygiene;

	@ViewById
	@MapToModelField
	protected EditText adviceToiletTraining;

	@ViewById
	@MapToModelField
	protected EditText adviceComplementaryFeeding;

	@ViewById
	@MapToModelField
	protected EditText adviceAccidentPrevention;

	@ViewById
	@MapToModelField
	protected EditText mdPhysical;

	@ViewById
	@MapToModelField
	protected EditText mdVerbal;

	@ViewById
	@MapToModelField
	protected EditText mdSocial;

	@ViewById
	@MapToModelField
	protected EditText mdSpiritualReligious;

	@ViewById
	@MapToModelField
	protected EditText mdMotor;

	@ViewById
	@MapToModelField
	protected EditText mdIntellectual;

	@ViewById
	@MapToModelField
	protected EditText mdEmotional;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(adviceBreastFeeding,pojo.getAdviceBreastFeeding());
					PojoToViewMapper.setViewValue(adviceDentalHygiene,pojo.getAdviceDentalHygiene());
					PojoToViewMapper.setViewValue(adviceToiletTraining,pojo.getAdviceToiletTraining());
					PojoToViewMapper.setViewValue(adviceComplementaryFeeding,pojo.getAdviceComplementaryFeeding());
					PojoToViewMapper.setViewValue(adviceAccidentPrevention,pojo.getAdviceAccidentPrevention());
					PojoToViewMapper.setViewValue(mdPhysical,pojo.getMdPhysical());
					PojoToViewMapper.setViewValue(mdVerbal,pojo.getMdVerbal());
					PojoToViewMapper.setViewValue(mdSocial,pojo.getMdSocial());
					PojoToViewMapper.setViewValue(mdSpiritualReligious,pojo.getMdSpiritualReligious());
					PojoToViewMapper.setViewValue(mdMotor,pojo.getMdMotor());
					PojoToViewMapper.setViewValue(mdIntellectual,pojo.getMdIntellectual());
					PojoToViewMapper.setViewValue(mdEmotional,pojo.getMdEmotional());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setAdviceBreastFeeding((String) ViewToPojoMapper.getValueByView(adviceBreastFeeding));
					pojo.setAdviceDentalHygiene((String) ViewToPojoMapper.getValueByView(adviceDentalHygiene));
					pojo.setAdviceToiletTraining((String) ViewToPojoMapper.getValueByView(adviceToiletTraining));
					pojo.setAdviceComplementaryFeeding((String) ViewToPojoMapper.getValueByView(adviceComplementaryFeeding));
					pojo.setAdviceAccidentPrevention((String) ViewToPojoMapper.getValueByView(adviceAccidentPrevention));
					pojo.setMdPhysical((String) ViewToPojoMapper.getValueByView(mdPhysical));
					pojo.setMdVerbal((String) ViewToPojoMapper.getValueByView(mdVerbal));
					pojo.setMdSocial((String) ViewToPojoMapper.getValueByView(mdSocial));
					pojo.setMdSpiritualReligious((String) ViewToPojoMapper.getValueByView(mdSpiritualReligious));
					pojo.setMdMotor((String) ViewToPojoMapper.getValueByView(mdMotor));
					pojo.setMdIntellectual((String) ViewToPojoMapper.getValueByView(mdIntellectual));
					pojo.setMdEmotional((String) ViewToPojoMapper.getValueByView(mdEmotional));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
			{
				getView().findViewById(R.id.adviceBreastFeedingContainer).setVisibility(View.GONE);
				update = true;				
				model.setAdviceBreastFeeding(null);

			}
			else
			{
				getView().findViewById(R.id.adviceBreastFeedingContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
