package com.ajwcc.epinurse.motherandchild.gen;

import android.support.v4.view.ViewPager;
import android.widget.SeekBar;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import com.ajwcc.epinurse.common.SaveFragment_;


@EActivity(R.layout.gen_activity_mother_and_child)
public class MotherAndChildActivity extends BaseEpinurseFormActivity
{

    @Extra
    public String uuid;

    @ViewById(R.id.title)
    public TextView title;

    @ViewById(R.id.pager)
    public ViewPager viewPager;

    @ViewById(R.id.seekbar)
    public SeekBar seekBar;

	@ViewById(R.id.previous)
    public Button previous;

    @ViewById(R.id.next)
    public Button next;
    
    @AfterViews
    public void init()
    {
    	
    	//title.setText("Mother and Child");
    	
    	title.setText(getResources().getString(R.string.mother_and_child));
    	
    
        // load existing for editing at this point, given a uuid
        initModel(MotherAndChild.class, uuid);


        initViewPagerAndSeekbar(createFragmentList(), viewPager, seekBar, previous, next);
        
        createToC();
    	setValidationHandler(getValidationHandler());
    }


	public ValidationHandler<MotherAndChild> getValidationHandler()
	{
		MotherAndChildValidator validator = new MotherAndChildValidator(this);
		validator.setModel((MotherAndChild)getModel());	
		return validator;
	}

    LinkedHashMap<String, Integer> pageMap = new LinkedHashMap<>();

    private void createToC()
    {
        List<String> toc = createTableOfContents();
        for (int i=0; i<toc.size(); i++)
        {
            pageMap.put(toc.get(i), i);
        }
    }

    public Integer getPage(String name)
    {
        return pageMap.get(name);
    }

    protected List<String> createTableOfContents()
    {
    	List<String> names = new ArrayList<>();
    	
		names.add("Menstruation");
		names.add("Pregnancy");
		names.add("Childbirth");
		names.add("Post Natal");
		names.add("Family Planning");
		names.add("Child Basic Information");
		names.add("Child Breast Feeding");
		names.add("Accident and Injury");
		names.add("Immunization");
		names.add("Advices");

    	
        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = new ArrayList<>();
        
		list.add(() -> MenstruationFragment_.builder().build());
		list.add(() -> PregnancyFragment_.builder().build());
		list.add(() -> ChildbirthFragment_.builder().build());
		list.add(() -> PostNatalFragment_.builder().build());
		list.add(() -> FamilyPlanningFragment_.builder().build());
		list.add(() -> ChildBasicInformationFragment_.builder().build());
		list.add(() -> ChildBreastFeedingFragment_.builder().build());
		list.add(() -> AccidentAndInjuryFragment_.builder().build());
		list.add(() -> ImmunizationFragment_.builder().build());
		list.add(() -> AdvicesFragment_.builder().build());


        list.add(() -> SaveFragment_.builder().build());


        return list;
    }

}
