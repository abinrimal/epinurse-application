package com.ajwcc.epinurse.motherandchild.ui;


import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.OwnerSelectFragment_;
import com.ajwcc.epinurse.common.image.ImageListFragment_;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChild;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import org.androidannotations.annotations.EActivity;

import java.util.List;

import io.realm.RealmQuery;

@EActivity(R.layout.gen_activity_mother_and_child)
public class MotherAndChildActivity extends com.ajwcc.epinurse.motherandchild.gen.MotherAndChildActivity
{
    public ValidationHandler<MotherAndChild> getValidationHandler()
    {
        MotherAndChildValidator validator = new MotherAndChildValidator(this);
        validator.setModel((MotherAndChild)getModel());
        return validator;
    }

    protected List<String> createTableOfContents()
    {
        List<String> names = super.createTableOfContents();


        names.add(2,"Medicines");
        names.add(0,"Owner");

        return names;
    }

    public static class FemaleOnlyProfiles implements BaseEpinurseSelectActivity.QueryFilter<BasicInformation>
    {
        public String getName()
        {
            return "Females";
        }

        // must be static or an outer class
        public RealmQuery<BasicInformation> adjust(RealmQuery<BasicInformation> query)
        {

            query.equalTo("sex", 1);  // females only
            return query;
        }
    }

    protected List<LazyFragment> createFragmentList() {

        List<LazyFragment> list = super.createFragmentList();

        // add the custom fragment for Medicine addition --> index 2
        // add custom Page1
        list.set(2, () -> ChildbirthFragment_.builder().build());
        list.set(5, () -> ChildBasicInformationFragment_.builder().build());
        list.set(6, () -> ChildBreastFeedingFragment_.builder().build());


        // add backwards
        list.add(2, () -> MedicineFragment_.builder().build());
        list.add(0, () -> OwnerSelectFragment_.builder().arg("filter", new FemaleOnlyProfiles()).build());

        list.add(list.size()-1, ()-> ImageListFragment_.builder().build());


        return list;
    }

}
