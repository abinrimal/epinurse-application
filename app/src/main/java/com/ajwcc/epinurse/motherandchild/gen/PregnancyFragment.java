package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_pregnancy)
public class PregnancyFragment extends BaseEpinurseFragment {


    public PregnancyFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText obstetricHistoryGravida;

	@ViewById
	@MapToModelField
	protected EditText obstetricHistoryPara;

	@ViewById
	@MapToModelField
	protected EditText obstetricHistoryAbortion;

	@ViewById
	@MapToModelField
	protected EditText obstetricHistoryLiving;

	@ViewById
	@MapToModelField
	protected EditText periodGestation;

	@ViewById
	@MapToModelField
	protected EditText numAntenatalVisits;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceAncCheckUp;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceAlbendazole;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceTdImmunization;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceIrontabs;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceCouseling;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServicePmtct;

	@ViewById
	@MapToModelField
	protected CheckBox antenalServiceOthers;

	@ViewById
	@MapToModelField
	protected EditText antenalServiceSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup healthSeekingBehaviour;

	@ViewById
	@MapToModelField
	protected EditText healthSeekingBehaviourSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(obstetricHistoryGravida,pojo.getObstetricHistoryGravida());
					PojoToViewMapper.setViewValue(obstetricHistoryPara,pojo.getObstetricHistoryPara());
					PojoToViewMapper.setViewValue(obstetricHistoryAbortion,pojo.getObstetricHistoryAbortion());
					PojoToViewMapper.setViewValue(obstetricHistoryLiving,pojo.getObstetricHistoryLiving());
					PojoToViewMapper.setViewValue(periodGestation,pojo.getPeriodGestation());
					PojoToViewMapper.setViewValue(numAntenatalVisits,pojo.getNumAntenatalVisits());
					PojoToViewMapper.setViewValue(antenalServiceAncCheckUp,pojo.getAntenalServiceAncCheckUp());
					PojoToViewMapper.setViewValue(antenalServiceAlbendazole,pojo.getAntenalServiceAlbendazole());
					PojoToViewMapper.setViewValue(antenalServiceTdImmunization,pojo.getAntenalServiceTdImmunization());
					PojoToViewMapper.setViewValue(antenalServiceIrontabs,pojo.getAntenalServiceIrontabs());
					PojoToViewMapper.setViewValue(antenalServiceCouseling,pojo.getAntenalServiceCouseling());
					PojoToViewMapper.setViewValue(antenalServicePmtct,pojo.getAntenalServicePmtct());
					PojoToViewMapper.setViewValue(antenalServiceOthers,pojo.getAntenalServiceOthers());
					PojoToViewMapper.setViewValue(antenalServiceSpecify,pojo.getAntenalServiceSpecify());
					PojoToViewMapper.setViewValue(healthSeekingBehaviour,pojo.getHealthSeekingBehaviour());
					PojoToViewMapper.setViewValue(healthSeekingBehaviourSpecify,pojo.getHealthSeekingBehaviourSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setObstetricHistoryGravida((Integer) ViewToPojoMapper.getValueByView(obstetricHistoryGravida));
					pojo.setObstetricHistoryPara((Integer) ViewToPojoMapper.getValueByView(obstetricHistoryPara));
					pojo.setObstetricHistoryAbortion((Integer) ViewToPojoMapper.getValueByView(obstetricHistoryAbortion));
					pojo.setObstetricHistoryLiving((Integer) ViewToPojoMapper.getValueByView(obstetricHistoryLiving));
					pojo.setPeriodGestation((Integer) ViewToPojoMapper.getValueByView(periodGestation));
					pojo.setNumAntenatalVisits((Integer) ViewToPojoMapper.getValueByView(numAntenatalVisits));
					pojo.setAntenalServiceAncCheckUp((Integer) ViewToPojoMapper.getValueByView(antenalServiceAncCheckUp));
					pojo.setAntenalServiceAlbendazole((Integer) ViewToPojoMapper.getValueByView(antenalServiceAlbendazole));
					pojo.setAntenalServiceTdImmunization((Integer) ViewToPojoMapper.getValueByView(antenalServiceTdImmunization));
					pojo.setAntenalServiceIrontabs((Integer) ViewToPojoMapper.getValueByView(antenalServiceIrontabs));
					pojo.setAntenalServiceCouseling((Integer) ViewToPojoMapper.getValueByView(antenalServiceCouseling));
					pojo.setAntenalServicePmtct((Integer) ViewToPojoMapper.getValueByView(antenalServicePmtct));
					pojo.setAntenalServiceOthers((Integer) ViewToPojoMapper.getValueByView(antenalServiceOthers));
					pojo.setAntenalServiceSpecify((String) ViewToPojoMapper.getValueByView(antenalServiceSpecify));
					pojo.setHealthSeekingBehaviour((Integer) ViewToPojoMapper.getValueByView(healthSeekingBehaviour));
					pojo.setHealthSeekingBehaviourSpecify((String) ViewToPojoMapper.getValueByView(healthSeekingBehaviourSpecify));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.antenalServiceOthers,R.id.healthSeekingBehaviour4})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.antenalServiceOthers:
				 UiUtils.toggleSpecify(view, antenalServiceSpecify);
				break;
			case R.id.healthSeekingBehaviour4:
				 UiUtils.toggleSpecify(view, healthSeekingBehaviourSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>0)))
			{
				getView().findViewById(R.id.obstetricHistoryParaContainer).setVisibility(View.GONE);
				update = true;				
				model.setObstetricHistoryPara(null);

			}
			else
			{
				getView().findViewById(R.id.obstetricHistoryParaContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>0)))
			{
				getView().findViewById(R.id.obstetricHistoryLivingContainer).setVisibility(View.GONE);
				update = true;				
				model.setObstetricHistoryLiving(null);

			}
			else
			{
				getView().findViewById(R.id.obstetricHistoryLivingContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



	
	@TextChange({R.id.obstetricHistoryGravida})
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
