package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_family_planning)
public class FamilyPlanningFragment extends BaseEpinurseFragment {


    public FamilyPlanningFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup hadCounselingFamilyPlanning;

	@ViewById
	@MapToModelField
	protected RadioGroup usedFamilyPlanningMethods;

	@ViewById
	@MapToModelField
	protected CheckBox permanentVasectomy;

	@ViewById
	@MapToModelField
	protected CheckBox permanentMinilap;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryNatural;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryCondom;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryDepo;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryPills;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryIucd;

	@ViewById
	@MapToModelField
	protected CheckBox temporaryImplant;

	@ViewById
	@MapToModelField
	protected RadioGroup sourceMaternityInfo;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(hadCounselingFamilyPlanning,pojo.getHadCounselingFamilyPlanning());
					PojoToViewMapper.setViewValue(usedFamilyPlanningMethods,pojo.getUsedFamilyPlanningMethods());
					PojoToViewMapper.setViewValue(permanentVasectomy,pojo.getPermanentVasectomy());
					PojoToViewMapper.setViewValue(permanentMinilap,pojo.getPermanentMinilap());
					PojoToViewMapper.setViewValue(temporaryNatural,pojo.getTemporaryNatural());
					PojoToViewMapper.setViewValue(temporaryCondom,pojo.getTemporaryCondom());
					PojoToViewMapper.setViewValue(temporaryDepo,pojo.getTemporaryDepo());
					PojoToViewMapper.setViewValue(temporaryPills,pojo.getTemporaryPills());
					PojoToViewMapper.setViewValue(temporaryIucd,pojo.getTemporaryIucd());
					PojoToViewMapper.setViewValue(temporaryImplant,pojo.getTemporaryImplant());
					PojoToViewMapper.setViewValue(sourceMaternityInfo,pojo.getSourceMaternityInfo());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setHadCounselingFamilyPlanning((Integer) ViewToPojoMapper.getValueByView(hadCounselingFamilyPlanning));
					pojo.setUsedFamilyPlanningMethods((Integer) ViewToPojoMapper.getValueByView(usedFamilyPlanningMethods));
					pojo.setPermanentVasectomy((Integer) ViewToPojoMapper.getValueByView(permanentVasectomy));
					pojo.setPermanentMinilap((Integer) ViewToPojoMapper.getValueByView(permanentMinilap));
					pojo.setTemporaryNatural((Integer) ViewToPojoMapper.getValueByView(temporaryNatural));
					pojo.setTemporaryCondom((Integer) ViewToPojoMapper.getValueByView(temporaryCondom));
					pojo.setTemporaryDepo((Integer) ViewToPojoMapper.getValueByView(temporaryDepo));
					pojo.setTemporaryPills((Integer) ViewToPojoMapper.getValueByView(temporaryPills));
					pojo.setTemporaryIucd((Integer) ViewToPojoMapper.getValueByView(temporaryIucd));
					pojo.setTemporaryImplant((Integer) ViewToPojoMapper.getValueByView(temporaryImplant));
					pojo.setSourceMaternityInfo((Integer) ViewToPojoMapper.getValueByView(sourceMaternityInfo));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
