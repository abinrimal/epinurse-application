package com.ajwcc.epinurse.motherandchild.ui;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChild;
import com.ajwcc.util.reflect.MapToModelField;
import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;

import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.gen_fragment_mother_and_child_child_breast_feeding)
public class ChildBreastFeedingFragment extends com.ajwcc.epinurse.motherandchild.gen.ChildBreastFeedingFragment {


	public boolean isAnyFoodSupplementSet(MotherAndChild model)
	{
		return (model.getFoodSupplementRicePudding()==1
				|| model.getFoodSupplementJaulo()==1
				|| model.getFoodSupplementLito()==1
				|| model.getFoodSupplementCerelac()==1
				|| model.getFoodSupplementOthers()==1);
	}

	@Click({R.id.foodSupplementRicePudding, R.id.foodSupplementJaulo, R.id.foodSupplementLito, R.id.foodSupplementCerelac, R.id.foodSupplementOthers })
	public void clickSupplement()
	{
		mapViewsToModel(false);
		checkDependencies();
	}

	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild model, boolean update)
	{
		// need to check if any of the foodSupplementXXX fields are set


		if (isAnyFoodSupplementSet(model))
		{
			getView().findViewById(R.id.whyNoSupplementContainer).setVisibility(View.GONE);
			update = true;
			model.setWhyNoSupplement(null);

		}
		else
		{
			getView().findViewById(R.id.whyNoSupplementContainer).setVisibility(View.VISIBLE);
		}

		if (isAnyFoodSupplementSet(model))
		{
			getView().findViewById(R.id.whyNoSupplementSpecifyContainer).setVisibility(View.GONE);
			update = true;
			model.setWhyNoSupplementSpecify(null);

		}
		else
		{
			getView().findViewById(R.id.whyNoSupplementSpecifyContainer).setVisibility(View.VISIBLE);
		}


		return update;
	}





}
