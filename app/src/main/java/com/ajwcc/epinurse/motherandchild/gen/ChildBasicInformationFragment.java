package com.ajwcc.epinurse.motherandchild.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_mother_and_child_child_basic_information)
public class ChildBasicInformationFragment extends BaseEpinurseFragment {


    public ChildBasicInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText childUniqueId;

	@ViewById
	@MapToModelField
	protected EditText childName;

	@ViewById
	@MapToModelField
	protected RadioGroup childSex;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInAd;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInBs;

	@ViewById
	@MapToModelField
	protected EditText weightAtBirth;

	@ViewById
	@MapToModelField
	protected EditText weightCurrent;

	@ViewById
	@MapToModelField
	protected EditText heightCm;

	@ViewById
	@MapToModelField
	protected EditText midArmCircumference;

	@ViewById
	@MapToModelField
	protected RadioGroup hasCongentialAnomaly;

	@ViewById
	@MapToModelField
	protected RadioGroup healthCondition;

	@ViewById
	@MapToModelField
	protected RadioGroup hadVitKInjection;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
					PojoToViewMapper.setViewValue(childUniqueId,pojo.getChildUniqueId());
					PojoToViewMapper.setViewValue(childName,pojo.getChildName());
					PojoToViewMapper.setViewValue(childSex,pojo.getChildSex());
					PojoToViewMapper.setViewValue(dateOfBirthInAd,pojo.getDateOfBirthInAd());
					PojoToViewMapper.setViewValue(dateOfBirthInBs,pojo.getDateOfBirthInBs());
					PojoToViewMapper.setViewValue(weightAtBirth,pojo.getWeightAtBirth());
					PojoToViewMapper.setViewValue(weightCurrent,pojo.getWeightCurrent());
					PojoToViewMapper.setViewValue(heightCm,pojo.getHeightCm());
					PojoToViewMapper.setViewValue(midArmCircumference,pojo.getMidArmCircumference());
					PojoToViewMapper.setViewValue(hasCongentialAnomaly,pojo.getHasCongentialAnomaly());
					PojoToViewMapper.setViewValue(healthCondition,pojo.getHealthCondition());
					PojoToViewMapper.setViewValue(hadVitKInjection,pojo.getHadVitKInjection());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: MotherAndChild");
                	MotherAndChild pojo = (MotherAndChild) getModel();
                	
					pojo.setChildUniqueId((String) ViewToPojoMapper.getValueByView(childUniqueId));
					pojo.setChildName((String) ViewToPojoMapper.getValueByView(childName));
					pojo.setChildSex((Integer) ViewToPojoMapper.getValueByView(childSex));
					pojo.setDateOfBirthInAd((String) ViewToPojoMapper.getValueByView(dateOfBirthInAd));
					pojo.setDateOfBirthInBs((String) ViewToPojoMapper.getValueByView(dateOfBirthInBs));
					pojo.setWeightAtBirth((Double) ViewToPojoMapper.getValueByView(weightAtBirth));
					pojo.setWeightCurrent((Double) ViewToPojoMapper.getValueByView(weightCurrent));
					pojo.setHeightCm((Integer) ViewToPojoMapper.getValueByView(heightCm));
					pojo.setMidArmCircumference((Double) ViewToPojoMapper.getValueByView(midArmCircumference));
					pojo.setHasCongentialAnomaly((Integer) ViewToPojoMapper.getValueByView(hasCongentialAnomaly));
					pojo.setHealthCondition((Integer) ViewToPojoMapper.getValueByView(healthCondition));
					pojo.setHadVitKInjection((Integer) ViewToPojoMapper.getValueByView(hadVitKInjection));

					if (save)
					{
                		System.out.println("Save to realm: MotherAndChild");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			MotherAndChild model = (MotherAndChild) getModel();
			boolean update = false;

			if ((model.getObstetricHistoryGravida()==null || !(model.getObstetricHistoryGravida()>1)))
			{
				getView().findViewById(R.id.childUniqueIdContainer).setVisibility(View.GONE);
				update = true;				
				model.setChildUniqueId(null);

			}
			else
			{
				getView().findViewById(R.id.childUniqueIdContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(MotherAndChild mode, boolean update)
	{
		return update;
	}
	



}
