package com.ajwcc.epinurse;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import com.ajwcc.epinurse.basicinformation.ui.BasicInformationSelectActivity_;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursingSelectActivity_;
import com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessmentSelectActivity_;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChildSelectActivity_;
import com.ajwcc.epinurse.common.network.NurseLogin_;
import com.ajwcc.epinurse.nurseregistration.ui.NurseRegistrationSelectActivity_;
import com.ajwcc.epinurse.schoolhealthassessment.ui.SchoolHealthAssessmentSelectActivity_;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessmentSelectActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import java.util.Locale;


@EActivity(R.layout.activity_main_menu)
public class MainMenuActivity extends AppCompatActivity
{
    @App
    EpiNurseApplication app;

    @Click(R.id.basicinformation)
    public void clickBasicInformation()
    {
        BasicInformationSelectActivity_.intent(this).start();
    }

    @Click(R.id.studenthealthassessment)
    public void clickStudentHealthAssessment()
    {
        StudentHealthAssessmentSelectActivity_.intent(this).start();
    }

    @Click(R.id.schoolhealthassessment)
    public void clickSchoolHealthAssessment()
    {
        SchoolHealthAssessmentSelectActivity_.intent(this).start();
    }

    @Click(R.id.employeehealthassessment)
    public void clickEmployeeHealthAssessment()
    {
        EmployeeHealthAssessmentSelectActivity_.intent(this).start();
    }

    @Click(R.id.community)
    public void clickCommunity()
    {
        CommunityNursingSelectActivity_.intent(this).start();

    }

    @Click(R.id.motherandchild)
    public void clickMotherAndChild()
    {
        MotherAndChildSelectActivity_.intent(this).start();
    }

    @Click(R.id.nurseregistration)
    public void clickNurseRegistration()
    {
        NurseRegistrationSelectActivity_.intent(this).start();
    }


    @Click(R.id.network)
    public void clickNetwork()
    {
        NurseLogin_.intent(this).start();
    }

    public Locale neLocale = new Locale("ne");
    public Locale enLocale = new Locale("en");

    @Click(R.id.language)
    public void clickLanguage() {


        if (!getResources().getConfiguration().locale.equals(neLocale)) {
            Locale.setDefault(neLocale);
            Configuration config = new Configuration();
            config.locale = neLocale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            saveLanguage("ne");
            recreate();

        } else {
            Locale.setDefault(enLocale);
            Configuration config = new Configuration();
            config.locale = enLocale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            saveLanguage("en");
            recreate();
        }
    }

    public void saveLanguage(String lang)
    {
        SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("lang", lang);
        edit.apply();
    }

    public String loadLanguage()
    {
        SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
        String lang = prefs.getString("lang", "en");
        return lang;
    }

    @AfterViews
    public void init()
    {
        String lang = loadLanguage();
        Locale locale;
        if (lang.equals("ne"))
        {
            locale = neLocale;
        }
        else
        {
            locale = enLocale;
        }

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }

}
