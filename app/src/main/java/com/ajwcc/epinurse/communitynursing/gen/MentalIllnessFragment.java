package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_mental_illness)
public class MentalIllnessFragment extends BaseEpinurseFragment {


    public MentalIllnessFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup familyMentalIllness;

	@ViewById
	@MapToModelField
	protected RadioGroup underMedication;

	@ViewById
	@MapToModelField
	protected CheckBox healthHazardsNone;

	@ViewById
	@MapToModelField
	protected CheckBox healthHazardsSmoking;

	@ViewById
	@MapToModelField
	protected CheckBox healthHazardsAlcoholism;

	@ViewById
	@MapToModelField
	protected CheckBox healthHazardsDrugAbuse;

	@ViewById
	@MapToModelField
	protected CheckBox healthHazardsOthers;

	@ViewById
	@MapToModelField
	protected EditText healthHazardsSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox abuseNo;

	@ViewById
	@MapToModelField
	protected CheckBox abuseSexualAbuse;

	@ViewById
	@MapToModelField
	protected CheckBox abusePhysicalAbuse;

	@ViewById
	@MapToModelField
	protected CheckBox abusePsychologicalAbuse;

	@ViewById
	@MapToModelField
	protected CheckBox abuseOthers;

	@ViewById
	@MapToModelField
	protected EditText abuseSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(familyMentalIllness,pojo.getFamilyMentalIllness());
					PojoToViewMapper.setViewValue(underMedication,pojo.getUnderMedication());
					PojoToViewMapper.setViewValue(healthHazardsNone,pojo.getHealthHazardsNone());
					PojoToViewMapper.setViewValue(healthHazardsSmoking,pojo.getHealthHazardsSmoking());
					PojoToViewMapper.setViewValue(healthHazardsAlcoholism,pojo.getHealthHazardsAlcoholism());
					PojoToViewMapper.setViewValue(healthHazardsDrugAbuse,pojo.getHealthHazardsDrugAbuse());
					PojoToViewMapper.setViewValue(healthHazardsOthers,pojo.getHealthHazardsOthers());
					PojoToViewMapper.setViewValue(healthHazardsSpecify,pojo.getHealthHazardsSpecify());
					PojoToViewMapper.setViewValue(abuseNo,pojo.getAbuseNo());
					PojoToViewMapper.setViewValue(abuseSexualAbuse,pojo.getAbuseSexualAbuse());
					PojoToViewMapper.setViewValue(abusePhysicalAbuse,pojo.getAbusePhysicalAbuse());
					PojoToViewMapper.setViewValue(abusePsychologicalAbuse,pojo.getAbusePsychologicalAbuse());
					PojoToViewMapper.setViewValue(abuseOthers,pojo.getAbuseOthers());
					PojoToViewMapper.setViewValue(abuseSpecify,pojo.getAbuseSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setFamilyMentalIllness((Integer) ViewToPojoMapper.getValueByView(familyMentalIllness));
					pojo.setUnderMedication((Integer) ViewToPojoMapper.getValueByView(underMedication));
					pojo.setHealthHazardsNone((Integer) ViewToPojoMapper.getValueByView(healthHazardsNone));
					pojo.setHealthHazardsSmoking((Integer) ViewToPojoMapper.getValueByView(healthHazardsSmoking));
					pojo.setHealthHazardsAlcoholism((Integer) ViewToPojoMapper.getValueByView(healthHazardsAlcoholism));
					pojo.setHealthHazardsDrugAbuse((Integer) ViewToPojoMapper.getValueByView(healthHazardsDrugAbuse));
					pojo.setHealthHazardsOthers((Integer) ViewToPojoMapper.getValueByView(healthHazardsOthers));
					pojo.setHealthHazardsSpecify((String) ViewToPojoMapper.getValueByView(healthHazardsSpecify));
					pojo.setAbuseNo((Integer) ViewToPojoMapper.getValueByView(abuseNo));
					pojo.setAbuseSexualAbuse((Integer) ViewToPojoMapper.getValueByView(abuseSexualAbuse));
					pojo.setAbusePhysicalAbuse((Integer) ViewToPojoMapper.getValueByView(abusePhysicalAbuse));
					pojo.setAbusePsychologicalAbuse((Integer) ViewToPojoMapper.getValueByView(abusePsychologicalAbuse));
					pojo.setAbuseOthers((Integer) ViewToPojoMapper.getValueByView(abuseOthers));
					pojo.setAbuseSpecify((String) ViewToPojoMapper.getValueByView(abuseSpecify));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.healthHazardsOthers,R.id.abuseOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.healthHazardsOthers:
				 UiUtils.toggleSpecify(view, healthHazardsSpecify);
				break;
			case R.id.abuseOthers:
				 UiUtils.toggleSpecify(view, abuseSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;

			if ((model.getFamilyMentalIllness()==null || !(model.getFamilyMentalIllness()==1)))
			{
				getView().findViewById(R.id.underMedicationContainer).setVisibility(View.GONE);
				update = true;				
				model.setUnderMedication(null);

			}
			else
			{
				getView().findViewById(R.id.underMedicationContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.familyMentalIllness0,R.id.familyMentalIllness1})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
