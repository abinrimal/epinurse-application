package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_nutrition)
public class NutritionFragment extends BaseEpinurseFragment {


    public NutritionFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup foodStockAvailable;

	@ViewById
	@MapToModelField
	protected CheckBox cookingFuelUsedGas;

	@ViewById
	@MapToModelField
	protected CheckBox cookingFuelUsedFirewood;

	@ViewById
	@MapToModelField
	protected CheckBox cookingFuelUsedKerosene;

	@ViewById
	@MapToModelField
	protected CheckBox cookingFuelUsedOthers;

	@ViewById
	@MapToModelField
	protected EditText cookingFuelUsedSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox kindOfFoodAvailableCooked;

	@ViewById
	@MapToModelField
	protected CheckBox kindOfFoodAvailableJunk;

	@ViewById
	@MapToModelField
	protected CheckBox kindOfFoodAvailableOthers;

	@ViewById
	@MapToModelField
	protected EditText kindOfFoodAvailableSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup foodHygienic;

	@ViewById
	@MapToModelField
	protected RadioGroup foodStorageAppropriate;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(foodStockAvailable,pojo.getFoodStockAvailable());
					PojoToViewMapper.setViewValue(cookingFuelUsedGas,pojo.getCookingFuelUsedGas());
					PojoToViewMapper.setViewValue(cookingFuelUsedFirewood,pojo.getCookingFuelUsedFirewood());
					PojoToViewMapper.setViewValue(cookingFuelUsedKerosene,pojo.getCookingFuelUsedKerosene());
					PojoToViewMapper.setViewValue(cookingFuelUsedOthers,pojo.getCookingFuelUsedOthers());
					PojoToViewMapper.setViewValue(cookingFuelUsedSpecify,pojo.getCookingFuelUsedSpecify());
					PojoToViewMapper.setViewValue(kindOfFoodAvailableCooked,pojo.getKindOfFoodAvailableCooked());
					PojoToViewMapper.setViewValue(kindOfFoodAvailableJunk,pojo.getKindOfFoodAvailableJunk());
					PojoToViewMapper.setViewValue(kindOfFoodAvailableOthers,pojo.getKindOfFoodAvailableOthers());
					PojoToViewMapper.setViewValue(kindOfFoodAvailableSpecify,pojo.getKindOfFoodAvailableSpecify());
					PojoToViewMapper.setViewValue(foodHygienic,pojo.getFoodHygienic());
					PojoToViewMapper.setViewValue(foodStorageAppropriate,pojo.getFoodStorageAppropriate());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setFoodStockAvailable((Integer) ViewToPojoMapper.getValueByView(foodStockAvailable));
					pojo.setCookingFuelUsedGas((Integer) ViewToPojoMapper.getValueByView(cookingFuelUsedGas));
					pojo.setCookingFuelUsedFirewood((Integer) ViewToPojoMapper.getValueByView(cookingFuelUsedFirewood));
					pojo.setCookingFuelUsedKerosene((Integer) ViewToPojoMapper.getValueByView(cookingFuelUsedKerosene));
					pojo.setCookingFuelUsedOthers((Integer) ViewToPojoMapper.getValueByView(cookingFuelUsedOthers));
					pojo.setCookingFuelUsedSpecify((String) ViewToPojoMapper.getValueByView(cookingFuelUsedSpecify));
					pojo.setKindOfFoodAvailableCooked((Integer) ViewToPojoMapper.getValueByView(kindOfFoodAvailableCooked));
					pojo.setKindOfFoodAvailableJunk((Integer) ViewToPojoMapper.getValueByView(kindOfFoodAvailableJunk));
					pojo.setKindOfFoodAvailableOthers((Integer) ViewToPojoMapper.getValueByView(kindOfFoodAvailableOthers));
					pojo.setKindOfFoodAvailableSpecify((String) ViewToPojoMapper.getValueByView(kindOfFoodAvailableSpecify));
					pojo.setFoodHygienic((Integer) ViewToPojoMapper.getValueByView(foodHygienic));
					pojo.setFoodStorageAppropriate((Integer) ViewToPojoMapper.getValueByView(foodStorageAppropriate));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.cookingFuelUsedOthers,R.id.kindOfFoodAvailableOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.cookingFuelUsedOthers:
				 UiUtils.toggleSpecify(view, cookingFuelUsedSpecify);
				break;
			case R.id.kindOfFoodAvailableOthers:
				 UiUtils.toggleSpecify(view, kindOfFoodAvailableSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



}
