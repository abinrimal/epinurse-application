package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_personal_hygiene_and_environmental_sanitation)
public class PersonalHygieneAndEnvironmentalSanitationFragment extends BaseEpinurseFragment {


    public PersonalHygieneAndEnvironmentalSanitationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected CheckBox whenWashHandsAfterToileting;

	@ViewById
	@MapToModelField
	protected CheckBox whenWashHandsBeforeCooking;

	@ViewById
	@MapToModelField
	protected CheckBox whenWashHandsBeforeEating;

	@ViewById
	@MapToModelField
	protected CheckBox whenWashHandsBeforeFeedingAChild;

	@ViewById
	@MapToModelField
	protected CheckBox whenWashHandsOthers;

	@ViewById
	@MapToModelField
	protected EditText whenWashHandsSpecify;

	@ViewById
	@MapToModelField
	protected EditText bathingHabit;

	@ViewById
	@MapToModelField
	protected RadioGroup waterSupplySource;

	@ViewById
	@MapToModelField
	protected EditText waterSupplySourceSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup adequacyWaterSupply;

	@ViewById
	@MapToModelField
	protected RadioGroup qualityDrinkingWater;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationNone;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationBoiling;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationFiltration;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationUsingChemical;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationSodis;

	@ViewById
	@MapToModelField
	protected CheckBox waterPurificationOthers;

	@ViewById
	@MapToModelField
	protected EditText waterPurificationSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup distanceFromWater;

	@ViewById
	@MapToModelField
	protected EditText distanceFromWaterSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup toiletAvailable;

	@ViewById
	@MapToModelField
	protected RadioGroup adequateToiletRatio;

	@ViewById
	@MapToModelField
	protected RadioGroup toiletType;

	@ViewById
	@MapToModelField
	protected EditText toiletTypeSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup distanceTolietFromShelter;

	@ViewById
	@MapToModelField
	protected EditText toiletDistance;

	@ViewById
	@MapToModelField
	protected RadioGroup toiletDistanceUnit;

	@ViewById
	@MapToModelField
	protected EditText wasteDistance;

	@ViewById
	@MapToModelField
	protected RadioGroup wasteDistanceUnit;

	@ViewById
	@MapToModelField
	protected CheckBox disposalMethodComposting;

	@ViewById
	@MapToModelField
	protected CheckBox disposalMethodBurning;

	@ViewById
	@MapToModelField
	protected CheckBox disposalMethodDumping;

	@ViewById
	@MapToModelField
	protected CheckBox disposalMethodOthers;

	@ViewById
	@MapToModelField
	protected EditText disposalMethodSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup wasteCollectorAvailable;

	@ViewById
	@MapToModelField
	protected RadioGroup timelyCollection;

	@ViewById
	@MapToModelField
	protected RadioGroup hasDeadAnimalDisposal;

	@ViewById
	@MapToModelField
	protected RadioGroup hasDrainage;

	@ViewById
	@MapToModelField
	protected RadioGroup surroundingsClean;

	@ViewById
	@MapToModelField
	protected RadioGroup problemsMenstruationHygiene;

	@ViewById
	@MapToModelField
	protected CheckBox menstruationHygieneProblemsNoWater;

	@ViewById
	@MapToModelField
	protected CheckBox menstruationHygieneProblemsNoPrivacy;

	@ViewById
	@MapToModelField
	protected CheckBox menstruationHygieneProblemsNoPads;

	@ViewById
	@MapToModelField
	protected CheckBox menstruationHygieneProblemsNoDisposal;

	@ViewById
	@MapToModelField
	protected CheckBox menstruationHygieneProblemsOthers;

	@ViewById
	@MapToModelField
	protected EditText menstruationHygieneProblemsSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsSlipperyFloor;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsNoLight;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsBadElectical;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsRoughSurface;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsSloppyLand;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsDumpingArea;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsPolluted;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsNoVentilation;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsNoisy;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsOthers;

	@ViewById
	@MapToModelField
	protected EditText visibleHazardsSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox visibleHazardsNone;

	@ViewById
	@MapToModelField
	protected CheckBox areaProneToNone;

	@ViewById
	@MapToModelField
	protected CheckBox snakeBite;

	@ViewById
	@MapToModelField
	protected CheckBox animalBite;

	@ViewById
	@MapToModelField
	protected EditText animalBiteSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox insectBite;

	@ViewById
	@MapToModelField
	protected EditText insectBiteSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox others;

	@ViewById
	@MapToModelField
	protected EditText othersSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup noVectors;

	@ViewById
	@MapToModelField
	protected CheckBox mossquitoPreventionNets;

	@ViewById
	@MapToModelField
	protected CheckBox mossquitoPreventionLiquid;

	@ViewById
	@MapToModelField
	protected CheckBox mossquitoPreventionDrugs;

	@ViewById
	@MapToModelField
	protected CheckBox mossquitoPreventionOthers;

	@ViewById
	@MapToModelField
	protected EditText mossquitoPreventionSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup pestControl;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(whenWashHandsAfterToileting,pojo.getWhenWashHandsAfterToileting());
					PojoToViewMapper.setViewValue(whenWashHandsBeforeCooking,pojo.getWhenWashHandsBeforeCooking());
					PojoToViewMapper.setViewValue(whenWashHandsBeforeEating,pojo.getWhenWashHandsBeforeEating());
					PojoToViewMapper.setViewValue(whenWashHandsBeforeFeedingAChild,pojo.getWhenWashHandsBeforeFeedingAChild());
					PojoToViewMapper.setViewValue(whenWashHandsOthers,pojo.getWhenWashHandsOthers());
					PojoToViewMapper.setViewValue(whenWashHandsSpecify,pojo.getWhenWashHandsSpecify());
					PojoToViewMapper.setViewValue(bathingHabit,pojo.getBathingHabit());
					PojoToViewMapper.setViewValue(waterSupplySource,pojo.getWaterSupplySource());
					PojoToViewMapper.setViewValue(waterSupplySourceSpecify,pojo.getWaterSupplySourceSpecify());
					PojoToViewMapper.setViewValue(adequacyWaterSupply,pojo.getAdequacyWaterSupply());
					PojoToViewMapper.setViewValue(qualityDrinkingWater,pojo.getQualityDrinkingWater());
					PojoToViewMapper.setViewValue(waterPurificationNone,pojo.getWaterPurificationNone());
					PojoToViewMapper.setViewValue(waterPurificationBoiling,pojo.getWaterPurificationBoiling());
					PojoToViewMapper.setViewValue(waterPurificationFiltration,pojo.getWaterPurificationFiltration());
					PojoToViewMapper.setViewValue(waterPurificationUsingChemical,pojo.getWaterPurificationUsingChemical());
					PojoToViewMapper.setViewValue(waterPurificationSodis,pojo.getWaterPurificationSodis());
					PojoToViewMapper.setViewValue(waterPurificationOthers,pojo.getWaterPurificationOthers());
					PojoToViewMapper.setViewValue(waterPurificationSpecify,pojo.getWaterPurificationSpecify());
					PojoToViewMapper.setViewValue(distanceFromWater,pojo.getDistanceFromWater());
					PojoToViewMapper.setViewValue(distanceFromWaterSpecify,pojo.getDistanceFromWaterSpecify());
					PojoToViewMapper.setViewValue(toiletAvailable,pojo.getToiletAvailable());
					PojoToViewMapper.setViewValue(adequateToiletRatio,pojo.getAdequateToiletRatio());
					PojoToViewMapper.setViewValue(toiletType,pojo.getToiletType());
					PojoToViewMapper.setViewValue(toiletTypeSpecify,pojo.getToiletTypeSpecify());
					PojoToViewMapper.setViewValue(distanceTolietFromShelter,pojo.getDistanceTolietFromShelter());
					PojoToViewMapper.setViewValue(toiletDistance,pojo.getToiletDistance());
					PojoToViewMapper.setViewValue(toiletDistanceUnit,pojo.getToiletDistanceUnit());
					PojoToViewMapper.setViewValue(wasteDistance,pojo.getWasteDistance());
					PojoToViewMapper.setViewValue(wasteDistanceUnit,pojo.getWasteDistanceUnit());
					PojoToViewMapper.setViewValue(disposalMethodComposting,pojo.getDisposalMethodComposting());
					PojoToViewMapper.setViewValue(disposalMethodBurning,pojo.getDisposalMethodBurning());
					PojoToViewMapper.setViewValue(disposalMethodDumping,pojo.getDisposalMethodDumping());
					PojoToViewMapper.setViewValue(disposalMethodOthers,pojo.getDisposalMethodOthers());
					PojoToViewMapper.setViewValue(disposalMethodSpecify,pojo.getDisposalMethodSpecify());
					PojoToViewMapper.setViewValue(wasteCollectorAvailable,pojo.getWasteCollectorAvailable());
					PojoToViewMapper.setViewValue(timelyCollection,pojo.getTimelyCollection());
					PojoToViewMapper.setViewValue(hasDeadAnimalDisposal,pojo.getHasDeadAnimalDisposal());
					PojoToViewMapper.setViewValue(hasDrainage,pojo.getHasDrainage());
					PojoToViewMapper.setViewValue(surroundingsClean,pojo.getSurroundingsClean());
					PojoToViewMapper.setViewValue(problemsMenstruationHygiene,pojo.getProblemsMenstruationHygiene());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsNoWater,pojo.getMenstruationHygieneProblemsNoWater());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsNoPrivacy,pojo.getMenstruationHygieneProblemsNoPrivacy());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsNoPads,pojo.getMenstruationHygieneProblemsNoPads());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsNoDisposal,pojo.getMenstruationHygieneProblemsNoDisposal());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsOthers,pojo.getMenstruationHygieneProblemsOthers());
					PojoToViewMapper.setViewValue(menstruationHygieneProblemsSpecify,pojo.getMenstruationHygieneProblemsSpecify());
					PojoToViewMapper.setViewValue(visibleHazardsSlipperyFloor,pojo.getVisibleHazardsSlipperyFloor());
					PojoToViewMapper.setViewValue(visibleHazardsNoLight,pojo.getVisibleHazardsNoLight());
					PojoToViewMapper.setViewValue(visibleHazardsBadElectical,pojo.getVisibleHazardsBadElectical());
					PojoToViewMapper.setViewValue(visibleHazardsRoughSurface,pojo.getVisibleHazardsRoughSurface());
					PojoToViewMapper.setViewValue(visibleHazardsSloppyLand,pojo.getVisibleHazardsSloppyLand());
					PojoToViewMapper.setViewValue(visibleHazardsDumpingArea,pojo.getVisibleHazardsDumpingArea());
					PojoToViewMapper.setViewValue(visibleHazardsPolluted,pojo.getVisibleHazardsPolluted());
					PojoToViewMapper.setViewValue(visibleHazardsNoVentilation,pojo.getVisibleHazardsNoVentilation());
					PojoToViewMapper.setViewValue(visibleHazardsNoisy,pojo.getVisibleHazardsNoisy());
					PojoToViewMapper.setViewValue(visibleHazardsOthers,pojo.getVisibleHazardsOthers());
					PojoToViewMapper.setViewValue(visibleHazardsSpecify,pojo.getVisibleHazardsSpecify());
					PojoToViewMapper.setViewValue(visibleHazardsNone,pojo.getVisibleHazardsNone());
					PojoToViewMapper.setViewValue(areaProneToNone,pojo.getAreaProneToNone());
					PojoToViewMapper.setViewValue(snakeBite,pojo.getSnakeBite());
					PojoToViewMapper.setViewValue(animalBite,pojo.getAnimalBite());
					PojoToViewMapper.setViewValue(animalBiteSpecify,pojo.getAnimalBiteSpecify());
					PojoToViewMapper.setViewValue(insectBite,pojo.getInsectBite());
					PojoToViewMapper.setViewValue(insectBiteSpecify,pojo.getInsectBiteSpecify());
					PojoToViewMapper.setViewValue(others,pojo.getOthers());
					PojoToViewMapper.setViewValue(othersSpecify,pojo.getOthersSpecify());
					PojoToViewMapper.setViewValue(noVectors,pojo.getNoVectors());
					PojoToViewMapper.setViewValue(mossquitoPreventionNets,pojo.getMossquitoPreventionNets());
					PojoToViewMapper.setViewValue(mossquitoPreventionLiquid,pojo.getMossquitoPreventionLiquid());
					PojoToViewMapper.setViewValue(mossquitoPreventionDrugs,pojo.getMossquitoPreventionDrugs());
					PojoToViewMapper.setViewValue(mossquitoPreventionOthers,pojo.getMossquitoPreventionOthers());
					PojoToViewMapper.setViewValue(mossquitoPreventionSpecify,pojo.getMossquitoPreventionSpecify());
					PojoToViewMapper.setViewValue(pestControl,pojo.getPestControl());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setWhenWashHandsAfterToileting((Integer) ViewToPojoMapper.getValueByView(whenWashHandsAfterToileting));
					pojo.setWhenWashHandsBeforeCooking((Integer) ViewToPojoMapper.getValueByView(whenWashHandsBeforeCooking));
					pojo.setWhenWashHandsBeforeEating((Integer) ViewToPojoMapper.getValueByView(whenWashHandsBeforeEating));
					pojo.setWhenWashHandsBeforeFeedingAChild((Integer) ViewToPojoMapper.getValueByView(whenWashHandsBeforeFeedingAChild));
					pojo.setWhenWashHandsOthers((Integer) ViewToPojoMapper.getValueByView(whenWashHandsOthers));
					pojo.setWhenWashHandsSpecify((String) ViewToPojoMapper.getValueByView(whenWashHandsSpecify));
					pojo.setBathingHabit((Integer) ViewToPojoMapper.getValueByView(bathingHabit));
					pojo.setWaterSupplySource((Integer) ViewToPojoMapper.getValueByView(waterSupplySource));
					pojo.setWaterSupplySourceSpecify((String) ViewToPojoMapper.getValueByView(waterSupplySourceSpecify));
					pojo.setAdequacyWaterSupply((Integer) ViewToPojoMapper.getValueByView(adequacyWaterSupply));
					pojo.setQualityDrinkingWater((Integer) ViewToPojoMapper.getValueByView(qualityDrinkingWater));
					pojo.setWaterPurificationNone((Integer) ViewToPojoMapper.getValueByView(waterPurificationNone));
					pojo.setWaterPurificationBoiling((Integer) ViewToPojoMapper.getValueByView(waterPurificationBoiling));
					pojo.setWaterPurificationFiltration((Integer) ViewToPojoMapper.getValueByView(waterPurificationFiltration));
					pojo.setWaterPurificationUsingChemical((Integer) ViewToPojoMapper.getValueByView(waterPurificationUsingChemical));
					pojo.setWaterPurificationSodis((Integer) ViewToPojoMapper.getValueByView(waterPurificationSodis));
					pojo.setWaterPurificationOthers((Integer) ViewToPojoMapper.getValueByView(waterPurificationOthers));
					pojo.setWaterPurificationSpecify((String) ViewToPojoMapper.getValueByView(waterPurificationSpecify));
					pojo.setDistanceFromWater((Integer) ViewToPojoMapper.getValueByView(distanceFromWater));
					pojo.setDistanceFromWaterSpecify((String) ViewToPojoMapper.getValueByView(distanceFromWaterSpecify));
					pojo.setToiletAvailable((Integer) ViewToPojoMapper.getValueByView(toiletAvailable));
					pojo.setAdequateToiletRatio((Integer) ViewToPojoMapper.getValueByView(adequateToiletRatio));
					pojo.setToiletType((Integer) ViewToPojoMapper.getValueByView(toiletType));
					pojo.setToiletTypeSpecify((String) ViewToPojoMapper.getValueByView(toiletTypeSpecify));
					pojo.setDistanceTolietFromShelter((Integer) ViewToPojoMapper.getValueByView(distanceTolietFromShelter));
					pojo.setToiletDistance((Double) ViewToPojoMapper.getValueByView(toiletDistance));
					pojo.setToiletDistanceUnit((Integer) ViewToPojoMapper.getValueByView(toiletDistanceUnit));
					pojo.setWasteDistance((Double) ViewToPojoMapper.getValueByView(wasteDistance));
					pojo.setWasteDistanceUnit((Integer) ViewToPojoMapper.getValueByView(wasteDistanceUnit));
					pojo.setDisposalMethodComposting((Integer) ViewToPojoMapper.getValueByView(disposalMethodComposting));
					pojo.setDisposalMethodBurning((Integer) ViewToPojoMapper.getValueByView(disposalMethodBurning));
					pojo.setDisposalMethodDumping((Integer) ViewToPojoMapper.getValueByView(disposalMethodDumping));
					pojo.setDisposalMethodOthers((Integer) ViewToPojoMapper.getValueByView(disposalMethodOthers));
					pojo.setDisposalMethodSpecify((String) ViewToPojoMapper.getValueByView(disposalMethodSpecify));
					pojo.setWasteCollectorAvailable((Integer) ViewToPojoMapper.getValueByView(wasteCollectorAvailable));
					pojo.setTimelyCollection((Integer) ViewToPojoMapper.getValueByView(timelyCollection));
					pojo.setHasDeadAnimalDisposal((Integer) ViewToPojoMapper.getValueByView(hasDeadAnimalDisposal));
					pojo.setHasDrainage((Integer) ViewToPojoMapper.getValueByView(hasDrainage));
					pojo.setSurroundingsClean((Integer) ViewToPojoMapper.getValueByView(surroundingsClean));
					pojo.setProblemsMenstruationHygiene((Integer) ViewToPojoMapper.getValueByView(problemsMenstruationHygiene));
					pojo.setMenstruationHygieneProblemsNoWater((Integer) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsNoWater));
					pojo.setMenstruationHygieneProblemsNoPrivacy((Integer) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsNoPrivacy));
					pojo.setMenstruationHygieneProblemsNoPads((Integer) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsNoPads));
					pojo.setMenstruationHygieneProblemsNoDisposal((Integer) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsNoDisposal));
					pojo.setMenstruationHygieneProblemsOthers((Integer) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsOthers));
					pojo.setMenstruationHygieneProblemsSpecify((String) ViewToPojoMapper.getValueByView(menstruationHygieneProblemsSpecify));
					pojo.setVisibleHazardsSlipperyFloor((Integer) ViewToPojoMapper.getValueByView(visibleHazardsSlipperyFloor));
					pojo.setVisibleHazardsNoLight((Integer) ViewToPojoMapper.getValueByView(visibleHazardsNoLight));
					pojo.setVisibleHazardsBadElectical((Integer) ViewToPojoMapper.getValueByView(visibleHazardsBadElectical));
					pojo.setVisibleHazardsRoughSurface((Integer) ViewToPojoMapper.getValueByView(visibleHazardsRoughSurface));
					pojo.setVisibleHazardsSloppyLand((Integer) ViewToPojoMapper.getValueByView(visibleHazardsSloppyLand));
					pojo.setVisibleHazardsDumpingArea((Integer) ViewToPojoMapper.getValueByView(visibleHazardsDumpingArea));
					pojo.setVisibleHazardsPolluted((Integer) ViewToPojoMapper.getValueByView(visibleHazardsPolluted));
					pojo.setVisibleHazardsNoVentilation((Integer) ViewToPojoMapper.getValueByView(visibleHazardsNoVentilation));
					pojo.setVisibleHazardsNoisy((Integer) ViewToPojoMapper.getValueByView(visibleHazardsNoisy));
					pojo.setVisibleHazardsOthers((Integer) ViewToPojoMapper.getValueByView(visibleHazardsOthers));
					pojo.setVisibleHazardsSpecify((String) ViewToPojoMapper.getValueByView(visibleHazardsSpecify));
					pojo.setVisibleHazardsNone((Integer) ViewToPojoMapper.getValueByView(visibleHazardsNone));
					pojo.setAreaProneToNone((Integer) ViewToPojoMapper.getValueByView(areaProneToNone));
					pojo.setSnakeBite((Integer) ViewToPojoMapper.getValueByView(snakeBite));
					pojo.setAnimalBite((Integer) ViewToPojoMapper.getValueByView(animalBite));
					pojo.setAnimalBiteSpecify((String) ViewToPojoMapper.getValueByView(animalBiteSpecify));
					pojo.setInsectBite((Integer) ViewToPojoMapper.getValueByView(insectBite));
					pojo.setInsectBiteSpecify((String) ViewToPojoMapper.getValueByView(insectBiteSpecify));
					pojo.setOthers((Integer) ViewToPojoMapper.getValueByView(others));
					pojo.setOthersSpecify((String) ViewToPojoMapper.getValueByView(othersSpecify));
					pojo.setNoVectors((Integer) ViewToPojoMapper.getValueByView(noVectors));
					pojo.setMossquitoPreventionNets((Integer) ViewToPojoMapper.getValueByView(mossquitoPreventionNets));
					pojo.setMossquitoPreventionLiquid((Integer) ViewToPojoMapper.getValueByView(mossquitoPreventionLiquid));
					pojo.setMossquitoPreventionDrugs((Integer) ViewToPojoMapper.getValueByView(mossquitoPreventionDrugs));
					pojo.setMossquitoPreventionOthers((Integer) ViewToPojoMapper.getValueByView(mossquitoPreventionOthers));
					pojo.setMossquitoPreventionSpecify((String) ViewToPojoMapper.getValueByView(mossquitoPreventionSpecify));
					pojo.setPestControl((Integer) ViewToPojoMapper.getValueByView(pestControl));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.whenWashHandsOthers,R.id.waterSupplySource4,R.id.waterPurificationOthers,R.id.distanceFromWater2,R.id.toiletType3,R.id.disposalMethodOthers,R.id.menstruationHygieneProblemsOthers,R.id.visibleHazardsOthers,R.id.animalBite,R.id.insectBite,R.id.others,R.id.mossquitoPreventionOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.whenWashHandsOthers:
				 UiUtils.toggleSpecify(view, whenWashHandsSpecify);
				break;
			case R.id.waterSupplySource4:
				 UiUtils.toggleSpecify(view, waterSupplySourceSpecify);
				break;
			case R.id.waterPurificationOthers:
				 UiUtils.toggleSpecify(view, waterPurificationSpecify);
				break;
			case R.id.distanceFromWater2:
				 UiUtils.toggleSpecify(view, distanceFromWaterSpecify);
				break;
			case R.id.toiletType3:
				 UiUtils.toggleSpecify(view, toiletTypeSpecify);
				break;
			case R.id.disposalMethodOthers:
				 UiUtils.toggleSpecify(view, disposalMethodSpecify);
				break;
			case R.id.menstruationHygieneProblemsOthers:
				 UiUtils.toggleSpecify(view, menstruationHygieneProblemsSpecify);
				break;
			case R.id.visibleHazardsOthers:
				 UiUtils.toggleSpecify(view, visibleHazardsSpecify);
				break;
			case R.id.animalBite:
				 UiUtils.toggleSpecify(view, animalBiteSpecify);
				break;
			case R.id.insectBite:
				 UiUtils.toggleSpecify(view, insectBiteSpecify);
				break;
			case R.id.others:
				 UiUtils.toggleSpecify(view, othersSpecify);
				break;
			case R.id.mossquitoPreventionOthers:
				 UiUtils.toggleSpecify(view, mossquitoPreventionSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;

			if ((model.getWasteCollectorAvailable()==null || !(model.getWasteCollectorAvailable()==1)))
			{
				getView().findViewById(R.id.timelyCollectionContainer).setVisibility(View.GONE);
				update = true;				
				model.setTimelyCollection(null);

			}
			else
			{
				getView().findViewById(R.id.timelyCollectionContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
			{
				getView().findViewById(R.id.menstruationHygieneProblemsContainer).setVisibility(View.GONE);
				update = true;				
				model.setMenstruationHygieneProblemsNoWater(null);
				model.setMenstruationHygieneProblemsNoPrivacy(null);
				model.setMenstruationHygieneProblemsNoPads(null);
				model.setMenstruationHygieneProblemsNoDisposal(null);
				model.setMenstruationHygieneProblemsOthers(null);
				model.setMenstruationHygieneProblemsSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.menstruationHygieneProblemsContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.wasteCollectorAvailable0,R.id.wasteCollectorAvailable1,R.id.problemsMenstruationHygiene0,R.id.problemsMenstruationHygiene1})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
