package com.ajwcc.epinurse.communitynursing.gen;

import android.support.v4.view.ViewPager;
import android.widget.SeekBar;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import com.ajwcc.epinurse.common.SaveFragment_;


@EActivity(R.layout.gen_activity_community_nursing)
public class CommunityNursingActivity extends BaseEpinurseFormActivity
{

    @Extra
    public String uuid;

    @ViewById(R.id.title)
    public TextView title;

    @ViewById(R.id.pager)
    public ViewPager viewPager;

    @ViewById(R.id.seekbar)
    public SeekBar seekBar;

	@ViewById(R.id.previous)
    public Button previous;

    @ViewById(R.id.next)
    public Button next;
    
    @AfterViews
    public void init()
    {
    	
    	//title.setText("Community Nursing");
    	
    	title.setText(getResources().getString(R.string.community_nursing));
    	
    
        // load existing for editing at this point, given a uuid
        initModel(CommunityNursing.class, uuid);


        initViewPagerAndSeekbar(createFragmentList(), viewPager, seekBar, previous, next);
        
        createToC();
    	setValidationHandler(getValidationHandler());
    }


	public ValidationHandler<CommunityNursing> getValidationHandler()
	{
		CommunityNursingValidator validator = new CommunityNursingValidator(this);
		validator.setModel((CommunityNursing)getModel());	
		return validator;
	}

    LinkedHashMap<String, Integer> pageMap = new LinkedHashMap<>();

    private void createToC()
    {
        List<String> toc = createTableOfContents();
        for (int i=0; i<toc.size(); i++)
        {
            pageMap.put(toc.get(i), i);
        }
    }

    public Integer getPage(String name)
    {
        return pageMap.get(name);
    }

    protected List<String> createTableOfContents()
    {
    	List<String> names = new ArrayList<>();
    	
		names.add("Basic Information");
		names.add("Shelter");
		names.add("Personal Hygiene and Environmental Sanitation");
		names.add("Nutrition");
		names.add("Family Planning");
		names.add("Health Condition");
		names.add("Mental Illness");

    	
        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = new ArrayList<>();
        
		list.add(() -> BasicInformationFragment_.builder().build());
		list.add(() -> ShelterFragment_.builder().build());
		list.add(() -> PersonalHygieneAndEnvironmentalSanitationFragment_.builder().build());
		list.add(() -> NutritionFragment_.builder().build());
		list.add(() -> FamilyPlanningFragment_.builder().build());
		list.add(() -> HealthConditionFragment_.builder().build());
		list.add(() -> MentalIllnessFragment_.builder().build());


        list.add(() -> SaveFragment_.builder().build());


        return list;
    }

}
