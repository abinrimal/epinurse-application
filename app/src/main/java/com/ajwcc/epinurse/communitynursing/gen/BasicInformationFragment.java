package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_basic_information)
public class BasicInformationFragment extends BaseEpinurseFragment {


    public BasicInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup disasterRecently;

	@ViewById
	@MapToModelField
	protected EditText typeOfDisaster;

	@ViewById
	@MapToModelField
	protected EditText durationAfterDisaster;

	@ViewById
	@MapToModelField
	protected EditText numberHumanLoss;

	@ViewById
	@MapToModelField
	protected EditText numberMissingPeople;

	@ViewById
	@MapToModelField
	protected EditText numberInjuredPeople;

	@ViewById
	@MapToModelField
	protected EditText numberLivestockLoss;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(disasterRecently,pojo.getDisasterRecently());
					PojoToViewMapper.setViewValue(typeOfDisaster,pojo.getTypeOfDisaster());
					PojoToViewMapper.setViewValue(durationAfterDisaster,pojo.getDurationAfterDisaster());
					PojoToViewMapper.setViewValue(numberHumanLoss,pojo.getNumberHumanLoss());
					PojoToViewMapper.setViewValue(numberMissingPeople,pojo.getNumberMissingPeople());
					PojoToViewMapper.setViewValue(numberInjuredPeople,pojo.getNumberInjuredPeople());
					PojoToViewMapper.setViewValue(numberLivestockLoss,pojo.getNumberLivestockLoss());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setDisasterRecently((Integer) ViewToPojoMapper.getValueByView(disasterRecently));
					pojo.setTypeOfDisaster((String) ViewToPojoMapper.getValueByView(typeOfDisaster));
					pojo.setDurationAfterDisaster((String) ViewToPojoMapper.getValueByView(durationAfterDisaster));
					pojo.setNumberHumanLoss((Integer) ViewToPojoMapper.getValueByView(numberHumanLoss));
					pojo.setNumberMissingPeople((Integer) ViewToPojoMapper.getValueByView(numberMissingPeople));
					pojo.setNumberInjuredPeople((Integer) ViewToPojoMapper.getValueByView(numberInjuredPeople));
					pojo.setNumberLivestockLoss((Integer) ViewToPojoMapper.getValueByView(numberLivestockLoss));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.typeOfDisasterContainer).setVisibility(View.GONE);
				update = true;				
				model.setTypeOfDisaster(null);

			}
			else
			{
				getView().findViewById(R.id.typeOfDisasterContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.durationAfterDisasterContainer).setVisibility(View.GONE);
				update = true;				
				model.setDurationAfterDisaster(null);

			}
			else
			{
				getView().findViewById(R.id.durationAfterDisasterContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.numberHumanLossContainer).setVisibility(View.GONE);
				update = true;				
				model.setNumberHumanLoss(null);

			}
			else
			{
				getView().findViewById(R.id.numberHumanLossContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.numberMissingPeopleContainer).setVisibility(View.GONE);
				update = true;				
				model.setNumberMissingPeople(null);

			}
			else
			{
				getView().findViewById(R.id.numberMissingPeopleContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.numberInjuredPeopleContainer).setVisibility(View.GONE);
				update = true;				
				model.setNumberInjuredPeople(null);

			}
			else
			{
				getView().findViewById(R.id.numberInjuredPeopleContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
			{
				getView().findViewById(R.id.numberLivestockLossContainer).setVisibility(View.GONE);
				update = true;				
				model.setNumberLivestockLoss(null);

			}
			else
			{
				getView().findViewById(R.id.numberLivestockLossContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.disasterRecently0,R.id.disasterRecently1})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
