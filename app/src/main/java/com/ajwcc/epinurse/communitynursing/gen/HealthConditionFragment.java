package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_health_condition)
public class HealthConditionFragment extends BaseEpinurseFragment {


    public HealthConditionFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup unsualDiseaseOutbreak;

	@ViewById
	@MapToModelField
	protected EditText unsualDiseaseOutbreakSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup healthCareOnSite;

	@ViewById
	@MapToModelField
	protected RadioGroup pfaPresent;

	@ViewById
	@MapToModelField
	protected RadioGroup timeToNearestHealthFacility;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityHealthPost;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityPrimaryHealthCenter;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityDistrictHospital;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityZonalHospital;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilitySubRegionalHospital;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityTertiaryHospital;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityPrivateHospital;

	@ViewById
	@MapToModelField
	protected CheckBox typeOfNearestFacilityOthers;

	@ViewById
	@MapToModelField
	protected EditText typeOfNearestFacilitySpecify;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessDiabetesMellitus;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessHypertension;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessCopd;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessCancer;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessMentalHealthProblems;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessOthers;

	@ViewById
	@MapToModelField
	protected EditText familyIllnessSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox familyIllnessNone;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(unsualDiseaseOutbreak,pojo.getUnsualDiseaseOutbreak());
					PojoToViewMapper.setViewValue(unsualDiseaseOutbreakSpecify,pojo.getUnsualDiseaseOutbreakSpecify());
					PojoToViewMapper.setViewValue(healthCareOnSite,pojo.getHealthCareOnSite());
					PojoToViewMapper.setViewValue(pfaPresent,pojo.getPfaPresent());
					PojoToViewMapper.setViewValue(timeToNearestHealthFacility,pojo.getTimeToNearestHealthFacility());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityHealthPost,pojo.getTypeOfNearestFacilityHealthPost());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityPrimaryHealthCenter,pojo.getTypeOfNearestFacilityPrimaryHealthCenter());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityDistrictHospital,pojo.getTypeOfNearestFacilityDistrictHospital());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityZonalHospital,pojo.getTypeOfNearestFacilityZonalHospital());
					PojoToViewMapper.setViewValue(typeOfNearestFacilitySubRegionalHospital,pojo.getTypeOfNearestFacilitySubRegionalHospital());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityTertiaryHospital,pojo.getTypeOfNearestFacilityTertiaryHospital());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityPrivateHospital,pojo.getTypeOfNearestFacilityPrivateHospital());
					PojoToViewMapper.setViewValue(typeOfNearestFacilityOthers,pojo.getTypeOfNearestFacilityOthers());
					PojoToViewMapper.setViewValue(typeOfNearestFacilitySpecify,pojo.getTypeOfNearestFacilitySpecify());
					PojoToViewMapper.setViewValue(familyIllnessDiabetesMellitus,pojo.getFamilyIllnessDiabetesMellitus());
					PojoToViewMapper.setViewValue(familyIllnessHypertension,pojo.getFamilyIllnessHypertension());
					PojoToViewMapper.setViewValue(familyIllnessCopd,pojo.getFamilyIllnessCopd());
					PojoToViewMapper.setViewValue(familyIllnessCancer,pojo.getFamilyIllnessCancer());
					PojoToViewMapper.setViewValue(familyIllnessMentalHealthProblems,pojo.getFamilyIllnessMentalHealthProblems());
					PojoToViewMapper.setViewValue(familyIllnessOthers,pojo.getFamilyIllnessOthers());
					PojoToViewMapper.setViewValue(familyIllnessSpecify,pojo.getFamilyIllnessSpecify());
					PojoToViewMapper.setViewValue(familyIllnessNone,pojo.getFamilyIllnessNone());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setUnsualDiseaseOutbreak((Integer) ViewToPojoMapper.getValueByView(unsualDiseaseOutbreak));
					pojo.setUnsualDiseaseOutbreakSpecify((String) ViewToPojoMapper.getValueByView(unsualDiseaseOutbreakSpecify));
					pojo.setHealthCareOnSite((Integer) ViewToPojoMapper.getValueByView(healthCareOnSite));
					pojo.setPfaPresent((Integer) ViewToPojoMapper.getValueByView(pfaPresent));
					pojo.setTimeToNearestHealthFacility((Integer) ViewToPojoMapper.getValueByView(timeToNearestHealthFacility));
					pojo.setTypeOfNearestFacilityHealthPost((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityHealthPost));
					pojo.setTypeOfNearestFacilityPrimaryHealthCenter((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityPrimaryHealthCenter));
					pojo.setTypeOfNearestFacilityDistrictHospital((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityDistrictHospital));
					pojo.setTypeOfNearestFacilityZonalHospital((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityZonalHospital));
					pojo.setTypeOfNearestFacilitySubRegionalHospital((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilitySubRegionalHospital));
					pojo.setTypeOfNearestFacilityTertiaryHospital((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityTertiaryHospital));
					pojo.setTypeOfNearestFacilityPrivateHospital((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityPrivateHospital));
					pojo.setTypeOfNearestFacilityOthers((Integer) ViewToPojoMapper.getValueByView(typeOfNearestFacilityOthers));
					pojo.setTypeOfNearestFacilitySpecify((String) ViewToPojoMapper.getValueByView(typeOfNearestFacilitySpecify));
					pojo.setFamilyIllnessDiabetesMellitus((Integer) ViewToPojoMapper.getValueByView(familyIllnessDiabetesMellitus));
					pojo.setFamilyIllnessHypertension((Integer) ViewToPojoMapper.getValueByView(familyIllnessHypertension));
					pojo.setFamilyIllnessCopd((Integer) ViewToPojoMapper.getValueByView(familyIllnessCopd));
					pojo.setFamilyIllnessCancer((Integer) ViewToPojoMapper.getValueByView(familyIllnessCancer));
					pojo.setFamilyIllnessMentalHealthProblems((Integer) ViewToPojoMapper.getValueByView(familyIllnessMentalHealthProblems));
					pojo.setFamilyIllnessOthers((Integer) ViewToPojoMapper.getValueByView(familyIllnessOthers));
					pojo.setFamilyIllnessSpecify((String) ViewToPojoMapper.getValueByView(familyIllnessSpecify));
					pojo.setFamilyIllnessNone((Integer) ViewToPojoMapper.getValueByView(familyIllnessNone));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.unsualDiseaseOutbreak1,R.id.typeOfNearestFacilityOthers,R.id.familyIllnessOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.unsualDiseaseOutbreak1:
				 UiUtils.toggleSpecify(view, unsualDiseaseOutbreakSpecify);
				break;
			case R.id.typeOfNearestFacilityOthers:
				 UiUtils.toggleSpecify(view, typeOfNearestFacilitySpecify);
				break;
			case R.id.familyIllnessOthers:
				 UiUtils.toggleSpecify(view, familyIllnessSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



}
