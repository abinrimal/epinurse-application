package com.ajwcc.epinurse.communitynursing.gen;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;


public class CommunityNursingJsonSerializer implements JsonSerializer<CommunityNursing> {

    @Override
    public JsonElement serialize(CommunityNursing model, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        
        object.add("uuid", context.serialize(model.getUuid()));
        object.add("ownerUuid", context.serialize(model.getOwnerUuid()));
        
		serializeShineId(model, object, context); // shine_id

		// Basic Information
		serializeDisasterRecently(model, object, context); // disaster_recently
		serializeTypeOfDisaster(model, object, context); // type_of_disaster
		serializeDurationAfterDisaster(model, object, context); // duration_after_disaster
		serializeNumberHumanLoss(model, object, context); // number_human_loss
		serializeNumberMissingPeople(model, object, context); // number_missing_people
		serializeNumberInjuredPeople(model, object, context); // number_injured_people
		serializeNumberLivestockLoss(model, object, context); // number_livestock_loss

		// Shelter
		serializeTypeOfShelter(model, object, context); // type_of_shelter
		serializeTypeOfShelterSpecify(model, object, context); // type_of_shelter_specify
		serializeAdequacyOfSpace(model, object, context); // adequacy_of_space
		serializeDamaged(model, object, context); // damaged
		serializeSourceOfLightElectricity(model, object, context); // source_of_light_electricity
		serializeSourceOfLightOilLamp(model, object, context); // source_of_light_oil_lamp
		serializeSourceOfLightSolar(model, object, context); // source_of_light_solar
		serializeSourceOfLightOthers(model, object, context); // source_of_light_others
		serializeSourceOfLightSpecify(model, object, context); // source_of_light_specify
		serializeAdequateVentilation(model, object, context); // adequate_ventilation
		serializeKitchen(model, object, context); // kitchen

		// Personal Hygiene and Environmental Sanitation
		serializeWhenWashHandsAfterToileting(model, object, context); // when_wash_hands_after_toileting
		serializeWhenWashHandsBeforeCooking(model, object, context); // when_wash_hands_before_cooking
		serializeWhenWashHandsBeforeEating(model, object, context); // when_wash_hands_before_eating
		serializeWhenWashHandsBeforeFeedingAChild(model, object, context); // when_wash_hands_before_feeding_a_child
		serializeWhenWashHandsOthers(model, object, context); // when_wash_hands_others
		serializeWhenWashHandsSpecify(model, object, context); // when_wash_hands_specify
		serializeBathingHabit(model, object, context); // bathing_habit
		serializeWaterSupplySource(model, object, context); // water_supply_source
		serializeWaterSupplySourceSpecify(model, object, context); // water_supply_source_specify
		serializeAdequacyWaterSupply(model, object, context); // adequacy_water_supply
		serializeQualityDrinkingWater(model, object, context); // quality_drinking_water
		serializeWaterPurificationNone(model, object, context); // water_purification_none
		serializeWaterPurificationBoiling(model, object, context); // water_purification_boiling
		serializeWaterPurificationFiltration(model, object, context); // water_purification_filtration
		serializeWaterPurificationUsingChemical(model, object, context); // water_purification_using_chemical
		serializeWaterPurificationSodis(model, object, context); // water_purification_sodis
		serializeWaterPurificationOthers(model, object, context); // water_purification_others
		serializeWaterPurificationSpecify(model, object, context); // water_purification_specify
		serializeDistanceFromWater(model, object, context); // distance_from_water
		serializeDistanceFromWaterSpecify(model, object, context); // distance_from_water_specify
		serializeToiletAvailable(model, object, context); // toilet_available
		serializeAdequateToiletRatio(model, object, context); // adequate_toilet_ratio
		serializeToiletType(model, object, context); // toilet_type
		serializeToiletTypeSpecify(model, object, context); // toilet_type_specify
		serializeDistanceTolietFromShelter(model, object, context); // distance_toliet_from_shelter
		serializeToiletDistance(model, object, context); // toilet_distance
		serializeToiletDistanceUnit(model, object, context); // toilet_distance_unit
		serializeWasteDistance(model, object, context); // waste_distance
		serializeWasteDistanceUnit(model, object, context); // waste_distance_unit
		serializeDisposalMethodComposting(model, object, context); // disposal_method_composting
		serializeDisposalMethodBurning(model, object, context); // disposal_method_burning
		serializeDisposalMethodDumping(model, object, context); // disposal_method_dumping
		serializeDisposalMethodOthers(model, object, context); // disposal_method_others
		serializeDisposalMethodSpecify(model, object, context); // disposal_method_specify
		serializeWasteCollectorAvailable(model, object, context); // waste_collector_available
		serializeTimelyCollection(model, object, context); // timely_collection
		serializeHasDeadAnimalDisposal(model, object, context); // has_dead_animal_disposal
		serializeHasDrainage(model, object, context); // has_drainage
		serializeSurroundingsClean(model, object, context); // surroundings_clean
		serializeProblemsMenstruationHygiene(model, object, context); // problems_menstruation_hygiene
		serializeMenstruationHygieneProblemsNoWater(model, object, context); // menstruation_hygiene_problems_no_water
		serializeMenstruationHygieneProblemsNoPrivacy(model, object, context); // menstruation_hygiene_problems_no_privacy
		serializeMenstruationHygieneProblemsNoPads(model, object, context); // menstruation_hygiene_problems_no_pads
		serializeMenstruationHygieneProblemsNoDisposal(model, object, context); // menstruation_hygiene_problems_no_disposal
		serializeMenstruationHygieneProblemsOthers(model, object, context); // menstruation_hygiene_problems_others
		serializeMenstruationHygieneProblemsSpecify(model, object, context); // menstruation_hygiene_problems_specify
		serializeVisibleHazardsSlipperyFloor(model, object, context); // visible_hazards_slippery_floor
		serializeVisibleHazardsNoLight(model, object, context); // visible_hazards_no_light
		serializeVisibleHazardsBadElectical(model, object, context); // visible_hazards_bad_electical
		serializeVisibleHazardsRoughSurface(model, object, context); // visible_hazards_rough_surface
		serializeVisibleHazardsSloppyLand(model, object, context); // visible_hazards_sloppy_land
		serializeVisibleHazardsDumpingArea(model, object, context); // visible_hazards_dumping_area
		serializeVisibleHazardsPolluted(model, object, context); // visible_hazards_polluted
		serializeVisibleHazardsNoVentilation(model, object, context); // visible_hazards_no_ventilation
		serializeVisibleHazardsNoisy(model, object, context); // visible_hazards_noisy
		serializeVisibleHazardsOthers(model, object, context); // visible_hazards_others
		serializeVisibleHazardsSpecify(model, object, context); // visible_hazards_specify
		serializeVisibleHazardsNone(model, object, context); // visible_hazards_none
		serializeAreaProneToNone(model, object, context); // area_prone_to_none
		serializeSnakeBite(model, object, context); // snake_bite
		serializeAnimalBite(model, object, context); // animal_bite
		serializeAnimalBiteSpecify(model, object, context); // animal_bite_specify
		serializeInsectBite(model, object, context); // insect_bite
		serializeInsectBiteSpecify(model, object, context); // insect_bite_specify
		serializeOthers(model, object, context); // others
		serializeOthersSpecify(model, object, context); // others_specify
		serializeNoVectors(model, object, context); // no_vectors
		serializeMossquitoPreventionNets(model, object, context); // mossquito_prevention_nets
		serializeMossquitoPreventionLiquid(model, object, context); // mossquito_prevention_liquid
		serializeMossquitoPreventionDrugs(model, object, context); // mossquito_prevention_drugs
		serializeMossquitoPreventionOthers(model, object, context); // mossquito_prevention_others
		serializeMossquitoPreventionSpecify(model, object, context); // mossquito_prevention_specify
		serializePestControl(model, object, context); // pest_control

		// Nutrition
		serializeFoodStockAvailable(model, object, context); // food_stock_available
		serializeCookingFuelUsedGas(model, object, context); // cooking_fuel_used_gas
		serializeCookingFuelUsedFirewood(model, object, context); // cooking_fuel_used_firewood
		serializeCookingFuelUsedKerosene(model, object, context); // cooking_fuel_used_kerosene
		serializeCookingFuelUsedOthers(model, object, context); // cooking_fuel_used_others
		serializeCookingFuelUsedSpecify(model, object, context); // cooking_fuel_used_specify
		serializeKindOfFoodAvailableCooked(model, object, context); // kind_of_food_available_cooked
		serializeKindOfFoodAvailableJunk(model, object, context); // kind_of_food_available_junk
		serializeKindOfFoodAvailableOthers(model, object, context); // kind_of_food_available_others
		serializeKindOfFoodAvailableSpecify(model, object, context); // kind_of_food_available_specify
		serializeFoodHygienic(model, object, context); // food_hygienic
		serializeFoodStorageAppropriate(model, object, context); // food_storage_appropriate

		// Family Planning
		serializeFamilyPlanningMethod(model, object, context); // family_planning_method
		serializeKnowOfServices(model, object, context); // know_of_services
		serializeKnowOfEcp(model, object, context); // know_of_ecp
		serializeWhereEcpAvailableMobileClinic(model, object, context); // where_ecp_available_mobile_clinic
		serializeWhereEcpAvailableHealthFacility(model, object, context); // where_ecp_available_health_facility
		serializeWhereEcpAvailableOthers(model, object, context); // where_ecp_available_others
		serializeWhereEcpAvailableSpecify(model, object, context); // where_ecp_available_specify

		// Health Condition
		serializeUnsualDiseaseOutbreak(model, object, context); // unsual_disease_outbreak
		serializeUnsualDiseaseOutbreakSpecify(model, object, context); // unsual_disease_outbreak_specify
		serializeHealthCareOnSite(model, object, context); // health_care_on_site
		serializePfaPresent(model, object, context); // pfa_present
		serializeTimeToNearestHealthFacility(model, object, context); // time_to_nearest_health_facility
		serializeTypeOfNearestFacilityHealthPost(model, object, context); // type_of_nearest_facility_health_post
		serializeTypeOfNearestFacilityPrimaryHealthCenter(model, object, context); // type_of_nearest_facility_primary_health_center
		serializeTypeOfNearestFacilityDistrictHospital(model, object, context); // type_of_nearest_facility_district_hospital
		serializeTypeOfNearestFacilityZonalHospital(model, object, context); // type_of_nearest_facility_zonal_hospital
		serializeTypeOfNearestFacilitySubRegionalHospital(model, object, context); // type_of_nearest_facility_sub_regional_hospital
		serializeTypeOfNearestFacilityTertiaryHospital(model, object, context); // type_of_nearest_facility_tertiary_hospital
		serializeTypeOfNearestFacilityPrivateHospital(model, object, context); // type_of_nearest_facility_private_hospital
		serializeTypeOfNearestFacilityOthers(model, object, context); // type_of_nearest_facility_others
		serializeTypeOfNearestFacilitySpecify(model, object, context); // type_of_nearest_facility_specify
		serializeFamilyIllnessDiabetesMellitus(model, object, context); // family_illness_diabetes_mellitus
		serializeFamilyIllnessHypertension(model, object, context); // family_illness_hypertension
		serializeFamilyIllnessCopd(model, object, context); // family_illness_copd
		serializeFamilyIllnessCancer(model, object, context); // family_illness_cancer
		serializeFamilyIllnessMentalHealthProblems(model, object, context); // family_illness_mental_health_problems
		serializeFamilyIllnessOthers(model, object, context); // family_illness_others
		serializeFamilyIllnessSpecify(model, object, context); // family_illness_specify
		serializeFamilyIllnessNone(model, object, context); // family_illness_none

		// Mental Illness
		serializeFamilyMentalIllness(model, object, context); // family_mental_illness
		serializeUnderMedication(model, object, context); // under_medication
		serializeHealthHazardsNone(model, object, context); // health_hazards_none
		serializeHealthHazardsSmoking(model, object, context); // health_hazards_smoking
		serializeHealthHazardsAlcoholism(model, object, context); // health_hazards_alcoholism
		serializeHealthHazardsDrugAbuse(model, object, context); // health_hazards_drug_abuse
		serializeHealthHazardsOthers(model, object, context); // health_hazards_others
		serializeHealthHazardsSpecify(model, object, context); // health_hazards_specify
		serializeAbuseNo(model, object, context); // abuse_no
		serializeAbuseSexualAbuse(model, object, context); // abuse_sexual_abuse
		serializeAbusePhysicalAbuse(model, object, context); // abuse_physical_abuse
		serializeAbusePsychologicalAbuse(model, object, context); // abuse_psychological_abuse
		serializeAbuseOthers(model, object, context); // abuse_others
		serializeAbuseSpecify(model, object, context); // abuse_specify
 
       
        return object;
    }
    

    public void serializeShineId(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("shine_id", context.serialize(model.getShineId()));
    }

    public void serializeDisasterRecently(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disaster_recently", context.serialize(model.getDisasterRecently()));
    }

    public void serializeTypeOfDisaster(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_disaster", context.serialize(model.getTypeOfDisaster()));
    }

    public void serializeDurationAfterDisaster(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("duration_after_disaster", context.serialize(model.getDurationAfterDisaster()));
    }

    public void serializeNumberHumanLoss(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("number_human_loss", context.serialize(model.getNumberHumanLoss()));
    }

    public void serializeNumberMissingPeople(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("number_missing_people", context.serialize(model.getNumberMissingPeople()));
    }

    public void serializeNumberInjuredPeople(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("number_injured_people", context.serialize(model.getNumberInjuredPeople()));
    }

    public void serializeNumberLivestockLoss(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("number_livestock_loss", context.serialize(model.getNumberLivestockLoss()));
    }

    public void serializeTypeOfShelter(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_shelter", context.serialize(model.getTypeOfShelter()));
    }

    public void serializeTypeOfShelterSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_shelter_specify", context.serialize(model.getTypeOfShelterSpecify()));
    }

    public void serializeAdequacyOfSpace(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("adequacy_of_space", context.serialize(model.getAdequacyOfSpace()));
    }

    public void serializeDamaged(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("damaged", context.serialize(model.getDamaged()));
    }

    public void serializeSourceOfLightElectricity(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_of_light_electricity", context.serialize(model.getSourceOfLightElectricity()));
    }

    public void serializeSourceOfLightOilLamp(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_of_light_oil_lamp", context.serialize(model.getSourceOfLightOilLamp()));
    }

    public void serializeSourceOfLightSolar(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_of_light_solar", context.serialize(model.getSourceOfLightSolar()));
    }

    public void serializeSourceOfLightOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_of_light_others", context.serialize(model.getSourceOfLightOthers()));
    }

    public void serializeSourceOfLightSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("source_of_light_specify", context.serialize(model.getSourceOfLightSpecify()));
    }

    public void serializeAdequateVentilation(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("adequate_ventilation", context.serialize(model.getAdequateVentilation()));
    }

    public void serializeKitchen(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("kitchen", context.serialize(model.getKitchen()));
    }

    public void serializeWhenWashHandsAfterToileting(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_after_toileting", context.serialize(model.getWhenWashHandsAfterToileting()));
    }

    public void serializeWhenWashHandsBeforeCooking(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_before_cooking", context.serialize(model.getWhenWashHandsBeforeCooking()));
    }

    public void serializeWhenWashHandsBeforeEating(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_before_eating", context.serialize(model.getWhenWashHandsBeforeEating()));
    }

    public void serializeWhenWashHandsBeforeFeedingAChild(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_before_feeding_a_child", context.serialize(model.getWhenWashHandsBeforeFeedingAChild()));
    }

    public void serializeWhenWashHandsOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_others", context.serialize(model.getWhenWashHandsOthers()));
    }

    public void serializeWhenWashHandsSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("when_wash_hands_specify", context.serialize(model.getWhenWashHandsSpecify()));
    }

    public void serializeBathingHabit(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("bathing_habit", context.serialize(model.getBathingHabit()));
    }

    public void serializeWaterSupplySource(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_supply_source", context.serialize(model.getWaterSupplySource()));
    }

    public void serializeWaterSupplySourceSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_supply_source_specify", context.serialize(model.getWaterSupplySourceSpecify()));
    }

    public void serializeAdequacyWaterSupply(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("adequacy_water_supply", context.serialize(model.getAdequacyWaterSupply()));
    }

    public void serializeQualityDrinkingWater(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("quality_drinking_water", context.serialize(model.getQualityDrinkingWater()));
    }

    public void serializeWaterPurificationNone(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_none", context.serialize(model.getWaterPurificationNone()));
    }

    public void serializeWaterPurificationBoiling(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_boiling", context.serialize(model.getWaterPurificationBoiling()));
    }

    public void serializeWaterPurificationFiltration(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_filtration", context.serialize(model.getWaterPurificationFiltration()));
    }

    public void serializeWaterPurificationUsingChemical(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_using_chemical", context.serialize(model.getWaterPurificationUsingChemical()));
    }

    public void serializeWaterPurificationSodis(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_sodis", context.serialize(model.getWaterPurificationSodis()));
    }

    public void serializeWaterPurificationOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_others", context.serialize(model.getWaterPurificationOthers()));
    }

    public void serializeWaterPurificationSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_purification_specify", context.serialize(model.getWaterPurificationSpecify()));
    }

    public void serializeDistanceFromWater(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("distance_from_water", context.serialize(model.getDistanceFromWater()));
    }

    public void serializeDistanceFromWaterSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("distance_from_water_specify", context.serialize(model.getDistanceFromWaterSpecify()));
    }

    public void serializeToiletAvailable(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_available", context.serialize(model.getToiletAvailable()));
    }

    public void serializeAdequateToiletRatio(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("adequate_toilet_ratio", context.serialize(model.getAdequateToiletRatio()));
    }

    public void serializeToiletType(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_type", context.serialize(model.getToiletType()));
    }

    public void serializeToiletTypeSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_type_specify", context.serialize(model.getToiletTypeSpecify()));
    }

    public void serializeDistanceTolietFromShelter(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("distance_toliet_from_shelter", context.serialize(model.getDistanceTolietFromShelter()));
    }

    public void serializeToiletDistance(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_distance", context.serialize(model.getToiletDistance()));
    }

    public void serializeToiletDistanceUnit(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_distance_unit", context.serialize(model.getToiletDistanceUnit()));
    }

    public void serializeWasteDistance(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("waste_distance", context.serialize(model.getWasteDistance()));
    }

    public void serializeWasteDistanceUnit(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("waste_distance_unit", context.serialize(model.getWasteDistanceUnit()));
    }

    public void serializeDisposalMethodComposting(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disposal_method_composting", context.serialize(model.getDisposalMethodComposting()));
    }

    public void serializeDisposalMethodBurning(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disposal_method_burning", context.serialize(model.getDisposalMethodBurning()));
    }

    public void serializeDisposalMethodDumping(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disposal_method_dumping", context.serialize(model.getDisposalMethodDumping()));
    }

    public void serializeDisposalMethodOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disposal_method_others", context.serialize(model.getDisposalMethodOthers()));
    }

    public void serializeDisposalMethodSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disposal_method_specify", context.serialize(model.getDisposalMethodSpecify()));
    }

    public void serializeWasteCollectorAvailable(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("waste_collector_available", context.serialize(model.getWasteCollectorAvailable()));
    }

    public void serializeTimelyCollection(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("timely_collection", context.serialize(model.getTimelyCollection()));
    }

    public void serializeHasDeadAnimalDisposal(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("has_dead_animal_disposal", context.serialize(model.getHasDeadAnimalDisposal()));
    }

    public void serializeHasDrainage(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("has_drainage", context.serialize(model.getHasDrainage()));
    }

    public void serializeSurroundingsClean(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("surroundings_clean", context.serialize(model.getSurroundingsClean()));
    }

    public void serializeProblemsMenstruationHygiene(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("problems_menstruation_hygiene", context.serialize(model.getProblemsMenstruationHygiene()));
    }

    public void serializeMenstruationHygieneProblemsNoWater(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_no_water", context.serialize(model.getMenstruationHygieneProblemsNoWater()));
    }

    public void serializeMenstruationHygieneProblemsNoPrivacy(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_no_privacy", context.serialize(model.getMenstruationHygieneProblemsNoPrivacy()));
    }

    public void serializeMenstruationHygieneProblemsNoPads(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_no_pads", context.serialize(model.getMenstruationHygieneProblemsNoPads()));
    }

    public void serializeMenstruationHygieneProblemsNoDisposal(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_no_disposal", context.serialize(model.getMenstruationHygieneProblemsNoDisposal()));
    }

    public void serializeMenstruationHygieneProblemsOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_others", context.serialize(model.getMenstruationHygieneProblemsOthers()));
    }

    public void serializeMenstruationHygieneProblemsSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("menstruation_hygiene_problems_specify", context.serialize(model.getMenstruationHygieneProblemsSpecify()));
    }

    public void serializeVisibleHazardsSlipperyFloor(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_slippery_floor", context.serialize(model.getVisibleHazardsSlipperyFloor()));
    }

    public void serializeVisibleHazardsNoLight(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_no_light", context.serialize(model.getVisibleHazardsNoLight()));
    }

    public void serializeVisibleHazardsBadElectical(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_bad_electical", context.serialize(model.getVisibleHazardsBadElectical()));
    }

    public void serializeVisibleHazardsRoughSurface(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_rough_surface", context.serialize(model.getVisibleHazardsRoughSurface()));
    }

    public void serializeVisibleHazardsSloppyLand(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_sloppy_land", context.serialize(model.getVisibleHazardsSloppyLand()));
    }

    public void serializeVisibleHazardsDumpingArea(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_dumping_area", context.serialize(model.getVisibleHazardsDumpingArea()));
    }

    public void serializeVisibleHazardsPolluted(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_polluted", context.serialize(model.getVisibleHazardsPolluted()));
    }

    public void serializeVisibleHazardsNoVentilation(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_no_ventilation", context.serialize(model.getVisibleHazardsNoVentilation()));
    }

    public void serializeVisibleHazardsNoisy(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_noisy", context.serialize(model.getVisibleHazardsNoisy()));
    }

    public void serializeVisibleHazardsOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_others", context.serialize(model.getVisibleHazardsOthers()));
    }

    public void serializeVisibleHazardsSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_specify", context.serialize(model.getVisibleHazardsSpecify()));
    }

    public void serializeVisibleHazardsNone(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("visible_hazards_none", context.serialize(model.getVisibleHazardsNone()));
    }

    public void serializeAreaProneToNone(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("area_prone_to_none", context.serialize(model.getAreaProneToNone()));
    }

    public void serializeSnakeBite(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("snake_bite", context.serialize(model.getSnakeBite()));
    }

    public void serializeAnimalBite(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("animal_bite", context.serialize(model.getAnimalBite()));
    }

    public void serializeAnimalBiteSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("animal_bite_specify", context.serialize(model.getAnimalBiteSpecify()));
    }

    public void serializeInsectBite(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("insect_bite", context.serialize(model.getInsectBite()));
    }

    public void serializeInsectBiteSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("insect_bite_specify", context.serialize(model.getInsectBiteSpecify()));
    }

    public void serializeOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("others", context.serialize(model.getOthers()));
    }

    public void serializeOthersSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("others_specify", context.serialize(model.getOthersSpecify()));
    }

    public void serializeNoVectors(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("no_vectors", context.serialize(model.getNoVectors()));
    }

    public void serializeMossquitoPreventionNets(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mossquito_prevention_nets", context.serialize(model.getMossquitoPreventionNets()));
    }

    public void serializeMossquitoPreventionLiquid(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mossquito_prevention_liquid", context.serialize(model.getMossquitoPreventionLiquid()));
    }

    public void serializeMossquitoPreventionDrugs(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mossquito_prevention_drugs", context.serialize(model.getMossquitoPreventionDrugs()));
    }

    public void serializeMossquitoPreventionOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mossquito_prevention_others", context.serialize(model.getMossquitoPreventionOthers()));
    }

    public void serializeMossquitoPreventionSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("mossquito_prevention_specify", context.serialize(model.getMossquitoPreventionSpecify()));
    }

    public void serializePestControl(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pest_control", context.serialize(model.getPestControl()));
    }

    public void serializeFoodStockAvailable(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_stock_available", context.serialize(model.getFoodStockAvailable()));
    }

    public void serializeCookingFuelUsedGas(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cooking_fuel_used_gas", context.serialize(model.getCookingFuelUsedGas()));
    }

    public void serializeCookingFuelUsedFirewood(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cooking_fuel_used_firewood", context.serialize(model.getCookingFuelUsedFirewood()));
    }

    public void serializeCookingFuelUsedKerosene(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cooking_fuel_used_kerosene", context.serialize(model.getCookingFuelUsedKerosene()));
    }

    public void serializeCookingFuelUsedOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cooking_fuel_used_others", context.serialize(model.getCookingFuelUsedOthers()));
    }

    public void serializeCookingFuelUsedSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("cooking_fuel_used_specify", context.serialize(model.getCookingFuelUsedSpecify()));
    }

    public void serializeKindOfFoodAvailableCooked(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("kind_of_food_available_cooked", context.serialize(model.getKindOfFoodAvailableCooked()));
    }

    public void serializeKindOfFoodAvailableJunk(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("kind_of_food_available_junk", context.serialize(model.getKindOfFoodAvailableJunk()));
    }

    public void serializeKindOfFoodAvailableOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("kind_of_food_available_others", context.serialize(model.getKindOfFoodAvailableOthers()));
    }

    public void serializeKindOfFoodAvailableSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("kind_of_food_available_specify", context.serialize(model.getKindOfFoodAvailableSpecify()));
    }

    public void serializeFoodHygienic(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_hygienic", context.serialize(model.getFoodHygienic()));
    }

    public void serializeFoodStorageAppropriate(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_storage_appropriate", context.serialize(model.getFoodStorageAppropriate()));
    }

    public void serializeFamilyPlanningMethod(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_planning_method", context.serialize(model.getFamilyPlanningMethod()));
    }

    public void serializeKnowOfServices(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("know_of_services", context.serialize(model.getKnowOfServices()));
    }

    public void serializeKnowOfEcp(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("know_of_ecp", context.serialize(model.getKnowOfEcp()));
    }

    public void serializeWhereEcpAvailableMobileClinic(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_ecp_available_mobile_clinic", context.serialize(model.getWhereEcpAvailableMobileClinic()));
    }

    public void serializeWhereEcpAvailableHealthFacility(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_ecp_available_health_facility", context.serialize(model.getWhereEcpAvailableHealthFacility()));
    }

    public void serializeWhereEcpAvailableOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_ecp_available_others", context.serialize(model.getWhereEcpAvailableOthers()));
    }

    public void serializeWhereEcpAvailableSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("where_ecp_available_specify", context.serialize(model.getWhereEcpAvailableSpecify()));
    }

    public void serializeUnsualDiseaseOutbreak(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("unsual_disease_outbreak", context.serialize(model.getUnsualDiseaseOutbreak()));
    }

    public void serializeUnsualDiseaseOutbreakSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("unsual_disease_outbreak_specify", context.serialize(model.getUnsualDiseaseOutbreakSpecify()));
    }

    public void serializeHealthCareOnSite(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_care_on_site", context.serialize(model.getHealthCareOnSite()));
    }

    public void serializePfaPresent(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pfa_present", context.serialize(model.getPfaPresent()));
    }

    public void serializeTimeToNearestHealthFacility(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("time_to_nearest_health_facility", context.serialize(model.getTimeToNearestHealthFacility()));
    }

    public void serializeTypeOfNearestFacilityHealthPost(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_health_post", context.serialize(model.getTypeOfNearestFacilityHealthPost()));
    }

    public void serializeTypeOfNearestFacilityPrimaryHealthCenter(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_primary_health_center", context.serialize(model.getTypeOfNearestFacilityPrimaryHealthCenter()));
    }

    public void serializeTypeOfNearestFacilityDistrictHospital(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_district_hospital", context.serialize(model.getTypeOfNearestFacilityDistrictHospital()));
    }

    public void serializeTypeOfNearestFacilityZonalHospital(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_zonal_hospital", context.serialize(model.getTypeOfNearestFacilityZonalHospital()));
    }

    public void serializeTypeOfNearestFacilitySubRegionalHospital(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_sub_regional_hospital", context.serialize(model.getTypeOfNearestFacilitySubRegionalHospital()));
    }

    public void serializeTypeOfNearestFacilityTertiaryHospital(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_tertiary_hospital", context.serialize(model.getTypeOfNearestFacilityTertiaryHospital()));
    }

    public void serializeTypeOfNearestFacilityPrivateHospital(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_private_hospital", context.serialize(model.getTypeOfNearestFacilityPrivateHospital()));
    }

    public void serializeTypeOfNearestFacilityOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_others", context.serialize(model.getTypeOfNearestFacilityOthers()));
    }

    public void serializeTypeOfNearestFacilitySpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_of_nearest_facility_specify", context.serialize(model.getTypeOfNearestFacilitySpecify()));
    }

    public void serializeFamilyIllnessDiabetesMellitus(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_diabetes_mellitus", context.serialize(model.getFamilyIllnessDiabetesMellitus()));
    }

    public void serializeFamilyIllnessHypertension(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_hypertension", context.serialize(model.getFamilyIllnessHypertension()));
    }

    public void serializeFamilyIllnessCopd(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_copd", context.serialize(model.getFamilyIllnessCopd()));
    }

    public void serializeFamilyIllnessCancer(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_cancer", context.serialize(model.getFamilyIllnessCancer()));
    }

    public void serializeFamilyIllnessMentalHealthProblems(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_mental_health_problems", context.serialize(model.getFamilyIllnessMentalHealthProblems()));
    }

    public void serializeFamilyIllnessOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_others", context.serialize(model.getFamilyIllnessOthers()));
    }

    public void serializeFamilyIllnessSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_specify", context.serialize(model.getFamilyIllnessSpecify()));
    }

    public void serializeFamilyIllnessNone(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_illness_none", context.serialize(model.getFamilyIllnessNone()));
    }

    public void serializeFamilyMentalIllness(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("family_mental_illness", context.serialize(model.getFamilyMentalIllness()));
    }

    public void serializeUnderMedication(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("under_medication", context.serialize(model.getUnderMedication()));
    }

    public void serializeHealthHazardsNone(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_none", context.serialize(model.getHealthHazardsNone()));
    }

    public void serializeHealthHazardsSmoking(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_smoking", context.serialize(model.getHealthHazardsSmoking()));
    }

    public void serializeHealthHazardsAlcoholism(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_alcoholism", context.serialize(model.getHealthHazardsAlcoholism()));
    }

    public void serializeHealthHazardsDrugAbuse(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_drug_abuse", context.serialize(model.getHealthHazardsDrugAbuse()));
    }

    public void serializeHealthHazardsOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_others", context.serialize(model.getHealthHazardsOthers()));
    }

    public void serializeHealthHazardsSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("health_hazards_specify", context.serialize(model.getHealthHazardsSpecify()));
    }

    public void serializeAbuseNo(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_no", context.serialize(model.getAbuseNo()));
    }

    public void serializeAbuseSexualAbuse(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_sexual_abuse", context.serialize(model.getAbuseSexualAbuse()));
    }

    public void serializeAbusePhysicalAbuse(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_physical_abuse", context.serialize(model.getAbusePhysicalAbuse()));
    }

    public void serializeAbusePsychologicalAbuse(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_psychological_abuse", context.serialize(model.getAbusePsychologicalAbuse()));
    }

    public void serializeAbuseOthers(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_others", context.serialize(model.getAbuseOthers()));
    }

    public void serializeAbuseSpecify(CommunityNursing model, JsonObject object, JsonSerializationContext context)
    {
        object.add("abuse_specify", context.serialize(model.getAbuseSpecify()));
    }
 
    

}