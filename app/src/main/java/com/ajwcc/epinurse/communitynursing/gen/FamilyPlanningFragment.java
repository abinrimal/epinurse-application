package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_family_planning)
public class FamilyPlanningFragment extends BaseEpinurseFragment {


    public FamilyPlanningFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup familyPlanningMethod;

	@ViewById
	@MapToModelField
	protected RadioGroup knowOfServices;

	@ViewById
	@MapToModelField
	protected RadioGroup knowOfEcp;

	@ViewById
	@MapToModelField
	protected CheckBox whereEcpAvailableMobileClinic;

	@ViewById
	@MapToModelField
	protected CheckBox whereEcpAvailableHealthFacility;

	@ViewById
	@MapToModelField
	protected CheckBox whereEcpAvailableOthers;

	@ViewById
	@MapToModelField
	protected EditText whereEcpAvailableSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(familyPlanningMethod,pojo.getFamilyPlanningMethod());
					PojoToViewMapper.setViewValue(knowOfServices,pojo.getKnowOfServices());
					PojoToViewMapper.setViewValue(knowOfEcp,pojo.getKnowOfEcp());
					PojoToViewMapper.setViewValue(whereEcpAvailableMobileClinic,pojo.getWhereEcpAvailableMobileClinic());
					PojoToViewMapper.setViewValue(whereEcpAvailableHealthFacility,pojo.getWhereEcpAvailableHealthFacility());
					PojoToViewMapper.setViewValue(whereEcpAvailableOthers,pojo.getWhereEcpAvailableOthers());
					PojoToViewMapper.setViewValue(whereEcpAvailableSpecify,pojo.getWhereEcpAvailableSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setFamilyPlanningMethod((Integer) ViewToPojoMapper.getValueByView(familyPlanningMethod));
					pojo.setKnowOfServices((Integer) ViewToPojoMapper.getValueByView(knowOfServices));
					pojo.setKnowOfEcp((Integer) ViewToPojoMapper.getValueByView(knowOfEcp));
					pojo.setWhereEcpAvailableMobileClinic((Integer) ViewToPojoMapper.getValueByView(whereEcpAvailableMobileClinic));
					pojo.setWhereEcpAvailableHealthFacility((Integer) ViewToPojoMapper.getValueByView(whereEcpAvailableHealthFacility));
					pojo.setWhereEcpAvailableOthers((Integer) ViewToPojoMapper.getValueByView(whereEcpAvailableOthers));
					pojo.setWhereEcpAvailableSpecify((String) ViewToPojoMapper.getValueByView(whereEcpAvailableSpecify));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.whereEcpAvailableOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.whereEcpAvailableOthers:
				 UiUtils.toggleSpecify(view, whereEcpAvailableSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;

			if ((model.getFamilyPlanningMethod()==null || !(model.getFamilyPlanningMethod()==0)))
			{
				getView().findViewById(R.id.knowOfServicesContainer).setVisibility(View.GONE);
				update = true;				
				model.setKnowOfServices(null);

			}
			else
			{
				getView().findViewById(R.id.knowOfServicesContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getFamilyPlanningMethod()==null || !(model.getFamilyPlanningMethod()==0)))
			{
				getView().findViewById(R.id.knowOfEcpContainer).setVisibility(View.GONE);
				update = true;				
				model.setKnowOfEcp(null);

			}
			else
			{
				getView().findViewById(R.id.knowOfEcpContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getKnowOfEcp()==null || !(model.getKnowOfEcp()==1)))
			{
				getView().findViewById(R.id.whereEcpAvailableContainer).setVisibility(View.GONE);
				update = true;				
				model.setWhereEcpAvailableMobileClinic(null);
				model.setWhereEcpAvailableHealthFacility(null);
				model.setWhereEcpAvailableOthers(null);
				model.setWhereEcpAvailableSpecify(null);

			}
			else
			{
				getView().findViewById(R.id.whereEcpAvailableContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.familyPlanningMethod0,R.id.familyPlanningMethod1,R.id.familyPlanningMethod2,R.id.familyPlanningMethod3,R.id.familyPlanningMethod4,R.id.knowOfEcp0,R.id.knowOfEcp1})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
