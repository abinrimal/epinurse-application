package com.ajwcc.epinurse.communitynursing.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursing;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursingActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

public class CommunityNursingValidator extends com.ajwcc.epinurse.communitynursing.gen.CommunityNursingValidator
{
    public CommunityNursingValidator(CommunityNursingActivity a)
    {
        super(a);
    }

    @Override
    public void validateModel()
    {
        // check if owenerUuid has been set
        validateNonNullField(model.getOwnerUuid(), activity.getPage("Owner"), R.id.button, context.getResources().getString(R.string.basic_information_profile));  // TODO: USE RES


        super.validateModel();
    }
}
