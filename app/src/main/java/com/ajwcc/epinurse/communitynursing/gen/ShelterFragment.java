package com.ajwcc.epinurse.communitynursing.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_community_nursing_shelter)
public class ShelterFragment extends BaseEpinurseFragment {


    public ShelterFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup typeOfShelter;

	@ViewById
	@MapToModelField
	protected EditText typeOfShelterSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup adequacyOfSpace;

	@ViewById
	@MapToModelField
	protected RadioGroup damaged;

	@ViewById
	@MapToModelField
	protected CheckBox sourceOfLightElectricity;

	@ViewById
	@MapToModelField
	protected CheckBox sourceOfLightOilLamp;

	@ViewById
	@MapToModelField
	protected CheckBox sourceOfLightSolar;

	@ViewById
	@MapToModelField
	protected CheckBox sourceOfLightOthers;

	@ViewById
	@MapToModelField
	protected EditText sourceOfLightSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup adequateVentilation;

	@ViewById
	@MapToModelField
	protected RadioGroup kitchen;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
					PojoToViewMapper.setViewValue(typeOfShelter,pojo.getTypeOfShelter());
					PojoToViewMapper.setViewValue(typeOfShelterSpecify,pojo.getTypeOfShelterSpecify());
					PojoToViewMapper.setViewValue(adequacyOfSpace,pojo.getAdequacyOfSpace());
					PojoToViewMapper.setViewValue(damaged,pojo.getDamaged());
					PojoToViewMapper.setViewValue(sourceOfLightElectricity,pojo.getSourceOfLightElectricity());
					PojoToViewMapper.setViewValue(sourceOfLightOilLamp,pojo.getSourceOfLightOilLamp());
					PojoToViewMapper.setViewValue(sourceOfLightSolar,pojo.getSourceOfLightSolar());
					PojoToViewMapper.setViewValue(sourceOfLightOthers,pojo.getSourceOfLightOthers());
					PojoToViewMapper.setViewValue(sourceOfLightSpecify,pojo.getSourceOfLightSpecify());
					PojoToViewMapper.setViewValue(adequateVentilation,pojo.getAdequateVentilation());
					PojoToViewMapper.setViewValue(kitchen,pojo.getKitchen());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: CommunityNursing");
                	CommunityNursing pojo = (CommunityNursing) getModel();
                	
					pojo.setTypeOfShelter((Integer) ViewToPojoMapper.getValueByView(typeOfShelter));
					pojo.setTypeOfShelterSpecify((String) ViewToPojoMapper.getValueByView(typeOfShelterSpecify));
					pojo.setAdequacyOfSpace((Integer) ViewToPojoMapper.getValueByView(adequacyOfSpace));
					pojo.setDamaged((Integer) ViewToPojoMapper.getValueByView(damaged));
					pojo.setSourceOfLightElectricity((Integer) ViewToPojoMapper.getValueByView(sourceOfLightElectricity));
					pojo.setSourceOfLightOilLamp((Integer) ViewToPojoMapper.getValueByView(sourceOfLightOilLamp));
					pojo.setSourceOfLightSolar((Integer) ViewToPojoMapper.getValueByView(sourceOfLightSolar));
					pojo.setSourceOfLightOthers((Integer) ViewToPojoMapper.getValueByView(sourceOfLightOthers));
					pojo.setSourceOfLightSpecify((String) ViewToPojoMapper.getValueByView(sourceOfLightSpecify));
					pojo.setAdequateVentilation((Integer) ViewToPojoMapper.getValueByView(adequateVentilation));
					pojo.setKitchen((Integer) ViewToPojoMapper.getValueByView(kitchen));

					if (save)
					{
                		System.out.println("Save to realm: CommunityNursing");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.typeOfShelter4,R.id.sourceOfLightOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.typeOfShelter4:
				 UiUtils.toggleSpecify(view, typeOfShelterSpecify);
				break;
			case R.id.sourceOfLightOthers:
				 UiUtils.toggleSpecify(view, sourceOfLightSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			CommunityNursing model = (CommunityNursing) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(CommunityNursing mode, boolean update)
	{
		return update;
	}
	



}
