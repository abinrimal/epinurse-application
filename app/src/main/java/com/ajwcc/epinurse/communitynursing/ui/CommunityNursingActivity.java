package com.ajwcc.epinurse.communitynursing.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.OwnerSelectFragment_;
import com.ajwcc.epinurse.common.image.ImageListFragment_;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursing;
import com.ajwcc.util.ui.validation.ValidationHandler;

import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;
import java.util.List;


@EActivity(R.layout.gen_activity_community_nursing)
public class CommunityNursingActivity extends com.ajwcc.epinurse.communitynursing.gen.CommunityNursingActivity
{
    public ValidationHandler<CommunityNursing> getValidationHandler()
    {
        CommunityNursingValidator validator = new CommunityNursingValidator(this);
        validator.setModel((CommunityNursing)getModel());
        return validator;
    }

    protected List<String> createTableOfContents()
    {
        List<String> names = super.createTableOfContents();

        names.add(0,"Owner");

        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = super.createFragmentList();

        // FORCE CHANGE THE GENERATED FRAGMENT HERE
        list.add(0, () -> OwnerSelectFragment_.builder().build());

        list.add(list.size()-1, ()-> ImageListFragment_.builder().build());

        return list;
    }

}
