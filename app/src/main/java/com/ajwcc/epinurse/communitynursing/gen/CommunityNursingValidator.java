package com.ajwcc.epinurse.communitynursing.gen;

import com.ajwcc.epinurse.common.BaseValidator;
import com.ajwcc.util.ui.validation.ValidationHandler;
import com.ajwcc.epinurse.R;

public class CommunityNursingValidator extends BaseValidator implements ValidationHandler<CommunityNursing>
{
    protected CommunityNursing model;
    protected CommunityNursingActivity activity;


    public CommunityNursingValidator(CommunityNursingActivity a)
    {
    	super(a);
        activity = a;
    }

    @Override
    public void setModel(CommunityNursing model)
    {
        this.model = model;
    }

    @Override
    public void validateModel()
    {
		validateDisasterRecently();
		validateTypeOfDisaster();
		validateDurationAfterDisaster();
		validateNumberHumanLoss();
		validateNumberMissingPeople();
		validateNumberInjuredPeople();
		validateNumberLivestockLoss();
		validateTypeOfShelter();
		validateTypeOfShelterSpecify();
		validateAdequacyOfSpace();
		validateDamaged();
		validateSourceOfLightElectricity();
		validateSourceOfLightOilLamp();
		validateSourceOfLightSolar();
		validateSourceOfLightOthers();
		validateSourceOfLightSpecify();
		validateAdequateVentilation();
		validateKitchen();
		validateWhenWashHandsAfterToileting();
		validateWhenWashHandsBeforeCooking();
		validateWhenWashHandsBeforeEating();
		validateWhenWashHandsBeforeFeedingAChild();
		validateWhenWashHandsOthers();
		validateWhenWashHandsSpecify();
		validateBathingHabit();
		validateWaterSupplySource();
		validateWaterSupplySourceSpecify();
		validateAdequacyWaterSupply();
		validateQualityDrinkingWater();
		validateWaterPurificationNone();
		validateWaterPurificationBoiling();
		validateWaterPurificationFiltration();
		validateWaterPurificationUsingChemical();
		validateWaterPurificationSodis();
		validateWaterPurificationOthers();
		validateWaterPurificationSpecify();
		validateDistanceFromWater();
		validateDistanceFromWaterSpecify();
		validateToiletAvailable();
		validateAdequateToiletRatio();
		validateToiletType();
		validateToiletTypeSpecify();
		validateDistanceTolietFromShelter();
		validateToiletDistance();
		validateToiletDistanceUnit();
		validateWasteDistance();
		validateWasteDistanceUnit();
		validateDisposalMethodComposting();
		validateDisposalMethodBurning();
		validateDisposalMethodDumping();
		validateDisposalMethodOthers();
		validateDisposalMethodSpecify();
		validateWasteCollectorAvailable();
		validateTimelyCollection();
		validateHasDeadAnimalDisposal();
		validateHasDrainage();
		validateSurroundingsClean();
		validateProblemsMenstruationHygiene();
		validateMenstruationHygieneProblemsNoWater();
		validateMenstruationHygieneProblemsNoPrivacy();
		validateMenstruationHygieneProblemsNoPads();
		validateMenstruationHygieneProblemsNoDisposal();
		validateMenstruationHygieneProblemsOthers();
		validateMenstruationHygieneProblemsSpecify();
		validateVisibleHazardsSlipperyFloor();
		validateVisibleHazardsNoLight();
		validateVisibleHazardsBadElectical();
		validateVisibleHazardsRoughSurface();
		validateVisibleHazardsSloppyLand();
		validateVisibleHazardsDumpingArea();
		validateVisibleHazardsPolluted();
		validateVisibleHazardsNoVentilation();
		validateVisibleHazardsNoisy();
		validateVisibleHazardsOthers();
		validateVisibleHazardsSpecify();
		validateVisibleHazardsNone();
		validateAreaProneToNone();
		validateSnakeBite();
		validateAnimalBite();
		validateAnimalBiteSpecify();
		validateInsectBite();
		validateInsectBiteSpecify();
		validateOthers();
		validateOthersSpecify();
		validateNoVectors();
		validateMossquitoPreventionNets();
		validateMossquitoPreventionLiquid();
		validateMossquitoPreventionDrugs();
		validateMossquitoPreventionOthers();
		validateMossquitoPreventionSpecify();
		validatePestControl();
		validateFoodStockAvailable();
		validateCookingFuelUsedGas();
		validateCookingFuelUsedFirewood();
		validateCookingFuelUsedKerosene();
		validateCookingFuelUsedOthers();
		validateCookingFuelUsedSpecify();
		validateKindOfFoodAvailableCooked();
		validateKindOfFoodAvailableJunk();
		validateKindOfFoodAvailableOthers();
		validateKindOfFoodAvailableSpecify();
		validateFoodHygienic();
		validateFoodStorageAppropriate();
		validateFamilyPlanningMethod();
		validateKnowOfServices();
		validateKnowOfEcp();
		validateWhereEcpAvailableMobileClinic();
		validateWhereEcpAvailableHealthFacility();
		validateWhereEcpAvailableOthers();
		validateWhereEcpAvailableSpecify();
		validateUnsualDiseaseOutbreak();
		validateUnsualDiseaseOutbreakSpecify();
		validateHealthCareOnSite();
		validatePfaPresent();
		validateTimeToNearestHealthFacility();
		validateTypeOfNearestFacilityHealthPost();
		validateTypeOfNearestFacilityPrimaryHealthCenter();
		validateTypeOfNearestFacilityDistrictHospital();
		validateTypeOfNearestFacilityZonalHospital();
		validateTypeOfNearestFacilitySubRegionalHospital();
		validateTypeOfNearestFacilityTertiaryHospital();
		validateTypeOfNearestFacilityPrivateHospital();
		validateTypeOfNearestFacilityOthers();
		validateTypeOfNearestFacilitySpecify();
		validateFamilyIllnessDiabetesMellitus();
		validateFamilyIllnessHypertension();
		validateFamilyIllnessCopd();
		validateFamilyIllnessCancer();
		validateFamilyIllnessMentalHealthProblems();
		validateFamilyIllnessOthers();
		validateFamilyIllnessSpecify();
		validateFamilyIllnessNone();
		validateFamilyMentalIllness();
		validateUnderMedication();
		validateHealthHazardsNone();
		validateHealthHazardsSmoking();
		validateHealthHazardsAlcoholism();
		validateHealthHazardsDrugAbuse();
		validateHealthHazardsOthers();
		validateHealthHazardsSpecify();
		validateAbuseNo();
		validateAbuseSexualAbuse();
		validateAbusePhysicalAbuse();
		validateAbusePsychologicalAbuse();
		validateAbuseOthers();
		validateAbuseSpecify();

    }


    public void validateDisasterRecently()
    {
    
        validateNonNullField(model.getDisasterRecently(), activity.getPage("Basic Information"), R.id.disasterRecentlyContainer, context.getResources().getString(R.string.community_nursing_disasterRecently));
    }

    public void validateTypeOfDisaster()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getTypeOfDisaster(), activity.getPage("Basic Information"), R.id.typeOfDisasterContainer, context.getResources().getString(R.string.community_nursing_typeOfDisaster));
    }

    public void validateDurationAfterDisaster()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getDurationAfterDisaster(), activity.getPage("Basic Information"), R.id.durationAfterDisasterContainer, context.getResources().getString(R.string.community_nursing_durationAfterDisaster));
    }

    public void validateNumberHumanLoss()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getNumberHumanLoss(), activity.getPage("Basic Information"), R.id.numberHumanLossContainer, context.getResources().getString(R.string.community_nursing_numberHumanLoss));
    }

    public void validateNumberMissingPeople()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getNumberMissingPeople(), activity.getPage("Basic Information"), R.id.numberMissingPeopleContainer, context.getResources().getString(R.string.community_nursing_numberMissingPeople));
    }

    public void validateNumberInjuredPeople()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getNumberInjuredPeople(), activity.getPage("Basic Information"), R.id.numberInjuredPeopleContainer, context.getResources().getString(R.string.community_nursing_numberInjuredPeople));
    }

    public void validateNumberLivestockLoss()
    {
		if((model.getDisasterRecently()==null || !(model.getDisasterRecently()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getNumberLivestockLoss(), activity.getPage("Basic Information"), R.id.numberLivestockLossContainer, context.getResources().getString(R.string.community_nursing_numberLivestockLoss));
    }

    public void validateTypeOfShelter()
    {
    
        validateNonNullField(model.getTypeOfShelter(), activity.getPage("Shelter"), R.id.typeOfShelterContainer, context.getResources().getString(R.string.community_nursing_typeOfShelter));
    }

    public void validateTypeOfShelterSpecify()
    {
  
        validateNonNullSpecifyField(model.getTypeOfShelter(),4,model.getTypeOfShelterSpecify(), activity.getPage("Shelter"), R.id.typeOfShelterSpecifyContainer, context.getResources().getString(R.string.community_nursing_typeOfShelterSpecify));   
    }

    public void validateAdequacyOfSpace()
    {
    
        validateNonNullField(model.getAdequacyOfSpace(), activity.getPage("Shelter"), R.id.adequacyOfSpaceContainer, context.getResources().getString(R.string.community_nursing_adequacyOfSpace));
    }

    public void validateDamaged()
    {
    
        validateNonNullField(model.getDamaged(), activity.getPage("Shelter"), R.id.damagedContainer, context.getResources().getString(R.string.community_nursing_damaged));
    }

    public void validateSourceOfLightElectricity()
    {
    
        validateNonNullField(model.getSourceOfLightElectricity(), activity.getPage("Shelter"), R.id.sourceOfLightElectricityContainer, context.getResources().getString(R.string.community_nursing_sourceOfLightElectricity));
    }

    public void validateSourceOfLightOilLamp()
    {
    
        validateNonNullField(model.getSourceOfLightOilLamp(), activity.getPage("Shelter"), R.id.sourceOfLightOilLampContainer, context.getResources().getString(R.string.community_nursing_sourceOfLightOilLamp));
    }

    public void validateSourceOfLightSolar()
    {
    
        validateNonNullField(model.getSourceOfLightSolar(), activity.getPage("Shelter"), R.id.sourceOfLightSolarContainer, context.getResources().getString(R.string.community_nursing_sourceOfLightSolar));
    }

    public void validateSourceOfLightOthers()
    {
    
        validateNonNullField(model.getSourceOfLightOthers(), activity.getPage("Shelter"), R.id.sourceOfLightOthersContainer, context.getResources().getString(R.string.community_nursing_sourceOfLightOthers));
    }

    public void validateSourceOfLightSpecify()
    {
  
        validateNonNullSpecifyField(model.getSourceOfLightOthers(),1,model.getSourceOfLightSpecify(), activity.getPage("Shelter"), R.id.sourceOfLightSpecifyContainer, context.getResources().getString(R.string.community_nursing_sourceOfLightSpecify));   
    }

    public void validateAdequateVentilation()
    {
    
        validateNonNullField(model.getAdequateVentilation(), activity.getPage("Shelter"), R.id.adequateVentilationContainer, context.getResources().getString(R.string.community_nursing_adequateVentilation));
    }

    public void validateKitchen()
    {
    
        validateNonNullField(model.getKitchen(), activity.getPage("Shelter"), R.id.kitchenContainer, context.getResources().getString(R.string.community_nursing_kitchen));
    }

    public void validateWhenWashHandsAfterToileting()
    {
    
        validateNonNullField(model.getWhenWashHandsAfterToileting(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsAfterToiletingContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsAfterToileting));
    }

    public void validateWhenWashHandsBeforeCooking()
    {
    
        validateNonNullField(model.getWhenWashHandsBeforeCooking(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsBeforeCookingContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsBeforeCooking));
    }

    public void validateWhenWashHandsBeforeEating()
    {
    
        validateNonNullField(model.getWhenWashHandsBeforeEating(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsBeforeEatingContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsBeforeEating));
    }

    public void validateWhenWashHandsBeforeFeedingAChild()
    {
    
        validateNonNullField(model.getWhenWashHandsBeforeFeedingAChild(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsBeforeFeedingAChildContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsBeforeFeedingAChild));
    }

    public void validateWhenWashHandsOthers()
    {
    
        validateNonNullField(model.getWhenWashHandsOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsOthersContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsOthers));
    }

    public void validateWhenWashHandsSpecify()
    {
  
        validateNonNullSpecifyField(model.getWhenWashHandsOthers(),1,model.getWhenWashHandsSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.whenWashHandsSpecifyContainer, context.getResources().getString(R.string.community_nursing_whenWashHandsSpecify));   
    }

    public void validateBathingHabit()
    {
    
        validateNonNullField(model.getBathingHabit(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.bathingHabitContainer, context.getResources().getString(R.string.community_nursing_bathingHabit));
    }

    public void validateWaterSupplySource()
    {
    
        validateNonNullField(model.getWaterSupplySource(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterSupplySourceContainer, context.getResources().getString(R.string.community_nursing_waterSupplySource));
    }

    public void validateWaterSupplySourceSpecify()
    {
  
        validateNonNullSpecifyField(model.getWaterSupplySource(),4,model.getWaterSupplySourceSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterSupplySourceSpecifyContainer, context.getResources().getString(R.string.community_nursing_waterSupplySourceSpecify));   
    }

    public void validateAdequacyWaterSupply()
    {
    
        validateNonNullField(model.getAdequacyWaterSupply(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.adequacyWaterSupplyContainer, context.getResources().getString(R.string.community_nursing_adequacyWaterSupply));
    }

    public void validateQualityDrinkingWater()
    {
    
        validateNonNullField(model.getQualityDrinkingWater(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.qualityDrinkingWaterContainer, context.getResources().getString(R.string.community_nursing_qualityDrinkingWater));
    }

    public void validateWaterPurificationNone()
    {
    
        validateNonNullField(model.getWaterPurificationNone(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationNoneContainer, context.getResources().getString(R.string.community_nursing_waterPurificationNone));
    }

    public void validateWaterPurificationBoiling()
    {
    
        validateNonNullField(model.getWaterPurificationBoiling(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationBoilingContainer, context.getResources().getString(R.string.community_nursing_waterPurificationBoiling));
    }

    public void validateWaterPurificationFiltration()
    {
    
        validateNonNullField(model.getWaterPurificationFiltration(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationFiltrationContainer, context.getResources().getString(R.string.community_nursing_waterPurificationFiltration));
    }

    public void validateWaterPurificationUsingChemical()
    {
    
        validateNonNullField(model.getWaterPurificationUsingChemical(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationUsingChemicalContainer, context.getResources().getString(R.string.community_nursing_waterPurificationUsingChemical));
    }

    public void validateWaterPurificationSodis()
    {
    
        validateNonNullField(model.getWaterPurificationSodis(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationSodisContainer, context.getResources().getString(R.string.community_nursing_waterPurificationSodis));
    }

    public void validateWaterPurificationOthers()
    {
    
        validateNonNullField(model.getWaterPurificationOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationOthersContainer, context.getResources().getString(R.string.community_nursing_waterPurificationOthers));
    }

    public void validateWaterPurificationSpecify()
    {
  
        validateNonNullSpecifyField(model.getWaterPurificationOthers(),1,model.getWaterPurificationSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.waterPurificationSpecifyContainer, context.getResources().getString(R.string.community_nursing_waterPurificationSpecify));   
    }

    public void validateDistanceFromWater()
    {
    
        validateNonNullField(model.getDistanceFromWater(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.distanceFromWaterContainer, context.getResources().getString(R.string.community_nursing_distanceFromWater));
    }

    public void validateDistanceFromWaterSpecify()
    {
  
        validateNonNullSpecifyField(model.getDistanceFromWater(),2,model.getDistanceFromWaterSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.distanceFromWaterSpecifyContainer, context.getResources().getString(R.string.community_nursing_distanceFromWaterSpecify));   
    }

    public void validateToiletAvailable()
    {
    
        validateNonNullField(model.getToiletAvailable(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.toiletAvailableContainer, context.getResources().getString(R.string.community_nursing_toiletAvailable));
    }

    public void validateAdequateToiletRatio()
    {
    
        validateNonNullField(model.getAdequateToiletRatio(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.adequateToiletRatioContainer, context.getResources().getString(R.string.community_nursing_adequateToiletRatio));
    }

    public void validateToiletType()
    {
    
        validateNonNullField(model.getToiletType(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.toiletTypeContainer, context.getResources().getString(R.string.community_nursing_toiletType));
    }

    public void validateToiletTypeSpecify()
    {
  
        validateNonNullSpecifyField(model.getToiletType(),3,model.getToiletTypeSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.toiletTypeSpecifyContainer, context.getResources().getString(R.string.community_nursing_toiletTypeSpecify));   
    }

    public void validateDistanceTolietFromShelter()
    {
    
        validateNonNullField(model.getDistanceTolietFromShelter(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.distanceTolietFromShelterContainer, context.getResources().getString(R.string.community_nursing_distanceTolietFromShelter));
    }

    public void validateToiletDistance()
    {
    
        validateNonNullField(model.getToiletDistance(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.toiletDistanceContainer, context.getResources().getString(R.string.community_nursing_toiletDistance));
    }

    public void validateToiletDistanceUnit()
    {
    
        validateNonNullField(model.getToiletDistanceUnit(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.toiletDistanceUnitContainer, context.getResources().getString(R.string.community_nursing_toiletDistanceUnit));
    }

    public void validateWasteDistance()
    {
    
        validateNonNullField(model.getWasteDistance(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.wasteDistanceContainer, context.getResources().getString(R.string.community_nursing_wasteDistance));
    }

    public void validateWasteDistanceUnit()
    {
    
        validateNonNullField(model.getWasteDistanceUnit(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.wasteDistanceUnitContainer, context.getResources().getString(R.string.community_nursing_wasteDistanceUnit));
    }

    public void validateDisposalMethodComposting()
    {
    
        validateNonNullField(model.getDisposalMethodComposting(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.disposalMethodCompostingContainer, context.getResources().getString(R.string.community_nursing_disposalMethodComposting));
    }

    public void validateDisposalMethodBurning()
    {
    
        validateNonNullField(model.getDisposalMethodBurning(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.disposalMethodBurningContainer, context.getResources().getString(R.string.community_nursing_disposalMethodBurning));
    }

    public void validateDisposalMethodDumping()
    {
    
        validateNonNullField(model.getDisposalMethodDumping(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.disposalMethodDumpingContainer, context.getResources().getString(R.string.community_nursing_disposalMethodDumping));
    }

    public void validateDisposalMethodOthers()
    {
    
        validateNonNullField(model.getDisposalMethodOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.disposalMethodOthersContainer, context.getResources().getString(R.string.community_nursing_disposalMethodOthers));
    }

    public void validateDisposalMethodSpecify()
    {
  
        validateNonNullSpecifyField(model.getDisposalMethodOthers(),1,model.getDisposalMethodSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.disposalMethodSpecifyContainer, context.getResources().getString(R.string.community_nursing_disposalMethodSpecify));   
    }

    public void validateWasteCollectorAvailable()
    {
    
        validateNonNullField(model.getWasteCollectorAvailable(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.wasteCollectorAvailableContainer, context.getResources().getString(R.string.community_nursing_wasteCollectorAvailable));
    }

    public void validateTimelyCollection()
    {
		if((model.getWasteCollectorAvailable()==null || !(model.getWasteCollectorAvailable()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getTimelyCollection(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.timelyCollectionContainer, context.getResources().getString(R.string.community_nursing_timelyCollection));
    }

    public void validateHasDeadAnimalDisposal()
    {
    
        validateNonNullField(model.getHasDeadAnimalDisposal(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.hasDeadAnimalDisposalContainer, context.getResources().getString(R.string.community_nursing_hasDeadAnimalDisposal));
    }

    public void validateHasDrainage()
    {
    
        validateNonNullField(model.getHasDrainage(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.hasDrainageContainer, context.getResources().getString(R.string.community_nursing_hasDrainage));
    }

    public void validateSurroundingsClean()
    {
    
        validateNonNullField(model.getSurroundingsClean(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.surroundingsCleanContainer, context.getResources().getString(R.string.community_nursing_surroundingsClean));
    }

    public void validateProblemsMenstruationHygiene()
    {
    
        validateNonNullField(model.getProblemsMenstruationHygiene(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.problemsMenstruationHygieneContainer, context.getResources().getString(R.string.community_nursing_problemsMenstruationHygiene));
    }

    public void validateMenstruationHygieneProblemsNoWater()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMenstruationHygieneProblemsNoWater(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsNoWaterContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsNoWater));
    }

    public void validateMenstruationHygieneProblemsNoPrivacy()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMenstruationHygieneProblemsNoPrivacy(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsNoPrivacyContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsNoPrivacy));
    }

    public void validateMenstruationHygieneProblemsNoPads()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMenstruationHygieneProblemsNoPads(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsNoPadsContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsNoPads));
    }

    public void validateMenstruationHygieneProblemsNoDisposal()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMenstruationHygieneProblemsNoDisposal(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsNoDisposalContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsNoDisposal));
    }

    public void validateMenstruationHygieneProblemsOthers()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMenstruationHygieneProblemsOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsOthersContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsOthers));
    }

    public void validateMenstruationHygieneProblemsSpecify()
    {
		if((model.getProblemsMenstruationHygiene()==null || !(model.getProblemsMenstruationHygiene()==1)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getMenstruationHygieneProblemsOthers(),1,model.getMenstruationHygieneProblemsSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.menstruationHygieneProblemsSpecifyContainer, context.getResources().getString(R.string.community_nursing_menstruationHygieneProblemsSpecify));   
    }

    public void validateVisibleHazardsSlipperyFloor()
    {
    
        validateNonNullField(model.getVisibleHazardsSlipperyFloor(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsSlipperyFloorContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsSlipperyFloor));
    }

    public void validateVisibleHazardsNoLight()
    {
    
        validateNonNullField(model.getVisibleHazardsNoLight(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsNoLightContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsNoLight));
    }

    public void validateVisibleHazardsBadElectical()
    {
    
        validateNonNullField(model.getVisibleHazardsBadElectical(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsBadElecticalContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsBadElectical));
    }

    public void validateVisibleHazardsRoughSurface()
    {
    
        validateNonNullField(model.getVisibleHazardsRoughSurface(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsRoughSurfaceContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsRoughSurface));
    }

    public void validateVisibleHazardsSloppyLand()
    {
    
        validateNonNullField(model.getVisibleHazardsSloppyLand(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsSloppyLandContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsSloppyLand));
    }

    public void validateVisibleHazardsDumpingArea()
    {
    
        validateNonNullField(model.getVisibleHazardsDumpingArea(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsDumpingAreaContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsDumpingArea));
    }

    public void validateVisibleHazardsPolluted()
    {
    
        validateNonNullField(model.getVisibleHazardsPolluted(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsPollutedContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsPolluted));
    }

    public void validateVisibleHazardsNoVentilation()
    {
    
        validateNonNullField(model.getVisibleHazardsNoVentilation(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsNoVentilationContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsNoVentilation));
    }

    public void validateVisibleHazardsNoisy()
    {
    
        validateNonNullField(model.getVisibleHazardsNoisy(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsNoisyContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsNoisy));
    }

    public void validateVisibleHazardsOthers()
    {
    
        validateNonNullField(model.getVisibleHazardsOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsOthersContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsOthers));
    }

    public void validateVisibleHazardsSpecify()
    {
  
        validateNonNullSpecifyField(model.getVisibleHazardsOthers(),1,model.getVisibleHazardsSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsSpecifyContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsSpecify));   
    }

    public void validateVisibleHazardsNone()
    {
    
        validateNonNullField(model.getVisibleHazardsNone(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.visibleHazardsNoneContainer, context.getResources().getString(R.string.community_nursing_visibleHazardsNone));
    }

    public void validateAreaProneToNone()
    {
    
        validateNonNullField(model.getAreaProneToNone(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.areaProneToNoneContainer, context.getResources().getString(R.string.community_nursing_areaProneToNone));
    }

    public void validateSnakeBite()
    {
    
        validateNonNullField(model.getSnakeBite(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.snakeBiteContainer, context.getResources().getString(R.string.community_nursing_snakeBite));
    }

    public void validateAnimalBite()
    {
    
        validateNonNullField(model.getAnimalBite(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.animalBiteContainer, context.getResources().getString(R.string.community_nursing_animalBite));
    }

    public void validateAnimalBiteSpecify()
    {
  
        validateNonNullSpecifyField(model.getAnimalBite(),1,model.getAnimalBiteSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.animalBiteSpecifyContainer, context.getResources().getString(R.string.community_nursing_animalBiteSpecify));   
    }

    public void validateInsectBite()
    {
    
        validateNonNullField(model.getInsectBite(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.insectBiteContainer, context.getResources().getString(R.string.community_nursing_insectBite));
    }

    public void validateInsectBiteSpecify()
    {
  
        validateNonNullSpecifyField(model.getInsectBite(),1,model.getInsectBiteSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.insectBiteSpecifyContainer, context.getResources().getString(R.string.community_nursing_insectBiteSpecify));   
    }

    public void validateOthers()
    {
    
        validateNonNullField(model.getOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.othersContainer, context.getResources().getString(R.string.community_nursing_others));
    }

    public void validateOthersSpecify()
    {
  
        validateNonNullSpecifyField(model.getOthers(),1,model.getOthersSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.othersSpecifyContainer, context.getResources().getString(R.string.community_nursing_othersSpecify));   
    }

    public void validateNoVectors()
    {
    
        validateNonNullField(model.getNoVectors(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.noVectorsContainer, context.getResources().getString(R.string.community_nursing_noVectors));
    }

    public void validateMossquitoPreventionNets()
    {
    
        validateNonNullField(model.getMossquitoPreventionNets(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.mossquitoPreventionNetsContainer, context.getResources().getString(R.string.community_nursing_mossquitoPreventionNets));
    }

    public void validateMossquitoPreventionLiquid()
    {
    
        validateNonNullField(model.getMossquitoPreventionLiquid(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.mossquitoPreventionLiquidContainer, context.getResources().getString(R.string.community_nursing_mossquitoPreventionLiquid));
    }

    public void validateMossquitoPreventionDrugs()
    {
    
        validateNonNullField(model.getMossquitoPreventionDrugs(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.mossquitoPreventionDrugsContainer, context.getResources().getString(R.string.community_nursing_mossquitoPreventionDrugs));
    }

    public void validateMossquitoPreventionOthers()
    {
    
        validateNonNullField(model.getMossquitoPreventionOthers(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.mossquitoPreventionOthersContainer, context.getResources().getString(R.string.community_nursing_mossquitoPreventionOthers));
    }

    public void validateMossquitoPreventionSpecify()
    {
  
        validateNonNullSpecifyField(model.getMossquitoPreventionOthers(),1,model.getMossquitoPreventionSpecify(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.mossquitoPreventionSpecifyContainer, context.getResources().getString(R.string.community_nursing_mossquitoPreventionSpecify));   
    }

    public void validatePestControl()
    {
    
        validateNonNullField(model.getPestControl(), activity.getPage("Personal Hygiene and Environmental Sanitation"), R.id.pestControlContainer, context.getResources().getString(R.string.community_nursing_pestControl));
    }

    public void validateFoodStockAvailable()
    {
    
        validateNonNullField(model.getFoodStockAvailable(), activity.getPage("Nutrition"), R.id.foodStockAvailableContainer, context.getResources().getString(R.string.community_nursing_foodStockAvailable));
    }

    public void validateCookingFuelUsedGas()
    {
    
        validateNonNullField(model.getCookingFuelUsedGas(), activity.getPage("Nutrition"), R.id.cookingFuelUsedGasContainer, context.getResources().getString(R.string.community_nursing_cookingFuelUsedGas));
    }

    public void validateCookingFuelUsedFirewood()
    {
    
        validateNonNullField(model.getCookingFuelUsedFirewood(), activity.getPage("Nutrition"), R.id.cookingFuelUsedFirewoodContainer, context.getResources().getString(R.string.community_nursing_cookingFuelUsedFirewood));
    }

    public void validateCookingFuelUsedKerosene()
    {
    
        validateNonNullField(model.getCookingFuelUsedKerosene(), activity.getPage("Nutrition"), R.id.cookingFuelUsedKeroseneContainer, context.getResources().getString(R.string.community_nursing_cookingFuelUsedKerosene));
    }

    public void validateCookingFuelUsedOthers()
    {
    
        validateNonNullField(model.getCookingFuelUsedOthers(), activity.getPage("Nutrition"), R.id.cookingFuelUsedOthersContainer, context.getResources().getString(R.string.community_nursing_cookingFuelUsedOthers));
    }

    public void validateCookingFuelUsedSpecify()
    {
  
        validateNonNullSpecifyField(model.getCookingFuelUsedOthers(),1,model.getCookingFuelUsedSpecify(), activity.getPage("Nutrition"), R.id.cookingFuelUsedSpecifyContainer, context.getResources().getString(R.string.community_nursing_cookingFuelUsedSpecify));   
    }

    public void validateKindOfFoodAvailableCooked()
    {
    
        validateNonNullField(model.getKindOfFoodAvailableCooked(), activity.getPage("Nutrition"), R.id.kindOfFoodAvailableCookedContainer, context.getResources().getString(R.string.community_nursing_kindOfFoodAvailableCooked));
    }

    public void validateKindOfFoodAvailableJunk()
    {
    
        validateNonNullField(model.getKindOfFoodAvailableJunk(), activity.getPage("Nutrition"), R.id.kindOfFoodAvailableJunkContainer, context.getResources().getString(R.string.community_nursing_kindOfFoodAvailableJunk));
    }

    public void validateKindOfFoodAvailableOthers()
    {
    
        validateNonNullField(model.getKindOfFoodAvailableOthers(), activity.getPage("Nutrition"), R.id.kindOfFoodAvailableOthersContainer, context.getResources().getString(R.string.community_nursing_kindOfFoodAvailableOthers));
    }

    public void validateKindOfFoodAvailableSpecify()
    {
  
        validateNonNullSpecifyField(model.getKindOfFoodAvailableOthers(),1,model.getKindOfFoodAvailableSpecify(), activity.getPage("Nutrition"), R.id.kindOfFoodAvailableSpecifyContainer, context.getResources().getString(R.string.community_nursing_kindOfFoodAvailableSpecify));   
    }

    public void validateFoodHygienic()
    {
    
        validateNonNullField(model.getFoodHygienic(), activity.getPage("Nutrition"), R.id.foodHygienicContainer, context.getResources().getString(R.string.community_nursing_foodHygienic));
    }

    public void validateFoodStorageAppropriate()
    {
    
        validateNonNullField(model.getFoodStorageAppropriate(), activity.getPage("Nutrition"), R.id.foodStorageAppropriateContainer, context.getResources().getString(R.string.community_nursing_foodStorageAppropriate));
    }

    public void validateFamilyPlanningMethod()
    {
    
        validateNonNullField(model.getFamilyPlanningMethod(), activity.getPage("Family Planning"), R.id.familyPlanningMethodContainer, context.getResources().getString(R.string.community_nursing_familyPlanningMethod));
    }

    public void validateKnowOfServices()
    {
		if((model.getFamilyPlanningMethod()==null || !(model.getFamilyPlanningMethod()==0)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getKnowOfServices(), activity.getPage("Family Planning"), R.id.knowOfServicesContainer, context.getResources().getString(R.string.community_nursing_knowOfServices));
    }

    public void validateKnowOfEcp()
    {
		if((model.getFamilyPlanningMethod()==null || !(model.getFamilyPlanningMethod()==0)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getKnowOfEcp(), activity.getPage("Family Planning"), R.id.knowOfEcpContainer, context.getResources().getString(R.string.community_nursing_knowOfEcp));
    }

    public void validateWhereEcpAvailableMobileClinic()
    {
		if((model.getKnowOfEcp()==null || !(model.getKnowOfEcp()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWhereEcpAvailableMobileClinic(), activity.getPage("Family Planning"), R.id.whereEcpAvailableMobileClinicContainer, context.getResources().getString(R.string.community_nursing_whereEcpAvailableMobileClinic));
    }

    public void validateWhereEcpAvailableHealthFacility()
    {
		if((model.getKnowOfEcp()==null || !(model.getKnowOfEcp()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWhereEcpAvailableHealthFacility(), activity.getPage("Family Planning"), R.id.whereEcpAvailableHealthFacilityContainer, context.getResources().getString(R.string.community_nursing_whereEcpAvailableHealthFacility));
    }

    public void validateWhereEcpAvailableOthers()
    {
		if((model.getKnowOfEcp()==null || !(model.getKnowOfEcp()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getWhereEcpAvailableOthers(), activity.getPage("Family Planning"), R.id.whereEcpAvailableOthersContainer, context.getResources().getString(R.string.community_nursing_whereEcpAvailableOthers));
    }

    public void validateWhereEcpAvailableSpecify()
    {
		if((model.getKnowOfEcp()==null || !(model.getKnowOfEcp()==1)))
		{
			// optional -- validation not required
			return;
		}  
        validateNonNullSpecifyField(model.getWhereEcpAvailableOthers(),1,model.getWhereEcpAvailableSpecify(), activity.getPage("Family Planning"), R.id.whereEcpAvailableSpecifyContainer, context.getResources().getString(R.string.community_nursing_whereEcpAvailableSpecify));   
    }

    public void validateUnsualDiseaseOutbreak()
    {
    
        validateNonNullField(model.getUnsualDiseaseOutbreak(), activity.getPage("Health Condition"), R.id.unsualDiseaseOutbreakContainer, context.getResources().getString(R.string.community_nursing_unsualDiseaseOutbreak));
    }

    public void validateUnsualDiseaseOutbreakSpecify()
    {
  
        validateNonNullSpecifyField(model.getUnsualDiseaseOutbreak(),1,model.getUnsualDiseaseOutbreakSpecify(), activity.getPage("Health Condition"), R.id.unsualDiseaseOutbreakSpecifyContainer, context.getResources().getString(R.string.community_nursing_unsualDiseaseOutbreakSpecify));   
    }

    public void validateHealthCareOnSite()
    {
    
        validateNonNullField(model.getHealthCareOnSite(), activity.getPage("Health Condition"), R.id.healthCareOnSiteContainer, context.getResources().getString(R.string.community_nursing_healthCareOnSite));
    }

    public void validatePfaPresent()
    {
    
        validateNonNullField(model.getPfaPresent(), activity.getPage("Health Condition"), R.id.pfaPresentContainer, context.getResources().getString(R.string.community_nursing_pfaPresent));
    }

    public void validateTimeToNearestHealthFacility()
    {
    
        validateNonNullField(model.getTimeToNearestHealthFacility(), activity.getPage("Health Condition"), R.id.timeToNearestHealthFacilityContainer, context.getResources().getString(R.string.community_nursing_timeToNearestHealthFacility));
    }

    public void validateTypeOfNearestFacilityHealthPost()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityHealthPost(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityHealthPostContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityHealthPost));
    }

    public void validateTypeOfNearestFacilityPrimaryHealthCenter()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityPrimaryHealthCenter(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityPrimaryHealthCenterContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityPrimaryHealthCenter));
    }

    public void validateTypeOfNearestFacilityDistrictHospital()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityDistrictHospital(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityDistrictHospitalContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityDistrictHospital));
    }

    public void validateTypeOfNearestFacilityZonalHospital()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityZonalHospital(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityZonalHospitalContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityZonalHospital));
    }

    public void validateTypeOfNearestFacilitySubRegionalHospital()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilitySubRegionalHospital(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilitySubRegionalHospitalContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilitySubRegionalHospital));
    }

    public void validateTypeOfNearestFacilityTertiaryHospital()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityTertiaryHospital(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityTertiaryHospitalContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityTertiaryHospital));
    }

    public void validateTypeOfNearestFacilityPrivateHospital()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityPrivateHospital(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityPrivateHospitalContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityPrivateHospital));
    }

    public void validateTypeOfNearestFacilityOthers()
    {
    
        validateNonNullField(model.getTypeOfNearestFacilityOthers(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilityOthersContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilityOthers));
    }

    public void validateTypeOfNearestFacilitySpecify()
    {
  
        validateNonNullSpecifyField(model.getTypeOfNearestFacilityOthers(),1,model.getTypeOfNearestFacilitySpecify(), activity.getPage("Health Condition"), R.id.typeOfNearestFacilitySpecifyContainer, context.getResources().getString(R.string.community_nursing_typeOfNearestFacilitySpecify));   
    }

    public void validateFamilyIllnessDiabetesMellitus()
    {
    
        validateNonNullField(model.getFamilyIllnessDiabetesMellitus(), activity.getPage("Health Condition"), R.id.familyIllnessDiabetesMellitusContainer, context.getResources().getString(R.string.community_nursing_familyIllnessDiabetesMellitus));
    }

    public void validateFamilyIllnessHypertension()
    {
    
        validateNonNullField(model.getFamilyIllnessHypertension(), activity.getPage("Health Condition"), R.id.familyIllnessHypertensionContainer, context.getResources().getString(R.string.community_nursing_familyIllnessHypertension));
    }

    public void validateFamilyIllnessCopd()
    {
    
        validateNonNullField(model.getFamilyIllnessCopd(), activity.getPage("Health Condition"), R.id.familyIllnessCopdContainer, context.getResources().getString(R.string.community_nursing_familyIllnessCopd));
    }

    public void validateFamilyIllnessCancer()
    {
    
        validateNonNullField(model.getFamilyIllnessCancer(), activity.getPage("Health Condition"), R.id.familyIllnessCancerContainer, context.getResources().getString(R.string.community_nursing_familyIllnessCancer));
    }

    public void validateFamilyIllnessMentalHealthProblems()
    {
    
        validateNonNullField(model.getFamilyIllnessMentalHealthProblems(), activity.getPage("Health Condition"), R.id.familyIllnessMentalHealthProblemsContainer, context.getResources().getString(R.string.community_nursing_familyIllnessMentalHealthProblems));
    }

    public void validateFamilyIllnessOthers()
    {
    
        validateNonNullField(model.getFamilyIllnessOthers(), activity.getPage("Health Condition"), R.id.familyIllnessOthersContainer, context.getResources().getString(R.string.community_nursing_familyIllnessOthers));
    }

    public void validateFamilyIllnessSpecify()
    {
  
        validateNonNullSpecifyField(model.getFamilyIllnessOthers(),1,model.getFamilyIllnessSpecify(), activity.getPage("Health Condition"), R.id.familyIllnessSpecifyContainer, context.getResources().getString(R.string.community_nursing_familyIllnessSpecify));   
    }

    public void validateFamilyIllnessNone()
    {
    
        validateNonNullField(model.getFamilyIllnessNone(), activity.getPage("Health Condition"), R.id.familyIllnessNoneContainer, context.getResources().getString(R.string.community_nursing_familyIllnessNone));
    }

    public void validateFamilyMentalIllness()
    {
    
        validateNonNullField(model.getFamilyMentalIllness(), activity.getPage("Mental Illness"), R.id.familyMentalIllnessContainer, context.getResources().getString(R.string.community_nursing_familyMentalIllness));
    }

    public void validateUnderMedication()
    {
		if((model.getFamilyMentalIllness()==null || !(model.getFamilyMentalIllness()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getUnderMedication(), activity.getPage("Mental Illness"), R.id.underMedicationContainer, context.getResources().getString(R.string.community_nursing_underMedication));
    }

    public void validateHealthHazardsNone()
    {
    
        validateNonNullField(model.getHealthHazardsNone(), activity.getPage("Mental Illness"), R.id.healthHazardsNoneContainer, context.getResources().getString(R.string.community_nursing_healthHazardsNone));
    }

    public void validateHealthHazardsSmoking()
    {
    
        validateNonNullField(model.getHealthHazardsSmoking(), activity.getPage("Mental Illness"), R.id.healthHazardsSmokingContainer, context.getResources().getString(R.string.community_nursing_healthHazardsSmoking));
    }

    public void validateHealthHazardsAlcoholism()
    {
    
        validateNonNullField(model.getHealthHazardsAlcoholism(), activity.getPage("Mental Illness"), R.id.healthHazardsAlcoholismContainer, context.getResources().getString(R.string.community_nursing_healthHazardsAlcoholism));
    }

    public void validateHealthHazardsDrugAbuse()
    {
    
        validateNonNullField(model.getHealthHazardsDrugAbuse(), activity.getPage("Mental Illness"), R.id.healthHazardsDrugAbuseContainer, context.getResources().getString(R.string.community_nursing_healthHazardsDrugAbuse));
    }

    public void validateHealthHazardsOthers()
    {
    
        validateNonNullField(model.getHealthHazardsOthers(), activity.getPage("Mental Illness"), R.id.healthHazardsOthersContainer, context.getResources().getString(R.string.community_nursing_healthHazardsOthers));
    }

    public void validateHealthHazardsSpecify()
    {
  
        validateNonNullSpecifyField(model.getHealthHazardsOthers(),1,model.getHealthHazardsSpecify(), activity.getPage("Mental Illness"), R.id.healthHazardsSpecifyContainer, context.getResources().getString(R.string.community_nursing_healthHazardsSpecify));   
    }

    public void validateAbuseNo()
    {
    
        validateNonNullField(model.getAbuseNo(), activity.getPage("Mental Illness"), R.id.abuseNoContainer, context.getResources().getString(R.string.community_nursing_abuseNo));
    }

    public void validateAbuseSexualAbuse()
    {
    
        validateNonNullField(model.getAbuseSexualAbuse(), activity.getPage("Mental Illness"), R.id.abuseSexualAbuseContainer, context.getResources().getString(R.string.community_nursing_abuseSexualAbuse));
    }

    public void validateAbusePhysicalAbuse()
    {
    
        validateNonNullField(model.getAbusePhysicalAbuse(), activity.getPage("Mental Illness"), R.id.abusePhysicalAbuseContainer, context.getResources().getString(R.string.community_nursing_abusePhysicalAbuse));
    }

    public void validateAbusePsychologicalAbuse()
    {
    
        validateNonNullField(model.getAbusePsychologicalAbuse(), activity.getPage("Mental Illness"), R.id.abusePsychologicalAbuseContainer, context.getResources().getString(R.string.community_nursing_abusePsychologicalAbuse));
    }

    public void validateAbuseOthers()
    {
    
        validateNonNullField(model.getAbuseOthers(), activity.getPage("Mental Illness"), R.id.abuseOthersContainer, context.getResources().getString(R.string.community_nursing_abuseOthers));
    }

    public void validateAbuseSpecify()
    {
  
        validateNonNullSpecifyField(model.getAbuseOthers(),1,model.getAbuseSpecify(), activity.getPage("Mental Illness"), R.id.abuseSpecifyContainer, context.getResources().getString(R.string.community_nursing_abuseSpecify));   
    }


}
