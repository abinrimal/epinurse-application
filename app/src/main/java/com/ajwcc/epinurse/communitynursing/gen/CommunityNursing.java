package com.ajwcc.epinurse.communitynursing.gen;

import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.epinurse.common.utils.network.ExcludeFromJson;

import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import com.zhuinden.realmautomigration.AutoMigration;

public class CommunityNursing extends RealmObject implements EpinurseModel
{
    @PrimaryKey
    private String uuid;
    
    public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public String getUuid()
	{
		return uuid;
	}

	@ExcludeFromJson
	private boolean editing;

	@Override
	public boolean isEditing() {
		return editing;
	}

	@Override
	public void setEditing(boolean editing) {
		this.editing = editing;
	}


	@ExcludeFromJson
	private String ownerUuid;

	@Override
	public String getOwnerUuid() {
		return ownerUuid;
	}

	@Override
	public void setOwnerUuid(String ownerUuid) {
		this.ownerUuid = ownerUuid;
	}


    
	@ExcludeFromJson
	private boolean synced;

	@Override
	public boolean isSynced() {
		return synced;
	}

	@Override
	public void setSynced(boolean synced) {
		this.synced = synced;
	}


	@ExcludeFromJson
	private Date createdAt;

	@Override
	public Date getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(Date date)
	{
		this.createdAt = date;
	}
    
	private String shineId;

	// Basic Information
	private Integer disasterRecently;
	private String typeOfDisaster;
	private String durationAfterDisaster;
	private Integer numberHumanLoss;
	private Integer numberMissingPeople;
	private Integer numberInjuredPeople;
	private Integer numberLivestockLoss;

	// Shelter
	private Integer typeOfShelter;
	private String typeOfShelterSpecify;
	private Integer adequacyOfSpace;
	private Integer damaged;
	private Integer sourceOfLightElectricity;
	private Integer sourceOfLightOilLamp;
	private Integer sourceOfLightSolar;
	private Integer sourceOfLightOthers;
	private String sourceOfLightSpecify;
	private Integer adequateVentilation;
	private Integer kitchen;

	// Personal Hygiene and Environmental Sanitation
	private Integer whenWashHandsAfterToileting;
	private Integer whenWashHandsBeforeCooking;
	private Integer whenWashHandsBeforeEating;
	private Integer whenWashHandsBeforeFeedingAChild;
	private Integer whenWashHandsOthers;
	private String whenWashHandsSpecify;
	private Integer bathingHabit;
	private Integer waterSupplySource;
	private String waterSupplySourceSpecify;
	private Integer adequacyWaterSupply;
	private Integer qualityDrinkingWater;
	private Integer waterPurificationNone;
	private Integer waterPurificationBoiling;
	private Integer waterPurificationFiltration;
	private Integer waterPurificationUsingChemical;
	private Integer waterPurificationSodis;
	private Integer waterPurificationOthers;
	private String waterPurificationSpecify;
	private Integer distanceFromWater;
	private String distanceFromWaterSpecify;
	private Integer toiletAvailable;
	private Integer adequateToiletRatio;
	private Integer toiletType;
	private String toiletTypeSpecify;
	private Integer distanceTolietFromShelter;
	private Double toiletDistance;
	private Integer toiletDistanceUnit;
	private Double wasteDistance;
	private Integer wasteDistanceUnit;
	private Integer disposalMethodComposting;
	private Integer disposalMethodBurning;
	private Integer disposalMethodDumping;
	private Integer disposalMethodOthers;
	private String disposalMethodSpecify;
	private Integer wasteCollectorAvailable;
	private Integer timelyCollection;
	private Integer hasDeadAnimalDisposal;
	private Integer hasDrainage;
	private Integer surroundingsClean;
	private Integer problemsMenstruationHygiene;
	private Integer menstruationHygieneProblemsNoWater;
	private Integer menstruationHygieneProblemsNoPrivacy;
	private Integer menstruationHygieneProblemsNoPads;
	private Integer menstruationHygieneProblemsNoDisposal;
	private Integer menstruationHygieneProblemsOthers;
	private String menstruationHygieneProblemsSpecify;
	private Integer visibleHazardsSlipperyFloor;
	private Integer visibleHazardsNoLight;
	private Integer visibleHazardsBadElectical;
	private Integer visibleHazardsRoughSurface;
	private Integer visibleHazardsSloppyLand;
	private Integer visibleHazardsDumpingArea;
	private Integer visibleHazardsPolluted;
	private Integer visibleHazardsNoVentilation;
	private Integer visibleHazardsNoisy;
	private Integer visibleHazardsOthers;
	private String visibleHazardsSpecify;
	private Integer visibleHazardsNone;
	private Integer areaProneToNone;
	private Integer snakeBite;
	private Integer animalBite;
	private String animalBiteSpecify;
	private Integer insectBite;
	private String insectBiteSpecify;
	private Integer others;
	private String othersSpecify;
	private Integer noVectors;
	private Integer mossquitoPreventionNets;
	private Integer mossquitoPreventionLiquid;
	private Integer mossquitoPreventionDrugs;
	private Integer mossquitoPreventionOthers;
	private String mossquitoPreventionSpecify;
	private Integer pestControl;

	// Nutrition
	private Integer foodStockAvailable;
	private Integer cookingFuelUsedGas;
	private Integer cookingFuelUsedFirewood;
	private Integer cookingFuelUsedKerosene;
	private Integer cookingFuelUsedOthers;
	private String cookingFuelUsedSpecify;
	private Integer kindOfFoodAvailableCooked;
	private Integer kindOfFoodAvailableJunk;
	private Integer kindOfFoodAvailableOthers;
	private String kindOfFoodAvailableSpecify;
	private Integer foodHygienic;
	private Integer foodStorageAppropriate;

	// Family Planning
	private Integer familyPlanningMethod;
	private Integer knowOfServices;
	private Integer knowOfEcp;
	private Integer whereEcpAvailableMobileClinic;
	private Integer whereEcpAvailableHealthFacility;
	private Integer whereEcpAvailableOthers;
	private String whereEcpAvailableSpecify;

	// Health Condition
	private Integer unsualDiseaseOutbreak;
	private String unsualDiseaseOutbreakSpecify;
	private Integer healthCareOnSite;
	private Integer pfaPresent;
	private Integer timeToNearestHealthFacility;
	private Integer typeOfNearestFacilityHealthPost;
	private Integer typeOfNearestFacilityPrimaryHealthCenter;
	private Integer typeOfNearestFacilityDistrictHospital;
	private Integer typeOfNearestFacilityZonalHospital;
	private Integer typeOfNearestFacilitySubRegionalHospital;
	private Integer typeOfNearestFacilityTertiaryHospital;
	private Integer typeOfNearestFacilityPrivateHospital;
	private Integer typeOfNearestFacilityOthers;
	private String typeOfNearestFacilitySpecify;
	private Integer familyIllnessDiabetesMellitus;
	private Integer familyIllnessHypertension;
	private Integer familyIllnessCopd;
	private Integer familyIllnessCancer;
	private Integer familyIllnessMentalHealthProblems;
	private Integer familyIllnessOthers;
	private String familyIllnessSpecify;
	private Integer familyIllnessNone;

	// Mental Illness
	private Integer familyMentalIllness;
	private Integer underMedication;
	private Integer healthHazardsNone;
	private Integer healthHazardsSmoking;
	private Integer healthHazardsAlcoholism;
	private Integer healthHazardsDrugAbuse;
	private Integer healthHazardsOthers;
	private String healthHazardsSpecify;
	private Integer abuseNo;
	private Integer abuseSexualAbuse;
	private Integer abusePhysicalAbuse;
	private Integer abusePsychologicalAbuse;
	private Integer abuseOthers;
	private String abuseSpecify;
    
    

	public String getShineId() {
		return this.shineId;
	}

	public void setShineId(String shineId) {
	
		if (this.shineId==null || !this.shineId.equals(shineId))
		{
			editing = true;
			synced = false;
		}
	
		this.shineId = shineId;
	}

	public Integer getDisasterRecently() {
		return this.disasterRecently;
	}

	public void setDisasterRecently(Integer disasterRecently) {
	
		if (this.disasterRecently==null || !this.disasterRecently.equals(disasterRecently))
		{
			editing = true;
			synced = false;
		}
	
		this.disasterRecently = disasterRecently;
	}

	public String getTypeOfDisaster() {
		return this.typeOfDisaster;
	}

	public void setTypeOfDisaster(String typeOfDisaster) {
	
		if (this.typeOfDisaster==null || !this.typeOfDisaster.equals(typeOfDisaster))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfDisaster = typeOfDisaster;
	}

	public String getDurationAfterDisaster() {
		return this.durationAfterDisaster;
	}

	public void setDurationAfterDisaster(String durationAfterDisaster) {
	
		if (this.durationAfterDisaster==null || !this.durationAfterDisaster.equals(durationAfterDisaster))
		{
			editing = true;
			synced = false;
		}
	
		this.durationAfterDisaster = durationAfterDisaster;
	}

	public Integer getNumberHumanLoss() {
		return this.numberHumanLoss;
	}

	public void setNumberHumanLoss(Integer numberHumanLoss) {
	
		if (this.numberHumanLoss==null || !this.numberHumanLoss.equals(numberHumanLoss))
		{
			editing = true;
			synced = false;
		}
	
		this.numberHumanLoss = numberHumanLoss;
	}

	public Integer getNumberMissingPeople() {
		return this.numberMissingPeople;
	}

	public void setNumberMissingPeople(Integer numberMissingPeople) {
	
		if (this.numberMissingPeople==null || !this.numberMissingPeople.equals(numberMissingPeople))
		{
			editing = true;
			synced = false;
		}
	
		this.numberMissingPeople = numberMissingPeople;
	}

	public Integer getNumberInjuredPeople() {
		return this.numberInjuredPeople;
	}

	public void setNumberInjuredPeople(Integer numberInjuredPeople) {
	
		if (this.numberInjuredPeople==null || !this.numberInjuredPeople.equals(numberInjuredPeople))
		{
			editing = true;
			synced = false;
		}
	
		this.numberInjuredPeople = numberInjuredPeople;
	}

	public Integer getNumberLivestockLoss() {
		return this.numberLivestockLoss;
	}

	public void setNumberLivestockLoss(Integer numberLivestockLoss) {
	
		if (this.numberLivestockLoss==null || !this.numberLivestockLoss.equals(numberLivestockLoss))
		{
			editing = true;
			synced = false;
		}
	
		this.numberLivestockLoss = numberLivestockLoss;
	}

	public Integer getTypeOfShelter() {
		return this.typeOfShelter;
	}

	public void setTypeOfShelter(Integer typeOfShelter) {
	
		if (this.typeOfShelter==null || !this.typeOfShelter.equals(typeOfShelter))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfShelter = typeOfShelter;
	}

	public String getTypeOfShelterSpecify() {
		return this.typeOfShelterSpecify;
	}

	public void setTypeOfShelterSpecify(String typeOfShelterSpecify) {
	
		if (this.typeOfShelterSpecify==null || !this.typeOfShelterSpecify.equals(typeOfShelterSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfShelterSpecify = typeOfShelterSpecify;
	}

	public Integer getAdequacyOfSpace() {
		return this.adequacyOfSpace;
	}

	public void setAdequacyOfSpace(Integer adequacyOfSpace) {
	
		if (this.adequacyOfSpace==null || !this.adequacyOfSpace.equals(adequacyOfSpace))
		{
			editing = true;
			synced = false;
		}
	
		this.adequacyOfSpace = adequacyOfSpace;
	}

	public Integer getDamaged() {
		return this.damaged;
	}

	public void setDamaged(Integer damaged) {
	
		if (this.damaged==null || !this.damaged.equals(damaged))
		{
			editing = true;
			synced = false;
		}
	
		this.damaged = damaged;
	}

	public Integer getSourceOfLightElectricity() {
		return this.sourceOfLightElectricity;
	}

	public void setSourceOfLightElectricity(Integer sourceOfLightElectricity) {
	
		if (this.sourceOfLightElectricity==null || !this.sourceOfLightElectricity.equals(sourceOfLightElectricity))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceOfLightElectricity = sourceOfLightElectricity;
	}

	public Integer getSourceOfLightOilLamp() {
		return this.sourceOfLightOilLamp;
	}

	public void setSourceOfLightOilLamp(Integer sourceOfLightOilLamp) {
	
		if (this.sourceOfLightOilLamp==null || !this.sourceOfLightOilLamp.equals(sourceOfLightOilLamp))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceOfLightOilLamp = sourceOfLightOilLamp;
	}

	public Integer getSourceOfLightSolar() {
		return this.sourceOfLightSolar;
	}

	public void setSourceOfLightSolar(Integer sourceOfLightSolar) {
	
		if (this.sourceOfLightSolar==null || !this.sourceOfLightSolar.equals(sourceOfLightSolar))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceOfLightSolar = sourceOfLightSolar;
	}

	public Integer getSourceOfLightOthers() {
		return this.sourceOfLightOthers;
	}

	public void setSourceOfLightOthers(Integer sourceOfLightOthers) {
	
		if (this.sourceOfLightOthers==null || !this.sourceOfLightOthers.equals(sourceOfLightOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceOfLightOthers = sourceOfLightOthers;
	}

	public String getSourceOfLightSpecify() {
		return this.sourceOfLightSpecify;
	}

	public void setSourceOfLightSpecify(String sourceOfLightSpecify) {
	
		if (this.sourceOfLightSpecify==null || !this.sourceOfLightSpecify.equals(sourceOfLightSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.sourceOfLightSpecify = sourceOfLightSpecify;
	}

	public Integer getAdequateVentilation() {
		return this.adequateVentilation;
	}

	public void setAdequateVentilation(Integer adequateVentilation) {
	
		if (this.adequateVentilation==null || !this.adequateVentilation.equals(adequateVentilation))
		{
			editing = true;
			synced = false;
		}
	
		this.adequateVentilation = adequateVentilation;
	}

	public Integer getKitchen() {
		return this.kitchen;
	}

	public void setKitchen(Integer kitchen) {
	
		if (this.kitchen==null || !this.kitchen.equals(kitchen))
		{
			editing = true;
			synced = false;
		}
	
		this.kitchen = kitchen;
	}

	public Integer getWhenWashHandsAfterToileting() {
		return this.whenWashHandsAfterToileting;
	}

	public void setWhenWashHandsAfterToileting(Integer whenWashHandsAfterToileting) {
	
		if (this.whenWashHandsAfterToileting==null || !this.whenWashHandsAfterToileting.equals(whenWashHandsAfterToileting))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsAfterToileting = whenWashHandsAfterToileting;
	}

	public Integer getWhenWashHandsBeforeCooking() {
		return this.whenWashHandsBeforeCooking;
	}

	public void setWhenWashHandsBeforeCooking(Integer whenWashHandsBeforeCooking) {
	
		if (this.whenWashHandsBeforeCooking==null || !this.whenWashHandsBeforeCooking.equals(whenWashHandsBeforeCooking))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsBeforeCooking = whenWashHandsBeforeCooking;
	}

	public Integer getWhenWashHandsBeforeEating() {
		return this.whenWashHandsBeforeEating;
	}

	public void setWhenWashHandsBeforeEating(Integer whenWashHandsBeforeEating) {
	
		if (this.whenWashHandsBeforeEating==null || !this.whenWashHandsBeforeEating.equals(whenWashHandsBeforeEating))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsBeforeEating = whenWashHandsBeforeEating;
	}

	public Integer getWhenWashHandsBeforeFeedingAChild() {
		return this.whenWashHandsBeforeFeedingAChild;
	}

	public void setWhenWashHandsBeforeFeedingAChild(Integer whenWashHandsBeforeFeedingAChild) {
	
		if (this.whenWashHandsBeforeFeedingAChild==null || !this.whenWashHandsBeforeFeedingAChild.equals(whenWashHandsBeforeFeedingAChild))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsBeforeFeedingAChild = whenWashHandsBeforeFeedingAChild;
	}

	public Integer getWhenWashHandsOthers() {
		return this.whenWashHandsOthers;
	}

	public void setWhenWashHandsOthers(Integer whenWashHandsOthers) {
	
		if (this.whenWashHandsOthers==null || !this.whenWashHandsOthers.equals(whenWashHandsOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsOthers = whenWashHandsOthers;
	}

	public String getWhenWashHandsSpecify() {
		return this.whenWashHandsSpecify;
	}

	public void setWhenWashHandsSpecify(String whenWashHandsSpecify) {
	
		if (this.whenWashHandsSpecify==null || !this.whenWashHandsSpecify.equals(whenWashHandsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.whenWashHandsSpecify = whenWashHandsSpecify;
	}

	public Integer getBathingHabit() {
		return this.bathingHabit;
	}

	public void setBathingHabit(Integer bathingHabit) {
	
		if (this.bathingHabit==null || !this.bathingHabit.equals(bathingHabit))
		{
			editing = true;
			synced = false;
		}
	
		this.bathingHabit = bathingHabit;
	}

	public Integer getWaterSupplySource() {
		return this.waterSupplySource;
	}

	public void setWaterSupplySource(Integer waterSupplySource) {
	
		if (this.waterSupplySource==null || !this.waterSupplySource.equals(waterSupplySource))
		{
			editing = true;
			synced = false;
		}
	
		this.waterSupplySource = waterSupplySource;
	}

	public String getWaterSupplySourceSpecify() {
		return this.waterSupplySourceSpecify;
	}

	public void setWaterSupplySourceSpecify(String waterSupplySourceSpecify) {
	
		if (this.waterSupplySourceSpecify==null || !this.waterSupplySourceSpecify.equals(waterSupplySourceSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.waterSupplySourceSpecify = waterSupplySourceSpecify;
	}

	public Integer getAdequacyWaterSupply() {
		return this.adequacyWaterSupply;
	}

	public void setAdequacyWaterSupply(Integer adequacyWaterSupply) {
	
		if (this.adequacyWaterSupply==null || !this.adequacyWaterSupply.equals(adequacyWaterSupply))
		{
			editing = true;
			synced = false;
		}
	
		this.adequacyWaterSupply = adequacyWaterSupply;
	}

	public Integer getQualityDrinkingWater() {
		return this.qualityDrinkingWater;
	}

	public void setQualityDrinkingWater(Integer qualityDrinkingWater) {
	
		if (this.qualityDrinkingWater==null || !this.qualityDrinkingWater.equals(qualityDrinkingWater))
		{
			editing = true;
			synced = false;
		}
	
		this.qualityDrinkingWater = qualityDrinkingWater;
	}

	public Integer getWaterPurificationNone() {
		return this.waterPurificationNone;
	}

	public void setWaterPurificationNone(Integer waterPurificationNone) {
	
		if (this.waterPurificationNone==null || !this.waterPurificationNone.equals(waterPurificationNone))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationNone = waterPurificationNone;
	}

	public Integer getWaterPurificationBoiling() {
		return this.waterPurificationBoiling;
	}

	public void setWaterPurificationBoiling(Integer waterPurificationBoiling) {
	
		if (this.waterPurificationBoiling==null || !this.waterPurificationBoiling.equals(waterPurificationBoiling))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationBoiling = waterPurificationBoiling;
	}

	public Integer getWaterPurificationFiltration() {
		return this.waterPurificationFiltration;
	}

	public void setWaterPurificationFiltration(Integer waterPurificationFiltration) {
	
		if (this.waterPurificationFiltration==null || !this.waterPurificationFiltration.equals(waterPurificationFiltration))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationFiltration = waterPurificationFiltration;
	}

	public Integer getWaterPurificationUsingChemical() {
		return this.waterPurificationUsingChemical;
	}

	public void setWaterPurificationUsingChemical(Integer waterPurificationUsingChemical) {
	
		if (this.waterPurificationUsingChemical==null || !this.waterPurificationUsingChemical.equals(waterPurificationUsingChemical))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationUsingChemical = waterPurificationUsingChemical;
	}

	public Integer getWaterPurificationSodis() {
		return this.waterPurificationSodis;
	}

	public void setWaterPurificationSodis(Integer waterPurificationSodis) {
	
		if (this.waterPurificationSodis==null || !this.waterPurificationSodis.equals(waterPurificationSodis))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationSodis = waterPurificationSodis;
	}

	public Integer getWaterPurificationOthers() {
		return this.waterPurificationOthers;
	}

	public void setWaterPurificationOthers(Integer waterPurificationOthers) {
	
		if (this.waterPurificationOthers==null || !this.waterPurificationOthers.equals(waterPurificationOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationOthers = waterPurificationOthers;
	}

	public String getWaterPurificationSpecify() {
		return this.waterPurificationSpecify;
	}

	public void setWaterPurificationSpecify(String waterPurificationSpecify) {
	
		if (this.waterPurificationSpecify==null || !this.waterPurificationSpecify.equals(waterPurificationSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.waterPurificationSpecify = waterPurificationSpecify;
	}

	public Integer getDistanceFromWater() {
		return this.distanceFromWater;
	}

	public void setDistanceFromWater(Integer distanceFromWater) {
	
		if (this.distanceFromWater==null || !this.distanceFromWater.equals(distanceFromWater))
		{
			editing = true;
			synced = false;
		}
	
		this.distanceFromWater = distanceFromWater;
	}

	public String getDistanceFromWaterSpecify() {
		return this.distanceFromWaterSpecify;
	}

	public void setDistanceFromWaterSpecify(String distanceFromWaterSpecify) {
	
		if (this.distanceFromWaterSpecify==null || !this.distanceFromWaterSpecify.equals(distanceFromWaterSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.distanceFromWaterSpecify = distanceFromWaterSpecify;
	}

	public Integer getToiletAvailable() {
		return this.toiletAvailable;
	}

	public void setToiletAvailable(Integer toiletAvailable) {
	
		if (this.toiletAvailable==null || !this.toiletAvailable.equals(toiletAvailable))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletAvailable = toiletAvailable;
	}

	public Integer getAdequateToiletRatio() {
		return this.adequateToiletRatio;
	}

	public void setAdequateToiletRatio(Integer adequateToiletRatio) {
	
		if (this.adequateToiletRatio==null || !this.adequateToiletRatio.equals(adequateToiletRatio))
		{
			editing = true;
			synced = false;
		}
	
		this.adequateToiletRatio = adequateToiletRatio;
	}

	public Integer getToiletType() {
		return this.toiletType;
	}

	public void setToiletType(Integer toiletType) {
	
		if (this.toiletType==null || !this.toiletType.equals(toiletType))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletType = toiletType;
	}

	public String getToiletTypeSpecify() {
		return this.toiletTypeSpecify;
	}

	public void setToiletTypeSpecify(String toiletTypeSpecify) {
	
		if (this.toiletTypeSpecify==null || !this.toiletTypeSpecify.equals(toiletTypeSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletTypeSpecify = toiletTypeSpecify;
	}

	public Integer getDistanceTolietFromShelter() {
		return this.distanceTolietFromShelter;
	}

	public void setDistanceTolietFromShelter(Integer distanceTolietFromShelter) {
	
		if (this.distanceTolietFromShelter==null || !this.distanceTolietFromShelter.equals(distanceTolietFromShelter))
		{
			editing = true;
			synced = false;
		}
	
		this.distanceTolietFromShelter = distanceTolietFromShelter;
	}

	public Double getToiletDistance() {
		return this.toiletDistance;
	}

	public void setToiletDistance(Double toiletDistance) {
	
		if (this.toiletDistance==null || !this.toiletDistance.equals(toiletDistance))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletDistance = toiletDistance;
	}

	public Integer getToiletDistanceUnit() {
		return this.toiletDistanceUnit;
	}

	public void setToiletDistanceUnit(Integer toiletDistanceUnit) {
	
		if (this.toiletDistanceUnit==null || !this.toiletDistanceUnit.equals(toiletDistanceUnit))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletDistanceUnit = toiletDistanceUnit;
	}

	public Double getWasteDistance() {
		return this.wasteDistance;
	}

	public void setWasteDistance(Double wasteDistance) {
	
		if (this.wasteDistance==null || !this.wasteDistance.equals(wasteDistance))
		{
			editing = true;
			synced = false;
		}
	
		this.wasteDistance = wasteDistance;
	}

	public Integer getWasteDistanceUnit() {
		return this.wasteDistanceUnit;
	}

	public void setWasteDistanceUnit(Integer wasteDistanceUnit) {
	
		if (this.wasteDistanceUnit==null || !this.wasteDistanceUnit.equals(wasteDistanceUnit))
		{
			editing = true;
			synced = false;
		}
	
		this.wasteDistanceUnit = wasteDistanceUnit;
	}

	public Integer getDisposalMethodComposting() {
		return this.disposalMethodComposting;
	}

	public void setDisposalMethodComposting(Integer disposalMethodComposting) {
	
		if (this.disposalMethodComposting==null || !this.disposalMethodComposting.equals(disposalMethodComposting))
		{
			editing = true;
			synced = false;
		}
	
		this.disposalMethodComposting = disposalMethodComposting;
	}

	public Integer getDisposalMethodBurning() {
		return this.disposalMethodBurning;
	}

	public void setDisposalMethodBurning(Integer disposalMethodBurning) {
	
		if (this.disposalMethodBurning==null || !this.disposalMethodBurning.equals(disposalMethodBurning))
		{
			editing = true;
			synced = false;
		}
	
		this.disposalMethodBurning = disposalMethodBurning;
	}

	public Integer getDisposalMethodDumping() {
		return this.disposalMethodDumping;
	}

	public void setDisposalMethodDumping(Integer disposalMethodDumping) {
	
		if (this.disposalMethodDumping==null || !this.disposalMethodDumping.equals(disposalMethodDumping))
		{
			editing = true;
			synced = false;
		}
	
		this.disposalMethodDumping = disposalMethodDumping;
	}

	public Integer getDisposalMethodOthers() {
		return this.disposalMethodOthers;
	}

	public void setDisposalMethodOthers(Integer disposalMethodOthers) {
	
		if (this.disposalMethodOthers==null || !this.disposalMethodOthers.equals(disposalMethodOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.disposalMethodOthers = disposalMethodOthers;
	}

	public String getDisposalMethodSpecify() {
		return this.disposalMethodSpecify;
	}

	public void setDisposalMethodSpecify(String disposalMethodSpecify) {
	
		if (this.disposalMethodSpecify==null || !this.disposalMethodSpecify.equals(disposalMethodSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.disposalMethodSpecify = disposalMethodSpecify;
	}

	public Integer getWasteCollectorAvailable() {
		return this.wasteCollectorAvailable;
	}

	public void setWasteCollectorAvailable(Integer wasteCollectorAvailable) {
	
		if (this.wasteCollectorAvailable==null || !this.wasteCollectorAvailable.equals(wasteCollectorAvailable))
		{
			editing = true;
			synced = false;
		}
	
		this.wasteCollectorAvailable = wasteCollectorAvailable;
	}

	public Integer getTimelyCollection() {
		return this.timelyCollection;
	}

	public void setTimelyCollection(Integer timelyCollection) {
	
		if (this.timelyCollection==null || !this.timelyCollection.equals(timelyCollection))
		{
			editing = true;
			synced = false;
		}
	
		this.timelyCollection = timelyCollection;
	}

	public Integer getHasDeadAnimalDisposal() {
		return this.hasDeadAnimalDisposal;
	}

	public void setHasDeadAnimalDisposal(Integer hasDeadAnimalDisposal) {
	
		if (this.hasDeadAnimalDisposal==null || !this.hasDeadAnimalDisposal.equals(hasDeadAnimalDisposal))
		{
			editing = true;
			synced = false;
		}
	
		this.hasDeadAnimalDisposal = hasDeadAnimalDisposal;
	}

	public Integer getHasDrainage() {
		return this.hasDrainage;
	}

	public void setHasDrainage(Integer hasDrainage) {
	
		if (this.hasDrainage==null || !this.hasDrainage.equals(hasDrainage))
		{
			editing = true;
			synced = false;
		}
	
		this.hasDrainage = hasDrainage;
	}

	public Integer getSurroundingsClean() {
		return this.surroundingsClean;
	}

	public void setSurroundingsClean(Integer surroundingsClean) {
	
		if (this.surroundingsClean==null || !this.surroundingsClean.equals(surroundingsClean))
		{
			editing = true;
			synced = false;
		}
	
		this.surroundingsClean = surroundingsClean;
	}

	public Integer getProblemsMenstruationHygiene() {
		return this.problemsMenstruationHygiene;
	}

	public void setProblemsMenstruationHygiene(Integer problemsMenstruationHygiene) {
	
		if (this.problemsMenstruationHygiene==null || !this.problemsMenstruationHygiene.equals(problemsMenstruationHygiene))
		{
			editing = true;
			synced = false;
		}
	
		this.problemsMenstruationHygiene = problemsMenstruationHygiene;
	}

	public Integer getMenstruationHygieneProblemsNoWater() {
		return this.menstruationHygieneProblemsNoWater;
	}

	public void setMenstruationHygieneProblemsNoWater(Integer menstruationHygieneProblemsNoWater) {
	
		if (this.menstruationHygieneProblemsNoWater==null || !this.menstruationHygieneProblemsNoWater.equals(menstruationHygieneProblemsNoWater))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsNoWater = menstruationHygieneProblemsNoWater;
	}

	public Integer getMenstruationHygieneProblemsNoPrivacy() {
		return this.menstruationHygieneProblemsNoPrivacy;
	}

	public void setMenstruationHygieneProblemsNoPrivacy(Integer menstruationHygieneProblemsNoPrivacy) {
	
		if (this.menstruationHygieneProblemsNoPrivacy==null || !this.menstruationHygieneProblemsNoPrivacy.equals(menstruationHygieneProblemsNoPrivacy))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsNoPrivacy = menstruationHygieneProblemsNoPrivacy;
	}

	public Integer getMenstruationHygieneProblemsNoPads() {
		return this.menstruationHygieneProblemsNoPads;
	}

	public void setMenstruationHygieneProblemsNoPads(Integer menstruationHygieneProblemsNoPads) {
	
		if (this.menstruationHygieneProblemsNoPads==null || !this.menstruationHygieneProblemsNoPads.equals(menstruationHygieneProblemsNoPads))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsNoPads = menstruationHygieneProblemsNoPads;
	}

	public Integer getMenstruationHygieneProblemsNoDisposal() {
		return this.menstruationHygieneProblemsNoDisposal;
	}

	public void setMenstruationHygieneProblemsNoDisposal(Integer menstruationHygieneProblemsNoDisposal) {
	
		if (this.menstruationHygieneProblemsNoDisposal==null || !this.menstruationHygieneProblemsNoDisposal.equals(menstruationHygieneProblemsNoDisposal))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsNoDisposal = menstruationHygieneProblemsNoDisposal;
	}

	public Integer getMenstruationHygieneProblemsOthers() {
		return this.menstruationHygieneProblemsOthers;
	}

	public void setMenstruationHygieneProblemsOthers(Integer menstruationHygieneProblemsOthers) {
	
		if (this.menstruationHygieneProblemsOthers==null || !this.menstruationHygieneProblemsOthers.equals(menstruationHygieneProblemsOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsOthers = menstruationHygieneProblemsOthers;
	}

	public String getMenstruationHygieneProblemsSpecify() {
		return this.menstruationHygieneProblemsSpecify;
	}

	public void setMenstruationHygieneProblemsSpecify(String menstruationHygieneProblemsSpecify) {
	
		if (this.menstruationHygieneProblemsSpecify==null || !this.menstruationHygieneProblemsSpecify.equals(menstruationHygieneProblemsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.menstruationHygieneProblemsSpecify = menstruationHygieneProblemsSpecify;
	}

	public Integer getVisibleHazardsSlipperyFloor() {
		return this.visibleHazardsSlipperyFloor;
	}

	public void setVisibleHazardsSlipperyFloor(Integer visibleHazardsSlipperyFloor) {
	
		if (this.visibleHazardsSlipperyFloor==null || !this.visibleHazardsSlipperyFloor.equals(visibleHazardsSlipperyFloor))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsSlipperyFloor = visibleHazardsSlipperyFloor;
	}

	public Integer getVisibleHazardsNoLight() {
		return this.visibleHazardsNoLight;
	}

	public void setVisibleHazardsNoLight(Integer visibleHazardsNoLight) {
	
		if (this.visibleHazardsNoLight==null || !this.visibleHazardsNoLight.equals(visibleHazardsNoLight))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsNoLight = visibleHazardsNoLight;
	}

	public Integer getVisibleHazardsBadElectical() {
		return this.visibleHazardsBadElectical;
	}

	public void setVisibleHazardsBadElectical(Integer visibleHazardsBadElectical) {
	
		if (this.visibleHazardsBadElectical==null || !this.visibleHazardsBadElectical.equals(visibleHazardsBadElectical))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsBadElectical = visibleHazardsBadElectical;
	}

	public Integer getVisibleHazardsRoughSurface() {
		return this.visibleHazardsRoughSurface;
	}

	public void setVisibleHazardsRoughSurface(Integer visibleHazardsRoughSurface) {
	
		if (this.visibleHazardsRoughSurface==null || !this.visibleHazardsRoughSurface.equals(visibleHazardsRoughSurface))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsRoughSurface = visibleHazardsRoughSurface;
	}

	public Integer getVisibleHazardsSloppyLand() {
		return this.visibleHazardsSloppyLand;
	}

	public void setVisibleHazardsSloppyLand(Integer visibleHazardsSloppyLand) {
	
		if (this.visibleHazardsSloppyLand==null || !this.visibleHazardsSloppyLand.equals(visibleHazardsSloppyLand))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsSloppyLand = visibleHazardsSloppyLand;
	}

	public Integer getVisibleHazardsDumpingArea() {
		return this.visibleHazardsDumpingArea;
	}

	public void setVisibleHazardsDumpingArea(Integer visibleHazardsDumpingArea) {
	
		if (this.visibleHazardsDumpingArea==null || !this.visibleHazardsDumpingArea.equals(visibleHazardsDumpingArea))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsDumpingArea = visibleHazardsDumpingArea;
	}

	public Integer getVisibleHazardsPolluted() {
		return this.visibleHazardsPolluted;
	}

	public void setVisibleHazardsPolluted(Integer visibleHazardsPolluted) {
	
		if (this.visibleHazardsPolluted==null || !this.visibleHazardsPolluted.equals(visibleHazardsPolluted))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsPolluted = visibleHazardsPolluted;
	}

	public Integer getVisibleHazardsNoVentilation() {
		return this.visibleHazardsNoVentilation;
	}

	public void setVisibleHazardsNoVentilation(Integer visibleHazardsNoVentilation) {
	
		if (this.visibleHazardsNoVentilation==null || !this.visibleHazardsNoVentilation.equals(visibleHazardsNoVentilation))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsNoVentilation = visibleHazardsNoVentilation;
	}

	public Integer getVisibleHazardsNoisy() {
		return this.visibleHazardsNoisy;
	}

	public void setVisibleHazardsNoisy(Integer visibleHazardsNoisy) {
	
		if (this.visibleHazardsNoisy==null || !this.visibleHazardsNoisy.equals(visibleHazardsNoisy))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsNoisy = visibleHazardsNoisy;
	}

	public Integer getVisibleHazardsOthers() {
		return this.visibleHazardsOthers;
	}

	public void setVisibleHazardsOthers(Integer visibleHazardsOthers) {
	
		if (this.visibleHazardsOthers==null || !this.visibleHazardsOthers.equals(visibleHazardsOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsOthers = visibleHazardsOthers;
	}

	public String getVisibleHazardsSpecify() {
		return this.visibleHazardsSpecify;
	}

	public void setVisibleHazardsSpecify(String visibleHazardsSpecify) {
	
		if (this.visibleHazardsSpecify==null || !this.visibleHazardsSpecify.equals(visibleHazardsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsSpecify = visibleHazardsSpecify;
	}

	public Integer getVisibleHazardsNone() {
		return this.visibleHazardsNone;
	}

	public void setVisibleHazardsNone(Integer visibleHazardsNone) {
	
		if (this.visibleHazardsNone==null || !this.visibleHazardsNone.equals(visibleHazardsNone))
		{
			editing = true;
			synced = false;
		}
	
		this.visibleHazardsNone = visibleHazardsNone;
	}

	public Integer getAreaProneToNone() {
		return this.areaProneToNone;
	}

	public void setAreaProneToNone(Integer areaProneToNone) {
	
		if (this.areaProneToNone==null || !this.areaProneToNone.equals(areaProneToNone))
		{
			editing = true;
			synced = false;
		}
	
		this.areaProneToNone = areaProneToNone;
	}

	public Integer getSnakeBite() {
		return this.snakeBite;
	}

	public void setSnakeBite(Integer snakeBite) {
	
		if (this.snakeBite==null || !this.snakeBite.equals(snakeBite))
		{
			editing = true;
			synced = false;
		}
	
		this.snakeBite = snakeBite;
	}

	public Integer getAnimalBite() {
		return this.animalBite;
	}

	public void setAnimalBite(Integer animalBite) {
	
		if (this.animalBite==null || !this.animalBite.equals(animalBite))
		{
			editing = true;
			synced = false;
		}
	
		this.animalBite = animalBite;
	}

	public String getAnimalBiteSpecify() {
		return this.animalBiteSpecify;
	}

	public void setAnimalBiteSpecify(String animalBiteSpecify) {
	
		if (this.animalBiteSpecify==null || !this.animalBiteSpecify.equals(animalBiteSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.animalBiteSpecify = animalBiteSpecify;
	}

	public Integer getInsectBite() {
		return this.insectBite;
	}

	public void setInsectBite(Integer insectBite) {
	
		if (this.insectBite==null || !this.insectBite.equals(insectBite))
		{
			editing = true;
			synced = false;
		}
	
		this.insectBite = insectBite;
	}

	public String getInsectBiteSpecify() {
		return this.insectBiteSpecify;
	}

	public void setInsectBiteSpecify(String insectBiteSpecify) {
	
		if (this.insectBiteSpecify==null || !this.insectBiteSpecify.equals(insectBiteSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.insectBiteSpecify = insectBiteSpecify;
	}

	public Integer getOthers() {
		return this.others;
	}

	public void setOthers(Integer others) {
	
		if (this.others==null || !this.others.equals(others))
		{
			editing = true;
			synced = false;
		}
	
		this.others = others;
	}

	public String getOthersSpecify() {
		return this.othersSpecify;
	}

	public void setOthersSpecify(String othersSpecify) {
	
		if (this.othersSpecify==null || !this.othersSpecify.equals(othersSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.othersSpecify = othersSpecify;
	}

	public Integer getNoVectors() {
		return this.noVectors;
	}

	public void setNoVectors(Integer noVectors) {
	
		if (this.noVectors==null || !this.noVectors.equals(noVectors))
		{
			editing = true;
			synced = false;
		}
	
		this.noVectors = noVectors;
	}

	public Integer getMossquitoPreventionNets() {
		return this.mossquitoPreventionNets;
	}

	public void setMossquitoPreventionNets(Integer mossquitoPreventionNets) {
	
		if (this.mossquitoPreventionNets==null || !this.mossquitoPreventionNets.equals(mossquitoPreventionNets))
		{
			editing = true;
			synced = false;
		}
	
		this.mossquitoPreventionNets = mossquitoPreventionNets;
	}

	public Integer getMossquitoPreventionLiquid() {
		return this.mossquitoPreventionLiquid;
	}

	public void setMossquitoPreventionLiquid(Integer mossquitoPreventionLiquid) {
	
		if (this.mossquitoPreventionLiquid==null || !this.mossquitoPreventionLiquid.equals(mossquitoPreventionLiquid))
		{
			editing = true;
			synced = false;
		}
	
		this.mossquitoPreventionLiquid = mossquitoPreventionLiquid;
	}

	public Integer getMossquitoPreventionDrugs() {
		return this.mossquitoPreventionDrugs;
	}

	public void setMossquitoPreventionDrugs(Integer mossquitoPreventionDrugs) {
	
		if (this.mossquitoPreventionDrugs==null || !this.mossquitoPreventionDrugs.equals(mossquitoPreventionDrugs))
		{
			editing = true;
			synced = false;
		}
	
		this.mossquitoPreventionDrugs = mossquitoPreventionDrugs;
	}

	public Integer getMossquitoPreventionOthers() {
		return this.mossquitoPreventionOthers;
	}

	public void setMossquitoPreventionOthers(Integer mossquitoPreventionOthers) {
	
		if (this.mossquitoPreventionOthers==null || !this.mossquitoPreventionOthers.equals(mossquitoPreventionOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.mossquitoPreventionOthers = mossquitoPreventionOthers;
	}

	public String getMossquitoPreventionSpecify() {
		return this.mossquitoPreventionSpecify;
	}

	public void setMossquitoPreventionSpecify(String mossquitoPreventionSpecify) {
	
		if (this.mossquitoPreventionSpecify==null || !this.mossquitoPreventionSpecify.equals(mossquitoPreventionSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.mossquitoPreventionSpecify = mossquitoPreventionSpecify;
	}

	public Integer getPestControl() {
		return this.pestControl;
	}

	public void setPestControl(Integer pestControl) {
	
		if (this.pestControl==null || !this.pestControl.equals(pestControl))
		{
			editing = true;
			synced = false;
		}
	
		this.pestControl = pestControl;
	}

	public Integer getFoodStockAvailable() {
		return this.foodStockAvailable;
	}

	public void setFoodStockAvailable(Integer foodStockAvailable) {
	
		if (this.foodStockAvailable==null || !this.foodStockAvailable.equals(foodStockAvailable))
		{
			editing = true;
			synced = false;
		}
	
		this.foodStockAvailable = foodStockAvailable;
	}

	public Integer getCookingFuelUsedGas() {
		return this.cookingFuelUsedGas;
	}

	public void setCookingFuelUsedGas(Integer cookingFuelUsedGas) {
	
		if (this.cookingFuelUsedGas==null || !this.cookingFuelUsedGas.equals(cookingFuelUsedGas))
		{
			editing = true;
			synced = false;
		}
	
		this.cookingFuelUsedGas = cookingFuelUsedGas;
	}

	public Integer getCookingFuelUsedFirewood() {
		return this.cookingFuelUsedFirewood;
	}

	public void setCookingFuelUsedFirewood(Integer cookingFuelUsedFirewood) {
	
		if (this.cookingFuelUsedFirewood==null || !this.cookingFuelUsedFirewood.equals(cookingFuelUsedFirewood))
		{
			editing = true;
			synced = false;
		}
	
		this.cookingFuelUsedFirewood = cookingFuelUsedFirewood;
	}

	public Integer getCookingFuelUsedKerosene() {
		return this.cookingFuelUsedKerosene;
	}

	public void setCookingFuelUsedKerosene(Integer cookingFuelUsedKerosene) {
	
		if (this.cookingFuelUsedKerosene==null || !this.cookingFuelUsedKerosene.equals(cookingFuelUsedKerosene))
		{
			editing = true;
			synced = false;
		}
	
		this.cookingFuelUsedKerosene = cookingFuelUsedKerosene;
	}

	public Integer getCookingFuelUsedOthers() {
		return this.cookingFuelUsedOthers;
	}

	public void setCookingFuelUsedOthers(Integer cookingFuelUsedOthers) {
	
		if (this.cookingFuelUsedOthers==null || !this.cookingFuelUsedOthers.equals(cookingFuelUsedOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.cookingFuelUsedOthers = cookingFuelUsedOthers;
	}

	public String getCookingFuelUsedSpecify() {
		return this.cookingFuelUsedSpecify;
	}

	public void setCookingFuelUsedSpecify(String cookingFuelUsedSpecify) {
	
		if (this.cookingFuelUsedSpecify==null || !this.cookingFuelUsedSpecify.equals(cookingFuelUsedSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.cookingFuelUsedSpecify = cookingFuelUsedSpecify;
	}

	public Integer getKindOfFoodAvailableCooked() {
		return this.kindOfFoodAvailableCooked;
	}

	public void setKindOfFoodAvailableCooked(Integer kindOfFoodAvailableCooked) {
	
		if (this.kindOfFoodAvailableCooked==null || !this.kindOfFoodAvailableCooked.equals(kindOfFoodAvailableCooked))
		{
			editing = true;
			synced = false;
		}
	
		this.kindOfFoodAvailableCooked = kindOfFoodAvailableCooked;
	}

	public Integer getKindOfFoodAvailableJunk() {
		return this.kindOfFoodAvailableJunk;
	}

	public void setKindOfFoodAvailableJunk(Integer kindOfFoodAvailableJunk) {
	
		if (this.kindOfFoodAvailableJunk==null || !this.kindOfFoodAvailableJunk.equals(kindOfFoodAvailableJunk))
		{
			editing = true;
			synced = false;
		}
	
		this.kindOfFoodAvailableJunk = kindOfFoodAvailableJunk;
	}

	public Integer getKindOfFoodAvailableOthers() {
		return this.kindOfFoodAvailableOthers;
	}

	public void setKindOfFoodAvailableOthers(Integer kindOfFoodAvailableOthers) {
	
		if (this.kindOfFoodAvailableOthers==null || !this.kindOfFoodAvailableOthers.equals(kindOfFoodAvailableOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.kindOfFoodAvailableOthers = kindOfFoodAvailableOthers;
	}

	public String getKindOfFoodAvailableSpecify() {
		return this.kindOfFoodAvailableSpecify;
	}

	public void setKindOfFoodAvailableSpecify(String kindOfFoodAvailableSpecify) {
	
		if (this.kindOfFoodAvailableSpecify==null || !this.kindOfFoodAvailableSpecify.equals(kindOfFoodAvailableSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.kindOfFoodAvailableSpecify = kindOfFoodAvailableSpecify;
	}

	public Integer getFoodHygienic() {
		return this.foodHygienic;
	}

	public void setFoodHygienic(Integer foodHygienic) {
	
		if (this.foodHygienic==null || !this.foodHygienic.equals(foodHygienic))
		{
			editing = true;
			synced = false;
		}
	
		this.foodHygienic = foodHygienic;
	}

	public Integer getFoodStorageAppropriate() {
		return this.foodStorageAppropriate;
	}

	public void setFoodStorageAppropriate(Integer foodStorageAppropriate) {
	
		if (this.foodStorageAppropriate==null || !this.foodStorageAppropriate.equals(foodStorageAppropriate))
		{
			editing = true;
			synced = false;
		}
	
		this.foodStorageAppropriate = foodStorageAppropriate;
	}

	public Integer getFamilyPlanningMethod() {
		return this.familyPlanningMethod;
	}

	public void setFamilyPlanningMethod(Integer familyPlanningMethod) {
	
		if (this.familyPlanningMethod==null || !this.familyPlanningMethod.equals(familyPlanningMethod))
		{
			editing = true;
			synced = false;
		}
	
		this.familyPlanningMethod = familyPlanningMethod;
	}

	public Integer getKnowOfServices() {
		return this.knowOfServices;
	}

	public void setKnowOfServices(Integer knowOfServices) {
	
		if (this.knowOfServices==null || !this.knowOfServices.equals(knowOfServices))
		{
			editing = true;
			synced = false;
		}
	
		this.knowOfServices = knowOfServices;
	}

	public Integer getKnowOfEcp() {
		return this.knowOfEcp;
	}

	public void setKnowOfEcp(Integer knowOfEcp) {
	
		if (this.knowOfEcp==null || !this.knowOfEcp.equals(knowOfEcp))
		{
			editing = true;
			synced = false;
		}
	
		this.knowOfEcp = knowOfEcp;
	}

	public Integer getWhereEcpAvailableMobileClinic() {
		return this.whereEcpAvailableMobileClinic;
	}

	public void setWhereEcpAvailableMobileClinic(Integer whereEcpAvailableMobileClinic) {
	
		if (this.whereEcpAvailableMobileClinic==null || !this.whereEcpAvailableMobileClinic.equals(whereEcpAvailableMobileClinic))
		{
			editing = true;
			synced = false;
		}
	
		this.whereEcpAvailableMobileClinic = whereEcpAvailableMobileClinic;
	}

	public Integer getWhereEcpAvailableHealthFacility() {
		return this.whereEcpAvailableHealthFacility;
	}

	public void setWhereEcpAvailableHealthFacility(Integer whereEcpAvailableHealthFacility) {
	
		if (this.whereEcpAvailableHealthFacility==null || !this.whereEcpAvailableHealthFacility.equals(whereEcpAvailableHealthFacility))
		{
			editing = true;
			synced = false;
		}
	
		this.whereEcpAvailableHealthFacility = whereEcpAvailableHealthFacility;
	}

	public Integer getWhereEcpAvailableOthers() {
		return this.whereEcpAvailableOthers;
	}

	public void setWhereEcpAvailableOthers(Integer whereEcpAvailableOthers) {
	
		if (this.whereEcpAvailableOthers==null || !this.whereEcpAvailableOthers.equals(whereEcpAvailableOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.whereEcpAvailableOthers = whereEcpAvailableOthers;
	}

	public String getWhereEcpAvailableSpecify() {
		return this.whereEcpAvailableSpecify;
	}

	public void setWhereEcpAvailableSpecify(String whereEcpAvailableSpecify) {
	
		if (this.whereEcpAvailableSpecify==null || !this.whereEcpAvailableSpecify.equals(whereEcpAvailableSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.whereEcpAvailableSpecify = whereEcpAvailableSpecify;
	}

	public Integer getUnsualDiseaseOutbreak() {
		return this.unsualDiseaseOutbreak;
	}

	public void setUnsualDiseaseOutbreak(Integer unsualDiseaseOutbreak) {
	
		if (this.unsualDiseaseOutbreak==null || !this.unsualDiseaseOutbreak.equals(unsualDiseaseOutbreak))
		{
			editing = true;
			synced = false;
		}
	
		this.unsualDiseaseOutbreak = unsualDiseaseOutbreak;
	}

	public String getUnsualDiseaseOutbreakSpecify() {
		return this.unsualDiseaseOutbreakSpecify;
	}

	public void setUnsualDiseaseOutbreakSpecify(String unsualDiseaseOutbreakSpecify) {
	
		if (this.unsualDiseaseOutbreakSpecify==null || !this.unsualDiseaseOutbreakSpecify.equals(unsualDiseaseOutbreakSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.unsualDiseaseOutbreakSpecify = unsualDiseaseOutbreakSpecify;
	}

	public Integer getHealthCareOnSite() {
		return this.healthCareOnSite;
	}

	public void setHealthCareOnSite(Integer healthCareOnSite) {
	
		if (this.healthCareOnSite==null || !this.healthCareOnSite.equals(healthCareOnSite))
		{
			editing = true;
			synced = false;
		}
	
		this.healthCareOnSite = healthCareOnSite;
	}

	public Integer getPfaPresent() {
		return this.pfaPresent;
	}

	public void setPfaPresent(Integer pfaPresent) {
	
		if (this.pfaPresent==null || !this.pfaPresent.equals(pfaPresent))
		{
			editing = true;
			synced = false;
		}
	
		this.pfaPresent = pfaPresent;
	}

	public Integer getTimeToNearestHealthFacility() {
		return this.timeToNearestHealthFacility;
	}

	public void setTimeToNearestHealthFacility(Integer timeToNearestHealthFacility) {
	
		if (this.timeToNearestHealthFacility==null || !this.timeToNearestHealthFacility.equals(timeToNearestHealthFacility))
		{
			editing = true;
			synced = false;
		}
	
		this.timeToNearestHealthFacility = timeToNearestHealthFacility;
	}

	public Integer getTypeOfNearestFacilityHealthPost() {
		return this.typeOfNearestFacilityHealthPost;
	}

	public void setTypeOfNearestFacilityHealthPost(Integer typeOfNearestFacilityHealthPost) {
	
		if (this.typeOfNearestFacilityHealthPost==null || !this.typeOfNearestFacilityHealthPost.equals(typeOfNearestFacilityHealthPost))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityHealthPost = typeOfNearestFacilityHealthPost;
	}

	public Integer getTypeOfNearestFacilityPrimaryHealthCenter() {
		return this.typeOfNearestFacilityPrimaryHealthCenter;
	}

	public void setTypeOfNearestFacilityPrimaryHealthCenter(Integer typeOfNearestFacilityPrimaryHealthCenter) {
	
		if (this.typeOfNearestFacilityPrimaryHealthCenter==null || !this.typeOfNearestFacilityPrimaryHealthCenter.equals(typeOfNearestFacilityPrimaryHealthCenter))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityPrimaryHealthCenter = typeOfNearestFacilityPrimaryHealthCenter;
	}

	public Integer getTypeOfNearestFacilityDistrictHospital() {
		return this.typeOfNearestFacilityDistrictHospital;
	}

	public void setTypeOfNearestFacilityDistrictHospital(Integer typeOfNearestFacilityDistrictHospital) {
	
		if (this.typeOfNearestFacilityDistrictHospital==null || !this.typeOfNearestFacilityDistrictHospital.equals(typeOfNearestFacilityDistrictHospital))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityDistrictHospital = typeOfNearestFacilityDistrictHospital;
	}

	public Integer getTypeOfNearestFacilityZonalHospital() {
		return this.typeOfNearestFacilityZonalHospital;
	}

	public void setTypeOfNearestFacilityZonalHospital(Integer typeOfNearestFacilityZonalHospital) {
	
		if (this.typeOfNearestFacilityZonalHospital==null || !this.typeOfNearestFacilityZonalHospital.equals(typeOfNearestFacilityZonalHospital))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityZonalHospital = typeOfNearestFacilityZonalHospital;
	}

	public Integer getTypeOfNearestFacilitySubRegionalHospital() {
		return this.typeOfNearestFacilitySubRegionalHospital;
	}

	public void setTypeOfNearestFacilitySubRegionalHospital(Integer typeOfNearestFacilitySubRegionalHospital) {
	
		if (this.typeOfNearestFacilitySubRegionalHospital==null || !this.typeOfNearestFacilitySubRegionalHospital.equals(typeOfNearestFacilitySubRegionalHospital))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilitySubRegionalHospital = typeOfNearestFacilitySubRegionalHospital;
	}

	public Integer getTypeOfNearestFacilityTertiaryHospital() {
		return this.typeOfNearestFacilityTertiaryHospital;
	}

	public void setTypeOfNearestFacilityTertiaryHospital(Integer typeOfNearestFacilityTertiaryHospital) {
	
		if (this.typeOfNearestFacilityTertiaryHospital==null || !this.typeOfNearestFacilityTertiaryHospital.equals(typeOfNearestFacilityTertiaryHospital))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityTertiaryHospital = typeOfNearestFacilityTertiaryHospital;
	}

	public Integer getTypeOfNearestFacilityPrivateHospital() {
		return this.typeOfNearestFacilityPrivateHospital;
	}

	public void setTypeOfNearestFacilityPrivateHospital(Integer typeOfNearestFacilityPrivateHospital) {
	
		if (this.typeOfNearestFacilityPrivateHospital==null || !this.typeOfNearestFacilityPrivateHospital.equals(typeOfNearestFacilityPrivateHospital))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityPrivateHospital = typeOfNearestFacilityPrivateHospital;
	}

	public Integer getTypeOfNearestFacilityOthers() {
		return this.typeOfNearestFacilityOthers;
	}

	public void setTypeOfNearestFacilityOthers(Integer typeOfNearestFacilityOthers) {
	
		if (this.typeOfNearestFacilityOthers==null || !this.typeOfNearestFacilityOthers.equals(typeOfNearestFacilityOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilityOthers = typeOfNearestFacilityOthers;
	}

	public String getTypeOfNearestFacilitySpecify() {
		return this.typeOfNearestFacilitySpecify;
	}

	public void setTypeOfNearestFacilitySpecify(String typeOfNearestFacilitySpecify) {
	
		if (this.typeOfNearestFacilitySpecify==null || !this.typeOfNearestFacilitySpecify.equals(typeOfNearestFacilitySpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.typeOfNearestFacilitySpecify = typeOfNearestFacilitySpecify;
	}

	public Integer getFamilyIllnessDiabetesMellitus() {
		return this.familyIllnessDiabetesMellitus;
	}

	public void setFamilyIllnessDiabetesMellitus(Integer familyIllnessDiabetesMellitus) {
	
		if (this.familyIllnessDiabetesMellitus==null || !this.familyIllnessDiabetesMellitus.equals(familyIllnessDiabetesMellitus))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessDiabetesMellitus = familyIllnessDiabetesMellitus;
	}

	public Integer getFamilyIllnessHypertension() {
		return this.familyIllnessHypertension;
	}

	public void setFamilyIllnessHypertension(Integer familyIllnessHypertension) {
	
		if (this.familyIllnessHypertension==null || !this.familyIllnessHypertension.equals(familyIllnessHypertension))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessHypertension = familyIllnessHypertension;
	}

	public Integer getFamilyIllnessCopd() {
		return this.familyIllnessCopd;
	}

	public void setFamilyIllnessCopd(Integer familyIllnessCopd) {
	
		if (this.familyIllnessCopd==null || !this.familyIllnessCopd.equals(familyIllnessCopd))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessCopd = familyIllnessCopd;
	}

	public Integer getFamilyIllnessCancer() {
		return this.familyIllnessCancer;
	}

	public void setFamilyIllnessCancer(Integer familyIllnessCancer) {
	
		if (this.familyIllnessCancer==null || !this.familyIllnessCancer.equals(familyIllnessCancer))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessCancer = familyIllnessCancer;
	}

	public Integer getFamilyIllnessMentalHealthProblems() {
		return this.familyIllnessMentalHealthProblems;
	}

	public void setFamilyIllnessMentalHealthProblems(Integer familyIllnessMentalHealthProblems) {
	
		if (this.familyIllnessMentalHealthProblems==null || !this.familyIllnessMentalHealthProblems.equals(familyIllnessMentalHealthProblems))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessMentalHealthProblems = familyIllnessMentalHealthProblems;
	}

	public Integer getFamilyIllnessOthers() {
		return this.familyIllnessOthers;
	}

	public void setFamilyIllnessOthers(Integer familyIllnessOthers) {
	
		if (this.familyIllnessOthers==null || !this.familyIllnessOthers.equals(familyIllnessOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessOthers = familyIllnessOthers;
	}

	public String getFamilyIllnessSpecify() {
		return this.familyIllnessSpecify;
	}

	public void setFamilyIllnessSpecify(String familyIllnessSpecify) {
	
		if (this.familyIllnessSpecify==null || !this.familyIllnessSpecify.equals(familyIllnessSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessSpecify = familyIllnessSpecify;
	}

	public Integer getFamilyIllnessNone() {
		return this.familyIllnessNone;
	}

	public void setFamilyIllnessNone(Integer familyIllnessNone) {
	
		if (this.familyIllnessNone==null || !this.familyIllnessNone.equals(familyIllnessNone))
		{
			editing = true;
			synced = false;
		}
	
		this.familyIllnessNone = familyIllnessNone;
	}

	public Integer getFamilyMentalIllness() {
		return this.familyMentalIllness;
	}

	public void setFamilyMentalIllness(Integer familyMentalIllness) {
	
		if (this.familyMentalIllness==null || !this.familyMentalIllness.equals(familyMentalIllness))
		{
			editing = true;
			synced = false;
		}
	
		this.familyMentalIllness = familyMentalIllness;
	}

	public Integer getUnderMedication() {
		return this.underMedication;
	}

	public void setUnderMedication(Integer underMedication) {
	
		if (this.underMedication==null || !this.underMedication.equals(underMedication))
		{
			editing = true;
			synced = false;
		}
	
		this.underMedication = underMedication;
	}

	public Integer getHealthHazardsNone() {
		return this.healthHazardsNone;
	}

	public void setHealthHazardsNone(Integer healthHazardsNone) {
	
		if (this.healthHazardsNone==null || !this.healthHazardsNone.equals(healthHazardsNone))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsNone = healthHazardsNone;
	}

	public Integer getHealthHazardsSmoking() {
		return this.healthHazardsSmoking;
	}

	public void setHealthHazardsSmoking(Integer healthHazardsSmoking) {
	
		if (this.healthHazardsSmoking==null || !this.healthHazardsSmoking.equals(healthHazardsSmoking))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsSmoking = healthHazardsSmoking;
	}

	public Integer getHealthHazardsAlcoholism() {
		return this.healthHazardsAlcoholism;
	}

	public void setHealthHazardsAlcoholism(Integer healthHazardsAlcoholism) {
	
		if (this.healthHazardsAlcoholism==null || !this.healthHazardsAlcoholism.equals(healthHazardsAlcoholism))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsAlcoholism = healthHazardsAlcoholism;
	}

	public Integer getHealthHazardsDrugAbuse() {
		return this.healthHazardsDrugAbuse;
	}

	public void setHealthHazardsDrugAbuse(Integer healthHazardsDrugAbuse) {
	
		if (this.healthHazardsDrugAbuse==null || !this.healthHazardsDrugAbuse.equals(healthHazardsDrugAbuse))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsDrugAbuse = healthHazardsDrugAbuse;
	}

	public Integer getHealthHazardsOthers() {
		return this.healthHazardsOthers;
	}

	public void setHealthHazardsOthers(Integer healthHazardsOthers) {
	
		if (this.healthHazardsOthers==null || !this.healthHazardsOthers.equals(healthHazardsOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsOthers = healthHazardsOthers;
	}

	public String getHealthHazardsSpecify() {
		return this.healthHazardsSpecify;
	}

	public void setHealthHazardsSpecify(String healthHazardsSpecify) {
	
		if (this.healthHazardsSpecify==null || !this.healthHazardsSpecify.equals(healthHazardsSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.healthHazardsSpecify = healthHazardsSpecify;
	}

	public Integer getAbuseNo() {
		return this.abuseNo;
	}

	public void setAbuseNo(Integer abuseNo) {
	
		if (this.abuseNo==null || !this.abuseNo.equals(abuseNo))
		{
			editing = true;
			synced = false;
		}
	
		this.abuseNo = abuseNo;
	}

	public Integer getAbuseSexualAbuse() {
		return this.abuseSexualAbuse;
	}

	public void setAbuseSexualAbuse(Integer abuseSexualAbuse) {
	
		if (this.abuseSexualAbuse==null || !this.abuseSexualAbuse.equals(abuseSexualAbuse))
		{
			editing = true;
			synced = false;
		}
	
		this.abuseSexualAbuse = abuseSexualAbuse;
	}

	public Integer getAbusePhysicalAbuse() {
		return this.abusePhysicalAbuse;
	}

	public void setAbusePhysicalAbuse(Integer abusePhysicalAbuse) {
	
		if (this.abusePhysicalAbuse==null || !this.abusePhysicalAbuse.equals(abusePhysicalAbuse))
		{
			editing = true;
			synced = false;
		}
	
		this.abusePhysicalAbuse = abusePhysicalAbuse;
	}

	public Integer getAbusePsychologicalAbuse() {
		return this.abusePsychologicalAbuse;
	}

	public void setAbusePsychologicalAbuse(Integer abusePsychologicalAbuse) {
	
		if (this.abusePsychologicalAbuse==null || !this.abusePsychologicalAbuse.equals(abusePsychologicalAbuse))
		{
			editing = true;
			synced = false;
		}
	
		this.abusePsychologicalAbuse = abusePsychologicalAbuse;
	}

	public Integer getAbuseOthers() {
		return this.abuseOthers;
	}

	public void setAbuseOthers(Integer abuseOthers) {
	
		if (this.abuseOthers==null || !this.abuseOthers.equals(abuseOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.abuseOthers = abuseOthers;
	}

	public String getAbuseSpecify() {
		return this.abuseSpecify;
	}

	public void setAbuseSpecify(String abuseSpecify) {
	
		if (this.abuseSpecify==null || !this.abuseSpecify.equals(abuseSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.abuseSpecify = abuseSpecify;
	}

    
    
    @Override
	public String toString() {
		return "CommunityNursing{" +
				"uuid='" + uuid + '\'' +
				", editing=" + editing +
				", synced=" + synced +
				", createdAt=" + createdAt +
				", shineId='" + shineId + '\'' +
				", disasterRecently='" + disasterRecently + '\'' +
				", typeOfDisaster='" + typeOfDisaster + '\'' +
				", durationAfterDisaster='" + durationAfterDisaster + '\'' +
				", numberHumanLoss='" + numberHumanLoss + '\'' +
				", numberMissingPeople='" + numberMissingPeople + '\'' +
				", numberInjuredPeople='" + numberInjuredPeople + '\'' +
				", numberLivestockLoss='" + numberLivestockLoss + '\'' +
				", typeOfShelter='" + typeOfShelter + '\'' +
				", typeOfShelterSpecify='" + typeOfShelterSpecify + '\'' +
				", adequacyOfSpace='" + adequacyOfSpace + '\'' +
				", damaged='" + damaged + '\'' +
				", sourceOfLightElectricity='" + sourceOfLightElectricity + '\'' +
				", sourceOfLightOilLamp='" + sourceOfLightOilLamp + '\'' +
				", sourceOfLightSolar='" + sourceOfLightSolar + '\'' +
				", sourceOfLightOthers='" + sourceOfLightOthers + '\'' +
				", sourceOfLightSpecify='" + sourceOfLightSpecify + '\'' +
				", adequateVentilation='" + adequateVentilation + '\'' +
				", kitchen='" + kitchen + '\'' +
				", whenWashHandsAfterToileting='" + whenWashHandsAfterToileting + '\'' +
				", whenWashHandsBeforeCooking='" + whenWashHandsBeforeCooking + '\'' +
				", whenWashHandsBeforeEating='" + whenWashHandsBeforeEating + '\'' +
				", whenWashHandsBeforeFeedingAChild='" + whenWashHandsBeforeFeedingAChild + '\'' +
				", whenWashHandsOthers='" + whenWashHandsOthers + '\'' +
				", whenWashHandsSpecify='" + whenWashHandsSpecify + '\'' +
				", bathingHabit='" + bathingHabit + '\'' +
				", waterSupplySource='" + waterSupplySource + '\'' +
				", waterSupplySourceSpecify='" + waterSupplySourceSpecify + '\'' +
				", adequacyWaterSupply='" + adequacyWaterSupply + '\'' +
				", qualityDrinkingWater='" + qualityDrinkingWater + '\'' +
				", waterPurificationNone='" + waterPurificationNone + '\'' +
				", waterPurificationBoiling='" + waterPurificationBoiling + '\'' +
				", waterPurificationFiltration='" + waterPurificationFiltration + '\'' +
				", waterPurificationUsingChemical='" + waterPurificationUsingChemical + '\'' +
				", waterPurificationSodis='" + waterPurificationSodis + '\'' +
				", waterPurificationOthers='" + waterPurificationOthers + '\'' +
				", waterPurificationSpecify='" + waterPurificationSpecify + '\'' +
				", distanceFromWater='" + distanceFromWater + '\'' +
				", distanceFromWaterSpecify='" + distanceFromWaterSpecify + '\'' +
				", toiletAvailable='" + toiletAvailable + '\'' +
				", adequateToiletRatio='" + adequateToiletRatio + '\'' +
				", toiletType='" + toiletType + '\'' +
				", toiletTypeSpecify='" + toiletTypeSpecify + '\'' +
				", distanceTolietFromShelter='" + distanceTolietFromShelter + '\'' +
				", toiletDistance='" + toiletDistance + '\'' +
				", toiletDistanceUnit='" + toiletDistanceUnit + '\'' +
				", wasteDistance='" + wasteDistance + '\'' +
				", wasteDistanceUnit='" + wasteDistanceUnit + '\'' +
				", disposalMethodComposting='" + disposalMethodComposting + '\'' +
				", disposalMethodBurning='" + disposalMethodBurning + '\'' +
				", disposalMethodDumping='" + disposalMethodDumping + '\'' +
				", disposalMethodOthers='" + disposalMethodOthers + '\'' +
				", disposalMethodSpecify='" + disposalMethodSpecify + '\'' +
				", wasteCollectorAvailable='" + wasteCollectorAvailable + '\'' +
				", timelyCollection='" + timelyCollection + '\'' +
				", hasDeadAnimalDisposal='" + hasDeadAnimalDisposal + '\'' +
				", hasDrainage='" + hasDrainage + '\'' +
				", surroundingsClean='" + surroundingsClean + '\'' +
				", problemsMenstruationHygiene='" + problemsMenstruationHygiene + '\'' +
				", menstruationHygieneProblemsNoWater='" + menstruationHygieneProblemsNoWater + '\'' +
				", menstruationHygieneProblemsNoPrivacy='" + menstruationHygieneProblemsNoPrivacy + '\'' +
				", menstruationHygieneProblemsNoPads='" + menstruationHygieneProblemsNoPads + '\'' +
				", menstruationHygieneProblemsNoDisposal='" + menstruationHygieneProblemsNoDisposal + '\'' +
				", menstruationHygieneProblemsOthers='" + menstruationHygieneProblemsOthers + '\'' +
				", menstruationHygieneProblemsSpecify='" + menstruationHygieneProblemsSpecify + '\'' +
				", visibleHazardsSlipperyFloor='" + visibleHazardsSlipperyFloor + '\'' +
				", visibleHazardsNoLight='" + visibleHazardsNoLight + '\'' +
				", visibleHazardsBadElectical='" + visibleHazardsBadElectical + '\'' +
				", visibleHazardsRoughSurface='" + visibleHazardsRoughSurface + '\'' +
				", visibleHazardsSloppyLand='" + visibleHazardsSloppyLand + '\'' +
				", visibleHazardsDumpingArea='" + visibleHazardsDumpingArea + '\'' +
				", visibleHazardsPolluted='" + visibleHazardsPolluted + '\'' +
				", visibleHazardsNoVentilation='" + visibleHazardsNoVentilation + '\'' +
				", visibleHazardsNoisy='" + visibleHazardsNoisy + '\'' +
				", visibleHazardsOthers='" + visibleHazardsOthers + '\'' +
				", visibleHazardsSpecify='" + visibleHazardsSpecify + '\'' +
				", visibleHazardsNone='" + visibleHazardsNone + '\'' +
				", areaProneToNone='" + areaProneToNone + '\'' +
				", snakeBite='" + snakeBite + '\'' +
				", animalBite='" + animalBite + '\'' +
				", animalBiteSpecify='" + animalBiteSpecify + '\'' +
				", insectBite='" + insectBite + '\'' +
				", insectBiteSpecify='" + insectBiteSpecify + '\'' +
				", others='" + others + '\'' +
				", othersSpecify='" + othersSpecify + '\'' +
				", noVectors='" + noVectors + '\'' +
				", mossquitoPreventionNets='" + mossquitoPreventionNets + '\'' +
				", mossquitoPreventionLiquid='" + mossquitoPreventionLiquid + '\'' +
				", mossquitoPreventionDrugs='" + mossquitoPreventionDrugs + '\'' +
				", mossquitoPreventionOthers='" + mossquitoPreventionOthers + '\'' +
				", mossquitoPreventionSpecify='" + mossquitoPreventionSpecify + '\'' +
				", pestControl='" + pestControl + '\'' +
				", foodStockAvailable='" + foodStockAvailable + '\'' +
				", cookingFuelUsedGas='" + cookingFuelUsedGas + '\'' +
				", cookingFuelUsedFirewood='" + cookingFuelUsedFirewood + '\'' +
				", cookingFuelUsedKerosene='" + cookingFuelUsedKerosene + '\'' +
				", cookingFuelUsedOthers='" + cookingFuelUsedOthers + '\'' +
				", cookingFuelUsedSpecify='" + cookingFuelUsedSpecify + '\'' +
				", kindOfFoodAvailableCooked='" + kindOfFoodAvailableCooked + '\'' +
				", kindOfFoodAvailableJunk='" + kindOfFoodAvailableJunk + '\'' +
				", kindOfFoodAvailableOthers='" + kindOfFoodAvailableOthers + '\'' +
				", kindOfFoodAvailableSpecify='" + kindOfFoodAvailableSpecify + '\'' +
				", foodHygienic='" + foodHygienic + '\'' +
				", foodStorageAppropriate='" + foodStorageAppropriate + '\'' +
				", familyPlanningMethod='" + familyPlanningMethod + '\'' +
				", knowOfServices='" + knowOfServices + '\'' +
				", knowOfEcp='" + knowOfEcp + '\'' +
				", whereEcpAvailableMobileClinic='" + whereEcpAvailableMobileClinic + '\'' +
				", whereEcpAvailableHealthFacility='" + whereEcpAvailableHealthFacility + '\'' +
				", whereEcpAvailableOthers='" + whereEcpAvailableOthers + '\'' +
				", whereEcpAvailableSpecify='" + whereEcpAvailableSpecify + '\'' +
				", unsualDiseaseOutbreak='" + unsualDiseaseOutbreak + '\'' +
				", unsualDiseaseOutbreakSpecify='" + unsualDiseaseOutbreakSpecify + '\'' +
				", healthCareOnSite='" + healthCareOnSite + '\'' +
				", pfaPresent='" + pfaPresent + '\'' +
				", timeToNearestHealthFacility='" + timeToNearestHealthFacility + '\'' +
				", typeOfNearestFacilityHealthPost='" + typeOfNearestFacilityHealthPost + '\'' +
				", typeOfNearestFacilityPrimaryHealthCenter='" + typeOfNearestFacilityPrimaryHealthCenter + '\'' +
				", typeOfNearestFacilityDistrictHospital='" + typeOfNearestFacilityDistrictHospital + '\'' +
				", typeOfNearestFacilityZonalHospital='" + typeOfNearestFacilityZonalHospital + '\'' +
				", typeOfNearestFacilitySubRegionalHospital='" + typeOfNearestFacilitySubRegionalHospital + '\'' +
				", typeOfNearestFacilityTertiaryHospital='" + typeOfNearestFacilityTertiaryHospital + '\'' +
				", typeOfNearestFacilityPrivateHospital='" + typeOfNearestFacilityPrivateHospital + '\'' +
				", typeOfNearestFacilityOthers='" + typeOfNearestFacilityOthers + '\'' +
				", typeOfNearestFacilitySpecify='" + typeOfNearestFacilitySpecify + '\'' +
				", familyIllnessDiabetesMellitus='" + familyIllnessDiabetesMellitus + '\'' +
				", familyIllnessHypertension='" + familyIllnessHypertension + '\'' +
				", familyIllnessCopd='" + familyIllnessCopd + '\'' +
				", familyIllnessCancer='" + familyIllnessCancer + '\'' +
				", familyIllnessMentalHealthProblems='" + familyIllnessMentalHealthProblems + '\'' +
				", familyIllnessOthers='" + familyIllnessOthers + '\'' +
				", familyIllnessSpecify='" + familyIllnessSpecify + '\'' +
				", familyIllnessNone='" + familyIllnessNone + '\'' +
				", familyMentalIllness='" + familyMentalIllness + '\'' +
				", underMedication='" + underMedication + '\'' +
				", healthHazardsNone='" + healthHazardsNone + '\'' +
				", healthHazardsSmoking='" + healthHazardsSmoking + '\'' +
				", healthHazardsAlcoholism='" + healthHazardsAlcoholism + '\'' +
				", healthHazardsDrugAbuse='" + healthHazardsDrugAbuse + '\'' +
				", healthHazardsOthers='" + healthHazardsOthers + '\'' +
				", healthHazardsSpecify='" + healthHazardsSpecify + '\'' +
				", abuseNo='" + abuseNo + '\'' +
				", abuseSexualAbuse='" + abuseSexualAbuse + '\'' +
				", abusePhysicalAbuse='" + abusePhysicalAbuse + '\'' +
				", abusePsychologicalAbuse='" + abusePsychologicalAbuse + '\'' +
				", abuseOthers='" + abuseOthers + '\'' +
				", abuseSpecify='" + abuseSpecify + '\'' +

				'}';
	}
}
