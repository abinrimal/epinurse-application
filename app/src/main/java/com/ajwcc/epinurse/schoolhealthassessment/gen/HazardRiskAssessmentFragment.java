package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_hazard_risk_assessment)
public class HazardRiskAssessmentFragment extends BaseEpinurseFragment {


    public HazardRiskAssessmentFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected SeekBar earthquakeHazards;

	@ViewById
	@MapToModelField
	protected SeekBar earthquakeVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText earthquakeRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar landslideHazards;

	@ViewById
	@MapToModelField
	protected SeekBar landslideVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText landslideRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar wildlifeAttackHazards;

	@ViewById
	@MapToModelField
	protected SeekBar wildlifeAttackVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText wildlifeAttackRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar fireHazards;

	@ViewById
	@MapToModelField
	protected SeekBar fireVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText fireRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar pestDiseasesHazards;

	@ViewById
	@MapToModelField
	protected SeekBar pestDiseasesVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText pestDiseasesRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar floodHazards;

	@ViewById
	@MapToModelField
	protected SeekBar floodVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText floodRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar windstormHazards;

	@ViewById
	@MapToModelField
	protected SeekBar windstormVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText windstormRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar droughtHazards;

	@ViewById
	@MapToModelField
	protected SeekBar droughtVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText droughtRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar hailstormHazards;

	@ViewById
	@MapToModelField
	protected SeekBar hailstormVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText hailstormRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar soilErosionHazards;

	@ViewById
	@MapToModelField
	protected SeekBar soilErosionVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText soilErosionRiskScore;

	@ViewById
	@MapToModelField
	protected SeekBar epidemicsHazards;

	@ViewById
	@MapToModelField
	protected SeekBar epidemicsVulnerabilities;

	@ViewById
	@MapToModelField
	protected EditText epidemicsRiskScore;

	@ViewById
	@MapToModelField
	protected RadioGroup disasterDrill;

	@ViewById
	@MapToModelField
	protected EditText disasterDrillSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup disasterPreparednessMeasures;

	@ViewById
	@MapToModelField
	protected EditText disasterPreparednessMeasuresSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup earthquakeResistant;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(earthquakeHazards,pojo.getEarthquakeHazards());
					PojoToViewMapper.setViewValue(earthquakeVulnerabilities,pojo.getEarthquakeVulnerabilities());
					PojoToViewMapper.setViewValue(earthquakeRiskScore,pojo.getEarthquakeRiskScore());
					PojoToViewMapper.setViewValue(landslideHazards,pojo.getLandslideHazards());
					PojoToViewMapper.setViewValue(landslideVulnerabilities,pojo.getLandslideVulnerabilities());
					PojoToViewMapper.setViewValue(landslideRiskScore,pojo.getLandslideRiskScore());
					PojoToViewMapper.setViewValue(wildlifeAttackHazards,pojo.getWildlifeAttackHazards());
					PojoToViewMapper.setViewValue(wildlifeAttackVulnerabilities,pojo.getWildlifeAttackVulnerabilities());
					PojoToViewMapper.setViewValue(wildlifeAttackRiskScore,pojo.getWildlifeAttackRiskScore());
					PojoToViewMapper.setViewValue(fireHazards,pojo.getFireHazards());
					PojoToViewMapper.setViewValue(fireVulnerabilities,pojo.getFireVulnerabilities());
					PojoToViewMapper.setViewValue(fireRiskScore,pojo.getFireRiskScore());
					PojoToViewMapper.setViewValue(pestDiseasesHazards,pojo.getPestDiseasesHazards());
					PojoToViewMapper.setViewValue(pestDiseasesVulnerabilities,pojo.getPestDiseasesVulnerabilities());
					PojoToViewMapper.setViewValue(pestDiseasesRiskScore,pojo.getPestDiseasesRiskScore());
					PojoToViewMapper.setViewValue(floodHazards,pojo.getFloodHazards());
					PojoToViewMapper.setViewValue(floodVulnerabilities,pojo.getFloodVulnerabilities());
					PojoToViewMapper.setViewValue(floodRiskScore,pojo.getFloodRiskScore());
					PojoToViewMapper.setViewValue(windstormHazards,pojo.getWindstormHazards());
					PojoToViewMapper.setViewValue(windstormVulnerabilities,pojo.getWindstormVulnerabilities());
					PojoToViewMapper.setViewValue(windstormRiskScore,pojo.getWindstormRiskScore());
					PojoToViewMapper.setViewValue(droughtHazards,pojo.getDroughtHazards());
					PojoToViewMapper.setViewValue(droughtVulnerabilities,pojo.getDroughtVulnerabilities());
					PojoToViewMapper.setViewValue(droughtRiskScore,pojo.getDroughtRiskScore());
					PojoToViewMapper.setViewValue(hailstormHazards,pojo.getHailstormHazards());
					PojoToViewMapper.setViewValue(hailstormVulnerabilities,pojo.getHailstormVulnerabilities());
					PojoToViewMapper.setViewValue(hailstormRiskScore,pojo.getHailstormRiskScore());
					PojoToViewMapper.setViewValue(soilErosionHazards,pojo.getSoilErosionHazards());
					PojoToViewMapper.setViewValue(soilErosionVulnerabilities,pojo.getSoilErosionVulnerabilities());
					PojoToViewMapper.setViewValue(soilErosionRiskScore,pojo.getSoilErosionRiskScore());
					PojoToViewMapper.setViewValue(epidemicsHazards,pojo.getEpidemicsHazards());
					PojoToViewMapper.setViewValue(epidemicsVulnerabilities,pojo.getEpidemicsVulnerabilities());
					PojoToViewMapper.setViewValue(epidemicsRiskScore,pojo.getEpidemicsRiskScore());
					PojoToViewMapper.setViewValue(disasterDrill,pojo.getDisasterDrill());
					PojoToViewMapper.setViewValue(disasterDrillSpecify,pojo.getDisasterDrillSpecify());
					PojoToViewMapper.setViewValue(disasterPreparednessMeasures,pojo.getDisasterPreparednessMeasures());
					PojoToViewMapper.setViewValue(disasterPreparednessMeasuresSpecify,pojo.getDisasterPreparednessMeasuresSpecify());
					PojoToViewMapper.setViewValue(earthquakeResistant,pojo.getEarthquakeResistant());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setEarthquakeHazards((Integer) ViewToPojoMapper.getValueByView(earthquakeHazards));
					pojo.setEarthquakeVulnerabilities((Integer) ViewToPojoMapper.getValueByView(earthquakeVulnerabilities));
					pojo.setEarthquakeRiskScore((Integer) ViewToPojoMapper.getValueByView(earthquakeRiskScore));
					pojo.setLandslideHazards((Integer) ViewToPojoMapper.getValueByView(landslideHazards));
					pojo.setLandslideVulnerabilities((Integer) ViewToPojoMapper.getValueByView(landslideVulnerabilities));
					pojo.setLandslideRiskScore((Integer) ViewToPojoMapper.getValueByView(landslideRiskScore));
					pojo.setWildlifeAttackHazards((Integer) ViewToPojoMapper.getValueByView(wildlifeAttackHazards));
					pojo.setWildlifeAttackVulnerabilities((Integer) ViewToPojoMapper.getValueByView(wildlifeAttackVulnerabilities));
					pojo.setWildlifeAttackRiskScore((Integer) ViewToPojoMapper.getValueByView(wildlifeAttackRiskScore));
					pojo.setFireHazards((Integer) ViewToPojoMapper.getValueByView(fireHazards));
					pojo.setFireVulnerabilities((Integer) ViewToPojoMapper.getValueByView(fireVulnerabilities));
					pojo.setFireRiskScore((Integer) ViewToPojoMapper.getValueByView(fireRiskScore));
					pojo.setPestDiseasesHazards((Integer) ViewToPojoMapper.getValueByView(pestDiseasesHazards));
					pojo.setPestDiseasesVulnerabilities((Integer) ViewToPojoMapper.getValueByView(pestDiseasesVulnerabilities));
					pojo.setPestDiseasesRiskScore((Integer) ViewToPojoMapper.getValueByView(pestDiseasesRiskScore));
					pojo.setFloodHazards((Integer) ViewToPojoMapper.getValueByView(floodHazards));
					pojo.setFloodVulnerabilities((Integer) ViewToPojoMapper.getValueByView(floodVulnerabilities));
					pojo.setFloodRiskScore((Integer) ViewToPojoMapper.getValueByView(floodRiskScore));
					pojo.setWindstormHazards((Integer) ViewToPojoMapper.getValueByView(windstormHazards));
					pojo.setWindstormVulnerabilities((Integer) ViewToPojoMapper.getValueByView(windstormVulnerabilities));
					pojo.setWindstormRiskScore((Integer) ViewToPojoMapper.getValueByView(windstormRiskScore));
					pojo.setDroughtHazards((Integer) ViewToPojoMapper.getValueByView(droughtHazards));
					pojo.setDroughtVulnerabilities((Integer) ViewToPojoMapper.getValueByView(droughtVulnerabilities));
					pojo.setDroughtRiskScore((Integer) ViewToPojoMapper.getValueByView(droughtRiskScore));
					pojo.setHailstormHazards((Integer) ViewToPojoMapper.getValueByView(hailstormHazards));
					pojo.setHailstormVulnerabilities((Integer) ViewToPojoMapper.getValueByView(hailstormVulnerabilities));
					pojo.setHailstormRiskScore((Integer) ViewToPojoMapper.getValueByView(hailstormRiskScore));
					pojo.setSoilErosionHazards((Integer) ViewToPojoMapper.getValueByView(soilErosionHazards));
					pojo.setSoilErosionVulnerabilities((Integer) ViewToPojoMapper.getValueByView(soilErosionVulnerabilities));
					pojo.setSoilErosionRiskScore((Integer) ViewToPojoMapper.getValueByView(soilErosionRiskScore));
					pojo.setEpidemicsHazards((Integer) ViewToPojoMapper.getValueByView(epidemicsHazards));
					pojo.setEpidemicsVulnerabilities((Integer) ViewToPojoMapper.getValueByView(epidemicsVulnerabilities));
					pojo.setEpidemicsRiskScore((Integer) ViewToPojoMapper.getValueByView(epidemicsRiskScore));
					pojo.setDisasterDrill((Integer) ViewToPojoMapper.getValueByView(disasterDrill));
					pojo.setDisasterDrillSpecify((String) ViewToPojoMapper.getValueByView(disasterDrillSpecify));
					pojo.setDisasterPreparednessMeasures((Integer) ViewToPojoMapper.getValueByView(disasterPreparednessMeasures));
					pojo.setDisasterPreparednessMeasuresSpecify((String) ViewToPojoMapper.getValueByView(disasterPreparednessMeasuresSpecify));
					pojo.setEarthquakeResistant((Integer) ViewToPojoMapper.getValueByView(earthquakeResistant));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.disasterDrill1,R.id.disasterPreparednessMeasures1})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.disasterDrill1:
				 UiUtils.toggleSpecify(view, disasterDrillSpecify);
				break;
			case R.id.disasterPreparednessMeasures1:
				 UiUtils.toggleSpecify(view, disasterPreparednessMeasuresSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
