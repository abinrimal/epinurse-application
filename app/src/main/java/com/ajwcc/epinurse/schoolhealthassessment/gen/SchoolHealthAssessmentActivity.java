package com.ajwcc.epinurse.schoolhealthassessment.gen;

import android.support.v4.view.ViewPager;
import android.widget.SeekBar;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import com.ajwcc.epinurse.common.SaveFragment_;


@EActivity(R.layout.gen_activity_school_health_assessment)
public class SchoolHealthAssessmentActivity extends BaseEpinurseFormActivity
{

    @Extra
    public String uuid;

    @ViewById(R.id.title)
    public TextView title;

    @ViewById(R.id.pager)
    public ViewPager viewPager;

    @ViewById(R.id.seekbar)
    public SeekBar seekBar;

	@ViewById(R.id.previous)
    public Button previous;

    @ViewById(R.id.next)
    public Button next;
    
    @AfterViews
    public void init()
    {
    	
    	//title.setText("School Health Assessment");
    	
    	title.setText(getResources().getString(R.string.school_health_assessment));
    	
    
        // load existing for editing at this point, given a uuid
        initModel(SchoolHealthAssessment.class, uuid);


        initViewPagerAndSeekbar(createFragmentList(), viewPager, seekBar, previous, next);
        
        createToC();
    	setValidationHandler(getValidationHandler());
    }


	public ValidationHandler<SchoolHealthAssessment> getValidationHandler()
	{
		SchoolHealthAssessmentValidator validator = new SchoolHealthAssessmentValidator(this);
		validator.setModel((SchoolHealthAssessment)getModel());	
		return validator;
	}

    LinkedHashMap<String, Integer> pageMap = new LinkedHashMap<>();

    private void createToC()
    {
        List<String> toc = createTableOfContents();
        for (int i=0; i<toc.size(); i++)
        {
            pageMap.put(toc.get(i), i);
        }
    }

    public Integer getPage(String name)
    {
        return pageMap.get(name);
    }

    protected List<String> createTableOfContents()
    {
    	List<String> names = new ArrayList<>();
    	
		names.add("Basic Information");
		names.add("Ecd");
		names.add("Extra Curricular Activities");
		names.add("First Aid");
		names.add("Drinking Water");
		names.add("Food and Nutrition");
		names.add("Toilet Facilities");
		names.add("Infrastructure");
		names.add("Hazard Risk Assessment");

    	
        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = new ArrayList<>();
        
		list.add(() -> BasicInformationFragment_.builder().build());
		list.add(() -> EcdFragment_.builder().build());
		list.add(() -> ExtraCurricularActivitiesFragment_.builder().build());
		list.add(() -> FirstAidFragment_.builder().build());
		list.add(() -> DrinkingWaterFragment_.builder().build());
		list.add(() -> FoodAndNutritionFragment_.builder().build());
		list.add(() -> ToiletFacilitiesFragment_.builder().build());
		list.add(() -> InfrastructureFragment_.builder().build());
		list.add(() -> HazardRiskAssessmentFragment_.builder().build());


        list.add(() -> SaveFragment_.builder().build());


        return list;
    }

}
