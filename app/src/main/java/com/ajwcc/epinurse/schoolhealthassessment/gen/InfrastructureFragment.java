package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_infrastructure)
public class InfrastructureFragment extends BaseEpinurseFragment {


    public InfrastructureFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup schoolAt;

	@ViewById
	@MapToModelField
	protected EditText schoolAtSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup buildingType;

	@ViewById
	@MapToModelField
	protected EditText ageOfBuilding;

	@ViewById
	@MapToModelField
	protected EditText buildingHeight;

	@ViewById
	@MapToModelField
	protected RadioGroup waterDamage;

	@ViewById
	@MapToModelField
	protected RadioGroup firefightingInstrument;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(schoolAt,pojo.getSchoolAt());
					PojoToViewMapper.setViewValue(schoolAtSpecify,pojo.getSchoolAtSpecify());
					PojoToViewMapper.setViewValue(buildingType,pojo.getBuildingType());
					PojoToViewMapper.setViewValue(ageOfBuilding,pojo.getAgeOfBuilding());
					PojoToViewMapper.setViewValue(buildingHeight,pojo.getBuildingHeight());
					PojoToViewMapper.setViewValue(waterDamage,pojo.getWaterDamage());
					PojoToViewMapper.setViewValue(firefightingInstrument,pojo.getFirefightingInstrument());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setSchoolAt((Integer) ViewToPojoMapper.getValueByView(schoolAt));
					pojo.setSchoolAtSpecify((String) ViewToPojoMapper.getValueByView(schoolAtSpecify));
					pojo.setBuildingType((Integer) ViewToPojoMapper.getValueByView(buildingType));
					pojo.setAgeOfBuilding((Integer) ViewToPojoMapper.getValueByView(ageOfBuilding));
					pojo.setBuildingHeight((Integer) ViewToPojoMapper.getValueByView(buildingHeight));
					pojo.setWaterDamage((Integer) ViewToPojoMapper.getValueByView(waterDamage));
					pojo.setFirefightingInstrument((Integer) ViewToPojoMapper.getValueByView(firefightingInstrument));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.schoolAt4})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.schoolAt4:
				 UiUtils.toggleSpecify(view, schoolAtSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
