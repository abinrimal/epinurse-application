package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_toilet_facilities)
public class ToiletFacilitiesFragment extends BaseEpinurseFragment {


    public ToiletFacilitiesFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup toilHasRunningWater;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasCleaningWater;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasSoap;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasSepGender;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasSepStudentStaff;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasTrash;

	@ViewById
	@MapToModelField
	protected RadioGroup toilHasAdequateToilets;

	@ViewById
	@MapToModelField
	protected SeekBar toilHasToiletRating;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(toilHasRunningWater,pojo.getToilHasRunningWater());
					PojoToViewMapper.setViewValue(toilHasCleaningWater,pojo.getToilHasCleaningWater());
					PojoToViewMapper.setViewValue(toilHasSoap,pojo.getToilHasSoap());
					PojoToViewMapper.setViewValue(toilHasSepGender,pojo.getToilHasSepGender());
					PojoToViewMapper.setViewValue(toilHasSepStudentStaff,pojo.getToilHasSepStudentStaff());
					PojoToViewMapper.setViewValue(toilHasTrash,pojo.getToilHasTrash());
					PojoToViewMapper.setViewValue(toilHasAdequateToilets,pojo.getToilHasAdequateToilets());
					PojoToViewMapper.setViewValue(toilHasToiletRating,pojo.getToilHasToiletRating());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setToilHasRunningWater((Integer) ViewToPojoMapper.getValueByView(toilHasRunningWater));
					pojo.setToilHasCleaningWater((Integer) ViewToPojoMapper.getValueByView(toilHasCleaningWater));
					pojo.setToilHasSoap((Integer) ViewToPojoMapper.getValueByView(toilHasSoap));
					pojo.setToilHasSepGender((Integer) ViewToPojoMapper.getValueByView(toilHasSepGender));
					pojo.setToilHasSepStudentStaff((Integer) ViewToPojoMapper.getValueByView(toilHasSepStudentStaff));
					pojo.setToilHasTrash((Integer) ViewToPojoMapper.getValueByView(toilHasTrash));
					pojo.setToilHasAdequateToilets((Integer) ViewToPojoMapper.getValueByView(toilHasAdequateToilets));
					pojo.setToilHasToiletRating((Integer) ViewToPojoMapper.getValueByView(toilHasToiletRating));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
