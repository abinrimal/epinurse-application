package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_basic_information)
public class BasicInformationFragment extends BaseEpinurseFragment {


    public BasicInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText school;

	@ViewById
	@MapToModelField
	protected EditText contactPerson;

	@ViewById
	@MapToModelField
	protected EditText contactPosition;

	@ViewById
	@MapToModelField
	protected CheckBox activitiesStudentHealthAssessment;

	@ViewById
	@MapToModelField
	protected CheckBox activitiesStaffMembersHealthAssessment;

	@ViewById
	@MapToModelField
	protected CheckBox activitiesSanitationClass;

	@ViewById
	@MapToModelField
	protected CheckBox activitiesDietClass;

	@ViewById
	@MapToModelField
	protected EditText distanceOfSchoolKm;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(school,pojo.getSchool());
					PojoToViewMapper.setViewValue(contactPerson,pojo.getContactPerson());
					PojoToViewMapper.setViewValue(contactPosition,pojo.getContactPosition());
					PojoToViewMapper.setViewValue(activitiesStudentHealthAssessment,pojo.getActivitiesStudentHealthAssessment());
					PojoToViewMapper.setViewValue(activitiesStaffMembersHealthAssessment,pojo.getActivitiesStaffMembersHealthAssessment());
					PojoToViewMapper.setViewValue(activitiesSanitationClass,pojo.getActivitiesSanitationClass());
					PojoToViewMapper.setViewValue(activitiesDietClass,pojo.getActivitiesDietClass());
					PojoToViewMapper.setViewValue(distanceOfSchoolKm,pojo.getDistanceOfSchoolKm());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setSchool((String) ViewToPojoMapper.getValueByView(school));
					pojo.setContactPerson((String) ViewToPojoMapper.getValueByView(contactPerson));
					pojo.setContactPosition((String) ViewToPojoMapper.getValueByView(contactPosition));
					pojo.setActivitiesStudentHealthAssessment((Integer) ViewToPojoMapper.getValueByView(activitiesStudentHealthAssessment));
					pojo.setActivitiesStaffMembersHealthAssessment((Integer) ViewToPojoMapper.getValueByView(activitiesStaffMembersHealthAssessment));
					pojo.setActivitiesSanitationClass((Integer) ViewToPojoMapper.getValueByView(activitiesSanitationClass));
					pojo.setActivitiesDietClass((Integer) ViewToPojoMapper.getValueByView(activitiesDietClass));
					pojo.setDistanceOfSchoolKm((Double) ViewToPojoMapper.getValueByView(distanceOfSchoolKm));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
