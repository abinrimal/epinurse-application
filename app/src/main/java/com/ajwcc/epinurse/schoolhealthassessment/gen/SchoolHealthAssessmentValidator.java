package com.ajwcc.epinurse.schoolhealthassessment.gen;

import com.ajwcc.epinurse.common.BaseValidator;
import com.ajwcc.util.ui.validation.ValidationHandler;
import com.ajwcc.epinurse.R;

public class SchoolHealthAssessmentValidator extends BaseValidator implements ValidationHandler<SchoolHealthAssessment>
{
    protected SchoolHealthAssessment model;
    protected SchoolHealthAssessmentActivity activity;


    public SchoolHealthAssessmentValidator(SchoolHealthAssessmentActivity a)
    {
    	super(a);
        activity = a;
    }

    @Override
    public void setModel(SchoolHealthAssessment model)
    {
        this.model = model;
    }

    @Override
    public void validateModel()
    {
		validateSchool();
		validateContactPerson();
		validateContactPosition();
		validateActivitiesStudentHealthAssessment();
		validateActivitiesStaffMembersHealthAssessment();
		validateActivitiesSanitationClass();
		validateActivitiesDietClass();
		validateDistanceOfSchoolKm();
		validateEcdPresent();
		validateEcdTeacherPresent();
		validateClassroomFacilities();
		validateSeatingArrangements();
		validateApprPlayMaterials();
		validateColorsIllustrations();
		validateToiletHandwashing();
		validateCollisionAvoidance();
		validateNonslipFloors();
		validateLightingFacilities();
		validateHasPlayground();
		validateSpaceAtEcaFacility();
		validateEcaDancing();
		validateEcaSinging();
		validateEcaRunning();
		validateEcaFootball();
		validateEcaVolleyball();
		validateEcaBasketball();
		validateEcaPingpong();
		validateEcaBadminton();
		validateEcaMartialarts();
		validateEcaOthers();
		validateEcaSpecify();
		validateFirstAidBoxPresent();
		validateFirstAidBoxStocked();
		validateNumStudentsCoveredByFirstAid();
		validateTypeWaterSupplied();
		validateWaterOutletAvailability();
		validateSchoolProvidesFoodOpt();
		validateSchoolProvidesFoodBreakfast();
		validateSchoolProvidesFoodLunch();
		validateSchoolProvidesFoodSnacks();
		validateSchoolProvidesFoodDinner();
		validateFoodHygienicSafe();
		validateMealBalanced();
		validateFoodCanBeBroughtFromHome();
		validateJunkFoodAllowed();
		validateToilHasRunningWater();
		validateToilHasCleaningWater();
		validateToilHasSoap();
		validateToilHasSepGender();
		validateToilHasSepStudentStaff();
		validateToilHasTrash();
		validateToilHasAdequateToilets();
		validateToilHasToiletRating();
		validateSchoolAt();
		validateSchoolAtSpecify();
		validateBuildingType();
		validateAgeOfBuilding();
		validateBuildingHeight();
		//  waterDamage optional
		validateFirefightingInstrument();
		validateEarthquakeHazards();
		validateEarthquakeVulnerabilities();
		validateEarthquakeRiskScore();
		validateLandslideHazards();
		validateLandslideVulnerabilities();
		validateLandslideRiskScore();
		validateWildlifeAttackHazards();
		validateWildlifeAttackVulnerabilities();
		validateWildlifeAttackRiskScore();
		validateFireHazards();
		validateFireVulnerabilities();
		validateFireRiskScore();
		validatePestDiseasesHazards();
		validatePestDiseasesVulnerabilities();
		validatePestDiseasesRiskScore();
		validateFloodHazards();
		validateFloodVulnerabilities();
		validateFloodRiskScore();
		validateWindstormHazards();
		validateWindstormVulnerabilities();
		validateWindstormRiskScore();
		validateDroughtHazards();
		validateDroughtVulnerabilities();
		validateDroughtRiskScore();
		validateHailstormHazards();
		validateHailstormVulnerabilities();
		validateHailstormRiskScore();
		validateSoilErosionHazards();
		validateSoilErosionVulnerabilities();
		validateSoilErosionRiskScore();
		validateEpidemicsHazards();
		validateEpidemicsVulnerabilities();
		validateEpidemicsRiskScore();
		validateDisasterDrill();
		validateDisasterDrillSpecify();
		validateDisasterPreparednessMeasures();
		validateDisasterPreparednessMeasuresSpecify();
		validateEarthquakeResistant();

    }


    public void validateSchool()
    {
    
        validateNonNullField(model.getSchool(), activity.getPage("Basic Information"), R.id.schoolContainer, context.getResources().getString(R.string.school_health_assessment_school));
    }

    public void validateContactPerson()
    {
    
        validateNonNullField(model.getContactPerson(), activity.getPage("Basic Information"), R.id.contactPersonContainer, context.getResources().getString(R.string.school_health_assessment_contactPerson));
    }

    public void validateContactPosition()
    {
    
        validateNonNullField(model.getContactPosition(), activity.getPage("Basic Information"), R.id.contactPositionContainer, context.getResources().getString(R.string.school_health_assessment_contactPosition));
    }

    public void validateActivitiesStudentHealthAssessment()
    {
    
        validateNonNullField(model.getActivitiesStudentHealthAssessment(), activity.getPage("Basic Information"), R.id.activitiesStudentHealthAssessmentContainer, context.getResources().getString(R.string.school_health_assessment_activitiesStudentHealthAssessment));
    }

    public void validateActivitiesStaffMembersHealthAssessment()
    {
    
        validateNonNullField(model.getActivitiesStaffMembersHealthAssessment(), activity.getPage("Basic Information"), R.id.activitiesStaffMembersHealthAssessmentContainer, context.getResources().getString(R.string.school_health_assessment_activitiesStaffMembersHealthAssessment));
    }

    public void validateActivitiesSanitationClass()
    {
    
        validateNonNullField(model.getActivitiesSanitationClass(), activity.getPage("Basic Information"), R.id.activitiesSanitationClassContainer, context.getResources().getString(R.string.school_health_assessment_activitiesSanitationClass));
    }

    public void validateActivitiesDietClass()
    {
    
        validateNonNullField(model.getActivitiesDietClass(), activity.getPage("Basic Information"), R.id.activitiesDietClassContainer, context.getResources().getString(R.string.school_health_assessment_activitiesDietClass));
    }

    public void validateDistanceOfSchoolKm()
    {
    
        validateNonNullField(model.getDistanceOfSchoolKm(), activity.getPage("Basic Information"), R.id.distanceOfSchoolKmContainer, context.getResources().getString(R.string.school_health_assessment_distanceOfSchoolKm));
    }

    public void validateEcdPresent()
    {
    
        validateNonNullField(model.getEcdPresent(), activity.getPage("Ecd"), R.id.ecdPresentContainer, context.getResources().getString(R.string.school_health_assessment_ecdPresent));
    }

    public void validateEcdTeacherPresent()
    {
    
        validateNonNullField(model.getEcdTeacherPresent(), activity.getPage("Ecd"), R.id.ecdTeacherPresentContainer, context.getResources().getString(R.string.school_health_assessment_ecdTeacherPresent));
    }

    public void validateClassroomFacilities()
    {
    
        validateNonNullField(model.getClassroomFacilities(), activity.getPage("Ecd"), R.id.classroomFacilitiesContainer, context.getResources().getString(R.string.school_health_assessment_classroomFacilities));
    }

    public void validateSeatingArrangements()
    {
    
        validateNonNullField(model.getSeatingArrangements(), activity.getPage("Ecd"), R.id.seatingArrangementsContainer, context.getResources().getString(R.string.school_health_assessment_seatingArrangements));
    }

    public void validateApprPlayMaterials()
    {
    
        validateNonNullField(model.getApprPlayMaterials(), activity.getPage("Ecd"), R.id.apprPlayMaterialsContainer, context.getResources().getString(R.string.school_health_assessment_apprPlayMaterials));
    }

    public void validateColorsIllustrations()
    {
    
        validateNonNullField(model.getColorsIllustrations(), activity.getPage("Ecd"), R.id.colorsIllustrationsContainer, context.getResources().getString(R.string.school_health_assessment_colorsIllustrations));
    }

    public void validateToiletHandwashing()
    {
    
        validateNonNullField(model.getToiletHandwashing(), activity.getPage("Ecd"), R.id.toiletHandwashingContainer, context.getResources().getString(R.string.school_health_assessment_toiletHandwashing));
    }

    public void validateCollisionAvoidance()
    {
    
        validateNonNullField(model.getCollisionAvoidance(), activity.getPage("Ecd"), R.id.collisionAvoidanceContainer, context.getResources().getString(R.string.school_health_assessment_collisionAvoidance));
    }

    public void validateNonslipFloors()
    {
    
        validateNonNullField(model.getNonslipFloors(), activity.getPage("Ecd"), R.id.nonslipFloorsContainer, context.getResources().getString(R.string.school_health_assessment_nonslipFloors));
    }

    public void validateLightingFacilities()
    {
    
        validateNonNullField(model.getLightingFacilities(), activity.getPage("Ecd"), R.id.lightingFacilitiesContainer, context.getResources().getString(R.string.school_health_assessment_lightingFacilities));
    }

    public void validateHasPlayground()
    {
    
        validateNonNullField(model.getHasPlayground(), activity.getPage("Extra Curricular Activities"), R.id.hasPlaygroundContainer, context.getResources().getString(R.string.school_health_assessment_hasPlayground));
    }

    public void validateSpaceAtEcaFacility()
    {
    
        validateNonNullField(model.getSpaceAtEcaFacility(), activity.getPage("Extra Curricular Activities"), R.id.spaceAtEcaFacilityContainer, context.getResources().getString(R.string.school_health_assessment_spaceAtEcaFacility));
    }

    public void validateEcaDancing()
    {
    
        validateNonNullField(model.getEcaDancing(), activity.getPage("Extra Curricular Activities"), R.id.ecaDancingContainer, "@drawable/eca_1");
    }

    public void validateEcaSinging()
    {
    
        validateNonNullField(model.getEcaSinging(), activity.getPage("Extra Curricular Activities"), R.id.ecaSingingContainer, "@drawable/eca_2");
    }

    public void validateEcaRunning()
    {
    
        validateNonNullField(model.getEcaRunning(), activity.getPage("Extra Curricular Activities"), R.id.ecaRunningContainer, "@drawable/eca_3");
    }

    public void validateEcaFootball()
    {
    
        validateNonNullField(model.getEcaFootball(), activity.getPage("Extra Curricular Activities"), R.id.ecaFootballContainer, "@drawable/eca_4");
    }

    public void validateEcaVolleyball()
    {
    
        validateNonNullField(model.getEcaVolleyball(), activity.getPage("Extra Curricular Activities"), R.id.ecaVolleyballContainer, "@drawable/eca_5");
    }

    public void validateEcaBasketball()
    {
    
        validateNonNullField(model.getEcaBasketball(), activity.getPage("Extra Curricular Activities"), R.id.ecaBasketballContainer, "@drawable/eca_6");
    }

    public void validateEcaPingpong()
    {
    
        validateNonNullField(model.getEcaPingpong(), activity.getPage("Extra Curricular Activities"), R.id.ecaPingpongContainer, "@drawable/eca_7");
    }

    public void validateEcaBadminton()
    {
    
        validateNonNullField(model.getEcaBadminton(), activity.getPage("Extra Curricular Activities"), R.id.ecaBadmintonContainer, "@drawable/eca_8");
    }

    public void validateEcaMartialarts()
    {
    
        validateNonNullField(model.getEcaMartialarts(), activity.getPage("Extra Curricular Activities"), R.id.ecaMartialartsContainer, "@drawable/eca_9");
    }

    public void validateEcaOthers()
    {
    
        validateNonNullField(model.getEcaOthers(), activity.getPage("Extra Curricular Activities"), R.id.ecaOthersContainer, context.getResources().getString(R.string.school_health_assessment_ecaOthers));
    }

    public void validateEcaSpecify()
    {
  
        validateNonNullSpecifyField(model.getEcaOthers(),1,model.getEcaSpecify(), activity.getPage("Extra Curricular Activities"), R.id.ecaSpecifyContainer, context.getResources().getString(R.string.school_health_assessment_ecaSpecify));   
    }

    public void validateFirstAidBoxPresent()
    {
    
        validateNonNullField(model.getFirstAidBoxPresent(), activity.getPage("First Aid"), R.id.firstAidBoxPresentContainer, context.getResources().getString(R.string.school_health_assessment_firstAidBoxPresent));
    }

    public void validateFirstAidBoxStocked()
    {
    
        validateNonNullField(model.getFirstAidBoxStocked(), activity.getPage("First Aid"), R.id.firstAidBoxStockedContainer, context.getResources().getString(R.string.school_health_assessment_firstAidBoxStocked));
    }

    public void validateNumStudentsCoveredByFirstAid()
    {
    
        validateNonNullField(model.getNumStudentsCoveredByFirstAid(), activity.getPage("First Aid"), R.id.numStudentsCoveredByFirstAidContainer, context.getResources().getString(R.string.school_health_assessment_numStudentsCoveredByFirstAid));
    }

    public void validateTypeWaterSupplied()
    {
    
        validateNonNullField(model.getTypeWaterSupplied(), activity.getPage("Drinking Water"), R.id.typeWaterSuppliedContainer, context.getResources().getString(R.string.school_health_assessment_typeWaterSupplied));
    }

    public void validateWaterOutletAvailability()
    {
    
        validateNonNullField(model.getWaterOutletAvailability(), activity.getPage("Drinking Water"), R.id.waterOutletAvailabilityContainer, context.getResources().getString(R.string.school_health_assessment_waterOutletAvailability));
    }

    public void validateSchoolProvidesFoodOpt()
    {
    
        validateNonNullField(model.getSchoolProvidesFoodOpt(), activity.getPage("Food and Nutrition"), R.id.schoolProvidesFoodOptContainer, context.getResources().getString(R.string.school_health_assessment_schoolProvidesFoodOpt));
    }

    public void validateSchoolProvidesFoodBreakfast()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getSchoolProvidesFoodBreakfast(), activity.getPage("Food and Nutrition"), R.id.schoolProvidesFoodBreakfastContainer, context.getResources().getString(R.string.school_health_assessment_schoolProvidesFoodBreakfast));
    }

    public void validateSchoolProvidesFoodLunch()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getSchoolProvidesFoodLunch(), activity.getPage("Food and Nutrition"), R.id.schoolProvidesFoodLunchContainer, context.getResources().getString(R.string.school_health_assessment_schoolProvidesFoodLunch));
    }

    public void validateSchoolProvidesFoodSnacks()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getSchoolProvidesFoodSnacks(), activity.getPage("Food and Nutrition"), R.id.schoolProvidesFoodSnacksContainer, context.getResources().getString(R.string.school_health_assessment_schoolProvidesFoodSnacks));
    }

    public void validateSchoolProvidesFoodDinner()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getSchoolProvidesFoodDinner(), activity.getPage("Food and Nutrition"), R.id.schoolProvidesFoodDinnerContainer, context.getResources().getString(R.string.school_health_assessment_schoolProvidesFoodDinner));
    }

    public void validateFoodHygienicSafe()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getFoodHygienicSafe(), activity.getPage("Food and Nutrition"), R.id.foodHygienicSafeContainer, context.getResources().getString(R.string.school_health_assessment_foodHygienicSafe));
    }

    public void validateMealBalanced()
    {
		if((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
		{
			// optional -- validation not required
			return;
		}    
        validateNonNullField(model.getMealBalanced(), activity.getPage("Food and Nutrition"), R.id.mealBalancedContainer, context.getResources().getString(R.string.school_health_assessment_mealBalanced));
    }

    public void validateFoodCanBeBroughtFromHome()
    {
    
        validateNonNullField(model.getFoodCanBeBroughtFromHome(), activity.getPage("Food and Nutrition"), R.id.foodCanBeBroughtFromHomeContainer, context.getResources().getString(R.string.school_health_assessment_foodCanBeBroughtFromHome));
    }

    public void validateJunkFoodAllowed()
    {
    
        validateNonNullField(model.getJunkFoodAllowed(), activity.getPage("Food and Nutrition"), R.id.junkFoodAllowedContainer, context.getResources().getString(R.string.school_health_assessment_junkFoodAllowed));
    }

    public void validateToilHasRunningWater()
    {
    
        validateNonNullField(model.getToilHasRunningWater(), activity.getPage("Toilet Facilities"), R.id.toilHasRunningWaterContainer, "@drawable/toilet_facilities_1");
    }

    public void validateToilHasCleaningWater()
    {
    
        validateNonNullField(model.getToilHasCleaningWater(), activity.getPage("Toilet Facilities"), R.id.toilHasCleaningWaterContainer, "@drawable/toilet_facilities_2");
    }

    public void validateToilHasSoap()
    {
    
        validateNonNullField(model.getToilHasSoap(), activity.getPage("Toilet Facilities"), R.id.toilHasSoapContainer, "@drawable/toilet_facilities_3");
    }

    public void validateToilHasSepGender()
    {
    
        validateNonNullField(model.getToilHasSepGender(), activity.getPage("Toilet Facilities"), R.id.toilHasSepGenderContainer, "@drawable/toilet_facilities_4");
    }

    public void validateToilHasSepStudentStaff()
    {
    
        validateNonNullField(model.getToilHasSepStudentStaff(), activity.getPage("Toilet Facilities"), R.id.toilHasSepStudentStaffContainer, "@drawable/toilet_facilities_6");
    }

    public void validateToilHasTrash()
    {
    
        validateNonNullField(model.getToilHasTrash(), activity.getPage("Toilet Facilities"), R.id.toilHasTrashContainer, "@drawable/toilet_facilities_5");
    }

    public void validateToilHasAdequateToilets()
    {
    
        validateNonNullField(model.getToilHasAdequateToilets(), activity.getPage("Toilet Facilities"), R.id.toilHasAdequateToiletsContainer, "@drawable/toilet_facilities_7");
    }

    public void validateToilHasToiletRating()
    {
    
        validateNonNullField(model.getToilHasToiletRating(), activity.getPage("Toilet Facilities"), R.id.toilHasToiletRatingContainer, context.getResources().getString(R.string.school_health_assessment_toilHasToiletRating));
    }

    public void validateSchoolAt()
    {
    
        validateNonNullField(model.getSchoolAt(), activity.getPage("Infrastructure"), R.id.schoolAtContainer, context.getResources().getString(R.string.school_health_assessment_schoolAt));
    }

    public void validateSchoolAtSpecify()
    {
  
        validateNonNullSpecifyField(model.getSchoolAt(),4,model.getSchoolAtSpecify(), activity.getPage("Infrastructure"), R.id.schoolAtSpecifyContainer, context.getResources().getString(R.string.school_health_assessment_schoolAtSpecify));   
    }

    public void validateBuildingType()
    {
    
        validateNonNullField(model.getBuildingType(), activity.getPage("Infrastructure"), R.id.buildingTypeContainer, context.getResources().getString(R.string.school_health_assessment_buildingType));
    }

    public void validateAgeOfBuilding()
    {
    
        validateNonNullField(model.getAgeOfBuilding(), activity.getPage("Infrastructure"), R.id.ageOfBuildingContainer, context.getResources().getString(R.string.school_health_assessment_ageOfBuilding));
    }

    public void validateBuildingHeight()
    {
    
        validateNonNullField(model.getBuildingHeight(), activity.getPage("Infrastructure"), R.id.buildingHeightContainer, context.getResources().getString(R.string.school_health_assessment_buildingHeight));
    }

    public void validateFirefightingInstrument()
    {
    
        validateNonNullField(model.getFirefightingInstrument(), activity.getPage("Infrastructure"), R.id.firefightingInstrumentContainer, context.getResources().getString(R.string.school_health_assessment_firefightingInstrument));
    }

    public void validateEarthquakeHazards()
    {
    
        validateNonNullField(model.getEarthquakeHazards(), activity.getPage("Hazard Risk Assessment"), R.id.earthquakeHazardsContainer, context.getResources().getString(R.string.school_health_assessment_earthquakeHazards));
    }

    public void validateEarthquakeVulnerabilities()
    {
    
        validateNonNullField(model.getEarthquakeVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.earthquakeVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_earthquakeVulnerabilities));
    }

    public void validateEarthquakeRiskScore()
    {
    
        validateNonNullField(model.getEarthquakeRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.earthquakeRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_earthquakeRiskScore));
    }

    public void validateLandslideHazards()
    {
    
        validateNonNullField(model.getLandslideHazards(), activity.getPage("Hazard Risk Assessment"), R.id.landslideHazardsContainer, context.getResources().getString(R.string.school_health_assessment_landslideHazards));
    }

    public void validateLandslideVulnerabilities()
    {
    
        validateNonNullField(model.getLandslideVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.landslideVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_landslideVulnerabilities));
    }

    public void validateLandslideRiskScore()
    {
    
        validateNonNullField(model.getLandslideRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.landslideRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_landslideRiskScore));
    }

    public void validateWildlifeAttackHazards()
    {
    
        validateNonNullField(model.getWildlifeAttackHazards(), activity.getPage("Hazard Risk Assessment"), R.id.wildlifeAttackHazardsContainer, context.getResources().getString(R.string.school_health_assessment_wildlifeAttackHazards));
    }

    public void validateWildlifeAttackVulnerabilities()
    {
    
        validateNonNullField(model.getWildlifeAttackVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.wildlifeAttackVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_wildlifeAttackVulnerabilities));
    }

    public void validateWildlifeAttackRiskScore()
    {
    
        validateNonNullField(model.getWildlifeAttackRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.wildlifeAttackRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_wildlifeAttackRiskScore));
    }

    public void validateFireHazards()
    {
    
        validateNonNullField(model.getFireHazards(), activity.getPage("Hazard Risk Assessment"), R.id.fireHazardsContainer, context.getResources().getString(R.string.school_health_assessment_fireHazards));
    }

    public void validateFireVulnerabilities()
    {
    
        validateNonNullField(model.getFireVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.fireVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_fireVulnerabilities));
    }

    public void validateFireRiskScore()
    {
    
        validateNonNullField(model.getFireRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.fireRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_fireRiskScore));
    }

    public void validatePestDiseasesHazards()
    {
    
        validateNonNullField(model.getPestDiseasesHazards(), activity.getPage("Hazard Risk Assessment"), R.id.pestDiseasesHazardsContainer, context.getResources().getString(R.string.school_health_assessment_pestDiseasesHazards));
    }

    public void validatePestDiseasesVulnerabilities()
    {
    
        validateNonNullField(model.getPestDiseasesVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.pestDiseasesVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_pestDiseasesVulnerabilities));
    }

    public void validatePestDiseasesRiskScore()
    {
    
        validateNonNullField(model.getPestDiseasesRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.pestDiseasesRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_pestDiseasesRiskScore));
    }

    public void validateFloodHazards()
    {
    
        validateNonNullField(model.getFloodHazards(), activity.getPage("Hazard Risk Assessment"), R.id.floodHazardsContainer, context.getResources().getString(R.string.school_health_assessment_floodHazards));
    }

    public void validateFloodVulnerabilities()
    {
    
        validateNonNullField(model.getFloodVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.floodVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_floodVulnerabilities));
    }

    public void validateFloodRiskScore()
    {
    
        validateNonNullField(model.getFloodRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.floodRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_floodRiskScore));
    }

    public void validateWindstormHazards()
    {
    
        validateNonNullField(model.getWindstormHazards(), activity.getPage("Hazard Risk Assessment"), R.id.windstormHazardsContainer, context.getResources().getString(R.string.school_health_assessment_windstormHazards));
    }

    public void validateWindstormVulnerabilities()
    {
    
        validateNonNullField(model.getWindstormVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.windstormVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_windstormVulnerabilities));
    }

    public void validateWindstormRiskScore()
    {
    
        validateNonNullField(model.getWindstormRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.windstormRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_windstormRiskScore));
    }

    public void validateDroughtHazards()
    {
    
        validateNonNullField(model.getDroughtHazards(), activity.getPage("Hazard Risk Assessment"), R.id.droughtHazardsContainer, context.getResources().getString(R.string.school_health_assessment_droughtHazards));
    }

    public void validateDroughtVulnerabilities()
    {
    
        validateNonNullField(model.getDroughtVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.droughtVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_droughtVulnerabilities));
    }

    public void validateDroughtRiskScore()
    {
    
        validateNonNullField(model.getDroughtRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.droughtRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_droughtRiskScore));
    }

    public void validateHailstormHazards()
    {
    
        validateNonNullField(model.getHailstormHazards(), activity.getPage("Hazard Risk Assessment"), R.id.hailstormHazardsContainer, context.getResources().getString(R.string.school_health_assessment_hailstormHazards));
    }

    public void validateHailstormVulnerabilities()
    {
    
        validateNonNullField(model.getHailstormVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.hailstormVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_hailstormVulnerabilities));
    }

    public void validateHailstormRiskScore()
    {
    
        validateNonNullField(model.getHailstormRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.hailstormRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_hailstormRiskScore));
    }

    public void validateSoilErosionHazards()
    {
    
        validateNonNullField(model.getSoilErosionHazards(), activity.getPage("Hazard Risk Assessment"), R.id.soilErosionHazardsContainer, context.getResources().getString(R.string.school_health_assessment_soilErosionHazards));
    }

    public void validateSoilErosionVulnerabilities()
    {
    
        validateNonNullField(model.getSoilErosionVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.soilErosionVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_soilErosionVulnerabilities));
    }

    public void validateSoilErosionRiskScore()
    {
    
        validateNonNullField(model.getSoilErosionRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.soilErosionRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_soilErosionRiskScore));
    }

    public void validateEpidemicsHazards()
    {
    
        validateNonNullField(model.getEpidemicsHazards(), activity.getPage("Hazard Risk Assessment"), R.id.epidemicsHazardsContainer, context.getResources().getString(R.string.school_health_assessment_epidemicsHazards));
    }

    public void validateEpidemicsVulnerabilities()
    {
    
        validateNonNullField(model.getEpidemicsVulnerabilities(), activity.getPage("Hazard Risk Assessment"), R.id.epidemicsVulnerabilitiesContainer, context.getResources().getString(R.string.school_health_assessment_epidemicsVulnerabilities));
    }

    public void validateEpidemicsRiskScore()
    {
    
        validateNonNullField(model.getEpidemicsRiskScore(), activity.getPage("Hazard Risk Assessment"), R.id.epidemicsRiskScoreContainer, context.getResources().getString(R.string.school_health_assessment_epidemicsRiskScore));
    }

    public void validateDisasterDrill()
    {
    
        validateNonNullField(model.getDisasterDrill(), activity.getPage("Hazard Risk Assessment"), R.id.disasterDrillContainer, context.getResources().getString(R.string.school_health_assessment_disasterDrill));
    }

    public void validateDisasterDrillSpecify()
    {
  
        validateNonNullSpecifyField(model.getDisasterDrill(),1,model.getDisasterDrillSpecify(), activity.getPage("Hazard Risk Assessment"), R.id.disasterDrillSpecifyContainer, context.getResources().getString(R.string.school_health_assessment_disasterDrillSpecify));   
    }

    public void validateDisasterPreparednessMeasures()
    {
    
        validateNonNullField(model.getDisasterPreparednessMeasures(), activity.getPage("Hazard Risk Assessment"), R.id.disasterPreparednessMeasuresContainer, context.getResources().getString(R.string.school_health_assessment_disasterPreparednessMeasures));
    }

    public void validateDisasterPreparednessMeasuresSpecify()
    {
  
        validateNonNullSpecifyField(model.getDisasterPreparednessMeasures(),1,model.getDisasterPreparednessMeasuresSpecify(), activity.getPage("Hazard Risk Assessment"), R.id.disasterPreparednessMeasuresSpecifyContainer, context.getResources().getString(R.string.school_health_assessment_disasterPreparednessMeasuresSpecify));   
    }

    public void validateEarthquakeResistant()
    {
    
        validateNonNullField(model.getEarthquakeResistant(), activity.getPage("Hazard Risk Assessment"), R.id.earthquakeResistantContainer, context.getResources().getString(R.string.school_health_assessment_earthquakeResistant));
    }


}
