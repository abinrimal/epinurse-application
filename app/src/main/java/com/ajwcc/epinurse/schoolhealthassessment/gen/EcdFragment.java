package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_ecd)
public class EcdFragment extends BaseEpinurseFragment {


    public EcdFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup ecdPresent;

	@ViewById
	@MapToModelField
	protected RadioGroup ecdTeacherPresent;

	@ViewById
	@MapToModelField
	protected RadioGroup classroomFacilities;

	@ViewById
	@MapToModelField
	protected RadioGroup seatingArrangements;

	@ViewById
	@MapToModelField
	protected RadioGroup apprPlayMaterials;

	@ViewById
	@MapToModelField
	protected RadioGroup colorsIllustrations;

	@ViewById
	@MapToModelField
	protected RadioGroup toiletHandwashing;

	@ViewById
	@MapToModelField
	protected RadioGroup collisionAvoidance;

	@ViewById
	@MapToModelField
	protected RadioGroup nonslipFloors;

	@ViewById
	@MapToModelField
	protected RadioGroup lightingFacilities;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(ecdPresent,pojo.getEcdPresent());
					PojoToViewMapper.setViewValue(ecdTeacherPresent,pojo.getEcdTeacherPresent());
					PojoToViewMapper.setViewValue(classroomFacilities,pojo.getClassroomFacilities());
					PojoToViewMapper.setViewValue(seatingArrangements,pojo.getSeatingArrangements());
					PojoToViewMapper.setViewValue(apprPlayMaterials,pojo.getApprPlayMaterials());
					PojoToViewMapper.setViewValue(colorsIllustrations,pojo.getColorsIllustrations());
					PojoToViewMapper.setViewValue(toiletHandwashing,pojo.getToiletHandwashing());
					PojoToViewMapper.setViewValue(collisionAvoidance,pojo.getCollisionAvoidance());
					PojoToViewMapper.setViewValue(nonslipFloors,pojo.getNonslipFloors());
					PojoToViewMapper.setViewValue(lightingFacilities,pojo.getLightingFacilities());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setEcdPresent((Integer) ViewToPojoMapper.getValueByView(ecdPresent));
					pojo.setEcdTeacherPresent((Integer) ViewToPojoMapper.getValueByView(ecdTeacherPresent));
					pojo.setClassroomFacilities((Integer) ViewToPojoMapper.getValueByView(classroomFacilities));
					pojo.setSeatingArrangements((Integer) ViewToPojoMapper.getValueByView(seatingArrangements));
					pojo.setApprPlayMaterials((Integer) ViewToPojoMapper.getValueByView(apprPlayMaterials));
					pojo.setColorsIllustrations((Integer) ViewToPojoMapper.getValueByView(colorsIllustrations));
					pojo.setToiletHandwashing((Integer) ViewToPojoMapper.getValueByView(toiletHandwashing));
					pojo.setCollisionAvoidance((Integer) ViewToPojoMapper.getValueByView(collisionAvoidance));
					pojo.setNonslipFloors((Integer) ViewToPojoMapper.getValueByView(nonslipFloors));
					pojo.setLightingFacilities((Integer) ViewToPojoMapper.getValueByView(lightingFacilities));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
