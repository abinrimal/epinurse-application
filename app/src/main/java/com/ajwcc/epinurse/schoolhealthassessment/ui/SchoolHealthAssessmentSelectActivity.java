package com.ajwcc.epinurse.schoolhealthassessment.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessment;

import org.androidannotations.annotations.EActivity;

import io.realm.RealmRecyclerViewAdapter;

@EActivity(R.layout.gen_activity_school_health_assessment_select)
public class SchoolHealthAssessmentSelectActivity extends com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessmentSelectActivity
{

    public RealmRecyclerViewAdapter createAdapter()
    {
        // make adapter
        SchoolHealthAssessmentSelectAdapter adapter = new SchoolHealthAssessmentSelectAdapter(this, getResults(), true);
        return adapter;
    }

}
