package com.ajwcc.epinurse.schoolhealthassessment.gen;

import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.epinurse.common.utils.network.ExcludeFromJson;

import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import com.zhuinden.realmautomigration.AutoMigration;

public class SchoolHealthAssessment extends RealmObject implements EpinurseModel
{
    @PrimaryKey
    private String uuid;
    
    public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public String getUuid()
	{
		return uuid;
	}

	@ExcludeFromJson
	private boolean editing;

	@Override
	public boolean isEditing() {
		return editing;
	}

	@Override
	public void setEditing(boolean editing) {
		this.editing = editing;
	}


	@ExcludeFromJson
	private String ownerUuid;

	@Override
	public String getOwnerUuid() {
		return ownerUuid;
	}

	@Override
	public void setOwnerUuid(String ownerUuid) {
		this.ownerUuid = ownerUuid;
	}


    
	@ExcludeFromJson
	private boolean synced;

	@Override
	public boolean isSynced() {
		return synced;
	}

	@Override
	public void setSynced(boolean synced) {
		this.synced = synced;
	}


	@ExcludeFromJson
	private Date createdAt;

	@Override
	public Date getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(Date date)
	{
		this.createdAt = date;
	}
    
	private String shineId;

	// Basic Information
	private String school;
	private String contactPerson;
	private String contactPosition;
	private Integer activitiesStudentHealthAssessment;
	private Integer activitiesStaffMembersHealthAssessment;
	private Integer activitiesSanitationClass;
	private Integer activitiesDietClass;
	private Double distanceOfSchoolKm;

	// Ecd
	private Integer ecdPresent;
	private Integer ecdTeacherPresent;
	private Integer classroomFacilities;
	private Integer seatingArrangements;
	private Integer apprPlayMaterials;
	private Integer colorsIllustrations;
	private Integer toiletHandwashing;
	private Integer collisionAvoidance;
	private Integer nonslipFloors;
	private Integer lightingFacilities;

	// Extra Curricular Activities
	private Integer hasPlayground;
	private Integer spaceAtEcaFacility;
	private Integer ecaDancing;
	private Integer ecaSinging;
	private Integer ecaRunning;
	private Integer ecaFootball;
	private Integer ecaVolleyball;
	private Integer ecaBasketball;
	private Integer ecaPingpong;
	private Integer ecaBadminton;
	private Integer ecaMartialarts;
	private Integer ecaOthers;
	private String ecaSpecify;

	// First Aid
	private Integer firstAidBoxPresent;
	private Integer firstAidBoxStocked;
	private Integer numStudentsCoveredByFirstAid;

	// Drinking Water
	private Integer typeWaterSupplied;
	private Integer waterOutletAvailability;

	// Food and Nutrition
	private Integer schoolProvidesFoodOpt;
	private Integer schoolProvidesFoodBreakfast;
	private Integer schoolProvidesFoodLunch;
	private Integer schoolProvidesFoodSnacks;
	private Integer schoolProvidesFoodDinner;
	private Integer foodHygienicSafe;
	private Integer mealBalanced;
	private Integer foodCanBeBroughtFromHome;
	private Integer junkFoodAllowed;

	// Toilet Facilities
	private Integer toilHasRunningWater;
	private Integer toilHasCleaningWater;
	private Integer toilHasSoap;
	private Integer toilHasSepGender;
	private Integer toilHasSepStudentStaff;
	private Integer toilHasTrash;
	private Integer toilHasAdequateToilets;
	private Integer toilHasToiletRating;

	// Infrastructure
	private Integer schoolAt;
	private String schoolAtSpecify;
	private Integer buildingType;
	private Integer ageOfBuilding;
	private Integer buildingHeight;
	private Integer waterDamage;
	private Integer firefightingInstrument;

	// Hazard Risk Assessment
	private Integer earthquakeHazards;
	private Integer earthquakeVulnerabilities;
	private Integer earthquakeRiskScore;
	private Integer landslideHazards;
	private Integer landslideVulnerabilities;
	private Integer landslideRiskScore;
	private Integer wildlifeAttackHazards;
	private Integer wildlifeAttackVulnerabilities;
	private Integer wildlifeAttackRiskScore;
	private Integer fireHazards;
	private Integer fireVulnerabilities;
	private Integer fireRiskScore;
	private Integer pestDiseasesHazards;
	private Integer pestDiseasesVulnerabilities;
	private Integer pestDiseasesRiskScore;
	private Integer floodHazards;
	private Integer floodVulnerabilities;
	private Integer floodRiskScore;
	private Integer windstormHazards;
	private Integer windstormVulnerabilities;
	private Integer windstormRiskScore;
	private Integer droughtHazards;
	private Integer droughtVulnerabilities;
	private Integer droughtRiskScore;
	private Integer hailstormHazards;
	private Integer hailstormVulnerabilities;
	private Integer hailstormRiskScore;
	private Integer soilErosionHazards;
	private Integer soilErosionVulnerabilities;
	private Integer soilErosionRiskScore;
	private Integer epidemicsHazards;
	private Integer epidemicsVulnerabilities;
	private Integer epidemicsRiskScore;
	private Integer disasterDrill;
	private String disasterDrillSpecify;
	private Integer disasterPreparednessMeasures;
	private String disasterPreparednessMeasuresSpecify;
	private Integer earthquakeResistant;
    
    

	public String getShineId() {
		return this.shineId;
	}

	public void setShineId(String shineId) {
	
		if (this.shineId==null || !this.shineId.equals(shineId))
		{
			editing = true;
			synced = false;
		}
	
		this.shineId = shineId;
	}

	public String getSchool() {
		return this.school;
	}

	public void setSchool(String school) {
	
		if (this.school==null || !this.school.equals(school))
		{
			editing = true;
			synced = false;
		}
	
		this.school = school;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
	
		if (this.contactPerson==null || !this.contactPerson.equals(contactPerson))
		{
			editing = true;
			synced = false;
		}
	
		this.contactPerson = contactPerson;
	}

	public String getContactPosition() {
		return this.contactPosition;
	}

	public void setContactPosition(String contactPosition) {
	
		if (this.contactPosition==null || !this.contactPosition.equals(contactPosition))
		{
			editing = true;
			synced = false;
		}
	
		this.contactPosition = contactPosition;
	}

	public Integer getActivitiesStudentHealthAssessment() {
		return this.activitiesStudentHealthAssessment;
	}

	public void setActivitiesStudentHealthAssessment(Integer activitiesStudentHealthAssessment) {
	
		if (this.activitiesStudentHealthAssessment==null || !this.activitiesStudentHealthAssessment.equals(activitiesStudentHealthAssessment))
		{
			editing = true;
			synced = false;
		}
	
		this.activitiesStudentHealthAssessment = activitiesStudentHealthAssessment;
	}

	public Integer getActivitiesStaffMembersHealthAssessment() {
		return this.activitiesStaffMembersHealthAssessment;
	}

	public void setActivitiesStaffMembersHealthAssessment(Integer activitiesStaffMembersHealthAssessment) {
	
		if (this.activitiesStaffMembersHealthAssessment==null || !this.activitiesStaffMembersHealthAssessment.equals(activitiesStaffMembersHealthAssessment))
		{
			editing = true;
			synced = false;
		}
	
		this.activitiesStaffMembersHealthAssessment = activitiesStaffMembersHealthAssessment;
	}

	public Integer getActivitiesSanitationClass() {
		return this.activitiesSanitationClass;
	}

	public void setActivitiesSanitationClass(Integer activitiesSanitationClass) {
	
		if (this.activitiesSanitationClass==null || !this.activitiesSanitationClass.equals(activitiesSanitationClass))
		{
			editing = true;
			synced = false;
		}
	
		this.activitiesSanitationClass = activitiesSanitationClass;
	}

	public Integer getActivitiesDietClass() {
		return this.activitiesDietClass;
	}

	public void setActivitiesDietClass(Integer activitiesDietClass) {
	
		if (this.activitiesDietClass==null || !this.activitiesDietClass.equals(activitiesDietClass))
		{
			editing = true;
			synced = false;
		}
	
		this.activitiesDietClass = activitiesDietClass;
	}

	public Double getDistanceOfSchoolKm() {
		return this.distanceOfSchoolKm;
	}

	public void setDistanceOfSchoolKm(Double distanceOfSchoolKm) {
	
		if (this.distanceOfSchoolKm==null || !this.distanceOfSchoolKm.equals(distanceOfSchoolKm))
		{
			editing = true;
			synced = false;
		}
	
		this.distanceOfSchoolKm = distanceOfSchoolKm;
	}

	public Integer getEcdPresent() {
		return this.ecdPresent;
	}

	public void setEcdPresent(Integer ecdPresent) {
	
		if (this.ecdPresent==null || !this.ecdPresent.equals(ecdPresent))
		{
			editing = true;
			synced = false;
		}
	
		this.ecdPresent = ecdPresent;
	}

	public Integer getEcdTeacherPresent() {
		return this.ecdTeacherPresent;
	}

	public void setEcdTeacherPresent(Integer ecdTeacherPresent) {
	
		if (this.ecdTeacherPresent==null || !this.ecdTeacherPresent.equals(ecdTeacherPresent))
		{
			editing = true;
			synced = false;
		}
	
		this.ecdTeacherPresent = ecdTeacherPresent;
	}

	public Integer getClassroomFacilities() {
		return this.classroomFacilities;
	}

	public void setClassroomFacilities(Integer classroomFacilities) {
	
		if (this.classroomFacilities==null || !this.classroomFacilities.equals(classroomFacilities))
		{
			editing = true;
			synced = false;
		}
	
		this.classroomFacilities = classroomFacilities;
	}

	public Integer getSeatingArrangements() {
		return this.seatingArrangements;
	}

	public void setSeatingArrangements(Integer seatingArrangements) {
	
		if (this.seatingArrangements==null || !this.seatingArrangements.equals(seatingArrangements))
		{
			editing = true;
			synced = false;
		}
	
		this.seatingArrangements = seatingArrangements;
	}

	public Integer getApprPlayMaterials() {
		return this.apprPlayMaterials;
	}

	public void setApprPlayMaterials(Integer apprPlayMaterials) {
	
		if (this.apprPlayMaterials==null || !this.apprPlayMaterials.equals(apprPlayMaterials))
		{
			editing = true;
			synced = false;
		}
	
		this.apprPlayMaterials = apprPlayMaterials;
	}

	public Integer getColorsIllustrations() {
		return this.colorsIllustrations;
	}

	public void setColorsIllustrations(Integer colorsIllustrations) {
	
		if (this.colorsIllustrations==null || !this.colorsIllustrations.equals(colorsIllustrations))
		{
			editing = true;
			synced = false;
		}
	
		this.colorsIllustrations = colorsIllustrations;
	}

	public Integer getToiletHandwashing() {
		return this.toiletHandwashing;
	}

	public void setToiletHandwashing(Integer toiletHandwashing) {
	
		if (this.toiletHandwashing==null || !this.toiletHandwashing.equals(toiletHandwashing))
		{
			editing = true;
			synced = false;
		}
	
		this.toiletHandwashing = toiletHandwashing;
	}

	public Integer getCollisionAvoidance() {
		return this.collisionAvoidance;
	}

	public void setCollisionAvoidance(Integer collisionAvoidance) {
	
		if (this.collisionAvoidance==null || !this.collisionAvoidance.equals(collisionAvoidance))
		{
			editing = true;
			synced = false;
		}
	
		this.collisionAvoidance = collisionAvoidance;
	}

	public Integer getNonslipFloors() {
		return this.nonslipFloors;
	}

	public void setNonslipFloors(Integer nonslipFloors) {
	
		if (this.nonslipFloors==null || !this.nonslipFloors.equals(nonslipFloors))
		{
			editing = true;
			synced = false;
		}
	
		this.nonslipFloors = nonslipFloors;
	}

	public Integer getLightingFacilities() {
		return this.lightingFacilities;
	}

	public void setLightingFacilities(Integer lightingFacilities) {
	
		if (this.lightingFacilities==null || !this.lightingFacilities.equals(lightingFacilities))
		{
			editing = true;
			synced = false;
		}
	
		this.lightingFacilities = lightingFacilities;
	}

	public Integer getHasPlayground() {
		return this.hasPlayground;
	}

	public void setHasPlayground(Integer hasPlayground) {
	
		if (this.hasPlayground==null || !this.hasPlayground.equals(hasPlayground))
		{
			editing = true;
			synced = false;
		}
	
		this.hasPlayground = hasPlayground;
	}

	public Integer getSpaceAtEcaFacility() {
		return this.spaceAtEcaFacility;
	}

	public void setSpaceAtEcaFacility(Integer spaceAtEcaFacility) {
	
		if (this.spaceAtEcaFacility==null || !this.spaceAtEcaFacility.equals(spaceAtEcaFacility))
		{
			editing = true;
			synced = false;
		}
	
		this.spaceAtEcaFacility = spaceAtEcaFacility;
	}

	public Integer getEcaDancing() {
		return this.ecaDancing;
	}

	public void setEcaDancing(Integer ecaDancing) {
	
		if (this.ecaDancing==null || !this.ecaDancing.equals(ecaDancing))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaDancing = ecaDancing;
	}

	public Integer getEcaSinging() {
		return this.ecaSinging;
	}

	public void setEcaSinging(Integer ecaSinging) {
	
		if (this.ecaSinging==null || !this.ecaSinging.equals(ecaSinging))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaSinging = ecaSinging;
	}

	public Integer getEcaRunning() {
		return this.ecaRunning;
	}

	public void setEcaRunning(Integer ecaRunning) {
	
		if (this.ecaRunning==null || !this.ecaRunning.equals(ecaRunning))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaRunning = ecaRunning;
	}

	public Integer getEcaFootball() {
		return this.ecaFootball;
	}

	public void setEcaFootball(Integer ecaFootball) {
	
		if (this.ecaFootball==null || !this.ecaFootball.equals(ecaFootball))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaFootball = ecaFootball;
	}

	public Integer getEcaVolleyball() {
		return this.ecaVolleyball;
	}

	public void setEcaVolleyball(Integer ecaVolleyball) {
	
		if (this.ecaVolleyball==null || !this.ecaVolleyball.equals(ecaVolleyball))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaVolleyball = ecaVolleyball;
	}

	public Integer getEcaBasketball() {
		return this.ecaBasketball;
	}

	public void setEcaBasketball(Integer ecaBasketball) {
	
		if (this.ecaBasketball==null || !this.ecaBasketball.equals(ecaBasketball))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaBasketball = ecaBasketball;
	}

	public Integer getEcaPingpong() {
		return this.ecaPingpong;
	}

	public void setEcaPingpong(Integer ecaPingpong) {
	
		if (this.ecaPingpong==null || !this.ecaPingpong.equals(ecaPingpong))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaPingpong = ecaPingpong;
	}

	public Integer getEcaBadminton() {
		return this.ecaBadminton;
	}

	public void setEcaBadminton(Integer ecaBadminton) {
	
		if (this.ecaBadminton==null || !this.ecaBadminton.equals(ecaBadminton))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaBadminton = ecaBadminton;
	}

	public Integer getEcaMartialarts() {
		return this.ecaMartialarts;
	}

	public void setEcaMartialarts(Integer ecaMartialarts) {
	
		if (this.ecaMartialarts==null || !this.ecaMartialarts.equals(ecaMartialarts))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaMartialarts = ecaMartialarts;
	}

	public Integer getEcaOthers() {
		return this.ecaOthers;
	}

	public void setEcaOthers(Integer ecaOthers) {
	
		if (this.ecaOthers==null || !this.ecaOthers.equals(ecaOthers))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaOthers = ecaOthers;
	}

	public String getEcaSpecify() {
		return this.ecaSpecify;
	}

	public void setEcaSpecify(String ecaSpecify) {
	
		if (this.ecaSpecify==null || !this.ecaSpecify.equals(ecaSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.ecaSpecify = ecaSpecify;
	}

	public Integer getFirstAidBoxPresent() {
		return this.firstAidBoxPresent;
	}

	public void setFirstAidBoxPresent(Integer firstAidBoxPresent) {
	
		if (this.firstAidBoxPresent==null || !this.firstAidBoxPresent.equals(firstAidBoxPresent))
		{
			editing = true;
			synced = false;
		}
	
		this.firstAidBoxPresent = firstAidBoxPresent;
	}

	public Integer getFirstAidBoxStocked() {
		return this.firstAidBoxStocked;
	}

	public void setFirstAidBoxStocked(Integer firstAidBoxStocked) {
	
		if (this.firstAidBoxStocked==null || !this.firstAidBoxStocked.equals(firstAidBoxStocked))
		{
			editing = true;
			synced = false;
		}
	
		this.firstAidBoxStocked = firstAidBoxStocked;
	}

	public Integer getNumStudentsCoveredByFirstAid() {
		return this.numStudentsCoveredByFirstAid;
	}

	public void setNumStudentsCoveredByFirstAid(Integer numStudentsCoveredByFirstAid) {
	
		if (this.numStudentsCoveredByFirstAid==null || !this.numStudentsCoveredByFirstAid.equals(numStudentsCoveredByFirstAid))
		{
			editing = true;
			synced = false;
		}
	
		this.numStudentsCoveredByFirstAid = numStudentsCoveredByFirstAid;
	}

	public Integer getTypeWaterSupplied() {
		return this.typeWaterSupplied;
	}

	public void setTypeWaterSupplied(Integer typeWaterSupplied) {
	
		if (this.typeWaterSupplied==null || !this.typeWaterSupplied.equals(typeWaterSupplied))
		{
			editing = true;
			synced = false;
		}
	
		this.typeWaterSupplied = typeWaterSupplied;
	}

	public Integer getWaterOutletAvailability() {
		return this.waterOutletAvailability;
	}

	public void setWaterOutletAvailability(Integer waterOutletAvailability) {
	
		if (this.waterOutletAvailability==null || !this.waterOutletAvailability.equals(waterOutletAvailability))
		{
			editing = true;
			synced = false;
		}
	
		this.waterOutletAvailability = waterOutletAvailability;
	}

	public Integer getSchoolProvidesFoodOpt() {
		return this.schoolProvidesFoodOpt;
	}

	public void setSchoolProvidesFoodOpt(Integer schoolProvidesFoodOpt) {
	
		if (this.schoolProvidesFoodOpt==null || !this.schoolProvidesFoodOpt.equals(schoolProvidesFoodOpt))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolProvidesFoodOpt = schoolProvidesFoodOpt;
	}

	public Integer getSchoolProvidesFoodBreakfast() {
		return this.schoolProvidesFoodBreakfast;
	}

	public void setSchoolProvidesFoodBreakfast(Integer schoolProvidesFoodBreakfast) {
	
		if (this.schoolProvidesFoodBreakfast==null || !this.schoolProvidesFoodBreakfast.equals(schoolProvidesFoodBreakfast))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolProvidesFoodBreakfast = schoolProvidesFoodBreakfast;
	}

	public Integer getSchoolProvidesFoodLunch() {
		return this.schoolProvidesFoodLunch;
	}

	public void setSchoolProvidesFoodLunch(Integer schoolProvidesFoodLunch) {
	
		if (this.schoolProvidesFoodLunch==null || !this.schoolProvidesFoodLunch.equals(schoolProvidesFoodLunch))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolProvidesFoodLunch = schoolProvidesFoodLunch;
	}

	public Integer getSchoolProvidesFoodSnacks() {
		return this.schoolProvidesFoodSnacks;
	}

	public void setSchoolProvidesFoodSnacks(Integer schoolProvidesFoodSnacks) {
	
		if (this.schoolProvidesFoodSnacks==null || !this.schoolProvidesFoodSnacks.equals(schoolProvidesFoodSnacks))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolProvidesFoodSnacks = schoolProvidesFoodSnacks;
	}

	public Integer getSchoolProvidesFoodDinner() {
		return this.schoolProvidesFoodDinner;
	}

	public void setSchoolProvidesFoodDinner(Integer schoolProvidesFoodDinner) {
	
		if (this.schoolProvidesFoodDinner==null || !this.schoolProvidesFoodDinner.equals(schoolProvidesFoodDinner))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolProvidesFoodDinner = schoolProvidesFoodDinner;
	}

	public Integer getFoodHygienicSafe() {
		return this.foodHygienicSafe;
	}

	public void setFoodHygienicSafe(Integer foodHygienicSafe) {
	
		if (this.foodHygienicSafe==null || !this.foodHygienicSafe.equals(foodHygienicSafe))
		{
			editing = true;
			synced = false;
		}
	
		this.foodHygienicSafe = foodHygienicSafe;
	}

	public Integer getMealBalanced() {
		return this.mealBalanced;
	}

	public void setMealBalanced(Integer mealBalanced) {
	
		if (this.mealBalanced==null || !this.mealBalanced.equals(mealBalanced))
		{
			editing = true;
			synced = false;
		}
	
		this.mealBalanced = mealBalanced;
	}

	public Integer getFoodCanBeBroughtFromHome() {
		return this.foodCanBeBroughtFromHome;
	}

	public void setFoodCanBeBroughtFromHome(Integer foodCanBeBroughtFromHome) {
	
		if (this.foodCanBeBroughtFromHome==null || !this.foodCanBeBroughtFromHome.equals(foodCanBeBroughtFromHome))
		{
			editing = true;
			synced = false;
		}
	
		this.foodCanBeBroughtFromHome = foodCanBeBroughtFromHome;
	}

	public Integer getJunkFoodAllowed() {
		return this.junkFoodAllowed;
	}

	public void setJunkFoodAllowed(Integer junkFoodAllowed) {
	
		if (this.junkFoodAllowed==null || !this.junkFoodAllowed.equals(junkFoodAllowed))
		{
			editing = true;
			synced = false;
		}
	
		this.junkFoodAllowed = junkFoodAllowed;
	}

	public Integer getToilHasRunningWater() {
		return this.toilHasRunningWater;
	}

	public void setToilHasRunningWater(Integer toilHasRunningWater) {
	
		if (this.toilHasRunningWater==null || !this.toilHasRunningWater.equals(toilHasRunningWater))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasRunningWater = toilHasRunningWater;
	}

	public Integer getToilHasCleaningWater() {
		return this.toilHasCleaningWater;
	}

	public void setToilHasCleaningWater(Integer toilHasCleaningWater) {
	
		if (this.toilHasCleaningWater==null || !this.toilHasCleaningWater.equals(toilHasCleaningWater))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasCleaningWater = toilHasCleaningWater;
	}

	public Integer getToilHasSoap() {
		return this.toilHasSoap;
	}

	public void setToilHasSoap(Integer toilHasSoap) {
	
		if (this.toilHasSoap==null || !this.toilHasSoap.equals(toilHasSoap))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasSoap = toilHasSoap;
	}

	public Integer getToilHasSepGender() {
		return this.toilHasSepGender;
	}

	public void setToilHasSepGender(Integer toilHasSepGender) {
	
		if (this.toilHasSepGender==null || !this.toilHasSepGender.equals(toilHasSepGender))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasSepGender = toilHasSepGender;
	}

	public Integer getToilHasSepStudentStaff() {
		return this.toilHasSepStudentStaff;
	}

	public void setToilHasSepStudentStaff(Integer toilHasSepStudentStaff) {
	
		if (this.toilHasSepStudentStaff==null || !this.toilHasSepStudentStaff.equals(toilHasSepStudentStaff))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasSepStudentStaff = toilHasSepStudentStaff;
	}

	public Integer getToilHasTrash() {
		return this.toilHasTrash;
	}

	public void setToilHasTrash(Integer toilHasTrash) {
	
		if (this.toilHasTrash==null || !this.toilHasTrash.equals(toilHasTrash))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasTrash = toilHasTrash;
	}

	public Integer getToilHasAdequateToilets() {
		return this.toilHasAdequateToilets;
	}

	public void setToilHasAdequateToilets(Integer toilHasAdequateToilets) {
	
		if (this.toilHasAdequateToilets==null || !this.toilHasAdequateToilets.equals(toilHasAdequateToilets))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasAdequateToilets = toilHasAdequateToilets;
	}

	public Integer getToilHasToiletRating() {
		return this.toilHasToiletRating;
	}

	public void setToilHasToiletRating(Integer toilHasToiletRating) {
	
		if (this.toilHasToiletRating==null || !this.toilHasToiletRating.equals(toilHasToiletRating))
		{
			editing = true;
			synced = false;
		}
	
		this.toilHasToiletRating = toilHasToiletRating;
	}

	public Integer getSchoolAt() {
		return this.schoolAt;
	}

	public void setSchoolAt(Integer schoolAt) {
	
		if (this.schoolAt==null || !this.schoolAt.equals(schoolAt))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolAt = schoolAt;
	}

	public String getSchoolAtSpecify() {
		return this.schoolAtSpecify;
	}

	public void setSchoolAtSpecify(String schoolAtSpecify) {
	
		if (this.schoolAtSpecify==null || !this.schoolAtSpecify.equals(schoolAtSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.schoolAtSpecify = schoolAtSpecify;
	}

	public Integer getBuildingType() {
		return this.buildingType;
	}

	public void setBuildingType(Integer buildingType) {
	
		if (this.buildingType==null || !this.buildingType.equals(buildingType))
		{
			editing = true;
			synced = false;
		}
	
		this.buildingType = buildingType;
	}

	public Integer getAgeOfBuilding() {
		return this.ageOfBuilding;
	}

	public void setAgeOfBuilding(Integer ageOfBuilding) {
	
		if (this.ageOfBuilding==null || !this.ageOfBuilding.equals(ageOfBuilding))
		{
			editing = true;
			synced = false;
		}
	
		this.ageOfBuilding = ageOfBuilding;
	}

	public Integer getBuildingHeight() {
		return this.buildingHeight;
	}

	public void setBuildingHeight(Integer buildingHeight) {
	
		if (this.buildingHeight==null || !this.buildingHeight.equals(buildingHeight))
		{
			editing = true;
			synced = false;
		}
	
		this.buildingHeight = buildingHeight;
	}

	public Integer getWaterDamage() {
		return this.waterDamage;
	}

	public void setWaterDamage(Integer waterDamage) {
	
		if (this.waterDamage==null || !this.waterDamage.equals(waterDamage))
		{
			editing = true;
			synced = false;
		}
	
		this.waterDamage = waterDamage;
	}

	public Integer getFirefightingInstrument() {
		return this.firefightingInstrument;
	}

	public void setFirefightingInstrument(Integer firefightingInstrument) {
	
		if (this.firefightingInstrument==null || !this.firefightingInstrument.equals(firefightingInstrument))
		{
			editing = true;
			synced = false;
		}
	
		this.firefightingInstrument = firefightingInstrument;
	}

	public Integer getEarthquakeHazards() {
		return this.earthquakeHazards;
	}

	public void setEarthquakeHazards(Integer earthquakeHazards) {
	
		if (this.earthquakeHazards==null || !this.earthquakeHazards.equals(earthquakeHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.earthquakeHazards = earthquakeHazards;
	}

	public Integer getEarthquakeVulnerabilities() {
		return this.earthquakeVulnerabilities;
	}

	public void setEarthquakeVulnerabilities(Integer earthquakeVulnerabilities) {
	
		if (this.earthquakeVulnerabilities==null || !this.earthquakeVulnerabilities.equals(earthquakeVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.earthquakeVulnerabilities = earthquakeVulnerabilities;
	}

	public Integer getEarthquakeRiskScore() {
		return this.earthquakeRiskScore;
	}

	public void setEarthquakeRiskScore(Integer earthquakeRiskScore) {
	
		if (this.earthquakeRiskScore==null || !this.earthquakeRiskScore.equals(earthquakeRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.earthquakeRiskScore = earthquakeRiskScore;
	}

	public Integer getLandslideHazards() {
		return this.landslideHazards;
	}

	public void setLandslideHazards(Integer landslideHazards) {
	
		if (this.landslideHazards==null || !this.landslideHazards.equals(landslideHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.landslideHazards = landslideHazards;
	}

	public Integer getLandslideVulnerabilities() {
		return this.landslideVulnerabilities;
	}

	public void setLandslideVulnerabilities(Integer landslideVulnerabilities) {
	
		if (this.landslideVulnerabilities==null || !this.landslideVulnerabilities.equals(landslideVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.landslideVulnerabilities = landslideVulnerabilities;
	}

	public Integer getLandslideRiskScore() {
		return this.landslideRiskScore;
	}

	public void setLandslideRiskScore(Integer landslideRiskScore) {
	
		if (this.landslideRiskScore==null || !this.landslideRiskScore.equals(landslideRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.landslideRiskScore = landslideRiskScore;
	}

	public Integer getWildlifeAttackHazards() {
		return this.wildlifeAttackHazards;
	}

	public void setWildlifeAttackHazards(Integer wildlifeAttackHazards) {
	
		if (this.wildlifeAttackHazards==null || !this.wildlifeAttackHazards.equals(wildlifeAttackHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.wildlifeAttackHazards = wildlifeAttackHazards;
	}

	public Integer getWildlifeAttackVulnerabilities() {
		return this.wildlifeAttackVulnerabilities;
	}

	public void setWildlifeAttackVulnerabilities(Integer wildlifeAttackVulnerabilities) {
	
		if (this.wildlifeAttackVulnerabilities==null || !this.wildlifeAttackVulnerabilities.equals(wildlifeAttackVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.wildlifeAttackVulnerabilities = wildlifeAttackVulnerabilities;
	}

	public Integer getWildlifeAttackRiskScore() {
		return this.wildlifeAttackRiskScore;
	}

	public void setWildlifeAttackRiskScore(Integer wildlifeAttackRiskScore) {
	
		if (this.wildlifeAttackRiskScore==null || !this.wildlifeAttackRiskScore.equals(wildlifeAttackRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.wildlifeAttackRiskScore = wildlifeAttackRiskScore;
	}

	public Integer getFireHazards() {
		return this.fireHazards;
	}

	public void setFireHazards(Integer fireHazards) {
	
		if (this.fireHazards==null || !this.fireHazards.equals(fireHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.fireHazards = fireHazards;
	}

	public Integer getFireVulnerabilities() {
		return this.fireVulnerabilities;
	}

	public void setFireVulnerabilities(Integer fireVulnerabilities) {
	
		if (this.fireVulnerabilities==null || !this.fireVulnerabilities.equals(fireVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.fireVulnerabilities = fireVulnerabilities;
	}

	public Integer getFireRiskScore() {
		return this.fireRiskScore;
	}

	public void setFireRiskScore(Integer fireRiskScore) {
	
		if (this.fireRiskScore==null || !this.fireRiskScore.equals(fireRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.fireRiskScore = fireRiskScore;
	}

	public Integer getPestDiseasesHazards() {
		return this.pestDiseasesHazards;
	}

	public void setPestDiseasesHazards(Integer pestDiseasesHazards) {
	
		if (this.pestDiseasesHazards==null || !this.pestDiseasesHazards.equals(pestDiseasesHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.pestDiseasesHazards = pestDiseasesHazards;
	}

	public Integer getPestDiseasesVulnerabilities() {
		return this.pestDiseasesVulnerabilities;
	}

	public void setPestDiseasesVulnerabilities(Integer pestDiseasesVulnerabilities) {
	
		if (this.pestDiseasesVulnerabilities==null || !this.pestDiseasesVulnerabilities.equals(pestDiseasesVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.pestDiseasesVulnerabilities = pestDiseasesVulnerabilities;
	}

	public Integer getPestDiseasesRiskScore() {
		return this.pestDiseasesRiskScore;
	}

	public void setPestDiseasesRiskScore(Integer pestDiseasesRiskScore) {
	
		if (this.pestDiseasesRiskScore==null || !this.pestDiseasesRiskScore.equals(pestDiseasesRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.pestDiseasesRiskScore = pestDiseasesRiskScore;
	}

	public Integer getFloodHazards() {
		return this.floodHazards;
	}

	public void setFloodHazards(Integer floodHazards) {
	
		if (this.floodHazards==null || !this.floodHazards.equals(floodHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.floodHazards = floodHazards;
	}

	public Integer getFloodVulnerabilities() {
		return this.floodVulnerabilities;
	}

	public void setFloodVulnerabilities(Integer floodVulnerabilities) {
	
		if (this.floodVulnerabilities==null || !this.floodVulnerabilities.equals(floodVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.floodVulnerabilities = floodVulnerabilities;
	}

	public Integer getFloodRiskScore() {
		return this.floodRiskScore;
	}

	public void setFloodRiskScore(Integer floodRiskScore) {
	
		if (this.floodRiskScore==null || !this.floodRiskScore.equals(floodRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.floodRiskScore = floodRiskScore;
	}

	public Integer getWindstormHazards() {
		return this.windstormHazards;
	}

	public void setWindstormHazards(Integer windstormHazards) {
	
		if (this.windstormHazards==null || !this.windstormHazards.equals(windstormHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.windstormHazards = windstormHazards;
	}

	public Integer getWindstormVulnerabilities() {
		return this.windstormVulnerabilities;
	}

	public void setWindstormVulnerabilities(Integer windstormVulnerabilities) {
	
		if (this.windstormVulnerabilities==null || !this.windstormVulnerabilities.equals(windstormVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.windstormVulnerabilities = windstormVulnerabilities;
	}

	public Integer getWindstormRiskScore() {
		return this.windstormRiskScore;
	}

	public void setWindstormRiskScore(Integer windstormRiskScore) {
	
		if (this.windstormRiskScore==null || !this.windstormRiskScore.equals(windstormRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.windstormRiskScore = windstormRiskScore;
	}

	public Integer getDroughtHazards() {
		return this.droughtHazards;
	}

	public void setDroughtHazards(Integer droughtHazards) {
	
		if (this.droughtHazards==null || !this.droughtHazards.equals(droughtHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.droughtHazards = droughtHazards;
	}

	public Integer getDroughtVulnerabilities() {
		return this.droughtVulnerabilities;
	}

	public void setDroughtVulnerabilities(Integer droughtVulnerabilities) {
	
		if (this.droughtVulnerabilities==null || !this.droughtVulnerabilities.equals(droughtVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.droughtVulnerabilities = droughtVulnerabilities;
	}

	public Integer getDroughtRiskScore() {
		return this.droughtRiskScore;
	}

	public void setDroughtRiskScore(Integer droughtRiskScore) {
	
		if (this.droughtRiskScore==null || !this.droughtRiskScore.equals(droughtRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.droughtRiskScore = droughtRiskScore;
	}

	public Integer getHailstormHazards() {
		return this.hailstormHazards;
	}

	public void setHailstormHazards(Integer hailstormHazards) {
	
		if (this.hailstormHazards==null || !this.hailstormHazards.equals(hailstormHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.hailstormHazards = hailstormHazards;
	}

	public Integer getHailstormVulnerabilities() {
		return this.hailstormVulnerabilities;
	}

	public void setHailstormVulnerabilities(Integer hailstormVulnerabilities) {
	
		if (this.hailstormVulnerabilities==null || !this.hailstormVulnerabilities.equals(hailstormVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.hailstormVulnerabilities = hailstormVulnerabilities;
	}

	public Integer getHailstormRiskScore() {
		return this.hailstormRiskScore;
	}

	public void setHailstormRiskScore(Integer hailstormRiskScore) {
	
		if (this.hailstormRiskScore==null || !this.hailstormRiskScore.equals(hailstormRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.hailstormRiskScore = hailstormRiskScore;
	}

	public Integer getSoilErosionHazards() {
		return this.soilErosionHazards;
	}

	public void setSoilErosionHazards(Integer soilErosionHazards) {
	
		if (this.soilErosionHazards==null || !this.soilErosionHazards.equals(soilErosionHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.soilErosionHazards = soilErosionHazards;
	}

	public Integer getSoilErosionVulnerabilities() {
		return this.soilErosionVulnerabilities;
	}

	public void setSoilErosionVulnerabilities(Integer soilErosionVulnerabilities) {
	
		if (this.soilErosionVulnerabilities==null || !this.soilErosionVulnerabilities.equals(soilErosionVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.soilErosionVulnerabilities = soilErosionVulnerabilities;
	}

	public Integer getSoilErosionRiskScore() {
		return this.soilErosionRiskScore;
	}

	public void setSoilErosionRiskScore(Integer soilErosionRiskScore) {
	
		if (this.soilErosionRiskScore==null || !this.soilErosionRiskScore.equals(soilErosionRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.soilErosionRiskScore = soilErosionRiskScore;
	}

	public Integer getEpidemicsHazards() {
		return this.epidemicsHazards;
	}

	public void setEpidemicsHazards(Integer epidemicsHazards) {
	
		if (this.epidemicsHazards==null || !this.epidemicsHazards.equals(epidemicsHazards))
		{
			editing = true;
			synced = false;
		}
	
		this.epidemicsHazards = epidemicsHazards;
	}

	public Integer getEpidemicsVulnerabilities() {
		return this.epidemicsVulnerabilities;
	}

	public void setEpidemicsVulnerabilities(Integer epidemicsVulnerabilities) {
	
		if (this.epidemicsVulnerabilities==null || !this.epidemicsVulnerabilities.equals(epidemicsVulnerabilities))
		{
			editing = true;
			synced = false;
		}
	
		this.epidemicsVulnerabilities = epidemicsVulnerabilities;
	}

	public Integer getEpidemicsRiskScore() {
		return this.epidemicsRiskScore;
	}

	public void setEpidemicsRiskScore(Integer epidemicsRiskScore) {
	
		if (this.epidemicsRiskScore==null || !this.epidemicsRiskScore.equals(epidemicsRiskScore))
		{
			editing = true;
			synced = false;
		}
	
		this.epidemicsRiskScore = epidemicsRiskScore;
	}

	public Integer getDisasterDrill() {
		return this.disasterDrill;
	}

	public void setDisasterDrill(Integer disasterDrill) {
	
		if (this.disasterDrill==null || !this.disasterDrill.equals(disasterDrill))
		{
			editing = true;
			synced = false;
		}
	
		this.disasterDrill = disasterDrill;
	}

	public String getDisasterDrillSpecify() {
		return this.disasterDrillSpecify;
	}

	public void setDisasterDrillSpecify(String disasterDrillSpecify) {
	
		if (this.disasterDrillSpecify==null || !this.disasterDrillSpecify.equals(disasterDrillSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.disasterDrillSpecify = disasterDrillSpecify;
	}

	public Integer getDisasterPreparednessMeasures() {
		return this.disasterPreparednessMeasures;
	}

	public void setDisasterPreparednessMeasures(Integer disasterPreparednessMeasures) {
	
		if (this.disasterPreparednessMeasures==null || !this.disasterPreparednessMeasures.equals(disasterPreparednessMeasures))
		{
			editing = true;
			synced = false;
		}
	
		this.disasterPreparednessMeasures = disasterPreparednessMeasures;
	}

	public String getDisasterPreparednessMeasuresSpecify() {
		return this.disasterPreparednessMeasuresSpecify;
	}

	public void setDisasterPreparednessMeasuresSpecify(String disasterPreparednessMeasuresSpecify) {
	
		if (this.disasterPreparednessMeasuresSpecify==null || !this.disasterPreparednessMeasuresSpecify.equals(disasterPreparednessMeasuresSpecify))
		{
			editing = true;
			synced = false;
		}
	
		this.disasterPreparednessMeasuresSpecify = disasterPreparednessMeasuresSpecify;
	}

	public Integer getEarthquakeResistant() {
		return this.earthquakeResistant;
	}

	public void setEarthquakeResistant(Integer earthquakeResistant) {
	
		if (this.earthquakeResistant==null || !this.earthquakeResistant.equals(earthquakeResistant))
		{
			editing = true;
			synced = false;
		}
	
		this.earthquakeResistant = earthquakeResistant;
	}

    
    
    @Override
	public String toString() {
		return "SchoolHealthAssessment{" +
				"uuid='" + uuid + '\'' +
				", editing=" + editing +
				", synced=" + synced +
				", createdAt=" + createdAt +
				", shineId='" + shineId + '\'' +
				", school='" + school + '\'' +
				", contactPerson='" + contactPerson + '\'' +
				", contactPosition='" + contactPosition + '\'' +
				", activitiesStudentHealthAssessment='" + activitiesStudentHealthAssessment + '\'' +
				", activitiesStaffMembersHealthAssessment='" + activitiesStaffMembersHealthAssessment + '\'' +
				", activitiesSanitationClass='" + activitiesSanitationClass + '\'' +
				", activitiesDietClass='" + activitiesDietClass + '\'' +
				", distanceOfSchoolKm='" + distanceOfSchoolKm + '\'' +
				", ecdPresent='" + ecdPresent + '\'' +
				", ecdTeacherPresent='" + ecdTeacherPresent + '\'' +
				", classroomFacilities='" + classroomFacilities + '\'' +
				", seatingArrangements='" + seatingArrangements + '\'' +
				", apprPlayMaterials='" + apprPlayMaterials + '\'' +
				", colorsIllustrations='" + colorsIllustrations + '\'' +
				", toiletHandwashing='" + toiletHandwashing + '\'' +
				", collisionAvoidance='" + collisionAvoidance + '\'' +
				", nonslipFloors='" + nonslipFloors + '\'' +
				", lightingFacilities='" + lightingFacilities + '\'' +
				", hasPlayground='" + hasPlayground + '\'' +
				", spaceAtEcaFacility='" + spaceAtEcaFacility + '\'' +
				", ecaDancing='" + ecaDancing + '\'' +
				", ecaSinging='" + ecaSinging + '\'' +
				", ecaRunning='" + ecaRunning + '\'' +
				", ecaFootball='" + ecaFootball + '\'' +
				", ecaVolleyball='" + ecaVolleyball + '\'' +
				", ecaBasketball='" + ecaBasketball + '\'' +
				", ecaPingpong='" + ecaPingpong + '\'' +
				", ecaBadminton='" + ecaBadminton + '\'' +
				", ecaMartialarts='" + ecaMartialarts + '\'' +
				", ecaOthers='" + ecaOthers + '\'' +
				", ecaSpecify='" + ecaSpecify + '\'' +
				", firstAidBoxPresent='" + firstAidBoxPresent + '\'' +
				", firstAidBoxStocked='" + firstAidBoxStocked + '\'' +
				", numStudentsCoveredByFirstAid='" + numStudentsCoveredByFirstAid + '\'' +
				", typeWaterSupplied='" + typeWaterSupplied + '\'' +
				", waterOutletAvailability='" + waterOutletAvailability + '\'' +
				", schoolProvidesFoodOpt='" + schoolProvidesFoodOpt + '\'' +
				", schoolProvidesFoodBreakfast='" + schoolProvidesFoodBreakfast + '\'' +
				", schoolProvidesFoodLunch='" + schoolProvidesFoodLunch + '\'' +
				", schoolProvidesFoodSnacks='" + schoolProvidesFoodSnacks + '\'' +
				", schoolProvidesFoodDinner='" + schoolProvidesFoodDinner + '\'' +
				", foodHygienicSafe='" + foodHygienicSafe + '\'' +
				", mealBalanced='" + mealBalanced + '\'' +
				", foodCanBeBroughtFromHome='" + foodCanBeBroughtFromHome + '\'' +
				", junkFoodAllowed='" + junkFoodAllowed + '\'' +
				", toilHasRunningWater='" + toilHasRunningWater + '\'' +
				", toilHasCleaningWater='" + toilHasCleaningWater + '\'' +
				", toilHasSoap='" + toilHasSoap + '\'' +
				", toilHasSepGender='" + toilHasSepGender + '\'' +
				", toilHasSepStudentStaff='" + toilHasSepStudentStaff + '\'' +
				", toilHasTrash='" + toilHasTrash + '\'' +
				", toilHasAdequateToilets='" + toilHasAdequateToilets + '\'' +
				", toilHasToiletRating='" + toilHasToiletRating + '\'' +
				", schoolAt='" + schoolAt + '\'' +
				", schoolAtSpecify='" + schoolAtSpecify + '\'' +
				", buildingType='" + buildingType + '\'' +
				", ageOfBuilding='" + ageOfBuilding + '\'' +
				", buildingHeight='" + buildingHeight + '\'' +
				", waterDamage='" + waterDamage + '\'' +
				", firefightingInstrument='" + firefightingInstrument + '\'' +
				", earthquakeHazards='" + earthquakeHazards + '\'' +
				", earthquakeVulnerabilities='" + earthquakeVulnerabilities + '\'' +
				", earthquakeRiskScore='" + earthquakeRiskScore + '\'' +
				", landslideHazards='" + landslideHazards + '\'' +
				", landslideVulnerabilities='" + landslideVulnerabilities + '\'' +
				", landslideRiskScore='" + landslideRiskScore + '\'' +
				", wildlifeAttackHazards='" + wildlifeAttackHazards + '\'' +
				", wildlifeAttackVulnerabilities='" + wildlifeAttackVulnerabilities + '\'' +
				", wildlifeAttackRiskScore='" + wildlifeAttackRiskScore + '\'' +
				", fireHazards='" + fireHazards + '\'' +
				", fireVulnerabilities='" + fireVulnerabilities + '\'' +
				", fireRiskScore='" + fireRiskScore + '\'' +
				", pestDiseasesHazards='" + pestDiseasesHazards + '\'' +
				", pestDiseasesVulnerabilities='" + pestDiseasesVulnerabilities + '\'' +
				", pestDiseasesRiskScore='" + pestDiseasesRiskScore + '\'' +
				", floodHazards='" + floodHazards + '\'' +
				", floodVulnerabilities='" + floodVulnerabilities + '\'' +
				", floodRiskScore='" + floodRiskScore + '\'' +
				", windstormHazards='" + windstormHazards + '\'' +
				", windstormVulnerabilities='" + windstormVulnerabilities + '\'' +
				", windstormRiskScore='" + windstormRiskScore + '\'' +
				", droughtHazards='" + droughtHazards + '\'' +
				", droughtVulnerabilities='" + droughtVulnerabilities + '\'' +
				", droughtRiskScore='" + droughtRiskScore + '\'' +
				", hailstormHazards='" + hailstormHazards + '\'' +
				", hailstormVulnerabilities='" + hailstormVulnerabilities + '\'' +
				", hailstormRiskScore='" + hailstormRiskScore + '\'' +
				", soilErosionHazards='" + soilErosionHazards + '\'' +
				", soilErosionVulnerabilities='" + soilErosionVulnerabilities + '\'' +
				", soilErosionRiskScore='" + soilErosionRiskScore + '\'' +
				", epidemicsHazards='" + epidemicsHazards + '\'' +
				", epidemicsVulnerabilities='" + epidemicsVulnerabilities + '\'' +
				", epidemicsRiskScore='" + epidemicsRiskScore + '\'' +
				", disasterDrill='" + disasterDrill + '\'' +
				", disasterDrillSpecify='" + disasterDrillSpecify + '\'' +
				", disasterPreparednessMeasures='" + disasterPreparednessMeasures + '\'' +
				", disasterPreparednessMeasuresSpecify='" + disasterPreparednessMeasuresSpecify + '\'' +
				", earthquakeResistant='" + earthquakeResistant + '\'' +

				'}';
	}
}
