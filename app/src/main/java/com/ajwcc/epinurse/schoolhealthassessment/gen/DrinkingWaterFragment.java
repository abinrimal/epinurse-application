package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_drinking_water)
public class DrinkingWaterFragment extends BaseEpinurseFragment {


    public DrinkingWaterFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup typeWaterSupplied;

	@ViewById
	@MapToModelField
	protected RadioGroup waterOutletAvailability;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(typeWaterSupplied,pojo.getTypeWaterSupplied());
					PojoToViewMapper.setViewValue(waterOutletAvailability,pojo.getWaterOutletAvailability());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setTypeWaterSupplied((Integer) ViewToPojoMapper.getValueByView(typeWaterSupplied));
					pojo.setWaterOutletAvailability((Integer) ViewToPojoMapper.getValueByView(waterOutletAvailability));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
