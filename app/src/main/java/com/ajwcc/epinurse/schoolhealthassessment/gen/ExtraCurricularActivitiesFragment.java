package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_extra_curricular_activities)
public class ExtraCurricularActivitiesFragment extends BaseEpinurseFragment {


    public ExtraCurricularActivitiesFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup hasPlayground;

	@ViewById
	@MapToModelField
	protected RadioGroup spaceAtEcaFacility;

	@ViewById
	@MapToModelField
	protected CheckBox ecaDancing;

	@ViewById
	@MapToModelField
	protected CheckBox ecaSinging;

	@ViewById
	@MapToModelField
	protected CheckBox ecaRunning;

	@ViewById
	@MapToModelField
	protected CheckBox ecaFootball;

	@ViewById
	@MapToModelField
	protected CheckBox ecaVolleyball;

	@ViewById
	@MapToModelField
	protected CheckBox ecaBasketball;

	@ViewById
	@MapToModelField
	protected CheckBox ecaPingpong;

	@ViewById
	@MapToModelField
	protected CheckBox ecaBadminton;

	@ViewById
	@MapToModelField
	protected CheckBox ecaMartialarts;

	@ViewById
	@MapToModelField
	protected CheckBox ecaOthers;

	@ViewById
	@MapToModelField
	protected EditText ecaSpecify;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(hasPlayground,pojo.getHasPlayground());
					PojoToViewMapper.setViewValue(spaceAtEcaFacility,pojo.getSpaceAtEcaFacility());
					PojoToViewMapper.setViewValue(ecaDancing,pojo.getEcaDancing());
					PojoToViewMapper.setViewValue(ecaSinging,pojo.getEcaSinging());
					PojoToViewMapper.setViewValue(ecaRunning,pojo.getEcaRunning());
					PojoToViewMapper.setViewValue(ecaFootball,pojo.getEcaFootball());
					PojoToViewMapper.setViewValue(ecaVolleyball,pojo.getEcaVolleyball());
					PojoToViewMapper.setViewValue(ecaBasketball,pojo.getEcaBasketball());
					PojoToViewMapper.setViewValue(ecaPingpong,pojo.getEcaPingpong());
					PojoToViewMapper.setViewValue(ecaBadminton,pojo.getEcaBadminton());
					PojoToViewMapper.setViewValue(ecaMartialarts,pojo.getEcaMartialarts());
					PojoToViewMapper.setViewValue(ecaOthers,pojo.getEcaOthers());
					PojoToViewMapper.setViewValue(ecaSpecify,pojo.getEcaSpecify());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setHasPlayground((Integer) ViewToPojoMapper.getValueByView(hasPlayground));
					pojo.setSpaceAtEcaFacility((Integer) ViewToPojoMapper.getValueByView(spaceAtEcaFacility));
					pojo.setEcaDancing((Integer) ViewToPojoMapper.getValueByView(ecaDancing));
					pojo.setEcaSinging((Integer) ViewToPojoMapper.getValueByView(ecaSinging));
					pojo.setEcaRunning((Integer) ViewToPojoMapper.getValueByView(ecaRunning));
					pojo.setEcaFootball((Integer) ViewToPojoMapper.getValueByView(ecaFootball));
					pojo.setEcaVolleyball((Integer) ViewToPojoMapper.getValueByView(ecaVolleyball));
					pojo.setEcaBasketball((Integer) ViewToPojoMapper.getValueByView(ecaBasketball));
					pojo.setEcaPingpong((Integer) ViewToPojoMapper.getValueByView(ecaPingpong));
					pojo.setEcaBadminton((Integer) ViewToPojoMapper.getValueByView(ecaBadminton));
					pojo.setEcaMartialarts((Integer) ViewToPojoMapper.getValueByView(ecaMartialarts));
					pojo.setEcaOthers((Integer) ViewToPojoMapper.getValueByView(ecaOthers));
					pojo.setEcaSpecify((String) ViewToPojoMapper.getValueByView(ecaSpecify));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.ecaOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.ecaOthers:
				 UiUtils.toggleSpecify(view, ecaSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



}
