package com.ajwcc.epinurse.schoolhealthassessment.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessment;
import com.ajwcc.util.reflect.MapToModelField;
import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.SeekBarProgressChange;
import org.androidannotations.annotations.SeekBarTouchStop;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


@EFragment(R.layout.fragment_school_health_assessment_hazard_risk_assessment)
public class HazardRiskAssessmentFragment extends com.ajwcc.epinurse.schoolhealthassessment.gen.HazardRiskAssessmentFragment {

	@ViewById
	ScrollView scroller;

	@ViewById(R.id.hazardList)
	LinearLayout hazardList;

	ArrayList<EditText> riskScores = new ArrayList<>();

	@AfterViews
	public void init()
	{
		// mark the EditTexts as not editable
		earthquakeRiskScore.setEnabled(false);
		landslideRiskScore.setEnabled(false);
		wildlifeAttackRiskScore.setEnabled(false);
		fireRiskScore.setEnabled(false);
		pestDiseasesRiskScore.setEnabled(false);
		floodRiskScore.setEnabled(false);
		windstormRiskScore.setEnabled(false);
		droughtRiskScore.setEnabled(false);
		hailstormRiskScore.setEnabled(false);
		soilErosionRiskScore.setEnabled(false);
		epidemicsRiskScore.setEnabled(false);


		// WARNING: this is only valid the first time
		//			need to

		riskScores.add(earthquakeRiskScore);
		riskScores.add(landslideRiskScore);
		riskScores.add(wildlifeAttackRiskScore);
		riskScores.add(fireRiskScore);
		riskScores.add(pestDiseasesRiskScore);
		riskScores.add(floodRiskScore);
		riskScores.add(windstormRiskScore);
		riskScores.add(droughtRiskScore);
		riskScores.add(hailstormRiskScore);
		riskScores.add(soilErosionRiskScore);
		riskScores.add(epidemicsRiskScore);

		// tag the corresponding EditText to the container

		for (int i=0; i<hazardList.getChildCount(); i++)
		{
			View v = hazardList.getChildAt(i);
			v.setTag(riskScores.get(i));
		}

	}

	public void mapModelToViews()
	{
		super.mapModelToViews();

		// replace all black edittexts with 1
		for (EditText et : riskScores)
		{
			if (et.getText().toString().equals(""))
			{
				et.setText("1");
			}
		}

		sort();
	}

	private int getValueFromEditText(EditText et)
	{
		if (et.getText().toString().equals(""))
		{
			return 1;
		}
		return Integer.parseInt(et.getText().toString());
	}


	@Click(R.id.sort)
	public void sort(){
		for (int i = 0; i < hazardList.getChildCount() - 1; i++)
		{
			int index = i;
			for (int j = i + 1; j < hazardList.getChildCount(); j++){

				int rs1 = getValueFromEditText((EditText) hazardList.getChildAt(j).getTag());
				int rs2 = getValueFromEditText((EditText) hazardList.getChildAt(index).getTag());

				if (rs1 > rs2){
					index = j;//searching for index of largest
				}
			}

			if (index!=i) {
				View maxV = hazardList.getChildAt(index);
				View minV = hazardList.getChildAt(i);


				hazardList.removeView(maxV);
				hazardList.removeView(minV);

				hazardList.addView(maxV, i);
				hazardList.addView(minV, index);
			}
		}
	}


// ISSUE with a disabled EditText, color will not show

//	@TextChange({R.id.earthquakeRiskScore,
//				 R.id.landslideRiskScore,
//				 R.id.wildlifeAttackRiskScore,
//					R.id.fireRiskScore,
//					R.id.pestDiseasesRiskScore,
//					R.id.floodRiskScore,
//					R.id.windstormRiskScore,
//					R.id.droughtRiskScore,
//					R.id.hailstormRiskScore,
//					R.id.soilErosionRiskScore,
//					R.id.epidemicsRiskScore})
//	public void textChanged(TextView editText)
//	{
//		int value = Integer.parseInt(editText.getText().toString());
//
//		int color = 0;
//		if (value>=10)
//		{
//			color = ContextCompat.getColor(getActivity(), R.color.epinurse_severity_high);
//		}
//		else if (value>6)
//		{
//			color = ContextCompat.getColor(getActivity(), R.color.epinurse_severity_serious);
//		}
//		else if (value>3)
//		{
//			color = ContextCompat.getColor(getActivity(), R.color.epinurse_severity_medium);
//		}
//		else
//		{
//			color = ContextCompat.getColor(getActivity(), R.color.epinurse_severity_low);
//		}
//
//		editText.setBackgroundColor(color);
//	}


	private void computeRiskScore(SeekBar bar1, SeekBar bar2, EditText text)
	{
		// set text
		int value = (bar1.getProgress()+1) * (bar2.getProgress()+1);
		text.setText(String.valueOf(value));
	}

	public void confirmSort()
	{
		// prompt
		new AlertDialog.Builder(getActivity())
				.setTitle("Confirm Reordering")
				.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
				.setMessage("The Risk Priority Order has changed, do you wish to sort the list again?")
				.setPositiveButton(getResources().getString(R.string.button_confirm_delete_row_yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						sort();
					}
				})
				.setNegativeButton(getResources().getString(R.string.button_confirm_delete_row_no), null)
				.show();

	}


	@SeekBarTouchStop({
			R.id.earthquakeHazards, R.id.earthquakeVulnerabilities,
			R.id.landslideHazards, R.id.landslideVulnerabilities,
			R.id.wildlifeAttackHazards, R.id.wildlifeAttackVulnerabilities,
			R.id.fireHazards, R.id.fireVulnerabilities,
			R.id.pestDiseasesHazards, R.id.pestDiseasesVulnerabilities,
			R.id.floodHazards, R.id.floodVulnerabilities,
			R.id.windstormHazards, R.id.windstormVulnerabilities,
			R.id.droughtHazards, R.id.droughtVulnerabilities,
			R.id.hailstormHazards, R.id.hailstormVulnerabilities,
			R.id.soilErosionHazards, R.id.soilErosionVulnerabilities,
			R.id.epidemicsHazards, R.id.epidemicsVulnerabilities
			})
	public void seekbarChanged(SeekBar v)
	{
		switch(v.getId())
		{
			case R.id.earthquakeHazards:
			case R.id.earthquakeVulnerabilities:
				computeRiskScore(earthquakeHazards, earthquakeVulnerabilities, earthquakeRiskScore);
				break;

			case R.id.landslideHazards:
			case R.id.landslideVulnerabilities:
				computeRiskScore(landslideHazards, landslideVulnerabilities, landslideRiskScore);
				break;

			case R.id.wildlifeAttackHazards:
			case R.id.wildlifeAttackVulnerabilities:
				computeRiskScore(wildlifeAttackHazards, wildlifeAttackVulnerabilities, wildlifeAttackRiskScore);
				break;

			case R.id.fireHazards:
			case R.id.fireVulnerabilities:
				computeRiskScore(fireHazards, fireVulnerabilities, fireRiskScore);
				break;

			case R.id.pestDiseasesHazards:
			case R.id.pestDiseasesVulnerabilities:
				computeRiskScore(pestDiseasesHazards, pestDiseasesVulnerabilities, pestDiseasesRiskScore);
				break;

			case R.id.floodHazards:
			case R.id.floodVulnerabilities:
				computeRiskScore(floodHazards, floodVulnerabilities, floodRiskScore);
				break;

			case R.id.windstormHazards:
			case R.id.windstormVulnerabilities:
				computeRiskScore(windstormHazards, windstormVulnerabilities, windstormRiskScore);
				break;

			case R.id.droughtHazards:
			case R.id.droughtVulnerabilities:
				computeRiskScore(droughtHazards, droughtVulnerabilities, droughtRiskScore);
				break;

			case R.id.hailstormHazards:
			case R.id.hailstormVulnerabilities:
				computeRiskScore(hailstormHazards, hailstormVulnerabilities, hailstormRiskScore);
				break;

			case R.id.soilErosionHazards:
			case R.id.soilErosionVulnerabilities:
				computeRiskScore(soilErosionHazards, soilErosionVulnerabilities, soilErosionRiskScore);
				break;

			case R.id.epidemicsHazards:
			case R.id.epidemicsVulnerabilities:
				computeRiskScore(epidemicsHazards, epidemicsVulnerabilities, epidemicsRiskScore);
				break;

		}
	}



}
