package com.ajwcc.epinurse.schoolhealthassessment.ui;

import org.androidannotations.annotations.EActivity;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.image.ImageListFragment_;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;

import java.util.List;


@EActivity(R.layout.gen_activity_school_health_assessment)
public class SchoolHealthAssessmentActivity extends com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessmentActivity
{

    protected List<LazyFragment> createFragmentList() {

        List<LazyFragment> list = super.createFragmentList();

        list.set(8, ()-> HazardRiskAssessmentFragment_.builder().build());

        list.add(list.size()-1, ()-> ImageListFragment_.builder().build());

        return list;
    }

}
