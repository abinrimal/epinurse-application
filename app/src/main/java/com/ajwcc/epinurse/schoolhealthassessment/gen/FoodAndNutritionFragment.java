package com.ajwcc.epinurse.schoolhealthassessment.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_school_health_assessment_food_and_nutrition)
public class FoodAndNutritionFragment extends BaseEpinurseFragment {


    public FoodAndNutritionFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected RadioGroup schoolProvidesFoodOpt;

	@ViewById
	@MapToModelField
	protected CheckBox schoolProvidesFoodBreakfast;

	@ViewById
	@MapToModelField
	protected CheckBox schoolProvidesFoodLunch;

	@ViewById
	@MapToModelField
	protected CheckBox schoolProvidesFoodSnacks;

	@ViewById
	@MapToModelField
	protected CheckBox schoolProvidesFoodDinner;

	@ViewById
	@MapToModelField
	protected RadioGroup foodHygienicSafe;

	@ViewById
	@MapToModelField
	protected RadioGroup mealBalanced;

	@ViewById
	@MapToModelField
	protected RadioGroup foodCanBeBroughtFromHome;

	@ViewById
	@MapToModelField
	protected RadioGroup junkFoodAllowed;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
					PojoToViewMapper.setViewValue(schoolProvidesFoodOpt,pojo.getSchoolProvidesFoodOpt());
					PojoToViewMapper.setViewValue(schoolProvidesFoodBreakfast,pojo.getSchoolProvidesFoodBreakfast());
					PojoToViewMapper.setViewValue(schoolProvidesFoodLunch,pojo.getSchoolProvidesFoodLunch());
					PojoToViewMapper.setViewValue(schoolProvidesFoodSnacks,pojo.getSchoolProvidesFoodSnacks());
					PojoToViewMapper.setViewValue(schoolProvidesFoodDinner,pojo.getSchoolProvidesFoodDinner());
					PojoToViewMapper.setViewValue(foodHygienicSafe,pojo.getFoodHygienicSafe());
					PojoToViewMapper.setViewValue(mealBalanced,pojo.getMealBalanced());
					PojoToViewMapper.setViewValue(foodCanBeBroughtFromHome,pojo.getFoodCanBeBroughtFromHome());
					PojoToViewMapper.setViewValue(junkFoodAllowed,pojo.getJunkFoodAllowed());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: SchoolHealthAssessment");
                	SchoolHealthAssessment pojo = (SchoolHealthAssessment) getModel();
                	
					pojo.setSchoolProvidesFoodOpt((Integer) ViewToPojoMapper.getValueByView(schoolProvidesFoodOpt));
					pojo.setSchoolProvidesFoodBreakfast((Integer) ViewToPojoMapper.getValueByView(schoolProvidesFoodBreakfast));
					pojo.setSchoolProvidesFoodLunch((Integer) ViewToPojoMapper.getValueByView(schoolProvidesFoodLunch));
					pojo.setSchoolProvidesFoodSnacks((Integer) ViewToPojoMapper.getValueByView(schoolProvidesFoodSnacks));
					pojo.setSchoolProvidesFoodDinner((Integer) ViewToPojoMapper.getValueByView(schoolProvidesFoodDinner));
					pojo.setFoodHygienicSafe((Integer) ViewToPojoMapper.getValueByView(foodHygienicSafe));
					pojo.setMealBalanced((Integer) ViewToPojoMapper.getValueByView(mealBalanced));
					pojo.setFoodCanBeBroughtFromHome((Integer) ViewToPojoMapper.getValueByView(foodCanBeBroughtFromHome));
					pojo.setJunkFoodAllowed((Integer) ViewToPojoMapper.getValueByView(junkFoodAllowed));

					if (save)
					{
                		System.out.println("Save to realm: SchoolHealthAssessment");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			SchoolHealthAssessment model = (SchoolHealthAssessment) getModel();
			boolean update = false;

			if ((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
			{
				getView().findViewById(R.id.schoolProvidesFoodContainer).setVisibility(View.GONE);
				update = true;				
				model.setSchoolProvidesFoodBreakfast(null);
				model.setSchoolProvidesFoodLunch(null);
				model.setSchoolProvidesFoodSnacks(null);
				model.setSchoolProvidesFoodDinner(null);

			}
			else
			{
				getView().findViewById(R.id.schoolProvidesFoodContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
			{
				getView().findViewById(R.id.foodHygienicSafeContainer).setVisibility(View.GONE);
				update = true;				
				model.setFoodHygienicSafe(null);

			}
			else
			{
				getView().findViewById(R.id.foodHygienicSafeContainer).setVisibility(View.VISIBLE);
			}

			if ((model.getSchoolProvidesFoodOpt()==null || !(model.getSchoolProvidesFoodOpt()==1)))
			{
				getView().findViewById(R.id.mealBalancedContainer).setVisibility(View.GONE);
				update = true;				
				model.setMealBalanced(null);

			}
			else
			{
				getView().findViewById(R.id.mealBalancedContainer).setVisibility(View.VISIBLE);
			}
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(SchoolHealthAssessment mode, boolean update)
	{
		return update;
	}
	



	@Click({R.id.schoolProvidesFoodOpt0,R.id.schoolProvidesFoodOpt1})
	
	public void fireCheckDependency()
	{
		if (inCheckDependencies)
		{
			// suppress change events when inside checkDependencies()
			System.out.println("checkDependencies() suppressed in "+getClass().getName());
			return;
		}
	
		mapViewsToModel(false);
		checkDependencies();
	}

}
