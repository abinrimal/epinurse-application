package com.ajwcc.epinurse.schoolhealthassessment.gen;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;


public class SchoolHealthAssessmentJsonSerializer implements JsonSerializer<SchoolHealthAssessment> {

    @Override
    public JsonElement serialize(SchoolHealthAssessment model, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        
        object.add("uuid", context.serialize(model.getUuid()));
        object.add("ownerUuid", context.serialize(model.getOwnerUuid()));
        
		serializeShineId(model, object, context); // shine_id

		// Basic Information
		serializeSchool(model, object, context); // school
		serializeContactPerson(model, object, context); // contact_person
		serializeContactPosition(model, object, context); // contact_position
		serializeActivitiesStudentHealthAssessment(model, object, context); // activities_student_health_assessment
		serializeActivitiesStaffMembersHealthAssessment(model, object, context); // activities_staff_members_health_assessment
		serializeActivitiesSanitationClass(model, object, context); // activities_sanitation_class
		serializeActivitiesDietClass(model, object, context); // activities_diet_class
		serializeDistanceOfSchoolKm(model, object, context); // distance_of_school_km

		// Ecd
		serializeEcdPresent(model, object, context); // ecd_present
		serializeEcdTeacherPresent(model, object, context); // ecd_teacher_present
		serializeClassroomFacilities(model, object, context); // classroom_facilities
		serializeSeatingArrangements(model, object, context); // seating_arrangements
		serializeApprPlayMaterials(model, object, context); // appr_play_materials
		serializeColorsIllustrations(model, object, context); // colors_illustrations
		serializeToiletHandwashing(model, object, context); // toilet_handwashing
		serializeCollisionAvoidance(model, object, context); // collision_avoidance
		serializeNonslipFloors(model, object, context); // nonslip_floors
		serializeLightingFacilities(model, object, context); // lighting_facilities

		// Extra Curricular Activities
		serializeHasPlayground(model, object, context); // has_playground
		serializeSpaceAtEcaFacility(model, object, context); // space_at_eca_facility
		serializeEcaDancing(model, object, context); // eca_dancing
		serializeEcaSinging(model, object, context); // eca_singing
		serializeEcaRunning(model, object, context); // eca_running
		serializeEcaFootball(model, object, context); // eca_football
		serializeEcaVolleyball(model, object, context); // eca_volleyball
		serializeEcaBasketball(model, object, context); // eca_basketball
		serializeEcaPingpong(model, object, context); // eca_pingpong
		serializeEcaBadminton(model, object, context); // eca_badminton
		serializeEcaMartialarts(model, object, context); // eca_martialarts
		serializeEcaOthers(model, object, context); // eca_others
		serializeEcaSpecify(model, object, context); // eca_specify

		// First Aid
		serializeFirstAidBoxPresent(model, object, context); // first_aid_box_present
		serializeFirstAidBoxStocked(model, object, context); // first_aid_box_stocked
		serializeNumStudentsCoveredByFirstAid(model, object, context); // num_students_covered_by_first_aid

		// Drinking Water
		serializeTypeWaterSupplied(model, object, context); // type_water_supplied
		serializeWaterOutletAvailability(model, object, context); // water_outlet_availability

		// Food and Nutrition
		serializeSchoolProvidesFoodOpt(model, object, context); // school_provides_food_opt
		serializeSchoolProvidesFoodBreakfast(model, object, context); // school_provides_food_breakfast
		serializeSchoolProvidesFoodLunch(model, object, context); // school_provides_food_lunch
		serializeSchoolProvidesFoodSnacks(model, object, context); // school_provides_food_snacks
		serializeSchoolProvidesFoodDinner(model, object, context); // school_provides_food_dinner
		serializeFoodHygienicSafe(model, object, context); // food_hygienic_safe
		serializeMealBalanced(model, object, context); // meal_balanced
		serializeFoodCanBeBroughtFromHome(model, object, context); // food_can_be_brought_from_home
		serializeJunkFoodAllowed(model, object, context); // junk_food_allowed

		// Toilet Facilities
		serializeToilHasRunningWater(model, object, context); // toil_has_running_water
		serializeToilHasCleaningWater(model, object, context); // toil_has_cleaning_water
		serializeToilHasSoap(model, object, context); // toil_has_soap
		serializeToilHasSepGender(model, object, context); // toil_has_sep_gender
		serializeToilHasSepStudentStaff(model, object, context); // toil_has_sep_student_staff
		serializeToilHasTrash(model, object, context); // toil_has_trash
		serializeToilHasAdequateToilets(model, object, context); // toil_has_adequate_toilets
		serializeToilHasToiletRating(model, object, context); // toil_has_toilet_rating

		// Infrastructure
		serializeSchoolAt(model, object, context); // school_at
		serializeSchoolAtSpecify(model, object, context); // school_at_specify
		serializeBuildingType(model, object, context); // building_type
		serializeAgeOfBuilding(model, object, context); // age_of_building
		serializeBuildingHeight(model, object, context); // building_height
		serializeWaterDamage(model, object, context); // water_damage
		serializeFirefightingInstrument(model, object, context); // firefighting_instrument

		// Hazard Risk Assessment
		serializeEarthquakeHazards(model, object, context); // earthquake_hazards
		serializeEarthquakeVulnerabilities(model, object, context); // earthquake_vulnerabilities
		serializeEarthquakeRiskScore(model, object, context); // earthquake_risk_score
		serializeLandslideHazards(model, object, context); // landslide_hazards
		serializeLandslideVulnerabilities(model, object, context); // landslide_vulnerabilities
		serializeLandslideRiskScore(model, object, context); // landslide_risk_score
		serializeWildlifeAttackHazards(model, object, context); // wildlife_attack_hazards
		serializeWildlifeAttackVulnerabilities(model, object, context); // wildlife_attack_vulnerabilities
		serializeWildlifeAttackRiskScore(model, object, context); // wildlife_attack_risk_score
		serializeFireHazards(model, object, context); // fire_hazards
		serializeFireVulnerabilities(model, object, context); // fire_vulnerabilities
		serializeFireRiskScore(model, object, context); // fire_risk_score
		serializePestDiseasesHazards(model, object, context); // pest_diseases_hazards
		serializePestDiseasesVulnerabilities(model, object, context); // pest_diseases_vulnerabilities
		serializePestDiseasesRiskScore(model, object, context); // pest_diseases_risk_score
		serializeFloodHazards(model, object, context); // flood_hazards
		serializeFloodVulnerabilities(model, object, context); // flood_vulnerabilities
		serializeFloodRiskScore(model, object, context); // flood_risk_score
		serializeWindstormHazards(model, object, context); // windstorm_hazards
		serializeWindstormVulnerabilities(model, object, context); // windstorm_vulnerabilities
		serializeWindstormRiskScore(model, object, context); // windstorm_risk_score
		serializeDroughtHazards(model, object, context); // drought_hazards
		serializeDroughtVulnerabilities(model, object, context); // drought_vulnerabilities
		serializeDroughtRiskScore(model, object, context); // drought_risk_score
		serializeHailstormHazards(model, object, context); // hailstorm_hazards
		serializeHailstormVulnerabilities(model, object, context); // hailstorm_vulnerabilities
		serializeHailstormRiskScore(model, object, context); // hailstorm_risk_score
		serializeSoilErosionHazards(model, object, context); // soil_erosion_hazards
		serializeSoilErosionVulnerabilities(model, object, context); // soil_erosion_vulnerabilities
		serializeSoilErosionRiskScore(model, object, context); // soil_erosion_risk_score
		serializeEpidemicsHazards(model, object, context); // epidemics_hazards
		serializeEpidemicsVulnerabilities(model, object, context); // epidemics_vulnerabilities
		serializeEpidemicsRiskScore(model, object, context); // epidemics_risk_score
		serializeDisasterDrill(model, object, context); // disaster_drill
		serializeDisasterDrillSpecify(model, object, context); // disaster_drill_specify
		serializeDisasterPreparednessMeasures(model, object, context); // disaster_preparedness_measures
		serializeDisasterPreparednessMeasuresSpecify(model, object, context); // disaster_preparedness_measures_specify
		serializeEarthquakeResistant(model, object, context); // earthquake_resistant
 
       
        return object;
    }
    

    public void serializeShineId(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("shine_id", context.serialize(model.getShineId()));
    }

    public void serializeSchool(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school", context.serialize(model.getSchool()));
    }

    public void serializeContactPerson(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("contact_person", context.serialize(model.getContactPerson()));
    }

    public void serializeContactPosition(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("contact_position", context.serialize(model.getContactPosition()));
    }

    public void serializeActivitiesStudentHealthAssessment(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("activities_student_health_assessment", context.serialize(model.getActivitiesStudentHealthAssessment()));
    }

    public void serializeActivitiesStaffMembersHealthAssessment(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("activities_staff_members_health_assessment", context.serialize(model.getActivitiesStaffMembersHealthAssessment()));
    }

    public void serializeActivitiesSanitationClass(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("activities_sanitation_class", context.serialize(model.getActivitiesSanitationClass()));
    }

    public void serializeActivitiesDietClass(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("activities_diet_class", context.serialize(model.getActivitiesDietClass()));
    }

    public void serializeDistanceOfSchoolKm(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("distance_of_school_km", context.serialize(model.getDistanceOfSchoolKm()));
    }

    public void serializeEcdPresent(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("ecd_present", context.serialize(model.getEcdPresent()));
    }

    public void serializeEcdTeacherPresent(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("ecd_teacher_present", context.serialize(model.getEcdTeacherPresent()));
    }

    public void serializeClassroomFacilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("classroom_facilities", context.serialize(model.getClassroomFacilities()));
    }

    public void serializeSeatingArrangements(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("seating_arrangements", context.serialize(model.getSeatingArrangements()));
    }

    public void serializeApprPlayMaterials(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("appr_play_materials", context.serialize(model.getApprPlayMaterials()));
    }

    public void serializeColorsIllustrations(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("colors_illustrations", context.serialize(model.getColorsIllustrations()));
    }

    public void serializeToiletHandwashing(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toilet_handwashing", context.serialize(model.getToiletHandwashing()));
    }

    public void serializeCollisionAvoidance(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("collision_avoidance", context.serialize(model.getCollisionAvoidance()));
    }

    public void serializeNonslipFloors(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("nonslip_floors", context.serialize(model.getNonslipFloors()));
    }

    public void serializeLightingFacilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("lighting_facilities", context.serialize(model.getLightingFacilities()));
    }

    public void serializeHasPlayground(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("has_playground", context.serialize(model.getHasPlayground()));
    }

    public void serializeSpaceAtEcaFacility(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("space_at_eca_facility", context.serialize(model.getSpaceAtEcaFacility()));
    }

    public void serializeEcaDancing(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_dancing", context.serialize(model.getEcaDancing()));
    }

    public void serializeEcaSinging(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_singing", context.serialize(model.getEcaSinging()));
    }

    public void serializeEcaRunning(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_running", context.serialize(model.getEcaRunning()));
    }

    public void serializeEcaFootball(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_football", context.serialize(model.getEcaFootball()));
    }

    public void serializeEcaVolleyball(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_volleyball", context.serialize(model.getEcaVolleyball()));
    }

    public void serializeEcaBasketball(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_basketball", context.serialize(model.getEcaBasketball()));
    }

    public void serializeEcaPingpong(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_pingpong", context.serialize(model.getEcaPingpong()));
    }

    public void serializeEcaBadminton(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_badminton", context.serialize(model.getEcaBadminton()));
    }

    public void serializeEcaMartialarts(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_martialarts", context.serialize(model.getEcaMartialarts()));
    }

    public void serializeEcaOthers(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_others", context.serialize(model.getEcaOthers()));
    }

    public void serializeEcaSpecify(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("eca_specify", context.serialize(model.getEcaSpecify()));
    }

    public void serializeFirstAidBoxPresent(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("first_aid_box_present", context.serialize(model.getFirstAidBoxPresent()));
    }

    public void serializeFirstAidBoxStocked(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("first_aid_box_stocked", context.serialize(model.getFirstAidBoxStocked()));
    }

    public void serializeNumStudentsCoveredByFirstAid(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("num_students_covered_by_first_aid", context.serialize(model.getNumStudentsCoveredByFirstAid()));
    }

    public void serializeTypeWaterSupplied(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("type_water_supplied", context.serialize(model.getTypeWaterSupplied()));
    }

    public void serializeWaterOutletAvailability(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_outlet_availability", context.serialize(model.getWaterOutletAvailability()));
    }

    public void serializeSchoolProvidesFoodOpt(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_provides_food_opt", context.serialize(model.getSchoolProvidesFoodOpt()));
    }

    public void serializeSchoolProvidesFoodBreakfast(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_provides_food_breakfast", context.serialize(model.getSchoolProvidesFoodBreakfast()));
    }

    public void serializeSchoolProvidesFoodLunch(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_provides_food_lunch", context.serialize(model.getSchoolProvidesFoodLunch()));
    }

    public void serializeSchoolProvidesFoodSnacks(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_provides_food_snacks", context.serialize(model.getSchoolProvidesFoodSnacks()));
    }

    public void serializeSchoolProvidesFoodDinner(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_provides_food_dinner", context.serialize(model.getSchoolProvidesFoodDinner()));
    }

    public void serializeFoodHygienicSafe(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_hygienic_safe", context.serialize(model.getFoodHygienicSafe()));
    }

    public void serializeMealBalanced(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("meal_balanced", context.serialize(model.getMealBalanced()));
    }

    public void serializeFoodCanBeBroughtFromHome(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("food_can_be_brought_from_home", context.serialize(model.getFoodCanBeBroughtFromHome()));
    }

    public void serializeJunkFoodAllowed(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("junk_food_allowed", context.serialize(model.getJunkFoodAllowed()));
    }

    public void serializeToilHasRunningWater(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_running_water", context.serialize(model.getToilHasRunningWater()));
    }

    public void serializeToilHasCleaningWater(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_cleaning_water", context.serialize(model.getToilHasCleaningWater()));
    }

    public void serializeToilHasSoap(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_soap", context.serialize(model.getToilHasSoap()));
    }

    public void serializeToilHasSepGender(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_sep_gender", context.serialize(model.getToilHasSepGender()));
    }

    public void serializeToilHasSepStudentStaff(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_sep_student_staff", context.serialize(model.getToilHasSepStudentStaff()));
    }

    public void serializeToilHasTrash(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_trash", context.serialize(model.getToilHasTrash()));
    }

    public void serializeToilHasAdequateToilets(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_adequate_toilets", context.serialize(model.getToilHasAdequateToilets()));
    }

    public void serializeToilHasToiletRating(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("toil_has_toilet_rating", context.serialize(model.getToilHasToiletRating()));
    }

    public void serializeSchoolAt(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_at", context.serialize(model.getSchoolAt()));
    }

    public void serializeSchoolAtSpecify(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("school_at_specify", context.serialize(model.getSchoolAtSpecify()));
    }

    public void serializeBuildingType(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("building_type", context.serialize(model.getBuildingType()));
    }

    public void serializeAgeOfBuilding(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("age_of_building", context.serialize(model.getAgeOfBuilding()));
    }

    public void serializeBuildingHeight(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("building_height", context.serialize(model.getBuildingHeight()));
    }

    public void serializeWaterDamage(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("water_damage", context.serialize(model.getWaterDamage()));
    }

    public void serializeFirefightingInstrument(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("firefighting_instrument", context.serialize(model.getFirefightingInstrument()));
    }

    public void serializeEarthquakeHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("earthquake_hazards", context.serialize(model.getEarthquakeHazards()));
    }

    public void serializeEarthquakeVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("earthquake_vulnerabilities", context.serialize(model.getEarthquakeVulnerabilities()));
    }

    public void serializeEarthquakeRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("earthquake_risk_score", context.serialize(model.getEarthquakeRiskScore()));
    }

    public void serializeLandslideHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("landslide_hazards", context.serialize(model.getLandslideHazards()));
    }

    public void serializeLandslideVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("landslide_vulnerabilities", context.serialize(model.getLandslideVulnerabilities()));
    }

    public void serializeLandslideRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("landslide_risk_score", context.serialize(model.getLandslideRiskScore()));
    }

    public void serializeWildlifeAttackHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("wildlife_attack_hazards", context.serialize(model.getWildlifeAttackHazards()));
    }

    public void serializeWildlifeAttackVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("wildlife_attack_vulnerabilities", context.serialize(model.getWildlifeAttackVulnerabilities()));
    }

    public void serializeWildlifeAttackRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("wildlife_attack_risk_score", context.serialize(model.getWildlifeAttackRiskScore()));
    }

    public void serializeFireHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("fire_hazards", context.serialize(model.getFireHazards()));
    }

    public void serializeFireVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("fire_vulnerabilities", context.serialize(model.getFireVulnerabilities()));
    }

    public void serializeFireRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("fire_risk_score", context.serialize(model.getFireRiskScore()));
    }

    public void serializePestDiseasesHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pest_diseases_hazards", context.serialize(model.getPestDiseasesHazards()));
    }

    public void serializePestDiseasesVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pest_diseases_vulnerabilities", context.serialize(model.getPestDiseasesVulnerabilities()));
    }

    public void serializePestDiseasesRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("pest_diseases_risk_score", context.serialize(model.getPestDiseasesRiskScore()));
    }

    public void serializeFloodHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("flood_hazards", context.serialize(model.getFloodHazards()));
    }

    public void serializeFloodVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("flood_vulnerabilities", context.serialize(model.getFloodVulnerabilities()));
    }

    public void serializeFloodRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("flood_risk_score", context.serialize(model.getFloodRiskScore()));
    }

    public void serializeWindstormHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("windstorm_hazards", context.serialize(model.getWindstormHazards()));
    }

    public void serializeWindstormVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("windstorm_vulnerabilities", context.serialize(model.getWindstormVulnerabilities()));
    }

    public void serializeWindstormRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("windstorm_risk_score", context.serialize(model.getWindstormRiskScore()));
    }

    public void serializeDroughtHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("drought_hazards", context.serialize(model.getDroughtHazards()));
    }

    public void serializeDroughtVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("drought_vulnerabilities", context.serialize(model.getDroughtVulnerabilities()));
    }

    public void serializeDroughtRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("drought_risk_score", context.serialize(model.getDroughtRiskScore()));
    }

    public void serializeHailstormHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("hailstorm_hazards", context.serialize(model.getHailstormHazards()));
    }

    public void serializeHailstormVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("hailstorm_vulnerabilities", context.serialize(model.getHailstormVulnerabilities()));
    }

    public void serializeHailstormRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("hailstorm_risk_score", context.serialize(model.getHailstormRiskScore()));
    }

    public void serializeSoilErosionHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("soil_erosion_hazards", context.serialize(model.getSoilErosionHazards()));
    }

    public void serializeSoilErosionVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("soil_erosion_vulnerabilities", context.serialize(model.getSoilErosionVulnerabilities()));
    }

    public void serializeSoilErosionRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("soil_erosion_risk_score", context.serialize(model.getSoilErosionRiskScore()));
    }

    public void serializeEpidemicsHazards(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("epidemics_hazards", context.serialize(model.getEpidemicsHazards()));
    }

    public void serializeEpidemicsVulnerabilities(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("epidemics_vulnerabilities", context.serialize(model.getEpidemicsVulnerabilities()));
    }

    public void serializeEpidemicsRiskScore(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("epidemics_risk_score", context.serialize(model.getEpidemicsRiskScore()));
    }

    public void serializeDisasterDrill(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disaster_drill", context.serialize(model.getDisasterDrill()));
    }

    public void serializeDisasterDrillSpecify(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disaster_drill_specify", context.serialize(model.getDisasterDrillSpecify()));
    }

    public void serializeDisasterPreparednessMeasures(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disaster_preparedness_measures", context.serialize(model.getDisasterPreparednessMeasures()));
    }

    public void serializeDisasterPreparednessMeasuresSpecify(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("disaster_preparedness_measures_specify", context.serialize(model.getDisasterPreparednessMeasuresSpecify()));
    }

    public void serializeEarthquakeResistant(SchoolHealthAssessment model, JsonObject object, JsonSerializationContext context)
    {
        object.add("earthquake_resistant", context.serialize(model.getEarthquakeResistant()));
    }
 
    

}