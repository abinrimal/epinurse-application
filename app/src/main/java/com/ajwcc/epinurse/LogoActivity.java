package com.ajwcc.epinurse;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.util.network.AndroidNetUtils;
import com.ajwcc.util.network.NetworkConnectionDetector;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_logo)
public class LogoActivity extends AppCompatActivity
{

    @Click(R.id.start)
    public void start()
    {
        MainMenuActivity_.intent(this).start();
        finish();
    }
}