package com.ajwcc.epinurse.common.utils;

import android.os.Bundle;

import com.ajwcc.util.ui.BaseFormActivity;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;


public abstract class BaseEpinurseFormActivity extends BaseFormActivity
{

    boolean singleEditing = false;


    private Realm realm;


    public void onCreate(Bundle b)
    {
        super.onCreate(b);

        realm = Realm.getDefaultInstance();
    }

    public Realm getRealm()
    {
        return realm;
    }


    private EpinurseModel model;

    public void initModel(Class modelClass, String uuid)
    {

        try {
            if (uuid == null) {
                System.out.println("UUID = null");

                // check if there is an existing object of the modelClass that is "editing"
                // this only occurs if you never completed a form

                if (singleEditing)
                {
                    EpinurseModel found = findAnyEditing(modelClass);
                    //System.out.println(found.getClass().getName());
                    //System.out.println(Arrays.asList(found.getClass().getMethods()));

                    if (found!=null) {
                        System.out.println("using current edited model: " + modelClass.getName());
                        model = realm.copyFromRealm(found);  // need a detached copy for reflection to work

                        System.out.println(model);
                        //System.out.println(Arrays.asList(model.getClass().getMethods()));
                    }
                    else
                    {
                        // create a new one
                        System.out.println("creating new model: "+modelClass.getName());
                        model = createNewModel(modelClass);
                        saveModel();
                    }

                }
                else
                {
                    // create a new one
                    System.out.println("creating new model: "+modelClass.getName());
                    model = createNewModel(modelClass);
                    saveModel();
                }
            }
            else
            {
                System.out.println("UUID: '"+uuid+"'");

                EpinurseModel found = findByUUID(modelClass, uuid);
                if (found!=null)
                {
                    model = realm.copyFromRealm(found); // need a detached copy for reflection to work
                   // model.setEditing(true);

                    saveModel();
                }
                else
                {
                    throw new RuntimeException("Cannot find UUID for "+modelClass.getName());
                }

            }
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public EpinurseModel getModel()
    {
        return model;
    }


    protected EpinurseModel createNewModel(Class modelClass) throws Exception
    {

        EpinurseModel model = (EpinurseModel) modelClass.newInstance();
        model.setUuid(UUID.randomUUID().toString());
        model.setEditing(true);
        model.setSynced(false);
        model.setCreatedAt(new Date());

        return model;
    }

    public void saveModel()
    {
        System.out.println("Saving editing model");
        Realm r = getRealm();
        r.beginTransaction();

        r.copyToRealmOrUpdate(getModel());

        r.commitTransaction();
    }

    public void saveCompletedModel()
    {
        // some kind of validation has to occur here before toggling
        // the editing field

        System.out.println("Saving syncable model");


        Realm r = getRealm();
        r.beginTransaction();

        EpinurseModel model = getModel();
        model.setEditing(false);
        model.setSynced(false);


        r.copyToRealmOrUpdate(model);

        r.commitTransaction();
    }



    public void onPause()
    {
        // this will handle backing out of the activity as well as screen shutdowns
        // data will return onCreate

        super.onPause();
        saveCurrentFragmentContents();
    }



    public void finish()
    {
        super.finish();
        realm.close();
    }

    protected EpinurseModel findAnyEditing(Class modelClass)
    {
        System.out.println("Checking for editing");
        Realm realm = getRealm();

        long count = realm.where(modelClass).equalTo("editing", true).count();

        if (count==0)
        {
            return null;
        }
        else if (count==1) {
            return (EpinurseModel) realm.where(modelClass).equalTo("editing", true).findFirst();
        }
        else
        {
            throw new RuntimeException(modelClass.getName()+ " invalid editing count = "+count);
        }

    }

    protected EpinurseModel findByUUID(Class modelClass, String uuid)
    {
        System.out.println("Checking for uuid: "+uuid);

        Realm realm = getRealm();
        return (EpinurseModel) realm.where(modelClass).equalTo("uuid", uuid).findFirst();
    }




}
