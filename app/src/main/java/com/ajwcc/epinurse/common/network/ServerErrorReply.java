package com.ajwcc.epinurse.common.network;

import java.util.List;

public class ServerErrorReply
{
    private String status;
    private List<String> message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ServerErrorReply{" +
                "status='" + status + '\'' +
                ", message=" + message +
                '}';
    }
}
