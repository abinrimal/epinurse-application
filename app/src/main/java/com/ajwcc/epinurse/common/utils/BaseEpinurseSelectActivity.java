package com.ajwcc.epinurse.common.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.image.ImageEntry;

import java.io.File;
import java.io.Serializable;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;


public abstract class BaseEpinurseSelectActivity extends AppCompatActivity
{
    public Realm realm;


    public void clickAction(boolean selectMode, View view)
    {
        if (selectMode)
        {
            // selectMode
            EpinurseModel b = (EpinurseModel) view.getTag();
            Intent i = new Intent();
            i.putExtra("uuid", b.getUuid());
            setResult(0, i);
            finish();
        }
        else
        {
            openEditor(view);
        }
    }

    public abstract boolean getSelectMode();
    public abstract void newRow();
    public abstract void openEditor(View view);


    public void deleteAction(View v)
    {
        // prompt
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_confirm_delete)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setMessage(R.string.message_confirm_delete_row)
                .setPositiveButton(getResources().getString(R.string.button_confirm_delete_row_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteRow(v);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.button_confirm_delete_row_no), null)
                .show();
    }

    public void deleteRow(View v)
    {
        RealmObject realmObject = (RealmObject) v.getTag();

        EpinurseModel model = (EpinurseModel) realm.copyFromRealm(realmObject);
        System.out.println(model.getClass());

        // need to delete associated images too
        // add to specific classes
        deleteMisc(model);


        realm.beginTransaction();
        realmObject.deleteFromRealm();
        realm.commitTransaction();


    }

    public void deleteMisc(EpinurseModel model)
    {
        Class c = model.getClass();

        // load imageentry for this report and report's UUID
        RealmResults<ImageEntry> list = realm.where(ImageEntry.class)
                .equalTo("reportType", c.getSimpleName())
                .equalTo("reportUuid", model.getUuid())
                .findAll();


        // loop to delete all associated images

        for (ImageEntry entry : list)
        {
            String croppedPath = entry.getCroppedPath();
            String origPath = entry.getOrigPath();

            // remove from the list
            try {
                File f = new File(croppedPath);
                System.out.println("Deleted "+croppedPath+" = " + f.delete());

                File f2 = new File(origPath);
                System.out.println("Deleted "+origPath+" = " + f2.delete());
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }


        realm.beginTransaction();
        list.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public abstract RealmRecyclerViewAdapter createAdapter();

    public BasicInformation getProfile(String uuid)
    {
        throw new RuntimeException("Profile selection not supported");
    }


    public interface QueryFilter<T extends EpinurseModel> extends Serializable
    {
        public String getName();
        public RealmQuery<T> adjust(RealmQuery<T> query);
    }
}
