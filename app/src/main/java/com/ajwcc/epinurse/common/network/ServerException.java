package com.ajwcc.epinurse.common.network;

public class ServerException extends RuntimeException {
    ServerErrorReply errorReply;

    public ServerException(ServerErrorReply errorReply) {
        this.errorReply = errorReply;
    }

    public ServerErrorReply getErrorReply() {
        return errorReply;
    }

    public String getMessage()
    {
        return errorReply.toString();
    }


}
