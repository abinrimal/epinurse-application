package com.ajwcc.epinurse.common.image;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

@EActivity(R.layout.activity_capture_image)
public class ImageCaptureActivity extends AppCompatActivity implements LocationReader.LocationReaderListener {

    private static final int CROP_W = 1000;
    private static final int CROP_H = 1000;

    @Extra
    String imageUuid;     // this is used editing

    @Extra
    String reportType;  // string report type

    @Extra
    String reportUuid;  // associated report uuid


    @ViewById
    CropImageView cropImageView;

    @ViewById
    TextView location;

    @ViewById
    EditText remarks;

    @ViewById
    Button ok;

    @Bean
    LocationReader locationReader;


    private Realm realm;

    private ImageEntry entry;  // entry to be used to store info



    @AfterViews
    public void checkPermissions()
    {
        locationReader.setLocationReaderListener(this);



        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,

                        Manifest.permission.INTERNET
                )

                .withListener(new BaseMultiplePermissionsListener()
                {
                    public void onPermissionsChecked(MultiplePermissionsReport report)
                    {
                        if (report.areAllPermissionsGranted())
                        {
                            init();
                        }
                        else
                        {
                            finish();
                        }
                    }
                })
                .check();

    }

    public void init()
    {
        System.out.println("Setting up images for "+reportType);


        if (!locationReader.isLocationEnabled())
        {
            Toast.makeText(this,
                           getString(R.string.error_location_service_off),
                            Toast.LENGTH_LONG).show();
        }


        realm = Realm.getDefaultInstance();

        // init the photos if they already exist
        // used the cropped image
        if (imageUuid!=null) {

            entry = realm.where(ImageEntry.class).equalTo("uuid", imageUuid).findFirst();
            entry = realm.copyFromRealm(entry);  // make a detached copy

            // this is already a direct file reference
            Uri croppedUri = createUriFromFile(new File(entry.getCroppedPath()), false);

            cropImageView.setImageUriAsync(croppedUri);
            cropImageView.setShowCropOverlay(false);
            allowCropping = false;

            location.setText(entry.getGeocodedLocation());
            remarks.setText((entry.getRemarks()));
        }
        else
        {
            // new entry
            entry = new ImageEntry();
            entry.setUuid(UUID.randomUUID().toString());
            entry.setReportType(reportType);
            entry.setReportUuid(reportUuid);
            entry.setCreatedAt(new Date());

            ok.setEnabled(false);

        }
    }

    public void onStart()
    {
        super.onStart();

        //locationReader.start();  // this should only be done if the locations services are on
                                 // and when location services are toggled on
    }

    public void onPause()
    {
        super.onPause();

        locationReader.stop();
    }

    public void onDestroy()
    {
        super.onDestroy();

        try {
            realm.close();
        }
        catch(Exception e)
        {
        }
    }

    @Click(R.id.capture)
    public void loadImage() {
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    boolean allowCropping = true;

    @Click(R.id.crop)
    public void cropImage() {
        if (allowCropping) {
            Bitmap cropped = cropImageView.getCroppedImage(CROP_W, CROP_H);

            if (cropped != null) {
                System.out.println(cropped.getWidth() + " x " + cropped.getHeight());
                cropImageView.setImageBitmap(cropped);
                cropImageView.setCropRect(new Rect(0, 0, cropped.getWidth(), cropped.getHeight()));
                cropImageView.setShowCropOverlay(false);
                allowCropping = false;

                ok.setEnabled(true);

            }
        } else {
            // reset cropping
            // update cropImageView
            if (newPhotoTaken)
            {
                cropImageView.setImageUriAsync(getTempImageLocalUri());
            }
            else
            {
                cropImageView.setImageUriAsync(getOrigImageLocalUri());
            }

            cropImageView.setShowCropOverlay(true);
            allowCropping = true;

            ok.setEnabled(false);

        }
    }

    @Click(R.id.rotate)
    public void rotate() {
        cropImageView.rotateImage(90);
    }



    @Click(R.id.ok)
    public void ok() {
        try {

            // save cropped image
            Bitmap cropped = cropImageView.getCroppedImage(CROP_W, CROP_H);


            // WARNING: potential for fail if no cropped image


            System.out.println(cropped.getWidth() + " x " + cropped.getHeight());

            // encode
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            cropped.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            // write cropped image to file
            Uri croppedOutputFileUri = getCroppedImageLocalUri();
            FileOutputStream fos = new FileOutputStream(croppedOutputFileUri.getPath());
            fos.write(byteArray);
            fos.flush();

            // rename the temp file to the actual file
            Uri tempOutputFileUri = getTempImageLocalUri();
            Uri origOutputFileUri = getOrigImageLocalUri();

            File localTemp = new File(tempOutputFileUri.getPath());
            File localOrig = new File(origOutputFileUri.getPath());
            localTemp.renameTo(localOrig);


            // place all relevant info into ImageEntry
            // location already in
            // address already in


            // cropped
            entry.setCroppedFileSize(byteArray.length);
            entry.setCroppedWidth(cropped.getWidth());
            entry.setCroppedHeight(cropped.getHeight());
            entry.setPathCrop(croppedOutputFileUri.getPath());

            // original
            entry.setOrigFileSize(localOrig.length());
            entry.setPathOriginal(origOutputFileUri.getPath());


            // POTENTIAL ISSUE : HOW TO DETERMINE IF DATA WAS CHANGED


            // typed remarks
            entry.setRemarks(remarks.getText().toString());

            // in case this was a previously saved and a new pic is put it
            entry.setSynced(false);


            // save entry to realm
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(entry);
            realm.commitTransaction();




            // send this path back
            Intent i = new Intent();
            i.putExtra("newPhotoTaken", newPhotoTaken);

            setResult(100, i);

            finish();


        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


    }

    @Click(R.id.cancel)
    public void cancel() {

        if (imageUuid==null) {
            // delete original photo, if any
                // NOTE: do not delete if existing
            File f = new File(getOrigImageLocalUri().getPath());
            System.out.println("Deleted original = " + f.delete());
        }

        finish();
    }


    private boolean newPhotoTaken;

    public void onActivityResult(int responseCode, int resultCode, Intent data) {
        // MAKE SURE UNCOMMENT THIS IF USED IN A FRAGMENT
        // super.onActivityResult(responseCode, resultCode, data);

        // do stuff here
        if (resultCode == Activity.RESULT_OK) {

            // NOTE: this Uri is actually not needed if using purely the camera
                // if allowing use for the gallery, need to make a local copy accessible
                // by app
            Uri imageUri = getPickImageResultUri(data);


            try {
                Uri copy = getTempImageLocalUri();   // THIS IS ACTUALLY A LOCAL ALIAS TO THE REMOTE URI

                // update cropImageView
                cropImageView.setImageUriAsync(copy);
                cropImageView.setShowCropOverlay(true);
                newPhotoTaken = true;
                allowCropping = true;

                locationReader.start();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }


    }



    // FILE NAME / URI

    private String createOriginalOutputFilename() {
        return reportType +"_"+ reportUuid +"_"+entry.getUuid() + ".jpg";
    }

    private String createCroppedOutputFilename() {
        return reportType +"_"+ reportUuid +"_"+entry.getUuid() + "_cropped.jpg";
    }



    private Uri getCroppedImageLocalUri()
    {
        File getImageDir = getExternalCacheDir();
        if (getImageDir != null) {
            File dir= new File(getImageDir.getPath() + "/epinursePics");
            dir.mkdirs();

            File file = new File(dir, createCroppedOutputFilename());
            Uri croppedOutputFileUri = createUriFromFile(file, false);  // TODO: must change each time
            return croppedOutputFileUri;
        }
        return null;
    }

    private Uri getOrigImageLocalUri() {
        Uri outputFileUri = null;
        File getImageDir = getExternalCacheDir();
        if (getImageDir != null) {
            File dir = new File(getImageDir.getPath() + "/epinursePics");
            dir.mkdirs();
            File file = new File(dir, createOriginalOutputFilename());
            outputFileUri = createUriFromFile(file, false);  // TODO: must change each time
        }
        return outputFileUri;
    }

    // NOTE: TempLocal and RemoteImageOutput are actually the same file
    //       except latter allows access by external apps like camera

    private Uri getTempImageLocalUri() {
        Uri outputFileUri = null;
        File getImageDir = getExternalCacheDir();
        if (getImageDir != null) {
            File dir = new File(getImageDir.getPath() + "/epinursePics");
            dir.mkdirs();
            File file = new File(dir, "temp.jpg");
            outputFileUri = createUriFromFile(file, false);  // TODO: must change each time
        }
        return outputFileUri;
    }

    private Uri getRemoteImageOutputUri() {
        Uri outputFileUri = null;
        File getImageDir = getExternalCacheDir();
        if (getImageDir != null) {
            File dir = new File(getImageDir.getPath() + "/epinursePics");
            dir.mkdirs();
            File file = new File(dir, "temp.jpg");
            outputFileUri = createUriFromFile(file, true);  // TODO: must change each time
        }
        return outputFileUri;
    }



    public Uri createUriFromFile(File file, boolean useFileProvider)
    {
        // may require API check for SDKs prior to N?

        if (!useFileProvider) {
            return Uri.fromFile(file);
        }
        else {
            return FileProvider.getUriForFile(this,
                    getString(R.string.file_provider_authority),
                    file);
        }
    }


    /**
     * Get the URI of the selected image from  {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();

            System.out.println("ACTION: " + action);

            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getRemoteImageOutputUri() : data.getData();
    }




    // from sample code
    // https://theartofdev.com/2015/02/15/android-cropping-image-from-camera-or-gallery/


    /**
     * Create a chooser intent to select the  source to get image from.<br/>
     * The source can be camera's  (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the  intent chooser.
     */
    public Intent getPickImageChooserIntent() {

// Determine Uri of camera image to  save.
        Uri outputFileUri = getRemoteImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

// collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
//
//// collect all gallery intents
//        Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
//        galleryIntent.setType("image/*");
//        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
//        for (ResolveInfo res : listGallery) {
//            Intent intent = new  Intent(galleryIntent);
//            intent.setComponent(new  ComponentName(res.activityInfo.packageName, res.activityInfo.name));
//            intent.setPackage(res.activityInfo.packageName);
//            allIntents.add(intent);
//        }
//
// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    @Override
    public void newLocation(double latitude, double longitude, String address) {




        entry.setLatitude(latitude);
        entry.setLongitude(longitude);
        entry.setGeocodedLocation(address);


        location.setText(address);
        locationReader.stop();
    }

    @Click(R.id.geocode)
    public void forceGeocode()
    {

        locationReader.start();
    }
}
