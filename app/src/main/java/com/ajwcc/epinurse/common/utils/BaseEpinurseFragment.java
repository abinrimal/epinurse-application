package com.ajwcc.epinurse.common.utils;


import com.ajwcc.util.ui.BaseInputFragment;

import io.realm.Realm;
import io.realm.RealmModel;


public abstract class BaseEpinurseFragment extends BaseInputFragment {

    public Realm getRealm()
    {
        // this is the activity scoped instance

        return ((BaseEpinurseFormActivity) getActivity()).getRealm();
    }

    public RealmModel getModel()
    {
        // this is the activity scoped instance

        return ((BaseEpinurseFormActivity) getActivity()).getModel();
    }

    public void saveCompletedModel()
    {
        ((BaseEpinurseFormActivity) getActivity()).saveCompletedModel();
    }

}
