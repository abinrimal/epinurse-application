package com.ajwcc.epinurse.common.network;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import com.ajwcc.epinurse.R;


@EActivity(R.layout.activity_nurse_login)
public class NurseLogin extends AppCompatActivity {

    @Extra
    boolean closeOnSuccess;

    @ViewById
    LinearLayout fields;

    @ViewById(R.id.title)
    TextView title;

    @ViewById
    EditText login;

    @ViewById
    EditText password;

    @ViewById
    ProgressBar progressBar;

    @Bean
    ShineSender sender;


    @AfterViews
    public void init()
    {
    }



    @Click(R.id.button_clear)
    public void clearCredentials()
    {
        try {
            sender.clearCredentials();
            popMessage(getResources().getString(R.string.message_credentials_cleared));  // TODO: USE RES

        }
        catch(Exception e)
        {
            popError("Error:",e);
        }

    }


    // LOGIN HERE
    private boolean running = false;


    @Click(R.id.button_sign_in)
    void attemptLogin()
    {
        if (running == false) {
            running = true;

            // Reset errors.
            login.setError(null);
            password.setError(null);

            // Store values at the time of the login attempt.
            String email = login.getText().toString();
            String passwordString = password.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(passwordString)) {
                password.setError(getResources().getString(R.string.error_password_required));    // TODO: USE RES
                focusView = password;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                password.setError(getResources().getString(R.string.error_email_required));     // TODO: USE RES
                focusView = login;
                cancel = true;
            }


            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
                System.out.println("cancel");
                running = false;
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(false);
                loginBackground(email, passwordString);
            }

        }
    }


    @Background
    void loginBackground(String email, String password)
    {
        try {
            sender.login(email, password);

            loginDone();
        }
        catch(ServerException e)
        {
            // pop up as a toast
            popError("Server Error: ",e);
            resetProgress();
        }
        catch(Exception e)
        {
            // pop up as a toast
            popError("Error: ",e);
            resetProgress();
        }

    }


    @UiThread
    public void popError(String type, Exception e)
    {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    }

    @UiThread
    public void popMessage(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @UiThread
    void resetProgress()
    {
        showProgress(true);
        running = false;
    }


    @UiThread
    void loginDone()
    {
        resetProgress();



        if (closeOnSuccess) {
            finish();
        }
        else
        {
            popMessage(getResources().getString(R.string.message_login_successful)); // TODO: USE RES
        }
    }


    private void showProgress(final boolean show)
    {
        // toggle the buttons away
        if (show)
        {
            fields.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
        }
        else
        {
            fields.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}
