package com.ajwcc.epinurse.common.network;

import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;

import java.util.List;

public class ProfileListServerReply
{
    private String status;
    private String serverDatetime;
    private List<BasicInformation> data;

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServerDatetime() {
        return serverDatetime;
    }

    public void setServerDatetime(String serverDatetime) {
        this.serverDatetime = serverDatetime;
    }

    public List<BasicInformation> getData() {
        return data;
    }

    public void setData(List<BasicInformation> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
