package com.ajwcc.epinurse.common;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.image.ImageEntry;
import com.ajwcc.epinurse.common.network.ShineSender;
import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.util.network.AndroidNetUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

@EActivity(R.layout.activity_generic_sender)
public class GenericSenderActivity extends AppCompatActivity
{
    @Extra
    String reportName;

    @Extra
    String reportClassName;


    @Bean
    ShineSender sender;

    @ViewById
    View progress;

    @ViewById
    TextView message;

    @ViewById
    TextView reportType;

    @ViewById
    TextView numReports;

    @ViewById(R.id.button_upload)
    TextView upload;

    Class reportClass;
    Realm realm;

    long count;

    @AfterViews
    public void init()
    {
        try {
            reportClass = Class.forName(reportClassName);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }


        reportType.setText(reportName);

        // load up realm
        realm = Realm.getDefaultInstance();

        // get how many
        updateCount();


    }

    public void finish()
    {
        super.finish();

        // close Realm
        realm.close();
    }

    public void onDestroy()
    {
        BackgroundExecutor.cancelAll("upload", true);
        super.onDestroy();
    }

    @Click(R.id.button_cancel)
    public void cancel()
    {
        finish();
    }


    @Click(R.id.button_upload)
    public void upload()
    {
        if (!AndroidNetUtils.isNetworkAvailable(this))
        {
            Toast.makeText(this, getResources().getString(R.string.error_no_network_connection), Toast.LENGTH_LONG).show();  // TODO: USE RES
            return;
        }

        prepSend();
    }

    @UiThread
    public void prepSend()
    {
        // check if there is anything to send
        // prompt if none
        if (count == 0)
        {
            Toast.makeText(this, getString(R.string.error_nothing_available_to_upload), Toast.LENGTH_LONG).show();  // TODO: USE RES
            return;
        }


        // make progress indicator visible
        progress.setVisibility(View.VISIBLE);
        upload.setEnabled(false);

        // start background task
        send();
    }

    @Background(id = "upload")
    public void send()
    {
        // get new Realm instance since diff thread
        Realm realm = Realm.getDefaultInstance();

        // query all "ready to sync"
        // loop
        // send one at a time

        RealmResults<EpinurseModel> results = realm.where(reportClass)
                .equalTo("editing", false)
                .equalTo("synced", false)
                .findAll();

        for (EpinurseModel bi : results)
        {
            try {
                send(realm, bi);
                updateCount();
            }
            catch(Exception e)
            {
                e.printStackTrace();
                updateMessage(e.getMessage()+" : "+bi.getUuid());
                delay(1500);
            }
        }

        // close realm for this thread
        realm.close();

        sendDone();
    }


    public void delay(long ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch (InterruptedException e)
        {

        }
    }

    public void send(Realm realm, EpinurseModel m) throws Exception
    {
        if (m instanceof BasicInformation) {
            // send profile
            updateMessage(String.format(getString(R.string.sender_message_sending),reportName, m.getUuid()));  // TODO: USE RES
            sendProfile(realm, (BasicInformation) m);
            updateMessage(String.format(getString(R.string.sender_message_sent),reportName,m.getUuid()));   // TODO: USE RES
        }
        else
        {
            // send report
            sendReport(realm, m);
        }
    }

    @UiThread
    public void updateMessage(String txt)
    {
        message.setText(txt);
    }

    @UiThread
    public void updateCount()
    {
        count = realm.where(reportClass)
                .equalTo("editing", false)
                .equalTo("synced", false)
                .count();

        numReports.setText(String.valueOf(count));
    }

    public EpinurseModel detach(Realm realm, EpinurseModel o)
    {
        // cannot send managed realm objects these are proxies not the actual
        EpinurseModel unmanaged = realm.copyFromRealm(o);
        return unmanaged;
    }


    public void sendReport(Realm realm, EpinurseModel model) throws Exception
    {
        try {

            // check if the associated profile has been sent
            BasicInformation profile = realm.where(BasicInformation.class).equalTo("uuid", model.getOwnerUuid()).findFirst();
            String patientId = null;
            if (profile!=null)
            {
                if (!profile.isSynced()) {
                    updateMessage(String.format(getString(R.string.sender_message_sending_profile),profile.getUuid()));   // TODO: USE RES
                    sendProfile(realm, profile);
                    updateMessage(String.format(getString(R.string.sender_message_profile_sent),profile.getUuid(), patientId));  // TODO: USE RES
                    delay(500);
                }

                patientId = profile.getPatientId();
            }

            updateMessage(String.format(getString(R.string.sender_message_sending_report),reportName, model.getUuid()));  // TODO: USE RES

            // create a detached copy that will convert to JSON properly
            model = detach(realm, model);

            sender.submitReport(model, patientId);

            // TODO: send associated images to this report/profile if any
            sendAssociatedImages(realm, model.getClass().getSimpleName(), model.getUuid());

            model.setEditing(false);
            model.setSynced(true);

            // update object state
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();



            updateMessage(String.format(getString(R.string.sender_message_report_sent),reportName,model.getUuid()));
            delay(500);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(String.format(getString(R.string.error_unable_to_send_report),e.getMessage()));  // TODO: USE RES
        }
    }

    public String sendProfile(Realm realm, BasicInformation model) throws Exception
    {
        try {
            // create a detached copy that will convert to JSON properly
            model = (BasicInformation) detach(realm, model);

            String patientId = sender.submitProfile(model);

            // TODO: send associated images to this report/profile if any
            sendAssociatedImages(realm,"BasicInformation", model.getUuid());

            model.setPatientId(patientId);
            model.setEditing(false);
            model.setSynced(true);


            // update object state
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();


            return patientId;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(String.format(getString(R.string.error_unable_to_send_profile),e.getMessage()));  // TODO: USE RES
        }
    }


    public void sendAssociatedImages(Realm realm, String reportClassName, String reportUuid) throws Exception
    {
        System.out.println("Getting non-synced images for report "+reportClassName+" "+reportUuid);

        // load imageentry for this report and report's UUID
        RealmResults<ImageEntry> list = realm.where(ImageEntry.class)
                .equalTo("reportType", reportClassName)
                .equalTo("reportUuid", reportUuid)
                .equalTo("synced", false)
                .findAll();

        List<ImageEntry> detachedList = realm.copyFromRealm(list);

        for (ImageEntry entry : detachedList)
        {
            if (!entry.isSynced()) {
                updateMessage(String.format(getString(R.string.sender_message_sending_photo),entry.getUuid()));   // TODO: USE RES

                sender.uploadPhoto(entry);

                // update object state
                entry.setSynced(true);

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(entry);
                realm.commitTransaction();

                updateMessage(String.format(getString(R.string.sender_message_photo_sent),entry.getUuid()));
                delay(500);
            }
        }
    }



    @UiThread
    public void sendDone()
    {
        // change button to say upload
        // enable touch on recyclerView
        // make progress indicator invisible
        progress.setVisibility(View.INVISIBLE);
        upload.setEnabled(true);
    }
}
