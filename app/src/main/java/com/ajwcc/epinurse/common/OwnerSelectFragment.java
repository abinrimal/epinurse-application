package com.ajwcc.epinurse.common;


import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.basicinformation.ui.BasicInformationSelectActivity_;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;
import com.ajwcc.epinurse.common.utils.EpinurseModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import io.realm.Realm;


@EFragment(R.layout.fragment_select_owner)
public class OwnerSelectFragment extends BaseEpinurseFragment {

    @FragmentArg
    public BaseEpinurseSelectActivity.QueryFilter filter;

    @ViewById
    Button button;

    @ViewById
    Button delete;

    @ViewById
    View profile;

    @ViewById
    View prompt;


    Realm realm;

    @AfterViews
    public void init()
    {
        button.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);

        realm = Realm.getDefaultInstance();
    }

    @Click(R.id.selectProfile)
    public void selectOwner()
    {
        // opens the basic owner select screen
        BasicInformationSelectActivity_.intent(this)
                                        .selectMode(true)
                                        .filter(filter)
                                        .startForResult(0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("INTENT: "+data);

        // add to model and mapModelToViews
        if (data!=null)
        {
            String uuid = data.getStringExtra("uuid");

            EpinurseModel model = (EpinurseModel) getModel();
            model.setOwnerUuid(uuid);

            // load the realm stuff
            mapModelToViews();

        }
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");


    // nothing to map
    public void mapModelToViews()
    {
        EpinurseModel model = (EpinurseModel) getModel();
        if (model.getOwnerUuid()!=null)
        {
            // load via realm
            BasicInformation b = realm.where(BasicInformation.class).equalTo("uuid", model.getOwnerUuid()).findFirst();


            if (b!=null) {
                prompt.setVisibility(View.GONE);
                profile.setVisibility(View.VISIBLE);

                // load UI with realm data
                View itemView = getView();
                TextView name = itemView.findViewById(R.id.owner);
                TextView age = itemView.findViewById(R.id.age);
                TextView sex = itemView.findViewById(R.id.sex);
                TextView status = itemView.findViewById(R.id.status);
                TextView createdAt = itemView.findViewById(R.id.createdAt);

                name.setText(b.getLastName() + ", " + b.getFirstName());

                if (b.getAge() != null) {
                    age.setText(String.valueOf(b.getAge()));
                }

                if (b.getSex() != null) {
                    sex.setText(b.getSex() == 0 ? "M" : "F");  // TODO: USE RES
                }
                if (b.isEditing()) {
                    status.setText(getResources().getString(R.string.status_editing));
                } else {
                    if (b.isSynced()) {
                        status.setText(getResources().getString(R.string.status_synced));
                    } else {
                        status.setText(getResources().getString(R.string.status_ready_to_sync));
                    }
                }

                createdAt.setText(sdf.format(b.getCreatedAt()));
            }
            else
            {
                // potentially the user had been deleted before uploading it and resync'd
                // this will force the user to select again??
                model.setOwnerUuid(null);
            }

        }

    }

    public void mapViewsToModel()
    {
        saveModel();
    }
}
