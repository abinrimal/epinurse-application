package com.ajwcc.epinurse.common.image;

import com.ajwcc.epinurse.common.utils.network.ExcludeFromJson;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ImageEntry extends RealmObject {
    @PrimaryKey
    private String uuid;

    @ExcludeFromJson
    private boolean synced;


    @ExcludeFromJson
    private String pathOriginal; // full res

    @ExcludeFromJson
    private String pathCrop; // ??


    private Date createdAt;

    // report of photo
    private String reportType;
    private String reportUuid;

    // comment
    private String remarks;

    // geo location
    private Double latitude;
    private Double longitude;
    private String geocodedLocation;

    // dimensions
    private int croppedWidth;
    private int croppedHeight;

    // filesizes
    private long croppedFileSize;
    private long origFileSize;

    @Override
    public String toString() {
        return "ImageEntry{" +
                "uuid='" + uuid + '\'' +
                ", synced=" + synced +
                ", createdAt=" + createdAt +
                ", pathOriginal='" + pathOriginal + '\'' +
                ", pathCrop='" + pathCrop + '\'' +
                ", reportType='" + reportType + '\'' +
                ", reportUuid='" + reportUuid + '\'' +
                ", remarks='" + remarks + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", geocodedLocation='" + geocodedLocation + '\'' +
                ", croppedWidth=" + croppedWidth +
                ", croppedHeight=" + croppedHeight +
                ", croppedFileSize=" + croppedFileSize +
                ", origFileSize=" + origFileSize +
                '}';
    }

    public long getCroppedFileSize() {
        return croppedFileSize;
    }

    public void setCroppedFileSize(long croppedFileSize) {
        this.croppedFileSize = croppedFileSize;
    }

    public long getOrigFileSize() {
        return origFileSize;
    }

    public void setOrigFileSize(long origFileSize) {
        this.origFileSize = origFileSize;
    }

    public int getCroppedWidth() {
        return croppedWidth;
    }

    public void setCroppedWidth(int croppedWidth) {
        this.croppedWidth = croppedWidth;
    }

    public int getCroppedHeight() {
        return croppedHeight;
    }

    public void setCroppedHeight(int croppedHeight) {
        this.croppedHeight = croppedHeight;
    }

    public String getGeocodedLocation() {
        return geocodedLocation;
    }

    public void setGeocodedLocation(String geocodedLocation) {
        this.geocodedLocation = geocodedLocation;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrigPath() {
        return pathOriginal;
    }

    public void setPathOriginal(String pathOriginal) {
        this.pathOriginal = pathOriginal;
    }

    public String getCroppedPath() {
        return pathCrop;
    }

    public void setPathCrop(String pathCrop) {
        this.pathCrop = pathCrop;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportUuid() {
        return reportUuid;
    }

    public void setReportUuid(String reportUuid) {
        this.reportUuid = reportUuid;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
