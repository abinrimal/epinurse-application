package com.ajwcc.epinurse.common.utils;

import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePickerFragment extends android.support.v4.app.DialogFragment  {


    TimePickerDialog.OnTimeSetListener listener;

    public TimePickerFragment() {
        // Required empty public constructor
    }

    public void setOnTimeSetListener(TimePickerDialog.OnTimeSetListener listener)
    {
        this.listener = listener;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker

        String format = getArguments().getString("format");
        timeFormatter = new SimpleDateFormat(format!=null ? format : "HH:mm");

        String timeString = getArguments().getString("time");

        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        try {
            Date date = timeFormatter.parse(timeString);
            calendar.setTime(date);
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), listener, hour, minute,
                true);
    }

    SimpleDateFormat timeFormatter;

    public String getFormattedTimeString(int hour, int minute)
    {
        Calendar c =  Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);

        return timeFormatter.format(c.getTime());
    }
}