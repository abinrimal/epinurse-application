package com.ajwcc.epinurse.common.image;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.epinurse.common.utils.EpinurseModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

@EFragment(R.layout.fragment_image_entry_select)
public class ImageListFragment extends BaseEpinurseFragment
{
    @ViewById(R.id.recyclerView)
    public RecyclerView recyclerView;

    @AfterViews
    public void init()
    {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);


    }

    public void mapModelToViews()
    {
        recyclerView.setAdapter(createAdapter());
    }

    public void mapViewsToModel()
    {

    }

    @Click(R.id.addPhoto)
    public void addPhoto()
    {
        EpinurseModel model = (EpinurseModel) getModel();

        Class c = model.getClass();

        // open image capture activity
        ImageCaptureActivity_.intent(this)
                             .reportType(c.getSimpleName())
                             .reportUuid(model.getUuid())
                             .startForResult(1);
    }


    // triggered from adapter
    public void openEditor(View view)
    {
        // open image capture activity in edit mode
            // imageEntry will be tagged in the view
        ImageEntry entry = (ImageEntry) view.getTag();

        // open image capture activity
        ImageCaptureActivity_.intent(this)
                .reportType(entry.getReportType())
                .imageUuid(entry.getUuid())
                .startForResult(1);

    }

    public void deleteRow(View view)
    {
        // NOTE: this is currently an immediate delete

        ImageEntry entry = (ImageEntry) view.getTag();
        String croppedPath = entry.getCroppedPath();
        String origPath = entry.getOrigPath();


        // delete corresponding image files of this entry from the file system
        getRealm().beginTransaction();
        entry.deleteFromRealm();
        getRealm().commitTransaction();


        // remove from the list
        try {
            File f = new File(croppedPath);
            System.out.println("Deleted crop = " + f.delete());

            File f2 = new File(origPath);
            System.out.println("Deleted original = " + f2.delete());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


    }


    public RealmResults getResults()
    {
        EpinurseModel model = (EpinurseModel) getModel();
        Class c = model.getClass();

        System.out.println("Getting images for "+c.getSimpleName());

        // load imageentry for this report and report's UUID
        RealmResults<ImageEntry> list = getRealm().where(ImageEntry.class)
                                                  .equalTo("reportType", c.getSimpleName())
                                                  .equalTo("reportUuid", model.getUuid())
                                                  .findAll();
        return list;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // MAKE SURE UNCOMMENT THIS IF USED IN A FRAGMENT
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1) {
            // check if newPhoto was taken?
            System.out.println("Back from camera");
        }
    }


    public RealmRecyclerViewAdapter createAdapter()
    {
        return new ImageListAdapter(this, getResults(), true);
    }


}
