package com.ajwcc.epinurse.common.image;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;
import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;

import io.realm.OrderedRealmCollection;
import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;

public class ImageListAdapter extends RealmRecyclerViewAdapter<RealmObject, ImageListAdapter.ViewHolder> {
    ImageListFragment context;
    int rowLayoutFile = R.layout.row_image;


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView remarks;
        TextView location;
        TextView createdAt;

        Button editAction;
        Button deleteAction;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            remarks = itemView.findViewById(R.id.remarks);
            location = itemView.findViewById(R.id.location);
            createdAt = itemView.findViewById(R.id.createdAt);

            editAction = itemView.findViewById(R.id.button);
            deleteAction = itemView.findViewById(R.id.delete);
        }
    }


    View.OnClickListener editListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // edit
            context.openEditor(view);
        }
    };

    View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // delete
            context.deleteRow(view);
        }
    };


    public ImageListAdapter(ImageListFragment context, @Nullable OrderedRealmCollection data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = context.getLayoutInflater().inflate(rowLayoutFile, viewGroup, false);  // VERY IMPORTANT TO USE THIS STYLE
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        ImageEntry b = (ImageEntry) getItem(i);

        if (b.getCroppedPath()!=null) {

            Picasso.get().cancelRequest(viewHolder.image);
            Picasso.get()
                    .load(new File(b.getCroppedPath())).resize(200, 200)
                    .centerInside()
                    .onlyScaleDown()
                    .into(viewHolder.image);
        } else {
            viewHolder.image.setImageBitmap(null);
        }



        viewHolder.remarks.setText(b.getRemarks());
        viewHolder.location.setText(b.getGeocodedLocation());
        viewHolder.createdAt.setText(sdf.format(b.getCreatedAt()));

        viewHolder.editAction.setOnClickListener(editListener);
        viewHolder.editAction.setTag(b);

        viewHolder.deleteAction.setOnClickListener(deleteListener);
        viewHolder.deleteAction.setTag(b);

    }
}