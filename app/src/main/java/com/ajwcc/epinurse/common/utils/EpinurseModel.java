package com.ajwcc.epinurse.common.utils;

import java.util.Date;

import io.realm.RealmModel;

/**
 * Created by logan on 09/12/2017.
 */

public interface EpinurseModel extends RealmModel {
    public void setUuid(String s);
    public String getUuid();

    public boolean isEditing();
    public void setEditing(boolean editing);

    public boolean isSynced();
    public void setSynced(boolean synced);

    public Date getCreatedAt();
    public void setCreatedAt(Date date);

    public String getOwnerUuid();
    public void setOwnerUuid(String uuid);

}
