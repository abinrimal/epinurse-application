package com.ajwcc.epinurse.common;


import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.network.ShineSender;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;


@EFragment(R.layout.fragment_common_save)
public class SaveFragment extends BaseEpinurseFragment {

    public SaveFragment() {
        // Required empty public constructor
    }


    // TODO: should add a discard option
        // need to prompt data loss


    // open the time selector
    @Click(R.id.save)
    public void save()
    {
        try {

            boolean valid = validateModel();

            if (valid) {



                saveCompletedModel();
                getActivity().finish();  // close here
            }
        }
        catch(Exception e)
        {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // nothing to map
    public void mapModelToViews()
    {}

    public void mapViewsToModel()
    {}
}
