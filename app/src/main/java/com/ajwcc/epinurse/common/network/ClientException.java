package com.ajwcc.epinurse.common.network;

/**
 * Created by logan on 02/07/2017.
 */

public class ClientException extends RuntimeException {

    // this is reserved for Exceptions related to internet being off, timeouts, etc



    public ClientException() {
    }

    public ClientException(String detailMessage) {
        super(detailMessage);
    }

    public ClientException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ClientException(Throwable throwable) {
        super(throwable);
    }
}
