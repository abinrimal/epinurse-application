package com.ajwcc.epinurse.common.image;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.androidannotations.annotations.EBean;

import java.util.List;
import java.util.Locale;

@EBean
public class LocationReader
{
    private static int UPDATE_INTERVAL = 60000; // 1 min
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    private Context context;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    private Geocoder mGeocoder;

    public LocationReader(Context context)
    {
        this.context = context;

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            mGeocoder = new Geocoder(context, Locale.getDefault());

            mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location result : locationResult.getLocations())
                {
                    // Update UI with location data
                    // ...
                    System.out.println("location callback: "+result.getLatitude()+", "+result.getLongitude());

                    processLocation(result);

                }
            };
        };
        }


    // put code to indicate that location is off and must be turned on
    // background receiver may be needed for this



    public void start()
    {
        System.out.println("START LOCATION READER");
        try {


            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);

            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);

        }
        catch(SecurityException e)
        {
            e.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private void processLocation(Location result) {
        List<Address> list = null;
        String address = result.getLatitude()+", "+result.getLongitude();

        try {
            list = mGeocoder.getFromLocation(result.getLatitude(), result.getLongitude(), 1);

            if (list!=null && list.size()>0) {
                Address a = list.get(0);
                address = a.getAddressLine(0);
            }
            else
            {
                System.out.println("No Geocoder result");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.error_error_geocoding), Toast.LENGTH_LONG).show();
        }

        if (listener!=null)
        {
            listener.newLocation(result.getLatitude(), result.getLongitude(), address);
        }

    }


    public void lastLocation()
    {

        try {
            Task<Location> t = mFusedLocationClient.getLastLocation();
            t.addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location result = task.getResult();

                    System.out.println("TASK: " + result);
                    processLocation(result);
                }
            });
        }
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void stop()
    {
        System.out.println("STOP LOCATION READER");
        if (mLocationCallback!=null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }


    public interface LocationReaderListener
    {
        public void newLocation(double latitude, double longitude, String address);
    }


    LocationReaderListener listener;

    public void setLocationReaderListener(LocationReaderListener l)
    {
        listener = l;
    }


    public boolean isLocationEnabled()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // This is new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else
            {
            // This is Deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(),
                                             Settings.Secure.LOCATION_MODE,
                                             Settings.Secure.LOCATION_MODE_OFF);
            return  (mode != Settings.Secure.LOCATION_MODE_OFF);

        }
    }

}
