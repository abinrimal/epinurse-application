package com.ajwcc.epinurse.common;

import android.content.Context;

import com.ajwcc.epinurse.R;
import com.ajwcc.util.ui.validation.ValidationHandler;

public class BaseValidator
{
    protected Context context;

    public BaseValidator(Context c)
    {
        context = c;
    }

    // MODEL VALIDATION

    public void validateNonNullField(Object o, int page, int viewId, String fieldName)
    {
        if (o==null)
        {
            if (!fieldName.startsWith("@drawable")) {
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),fieldName), page, viewId);  // TODO: USE RES
            }
            else
            {
                throw new ValidationHandler.ValidationException(context.getResources().getString(R.string.error_must_have_value), page, viewId);   // TODO: USE RES
            }
        }

        if (o instanceof String)
        {
            String s = (String) o;
            if (s.isEmpty())
            {
                if (!fieldName.startsWith("@drawable")) {
                    throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),fieldName), page, viewId);  // TODO: USE RES
                }
                else
                {
                    throw new ValidationHandler.ValidationException(context.getResources().getString(R.string.error_must_have_value), page, viewId);   // TODO: USE RES
                }
            }
        }
    }

    public void validateNonNullSpecifyField(Integer selectable, int specifyPosition, String o, int page, int viewId, String fieldName)
    {
        // at this point the selectable should already be non-null
        //validateNonNullField(selectable, page, viewId);

        if (selectable==specifyPosition)
        {
            validateNonNullField(o, page, viewId, fieldName);
        }
    }






}
