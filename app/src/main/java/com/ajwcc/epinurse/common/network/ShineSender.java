package com.ajwcc.epinurse.common.network;


import android.content.Context;
import android.content.SharedPreferences;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformationJsonSerializer;
import com.ajwcc.epinurse.common.image.ImageEntry;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursing;
import com.ajwcc.epinurse.communitynursing.gen.CommunityNursingJsonSerializer;
import com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessment;
import com.ajwcc.epinurse.employeehealthassessment.gen.EmployeeHealthAssessmentJsonSerializer;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChild;
import com.ajwcc.epinurse.motherandchild.gen.MotherAndChildJsonSerializer;
import com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessment;
import com.ajwcc.epinurse.schoolhealthassessment.gen.SchoolHealthAssessmentJsonSerializer;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessment;
import com.ajwcc.epinurse.studenthealthassessment.gen.StudentHealthAssessmentJsonSerializer;
import com.ajwcc.epinurse.common.utils.EpinurseModel;
import com.ajwcc.epinurse.common.utils.network.AnnotationExclusionStrategy;
import com.ajwcc.util.network.AndroidNetUtils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.androidannotations.annotations.EBean;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

@EBean
public class ShineSender
{
	public String baseUrl = "http://202.125.102.203/shine-epinurse-dev/api/";
	private String SHINE_KEY = "3670407151512120101064700";
	private String SHINE_SECRET = "3670407151512120101064700";


	private String FACILITY_ID = "facility_id";
	private String USER_ID = "user_id";
	private String LAST_SYNC = "last_sync";

	private String PREFS = "ShinePrefs";

	private Context context;

	public interface EpinurseNet {

		// login
		@POST("users/login")
		Call<HashMap<String, Object>> login(@Header("ShineKey") String key,
											@Header("ShineSecret") String secret,
											@Body Map<String, Object> cred);

		// submit
		@POST("epinurse/submit")
		Call<ServerReply> submit(@Header("ShineKey") String key,
								@Header("ShineSecret") String secret,
								@Body Map<String, Object> data);

		@Multipart
		@POST("epinurse/uploadPhoto")
		Call<ServerReply> uploadPhoto(@Header("ShineKey") String key,
									  @Header("ShineSecret") String secret,
									  @Part MultipartBody.Part jsonPart,
									  @Part MultipartBody.Part filePart);


		// get patients
		@GET("epinurse/getpatients")
		Call<ProfileListServerReply> getPatients(@Header("ShineKey") String key,
									  @Header("ShineSecret") String secret,
									  @Query("facility_id") String facilityId,
									  @Query("last_sync") String lastSync);

		// get patient count
		@GET("epinurse/getpatientcount")
		Call<ProfileCountReply> getPatientCount(@Header("ShineKey") String key,
									  @Header("ShineSecret") String secret,
									  @Query("facility_id") String facilityId,
									  @Query("last_sync") String lastSync);


		// submit photos

	}

	public ShineSender(Context context)
	{
		this.context = context;

	}

	public void login(String email, String password) throws Exception
	{
		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));  // TODO: USE RES
		}

		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);

		HashMap<String, Object> cred =  new HashMap<>();

		cred.put("email", email);
		cred.put("password", password);

		Call<HashMap<String, Object>> call = service.login(SHINE_KEY, SHINE_SECRET, cred);

		Response<HashMap<String, Object>> response = call.execute();

		checkResponse(response);

		Map map = (Map) response.body();
		Map<String, Object> data  = (Map<String, Object>) map.get("data");
		List<Map<String, Object>>  facilityUser = (List<Map<String, Object>>) data.get("facility_user");

		Map<String, Object> facilityEntry = facilityUser.get(0);

		String facilityId = (String) facilityEntry.get("facility_id");
		String userId = (String) facilityEntry.get("user_id");

		System.out.println("Facility ID = "+facilityId);
		System.out.println("User ID = "+userId);

		// SAVE IDS TO SHARED PREFS
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putString(FACILITY_ID, facilityId);
		editor.putString(USER_ID, userId);
		editor.apply();

	}

	// NOTE: you will also need to send all the images with this report

	public String submitProfile(BasicInformation model) throws Exception
	{
		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));   // TODO: USE RES
		}


		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);

		Map<String, Object> data = wrapDataForSending(model, null);

		System.out.println("SENDING");
		Call<ServerReply> call = service.submit(SHINE_KEY, SHINE_SECRET, data);


		Response<ServerReply> response = call.execute();

		System.out.println("RESPONSE RECIEVED");

		checkResponse(response);

		ServerReply body = response.body();

		// return the patientID from data
		String patientId = body.getPatientId();
		return patientId;
	}

	public String submitReport(EpinurseModel model, String patientId) throws Exception
	{
		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));    // TODO: USE RES
		}


		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);

		Map<String, Object> data = wrapDataForSending(model, patientId);

		Call<ServerReply> call = service.submit(SHINE_KEY, SHINE_SECRET, data);

		Response<ServerReply> response = call.execute();

		checkResponse(response);

		ServerReply body = response.body();

		// not sure what else is needed here
			// maybe just message
		return body.getMessage();
	}

	public void clearLastSync()
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPreferences.edit();

		edit.remove(LAST_SYNC);
		edit.apply();
	}

	public long getNewProfileCount() throws Exception
	{
		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));   // TODO: USE RES
		}

		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);


		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		String facilityId = sharedPreferences.getString(FACILITY_ID, null);
		String lastSync = sharedPreferences.getString(LAST_SYNC, null);

		Call<ProfileCountReply> call = service.getPatientCount(SHINE_KEY, SHINE_SECRET, facilityId, lastSync);

		Response<ProfileCountReply> response = call.execute();

		checkResponse(response);

		ProfileCountReply body = response.body();

		return body.getData().getCount();
	}


	public List<BasicInformation> getNewProfiles() throws Exception
	{
		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));    // TODO: USE RES
		}

		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);


		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		String facilityId = sharedPreferences.getString(FACILITY_ID, null);
		String lastSync = sharedPreferences.getString(LAST_SYNC, null);

		Call<ProfileListServerReply> call = service.getPatients(SHINE_KEY, SHINE_SECRET, facilityId, lastSync);

		Response<ProfileListServerReply> response = call.execute();

		checkResponse(response);

		ProfileListServerReply body = response.body();

		List<BasicInformation> list = body.getData();  // need to doublecheck if deserialization works correct

		SharedPreferences.Editor edit = sharedPreferences.edit();
		edit.putString(LAST_SYNC, body.getServerDatetime());
		edit.apply();


		return list;
	}



	public boolean loginNeeded()
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);

		if (sharedPreferences.contains(FACILITY_ID) && sharedPreferences.contains(USER_ID))
		{
			return false;
		}

		return true;
	}

	public void clearCredentials()
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.clear();
		editor.apply();
	}

	// UTILS

	public void checkResponse(Response response)
	{
		if (response.code()!=200)
		{
			if (response.code()==400) {
				// extract errorBody
				ServerErrorReply errorReply = null;
				try
				{
					errorReply = gson.fromJson(response.errorBody().string(), ServerErrorReply.class);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new RuntimeException("Error: " + response.code()+" unable to deserialize errorBody");   // TODO: USE RES
				}

				throw new com.ajwcc.epinurse.common.network.ServerException(errorReply);
			}
			else
			{
				if (response.body()!=null) {
					System.out.println(response.body().toString());
				}
				throw new RuntimeException("Error: " + response.code());
			}
		}

		if (response.body()==null)
		{
			throw new RuntimeException("Error: No returned data");   // TODO: USE RES
		}
	}


	public Retrofit getRetrofit()
	{
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		OkHttpClient client = new OkHttpClient.Builder()
//				.addInterceptor(interceptor)
				.build();


		Retrofit retrofit = new Retrofit.Builder()
				.client(client)
				.baseUrl(baseUrl)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();

		return retrofit;
	}


	private static Gson gson =  new GsonBuilder().setPrettyPrinting()
			.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
			.setDateFormat("yyyy-MM-dd HH:mm:ss")
			.serializeNulls()		// disable this on the final
			.setExclusionStrategies(new AnnotationExclusionStrategy())  // do not send fields marked - ONLY NEEDED IF NO SPECIAL SERIALIZERS

			.registerTypeAdapter(BasicInformation.class,        new BasicInformationJsonSerializer())
			.registerTypeAdapter(StudentHealthAssessment.class, new StudentHealthAssessmentJsonSerializer())
			.registerTypeAdapter(EmployeeHealthAssessment.class,new EmployeeHealthAssessmentJsonSerializer())
			.registerTypeAdapter(CommunityNursing.class,        new CommunityNursingJsonSerializer())
			.registerTypeAdapter(MotherAndChild.class,          new MotherAndChildJsonSerializer())
			.registerTypeAdapter(SchoolHealthAssessment.class,  new SchoolHealthAssessmentJsonSerializer())
			.create();



	public static Gson getGson()
	{
		return gson;
	}

	private LinkedHashMap<String, Object> wrapDataForSending(EpinurseModel o, String patientId)
	{
		if (loginNeeded())
		{
			throw new ClientException(context.getResources().getString(R.string.error_no_credentials));   // TODO: USE RES
		}



		LinkedHashMap<String, Object> map = new LinkedHashMap<>();

		map.put("data", o);
		map.put("type", o.getClass().getSimpleName());

		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);

		// add facility id, user id\ from shared prefs
		// these should never be null at this point
		map.put("facility_id", sharedPreferences.getString(FACILITY_ID, ""));
		map.put("user_id", sharedPreferences.getString(USER_ID, ""));

		// need to add patient id if present
		if (patientId!=null)
		{
			map.put("patient_id", patientId);
		}

		return map;
	}


	// photo upload code
	public String uploadPhoto(ImageEntry imageEntry) throws Exception
	{

		if (!AndroidNetUtils.isNetworkAvailable(context))
		{
			throw new RuntimeException(context.getResources().getString(R.string.error_no_network_connection));    // TODO: USE RES
		}


		Retrofit retrofit = getRetrofit();
		EpinurseNet service = retrofit.create(EpinurseNet.class);


		MultipartBody.Part jsonPart = MultipartBody.Part.createFormData("json", gson.toJson(imageEntry));

		File file = new File(imageEntry.getCroppedPath());
		MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo",
																	file.getName(),
																	RequestBody.create(MediaType.parse("image/jpg"), file));

		Call<ServerReply> call = service.uploadPhoto(SHINE_KEY, SHINE_SECRET, jsonPart, photoPart);

		Response<ServerReply> response = call.execute();

		checkResponse(response);

		ServerReply body = response.body();

		// not sure what else is needed here
		// maybe just message
		return body.getMessage();
	}
}
