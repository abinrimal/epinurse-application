package com.ajwcc.epinurse.common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ajwcc.epinurse.R;
import com.ajwcc.util.ui.DatePickerFragment;

import com.ajwcc.util.ui.validation.ValidationHandler;
import com.hornet.dateconverter.DateConverter;
import com.hornet.dateconverter.DatePicker.DatePickerDialog;
import com.hornet.dateconverter.Model;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UiUtils
{
    // AGE
    public static String computeAge(String birthDateAD)
    {
        try {
            int years = 0;
            int months = 0;
            int days = 0;

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date birthDate = sdf.parse(birthDateAD);

            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());

            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);

            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;

            //if month difference is in negative then reduce years by one
            //and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }

            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }

            return String.valueOf(years);
        }
        catch(Exception e)
        {
            return "";
        }
    }



    // RADIOGROUP

    public static void disableRadioGroup(RadioGroup rg)
    {
        rg.setEnabled(false);
        for (int i = 0; i < rg.getChildCount(); i++)
        {
            rg.getChildAt(i).setEnabled(false);
        }

    }

    public static void setSelectedInRadioGroup(RadioGroup rg, int selected)
    {
        disableRadioGroup(rg);

        int id = -1;
        for (int i = 0; i < rg.getChildCount(); i++) {
            if (i == selected) {
                id = rg.getChildAt(i).getId();
                rg.getChildAt(i).setEnabled(true);
                break;
            }
        }
        rg.check(id);

    }



    // BMI MANAGEMENT


    public static void setBmi(RadioGroup bmi, EditText heightInCentimeter, EditText weightInKg, EditText bmiComputed)
    {

        String cm = heightInCentimeter.getText().toString();
        String kg = weightInKg.getText().toString();

        if (cm.isEmpty() || kg.isEmpty())
        {
            return;
        }

        try {
            double mD = Double.parseDouble(cm) / 100;
            double kgD = Double.parseDouble(kg);

            double bmiValue = (kgD / (mD * mD));

            bmiComputed.setText(String.valueOf(bmiValue));



            if (bmiValue > 30) {
                // obese
                setSelectedInRadioGroup(bmi, 3);
            } else if (bmiValue > 25) {
                // overweight
                setSelectedInRadioGroup(bmi, 2);
            } else if (bmiValue > 18.5) {
                // normal
                setSelectedInRadioGroup(bmi, 1);
            } else {
                // underweight
                setSelectedInRadioGroup(bmi, 0);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }






    // AD-NEPALI DATE MANAGEMENT

    public static void openNepaliCalendar(FragmentActivity context, EditText dateOfBirthInAd, EditText dateOfBirthInBs)
    {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                //String date = "You picked the following date: " + dayOfMonth + " " + getResources().getString(DateConverter.getNepaliMonthString(monthOfYear)) + " " + year;
                //System.out.println(date);

                dateOfBirthInBs.setText((monthOfYear+1)+"/"+dayOfMonth+"/"+year);


                System.out.println("BS YYYY: "+year);
                System.out.println("BS MM: "+monthOfYear);
                System.out.println("BS dd: "+dayOfMonth);


                // convert to AD here
                DateConverter converter = new DateConverter();

                // offset value to 1-based??
                Model engDate = converter.getEnglishDate(year, monthOfYear+1, dayOfMonth);

                // offset display value to 1-based
                String date = (engDate.getMonth()+1) + "/" +engDate.getDay()+"/"+engDate.getYear();
                dateOfBirthInAd.setText(date);

            }
        };

        try {
            String dateOfBirthInBsText = dateOfBirthInBs.getText().toString();
            String[] data = dateOfBirthInBsText.split("/");

            int month = Integer.parseInt(data[0])-1;  // offset display value to 0-based
            int day = Integer.parseInt(data[1]);
            int year = Integer.parseInt(data[2]);



            DatePickerDialog dpd = DatePickerDialog.newInstance(listener, year, month, day);
            dpd.setMaxDate(new DateConverter().getTodayNepaliDate());
            dpd.show(context.getSupportFragmentManager(), "NepaliDatePicker");;
        }
        catch (Exception e)
        {
            DatePickerDialog dpd = DatePickerDialog.newInstance(listener);

            dpd.show(context.getSupportFragmentManager(), "NepaliDatePicker");;

        }
    }


    public static void openGregorianCalendar(FragmentActivity context, EditText dateOfBirthInAd, EditText dateOfBirthInBs)
    {
        final com.ajwcc.util.ui.DatePickerFragment dialog = new DatePickerFragment();

        Bundle args = new Bundle();
        args.putString("format","MM/dd/yyyy");
        args.putString("date", dateOfBirthInAd.getText().toString());
        args.putSerializable("maxdate", new Date());

        Calendar min = Calendar.getInstance();
        min.set(Calendar.YEAR, 1913);
        min.set(Calendar.MONTH, 3);
        min.set(Calendar.DAY_OF_MONTH, 13);
        args.putSerializable("mindate",min.getTime());

        dialog.setArguments(args);



        dialog.setOnDateSetListener(new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                dateOfBirthInAd.setText(dialog.getFormattedDateString(year, month, day));

                System.out.println("AD YYYY: "+year);
                System.out.println("AD MM: "+month);
                System.out.println("AD dd: "+day);

                // convert to BS here
                DateConverter converter = new DateConverter();

                // offset display value to 1-based??
                Model nepDate = converter.getNepaliDate(year, month+1, day);

                // offset to 1-based??
                String date = (nepDate.getMonth()+1) + "/" + nepDate.getDay()+"/"+nepDate.getYear();
                dateOfBirthInBs.setText(date);
            }
        });
        dialog.show(context.getSupportFragmentManager(), "GregorianDatePicker");

    }



    public static void toggleSpecify(CompoundButton cb, EditText specify)
    {

        specify.setText("");
        if (cb.isChecked())
        {
            specify.setEnabled(true);
            specify.requestFocus();
        }
        else
        {
            specify.setEnabled(false);
            specify.clearFocus();
        }
    }


//    // UI VALIDATION
//
//    // validate EditText as not empty
//    public static void validateNonEmptyEditText(EditText et)
//    {
//        if (et.getText().toString().isEmpty())
//        {
//            throw new ValidationHandler.ValidationException("Cannot be empty");  // TODO: USE RES
//        }
//    }
//
//
//    public static void validateNonEmptyRadioGroup(RadioGroup rg)
//    {
//        for (int i = 0; i<rg.getChildCount();i++)
//        {
//            RadioButton rb = (RadioButton) rg.getChildAt(i);
//            if (rb.isChecked())
//            {
//                return;
//            }
//        }
//        throw new ValidationHandler.ValidationException("Must have a selection");   // TODO: USE RES
//    }
//
//
//    // validate RadioGroup With Specify as having a selection and a Specify value if (specify) option is used
//    public static void validateNonEmptyRadioGroupWithSpecify(RadioGroup rg, EditText specify)
//    {
//        for (int i = 0; i<rg.getChildCount();i++)
//        {
//            RadioButton rb = (RadioButton) rg.getChildAt(i);
//            if (rb.isChecked())
//            {
//                if (rb.getText().toString().toLowerCase().contains("(specify)"))   // TODO: USE RES
//                validateNonEmptyEditText(specify);
//                return;
//            }
//        }
//        throw new ValidationHandler.ValidationException("Must have a selection");   // TODO: USE RES
//    }
//
//
//    // validate CheckBox with Specify having a Specify value if checked
//    public static void validateCheckboxWithSpecify(CheckBox cb, EditText specify, int page)
//    {
//        if (cb.isChecked())
//        {
//            validateNonEmptyEditText(specify);
//            return;
//        }
//    }


}
