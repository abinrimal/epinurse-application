package com.ajwcc.epinurse.common;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;
import com.ajwcc.epinurse.common.utils.EpinurseModel;

import java.text.SimpleDateFormat;

import io.realm.OrderedRealmCollection;
import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;

public class GenericSelectAdapter extends RealmRecyclerViewAdapter<RealmObject, GenericSelectAdapter.ViewHolder> {
    BaseEpinurseSelectActivity context;
    int rowLayoutFile = R.layout.row_generic_select;


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView owner;
        TextView createdAt;
        TextView status;

        Button action;
        Button delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            owner = itemView.findViewById(R.id.owner);
            createdAt = itemView.findViewById(R.id.createdAt);
            status = itemView.findViewById(R.id.status);
            action = itemView.findViewById(R.id.button);

            delete = itemView.findViewById(R.id.delete);

            if (context.getSelectMode())
            {
                delete.setVisibility(View.GONE);
            }
        }
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.clickAction(context.getSelectMode(), view);
        }
    };

    View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.deleteAction(view);
        }
    };

    public GenericSelectAdapter(BaseEpinurseSelectActivity context, @Nullable OrderedRealmCollection data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = context.getLayoutInflater().inflate(rowLayoutFile, viewGroup, false);  // VERY IMPORTANT TO USE THIS STYLE
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        EpinurseModel b = (EpinurseModel) getItem(i);

        // owner
        try {
            BasicInformation profile = context.getProfile(b.getOwnerUuid());
            if (profile != null) {
                viewHolder.owner.setText((profile.getLastName()==null ? "?" : profile.getLastName())
                        + ", "
                        + (profile.getFirstName()==null ? "?" : profile.getFirstName()));

            }
        }
        catch(Exception e)
        { }

        // created at
        viewHolder.createdAt.setText(b.getCreatedAt().toString());



        viewHolder.createdAt.setText(sdf.format(b.getCreatedAt()));


        viewHolder.action.setOnClickListener(listener);
        viewHolder.action.setTag(b);

        viewHolder.delete.setOnClickListener(deleteListener);
        viewHolder.delete.setTag(b);


        if (b.isEditing())
        {
            viewHolder.status.setText(context.getResources().getString(R.string.status_editing));
        }
        else
        {
            if (b.isSynced())
            {
                viewHolder.status.setText(context.getResources().getString(R.string.status_synced));
            }
            else
            {
                viewHolder.status.setText(context.getResources().getString(R.string.status_ready_to_sync));
            }
        }


        String select = context.getResources().getString(R.string.adapter_select);
        String edit = context.getResources().getString(R.string.adapter_edit);

        viewHolder.action.setText(context.getSelectMode() ? select : edit);

        // TODO: DELETE

    }
}