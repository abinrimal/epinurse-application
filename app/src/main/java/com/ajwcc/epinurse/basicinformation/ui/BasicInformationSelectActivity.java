package com.ajwcc.epinurse.basicinformation.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.network.NurseLogin_;
import com.ajwcc.util.network.AndroidNetUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.realm.Case;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;


@EActivity(R.layout.activity_basic_information_select)
public class BasicInformationSelectActivity extends com.ajwcc.epinurse.basicinformation.gen.BasicInformationSelectActivity implements SearchView.OnQueryTextListener {
    @ViewById
    SearchView search;

    public RealmRecyclerViewAdapter createAdapter()
    {
        // make adapter
        BasicInformationSelectAdapter adapter = new BasicInformationSelectAdapter(this, getResults(), true);
        return adapter;
    }

    @Click(R.id.download)
    public void download() {
        // loading profiles from Shine

        if (!AndroidNetUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, getResources().getString(R.string.error_no_network_connection), Toast.LENGTH_LONG).show();  // TODO: USE RES
            return;
        }

        // open login to login
        // on success return here, onActivityResult to trigger download processs

        if (sender.loginNeeded()) {
            NurseLogin_.intent(this).closeOnSuccess(true).startForResult(2);
        } else {
            startDownload();
        }
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==2)
        {
            if (!sender.loginNeeded())
            {
                startDownload();
            }
        }
    }


    public void startDownload()
    {
        BasicInformationSyncActivity_.intent(this).start();
    }

    @AfterViews
    public void initSearchView()
    {
        search.setOnQueryTextListener(this);
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String s) {

        // reload query
        if (s.equals("")) {
            recyclerView.setAdapter(createAdapter());
        }
        else
        {
            recyclerView.setAdapter(createAdapter(new QueryFilter<BasicInformation>() {
                @Override
                public String getName() {
                    return "Filtered";
                }  // TODO: USE RES

                @Override
                public RealmQuery<BasicInformation> adjust(RealmQuery<BasicInformation> query) {

                    query = query.beginGroup()
                                 .beginsWith("firstName", s, Case.INSENSITIVE)
                                 .or()
                                 .beginsWith("lastName", s, Case.INSENSITIVE)
                                 .or()
                                 .beginsWith("patientId", s, Case.INSENSITIVE)
                                 .endGroup();

                    return query;
                }
            }));
        }
        return false;
    }

    public RealmRecyclerViewAdapter createAdapter(QueryFilter<BasicInformation> searchFilter)
    {
        // make adapter
        BasicInformationSelectAdapter adapter = new BasicInformationSelectAdapter(this, getResults(searchFilter), true);
        return adapter;
    }



    public RealmResults<BasicInformation> getResults(QueryFilter<BasicInformation> searchFilter)
    {
        // load realm results
        RealmQuery<BasicInformation> query = realm.where(BasicInformation.class);

        if (filter!=null)
        {
            query = filter.adjust(query);  // the basic filter
        }

        if (searchFilter!=null)
        {
            query = searchFilter.adjust(query);  // the basic filter
        }

        RealmResults<BasicInformation> results = query.findAll();

        results = results.sort("editing", Sort.DESCENDING, "createdAt", Sort.DESCENDING);

        //System.out.println(results);
        return results;
    }



}
