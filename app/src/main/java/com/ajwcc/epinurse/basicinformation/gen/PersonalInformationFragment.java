package com.ajwcc.epinurse.basicinformation.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_basic_information_personal_information)
public class PersonalInformationFragment extends BaseEpinurseFragment {


    public PersonalInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText firstName;

	@ViewById
	@MapToModelField
	protected EditText middleName;

	@ViewById
	@MapToModelField
	protected EditText lastName;

	@ViewById
	@MapToModelField
	protected RadioGroup sex;

	@ViewById
	@MapToModelField
	protected RadioGroup maritalStatus;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInAd;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInBs;

	@ViewById
	@MapToModelField
	protected EditText age;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
					PojoToViewMapper.setViewValue(firstName,pojo.getFirstName());
					PojoToViewMapper.setViewValue(middleName,pojo.getMiddleName());
					PojoToViewMapper.setViewValue(lastName,pojo.getLastName());
					PojoToViewMapper.setViewValue(sex,pojo.getSex());
					PojoToViewMapper.setViewValue(maritalStatus,pojo.getMaritalStatus());
					PojoToViewMapper.setViewValue(dateOfBirthInAd,pojo.getDateOfBirthInAd());
					PojoToViewMapper.setViewValue(dateOfBirthInBs,pojo.getDateOfBirthInBs());
					PojoToViewMapper.setViewValue(age,pojo.getAge());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
                	
					pojo.setFirstName((String) ViewToPojoMapper.getValueByView(firstName));
					pojo.setMiddleName((String) ViewToPojoMapper.getValueByView(middleName));
					pojo.setLastName((String) ViewToPojoMapper.getValueByView(lastName));
					pojo.setSex((Integer) ViewToPojoMapper.getValueByView(sex));
					pojo.setMaritalStatus((Integer) ViewToPojoMapper.getValueByView(maritalStatus));
					pojo.setDateOfBirthInAd((String) ViewToPojoMapper.getValueByView(dateOfBirthInAd));
					pojo.setDateOfBirthInBs((String) ViewToPojoMapper.getValueByView(dateOfBirthInBs));
					pojo.setAge((Integer) ViewToPojoMapper.getValueByView(age));

					if (save)
					{
                		System.out.println("Save to realm: BasicInformation");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			BasicInformation model = (BasicInformation) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(BasicInformation mode, boolean update)
	{
		return update;
	}
	



}
