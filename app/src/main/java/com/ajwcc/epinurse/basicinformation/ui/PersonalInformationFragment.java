package com.ajwcc.epinurse.basicinformation.ui;

import android.view.View;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.logging.SimpleFormatter;

@EFragment(R.layout.gen_fragment_basic_information_personal_information)
public class PersonalInformationFragment extends com.ajwcc.epinurse.basicinformation.gen.PersonalInformationFragment {


	@AfterViews
	public void init()
	{
		age.setEnabled(false);
	}

	@AfterTextChange(R.id.dateOfBirthInAd)   // only need this one since it gets adjusted on both dates
	public void adjustAge()
	{
		age.setText(UiUtils.computeAge(dateOfBirthInAd.getText().toString()));
	}


    @Click(R.id.dateOfBirthInAdButton)
	public void openADCalendar()
	{
		UiUtils.openGregorianCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);
	}

	@Click(R.id.dateOfBirthInBsButton)
	public void openBSCalendar()
	{
		UiUtils.openNepaliCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);

	}



}
