package com.ajwcc.epinurse.basicinformation.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_basic_information_socio_economic_information)
public class SocioEconomicInformationFragment extends BaseEpinurseFragment {


    public SocioEconomicInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText ethnicity;

	@ViewById
	@MapToModelField
	protected EditText religion;

	@ViewById
	@MapToModelField
	protected RadioGroup educationalStatus;

	@ViewById
	@MapToModelField
	protected RadioGroup occupation;

	@ViewById
	@MapToModelField
	protected EditText occupationSpecify;

	@ViewById
	@MapToModelField
	protected CheckBox incomeAgriculture;

	@ViewById
	@MapToModelField
	protected CheckBox incomeBusiness;

	@ViewById
	@MapToModelField
	protected CheckBox incomeService;

	@ViewById
	@MapToModelField
	protected CheckBox incomeLabor;

	@ViewById
	@MapToModelField
	protected CheckBox incomeRemittance;

	@ViewById
	@MapToModelField
	protected CheckBox incomeOthers;

	@ViewById
	@MapToModelField
	protected EditText incomeSpecify;

	@ViewById
	@MapToModelField
	protected RadioGroup foodSufficiency;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
					PojoToViewMapper.setViewValue(ethnicity,pojo.getEthnicity());
					PojoToViewMapper.setViewValue(religion,pojo.getReligion());
					PojoToViewMapper.setViewValue(educationalStatus,pojo.getEducationalStatus());
					PojoToViewMapper.setViewValue(occupation,pojo.getOccupation());
					PojoToViewMapper.setViewValue(occupationSpecify,pojo.getOccupationSpecify());
					PojoToViewMapper.setViewValue(incomeAgriculture,pojo.getIncomeAgriculture());
					PojoToViewMapper.setViewValue(incomeBusiness,pojo.getIncomeBusiness());
					PojoToViewMapper.setViewValue(incomeService,pojo.getIncomeService());
					PojoToViewMapper.setViewValue(incomeLabor,pojo.getIncomeLabor());
					PojoToViewMapper.setViewValue(incomeRemittance,pojo.getIncomeRemittance());
					PojoToViewMapper.setViewValue(incomeOthers,pojo.getIncomeOthers());
					PojoToViewMapper.setViewValue(incomeSpecify,pojo.getIncomeSpecify());
					PojoToViewMapper.setViewValue(foodSufficiency,pojo.getFoodSufficiency());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
                	
					pojo.setEthnicity((String) ViewToPojoMapper.getValueByView(ethnicity));
					pojo.setReligion((String) ViewToPojoMapper.getValueByView(religion));
					pojo.setEducationalStatus((Integer) ViewToPojoMapper.getValueByView(educationalStatus));
					pojo.setOccupation((Integer) ViewToPojoMapper.getValueByView(occupation));
					pojo.setOccupationSpecify((String) ViewToPojoMapper.getValueByView(occupationSpecify));
					pojo.setIncomeAgriculture((Integer) ViewToPojoMapper.getValueByView(incomeAgriculture));
					pojo.setIncomeBusiness((Integer) ViewToPojoMapper.getValueByView(incomeBusiness));
					pojo.setIncomeService((Integer) ViewToPojoMapper.getValueByView(incomeService));
					pojo.setIncomeLabor((Integer) ViewToPojoMapper.getValueByView(incomeLabor));
					pojo.setIncomeRemittance((Integer) ViewToPojoMapper.getValueByView(incomeRemittance));
					pojo.setIncomeOthers((Integer) ViewToPojoMapper.getValueByView(incomeOthers));
					pojo.setIncomeSpecify((String) ViewToPojoMapper.getValueByView(incomeSpecify));
					pojo.setFoodSufficiency((Integer) ViewToPojoMapper.getValueByView(foodSufficiency));

					if (save)
					{
                		System.out.println("Save to realm: BasicInformation");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
	@CheckedChange({R.id.occupation5,R.id.incomeOthers})
	public void toggleSpecify(android.widget.CompoundButton view)
	{
		switch(view.getId())
		{
			case R.id.occupation5:
				 UiUtils.toggleSpecify(view, occupationSpecify);
				break;
			case R.id.incomeOthers:
				 UiUtils.toggleSpecify(view, incomeSpecify);
				break;

		}
	}

	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			BasicInformation model = (BasicInformation) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(BasicInformation mode, boolean update)
	{
		return update;
	}
	



}
