package com.ajwcc.epinurse.basicinformation.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_basic_information_household_information)
public class HouseholdInformationFragment extends BaseEpinurseFragment {


    public HouseholdInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText familyID;

	@ViewById
	@MapToModelField
	protected RadioGroup typeOfFamily;

	@ViewById
	@MapToModelField
	protected EditText houseNumber;

	@ViewById
	@MapToModelField
	protected EditText wardNumber;

	@ViewById
	@MapToModelField
	protected EditText municipality;

	@ViewById
	@MapToModelField
	protected EditText district;

	@ViewById
	@MapToModelField
	protected EditText country;

	@ViewById
	@MapToModelField
	protected EditText zipcode;

	@ViewById
	@MapToModelField
	protected EditText fathersName;

	@ViewById
	@MapToModelField
	protected CheckBox fatherLate;

	@ViewById
	@MapToModelField
	protected EditText mothersName;

	@ViewById
	@MapToModelField
	protected CheckBox motherLate;

	@ViewById
	@MapToModelField
	protected EditText localGuardiansName;

	@ViewById
	@MapToModelField
	protected EditText contactOfParentGuardian;

	@ViewById
	@MapToModelField
	protected EditText numberMale;

	@ViewById
	@MapToModelField
	protected EditText numberFemale;

	@ViewById
	@MapToModelField
	protected EditText numberOthers;

	@ViewById
	@MapToModelField
	protected EditText numberMaleUnder5;

	@ViewById
	@MapToModelField
	protected EditText numberFemaleUnder5;

	@ViewById
	@MapToModelField
	protected EditText numberMaleAbove60;

	@ViewById
	@MapToModelField
	protected EditText numberFemaleAbove60;

	@ViewById
	@MapToModelField
	protected EditText numberDisabled;

	@ViewById
	@MapToModelField
	protected EditText numberPregnant;

	@ViewById
	@MapToModelField
	protected EditText numberLactatingMothers;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
					PojoToViewMapper.setViewValue(familyID,pojo.getFamilyID());
					PojoToViewMapper.setViewValue(typeOfFamily,pojo.getTypeOfFamily());
					PojoToViewMapper.setViewValue(houseNumber,pojo.getHouseNumber());
					PojoToViewMapper.setViewValue(wardNumber,pojo.getWardNumber());
					PojoToViewMapper.setViewValue(municipality,pojo.getMunicipality());
					PojoToViewMapper.setViewValue(district,pojo.getDistrict());
					PojoToViewMapper.setViewValue(country,pojo.getCountry());
					PojoToViewMapper.setViewValue(zipcode,pojo.getZipcode());
					PojoToViewMapper.setViewValue(fathersName,pojo.getFathersName());
					PojoToViewMapper.setViewValue(fatherLate,pojo.getFatherLate());
					PojoToViewMapper.setViewValue(mothersName,pojo.getMothersName());
					PojoToViewMapper.setViewValue(motherLate,pojo.getMotherLate());
					PojoToViewMapper.setViewValue(localGuardiansName,pojo.getLocalGuardiansName());
					PojoToViewMapper.setViewValue(contactOfParentGuardian,pojo.getContactOfParentGuardian());
					PojoToViewMapper.setViewValue(numberMale,pojo.getNumberMale());
					PojoToViewMapper.setViewValue(numberFemale,pojo.getNumberFemale());
					PojoToViewMapper.setViewValue(numberOthers,pojo.getNumberOthers());
					PojoToViewMapper.setViewValue(numberMaleUnder5,pojo.getNumberMaleUnder5());
					PojoToViewMapper.setViewValue(numberFemaleUnder5,pojo.getNumberFemaleUnder5());
					PojoToViewMapper.setViewValue(numberMaleAbove60,pojo.getNumberMaleAbove60());
					PojoToViewMapper.setViewValue(numberFemaleAbove60,pojo.getNumberFemaleAbove60());
					PojoToViewMapper.setViewValue(numberDisabled,pojo.getNumberDisabled());
					PojoToViewMapper.setViewValue(numberPregnant,pojo.getNumberPregnant());
					PojoToViewMapper.setViewValue(numberLactatingMothers,pojo.getNumberLactatingMothers());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: BasicInformation");
                	BasicInformation pojo = (BasicInformation) getModel();
                	
					pojo.setFamilyID((String) ViewToPojoMapper.getValueByView(familyID));
					pojo.setTypeOfFamily((Integer) ViewToPojoMapper.getValueByView(typeOfFamily));
					pojo.setHouseNumber((String) ViewToPojoMapper.getValueByView(houseNumber));
					pojo.setWardNumber((String) ViewToPojoMapper.getValueByView(wardNumber));
					pojo.setMunicipality((String) ViewToPojoMapper.getValueByView(municipality));
					pojo.setDistrict((String) ViewToPojoMapper.getValueByView(district));
					pojo.setCountry((String) ViewToPojoMapper.getValueByView(country));
					pojo.setZipcode((String) ViewToPojoMapper.getValueByView(zipcode));
					pojo.setFathersName((String) ViewToPojoMapper.getValueByView(fathersName));
					pojo.setFatherLate((Integer) ViewToPojoMapper.getValueByView(fatherLate));
					pojo.setMothersName((String) ViewToPojoMapper.getValueByView(mothersName));
					pojo.setMotherLate((Integer) ViewToPojoMapper.getValueByView(motherLate));
					pojo.setLocalGuardiansName((String) ViewToPojoMapper.getValueByView(localGuardiansName));
					pojo.setContactOfParentGuardian((String) ViewToPojoMapper.getValueByView(contactOfParentGuardian));
					pojo.setNumberMale((Integer) ViewToPojoMapper.getValueByView(numberMale));
					pojo.setNumberFemale((Integer) ViewToPojoMapper.getValueByView(numberFemale));
					pojo.setNumberOthers((Integer) ViewToPojoMapper.getValueByView(numberOthers));
					pojo.setNumberMaleUnder5((Integer) ViewToPojoMapper.getValueByView(numberMaleUnder5));
					pojo.setNumberFemaleUnder5((Integer) ViewToPojoMapper.getValueByView(numberFemaleUnder5));
					pojo.setNumberMaleAbove60((Integer) ViewToPojoMapper.getValueByView(numberMaleAbove60));
					pojo.setNumberFemaleAbove60((Integer) ViewToPojoMapper.getValueByView(numberFemaleAbove60));
					pojo.setNumberDisabled((Integer) ViewToPojoMapper.getValueByView(numberDisabled));
					pojo.setNumberPregnant((Integer) ViewToPojoMapper.getValueByView(numberPregnant));
					pojo.setNumberLactatingMothers((Integer) ViewToPojoMapper.getValueByView(numberLactatingMothers));

					if (save)
					{
                		System.out.println("Save to realm: BasicInformation");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			BasicInformation model = (BasicInformation) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(BasicInformation mode, boolean update)
	{
		return update;
	}
	



}
