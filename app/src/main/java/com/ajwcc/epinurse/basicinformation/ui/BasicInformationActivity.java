package com.ajwcc.epinurse.basicinformation.ui;

import org.androidannotations.annotations.EActivity;

import java.util.List;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.image.ImageListFragment_;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;



@EActivity(R.layout.gen_activity_basic_information)
public class BasicInformationActivity extends com.ajwcc.epinurse.basicinformation.gen.BasicInformationActivity
{



    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = super.createFragmentList();

        // FORCE CHANGE THE GENERATED FRAGMENT HERE
        list.set(0, () -> PersonalInformationFragment_.builder().build());
        list.set(1, () -> HouseholdInformationFragment_.builder().build());


        list.add(list.size()-1, ()-> ImageListFragment_.builder().build());

        return list;
    }
}
