package com.ajwcc.epinurse.basicinformation.gen;

import android.support.v4.view.ViewPager;
import android.widget.SeekBar;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import com.ajwcc.epinurse.common.SaveFragment_;


@EActivity(R.layout.gen_activity_basic_information)
public class BasicInformationActivity extends BaseEpinurseFormActivity
{

    @Extra
    public String uuid;

    @ViewById(R.id.title)
    public TextView title;

    @ViewById(R.id.pager)
    public ViewPager viewPager;

    @ViewById(R.id.seekbar)
    public SeekBar seekBar;

	@ViewById(R.id.previous)
    public Button previous;

    @ViewById(R.id.next)
    public Button next;
    
    @AfterViews
    public void init()
    {
    	
    	//title.setText("Basic Information");
    	
    	title.setText(getResources().getString(R.string.basic_information));
    	
    
        // load existing for editing at this point, given a uuid
        initModel(BasicInformation.class, uuid);


        initViewPagerAndSeekbar(createFragmentList(), viewPager, seekBar, previous, next);
        
        createToC();
    	setValidationHandler(getValidationHandler());
    }


	public ValidationHandler<BasicInformation> getValidationHandler()
	{
		BasicInformationValidator validator = new BasicInformationValidator(this);
		validator.setModel((BasicInformation)getModel());	
		return validator;
	}

    LinkedHashMap<String, Integer> pageMap = new LinkedHashMap<>();

    private void createToC()
    {
        List<String> toc = createTableOfContents();
        for (int i=0; i<toc.size(); i++)
        {
            pageMap.put(toc.get(i), i);
        }
    }

    public Integer getPage(String name)
    {
        return pageMap.get(name);
    }

    protected List<String> createTableOfContents()
    {
    	List<String> names = new ArrayList<>();
    	
		names.add("Personal Information");
		names.add("Household Information");
		names.add("Socio-Economic Information");

    	
        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = new ArrayList<>();
        
		list.add(() -> PersonalInformationFragment_.builder().build());
		list.add(() -> HouseholdInformationFragment_.builder().build());
		list.add(() -> SocioEconomicInformationFragment_.builder().build());


        list.add(() -> SaveFragment_.builder().build());


        return list;
    }

}
