package com.ajwcc.epinurse.basicinformation.ui;

import android.content.Intent;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.epinurse.common.utils.EpinurseModel;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;


@EFragment(R.layout.fragment_basic_information_household_information)
public class HouseholdInformationFragment extends com.ajwcc.epinurse.basicinformation.gen.HouseholdInformationFragment {


    public HouseholdInformationFragment() {
        // Required empty public constructor
    }

    @Click(R.id.copySettings)
    public void copySettings()
	{
		// sout copy
        FamilySelectActivity_.intent(this).startForResult(30);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode==30) {
            // check if "uuid" is set
            // add to model and mapModelToViews
            if (data != null) {
                String uuid = data.getStringExtra("uuid");

                BasicInformation bi = getRealm().where(BasicInformation.class).equalTo("uuid", uuid).findFirst();

                // copy fields
                BasicInformation model = (BasicInformation) getModel();

                model.setFamilyID(bi.getFamilyID());

                model.setTypeOfFamily(bi.getTypeOfFamily());
                model.setHouseNumber(bi.getHouseNumber());
                model.setWardNumber(bi.getWardNumber());
                model.setMunicipality(bi.getMunicipality());
                model.setDistrict(bi.getDistrict());
                model.setCountry(bi.getCountry());
                model.setZipcode(bi.getZipcode());

                model.setFathersName(bi.getFathersName());
                model.setMothersName(bi.getMothersName());
                model.setLocalGuardiansName(bi.getLocalGuardiansName());
                model.setContactOfParentGuardian(bi.getContactOfParentGuardian());

                model.setNumberMale(bi.getNumberMale());
                model.setNumberFemale(bi.getNumberFemale());
                model.setNumberOthers(bi.getNumberOthers());
                model.setNumberMaleUnder5(bi.getNumberMaleUnder5());
                model.setNumberFemaleUnder5(bi.getNumberFemaleUnder5());
                model.setNumberMaleAbove60(bi.getNumberMaleAbove60());
                model.setNumberFemaleAbove60(bi.getNumberFemaleAbove60());
                model.setNumberDisabled(bi.getNumberDisabled());
                model.setNumberPregnant(bi.getNumberPregnant());
                model.setNumberLactatingMothers(bi.getNumberLactatingMothers());

                // save
                saveModel();

                // update
                mapModelToViews();

                // toast
                Toast.makeText(getActivity(), getResources().getString(R.string.message_settings_copied), Toast.LENGTH_LONG).show();  // TODO: USE RES

            }
        }
    }

}
