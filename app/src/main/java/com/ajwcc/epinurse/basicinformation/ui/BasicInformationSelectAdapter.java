package com.ajwcc.epinurse.basicinformation.ui;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;

import java.text.SimpleDateFormat;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class BasicInformationSelectAdapter extends RealmRecyclerViewAdapter<BasicInformation, BasicInformationSelectAdapter.BasicInformationViewHolder> {
    BaseEpinurseSelectActivity context;
    int rowLayoutFile = R.layout.row_basic_information;


    public class BasicInformationViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView age;
        TextView sex;
        TextView status;
        TextView createdAt;
        TextView patientId;

        Button action;
        Button delete;

        public BasicInformationViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.owner);
            age = itemView.findViewById(R.id.age);
            sex = itemView.findViewById(R.id.sex);
            status = itemView.findViewById(R.id.status);
            createdAt = itemView.findViewById(R.id.createdAt);
            patientId = itemView.findViewById(R.id.patientId);
            action = itemView.findViewById(R.id.button);
            delete = itemView.findViewById(R.id.delete);

            if (context.getSelectMode())
            {
                delete.setVisibility(View.GONE);
            }
        }
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.clickAction(context.getSelectMode(), view);
        }
    };

    View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.deleteAction(view);
        }
    };

    public BasicInformationSelectAdapter(BaseEpinurseSelectActivity context, @Nullable OrderedRealmCollection<BasicInformation> data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.context = context;
    }

    @NonNull
    @Override
    public BasicInformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = context.getLayoutInflater().inflate(rowLayoutFile, viewGroup, false);  // VERY IMPORTANT TO USE THIS STYLE
        BasicInformationViewHolder viewHolder = new BasicInformationViewHolder(v);
        return viewHolder;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    @Override
    public void onBindViewHolder(@NonNull BasicInformationViewHolder basicInformationViewHolder, int i) {

        BasicInformation b = getItem(i);
        basicInformationViewHolder.name.setText((b.getLastName()==null ? "?" : b.getLastName())
                                                + ", "
                                                + (b.getFirstName()==null ? "?" : b.getFirstName()));



        if (b.getAge()!=null)
        {
            basicInformationViewHolder.age.setText(String.valueOf(b.getAge()));
        }

        if (b.getSex()!=null) {
            basicInformationViewHolder.sex.setText(b.getSex() == 0 ? "M" : "F");
        }

        basicInformationViewHolder.createdAt.setText(sdf.format(b.getCreatedAt()));

        basicInformationViewHolder.patientId.setText(b.getPatientId()==null ? "?" : b.getPatientId());

        basicInformationViewHolder.action.setOnClickListener(listener);
        basicInformationViewHolder.action.setTag(b);


        basicInformationViewHolder.delete.setOnClickListener(deleteListener);
        basicInformationViewHolder.delete.setTag(b);


        if (b.isEditing())
        {
            basicInformationViewHolder.status.setText(context.getResources().getString(R.string.status_editing));
        }
        else
        {
            if (b.isSynced())
            {
                basicInformationViewHolder.status.setText(context.getResources().getString(R.string.status_synced));
            }
            else
            {
                basicInformationViewHolder.status.setText(context.getResources().getString(R.string.status_ready_to_sync));
            }
        }


        String select = context.getResources().getString(R.string.adapter_select);
        String edit = context.getResources().getString(R.string.adapter_edit);

        basicInformationViewHolder.action.setText(context.getSelectMode() ? select : edit);

        // TODO: DELETE

    }


}