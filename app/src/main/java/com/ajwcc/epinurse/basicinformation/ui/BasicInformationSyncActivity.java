package com.ajwcc.epinurse.basicinformation.ui;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.image.ImageEntry;
import com.ajwcc.epinurse.common.network.ShineSender;
import com.ajwcc.util.network.AndroidNetUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;

import java.io.File;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

@EActivity(R.layout.activity_basic_information_download)
public class BasicInformationSyncActivity extends AppCompatActivity
{

    @Bean
    ShineSender sender;

    @ViewById
    View progress;

    @ViewById
    TextView message;

    @ViewById
    TextView reportType;

    @ViewById
    TextView numReports;

    @ViewById(R.id.button_download)
    Button download;

    Realm realm;

    long count;

    @AfterViews
    public void init()
    {
        // load up realm
        realm = Realm.getDefaultInstance();


        // check if there are existing profiles and ask if full download needed
        if (realm.where(BasicInformation.class).count()>0)
        {
            // prompt if full download required
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_clear_local_profiles)
                    .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                    .setMessage(R.string.prompt_clear_local_profiles)
                    .setPositiveButton(getResources().getString(R.string.button_confirm_delete_row_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            clearAllProfiles();
                            // get how many
                            sender.clearLastSync();
                            download.setEnabled(false);
                            updateCount();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.button_confirm_delete_row_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // get how many
                            download.setEnabled(false);
                            updateCount();
                        }
                    })
                    .show();
        }
        else
        {
            // get how many
            download.setEnabled(false);
            updateCount();
        }

    }

    public void clearAllProfiles()
    {
        // clear all BasicInformation and all corresponding images

        RealmResults<BasicInformation> results = realm.where(BasicInformation.class).findAll();

        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();

        RealmResults<ImageEntry> imageResults = realm.where(ImageEntry.class)
                                                    .equalTo("reportType", "BasicInformation")
                                                    .findAll();

        for (ImageEntry entry : imageResults)
        {
            deleteAssociatedImages(entry);
        }

        realm.beginTransaction();
        imageResults.deleteAllFromRealm();
        realm.commitTransaction();

    }

    private void deleteAssociatedImages(ImageEntry entry)
    {
        String croppedPath = entry.getCroppedPath();
        String origPath = entry.getOrigPath();

        // remove from the list
        try {
            File f = new File(croppedPath);
            System.out.println("Deleted "+croppedPath+" = " + f.delete());

            File f2 = new File(origPath);
            System.out.println("Deleted "+origPath+" = " + f2.delete());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    @Background(id ="updateCount")
    public void updateCount()
    {
        try {
            count = sender.getNewProfileCount();
            updateCountDone(count);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @UiThread
    public void updateCountDone(long count)
    {
        numReports.setText(String.valueOf(count));
        download.setEnabled(true);
    }


    public void finish()
    {
        super.finish();

        // close Realm
        realm.close();
    }

    public void onDestroy()
    {
        BackgroundExecutor.cancelAll("download", true);
        BackgroundExecutor.cancelAll("updateCount", true);
        super.onDestroy();
    }

    @Click(R.id.button_cancel)
    public void cancel()
    {
        finish();
    }


    @Click(R.id.button_download)
    public void upload()
    {
        if (!AndroidNetUtils.isNetworkAvailable(this))
        {
            Toast.makeText(this, getResources().getString(R.string.error_no_network_connection), Toast.LENGTH_LONG).show();  // TODO: USE RES
            return;
        }

        // check if there is anything to send
        // prompt if none
        if (count == 0)
        {
            Toast.makeText(this, getResources().getString(R.string.error_no_new_profiles_found), Toast.LENGTH_LONG).show();  // TODO: USE RES
            return;
        }

        prepDownload();
        download();
    }

    @UiThread
    public void prepDownload()
    {


        // make progress indicator visible
        progress.setVisibility(View.VISIBLE);
        download.setEnabled(false);
    }

    @Background(id = "download")
    public void download()
    {

        try {
            try
            {
                updateMessage(getString(R.string.sync_message_downloading_profiles));
                List<BasicInformation> newProfiles = sender.getNewProfiles();


                if (newProfiles.size()==0)
                {
                    popMessage(getResources().getString(R.string.error_no_new_profiles_found));  // TODO: USE RES
                }
                else
                {
                    // get new Realm instance since diff thread
                    Realm realm = Realm.getDefaultInstance();

                    realm.beginTransaction();

                    for (BasicInformation profile : newProfiles)
                    {
                        System.out.println(profile);

                        profile.setEditing(false);
                        profile.setSynced(true);
                        profile.setCreatedAt(new Date());

                        realm.copyToRealmOrUpdate(profile);
                    }

                    realm.commitTransaction();

                    popMessage(String.format(getResources().getString(R.string.message_update_profiles), newProfiles.size()));  // TODO: USE RES

                    updateCount();

                    realm.close();
                }
            }
            catch(Exception e)
            {
                popMessage("Download failed: "+e.getMessage());  // TODO: USE RES
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
            updateMessage(e.getMessage());
            delay(1500);
        }

        downloadDone();
    }


    public void delay(long ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch (InterruptedException e)
        {

        }
    }

    @UiThread
    public void updateMessage(String txt)
    {
        message.setText(txt);
    }



    @UiThread
    public void downloadDone()
    {
        // change button to say upload
        // enable touch on recyclerView
        // make progress indicator invisible
        progress.setVisibility(View.INVISIBLE);
        download.setEnabled(true);
    }


    @UiThread
    public void popMessage(String s)
    {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }


}
