package com.ajwcc.epinurse.basicinformation.ui;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;

import java.text.SimpleDateFormat;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class FamilySelectAdapter extends RealmRecyclerViewAdapter<BasicInformation, FamilySelectAdapter.BasicInformationViewHolder> {
    BaseEpinurseSelectActivity context;
    int rowLayoutFile = R.layout.row_family_information;


    public class BasicInformationViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView createdAt;
        TextView familyId;

        Button action;

        public BasicInformationViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.owner);
            createdAt = itemView.findViewById(R.id.createdAt);
            familyId = itemView.findViewById(R.id.familyID);
            action = itemView.findViewById(R.id.button);

        }
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.clickAction(context.getSelectMode(), view);
        }
    };

    public FamilySelectAdapter(BaseEpinurseSelectActivity context, @Nullable OrderedRealmCollection<BasicInformation> data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.context = context;
    }

    @NonNull
    @Override
    public BasicInformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = context.getLayoutInflater().inflate(rowLayoutFile, viewGroup, false);  // VERY IMPORTANT TO USE THIS STYLE
        BasicInformationViewHolder viewHolder = new BasicInformationViewHolder(v);
        return viewHolder;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    @Override
    public void onBindViewHolder(@NonNull BasicInformationViewHolder basicInformationViewHolder, int i) {

        BasicInformation b = getItem(i);
        basicInformationViewHolder.name.setText((b.getLastName()==null ? "?" : b.getLastName())
                                                + ", "
                                                + (b.getFirstName()==null ? "?" : b.getFirstName()));




        basicInformationViewHolder.createdAt.setText(sdf.format(b.getCreatedAt()));

        basicInformationViewHolder.familyId.setText(b.getFamilyID()==null ? "?" : b.getFamilyID());

        basicInformationViewHolder.action.setOnClickListener(listener);
        basicInformationViewHolder.action.setTag(b);

        String select = context.getResources().getString(R.string.adapter_select);
        String edit = context.getResources().getString(R.string.adapter_edit);

        basicInformationViewHolder.action.setText(context.getSelectMode() ? select : edit);

    }
}