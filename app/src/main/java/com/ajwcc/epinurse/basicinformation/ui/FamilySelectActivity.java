package com.ajwcc.epinurse.basicinformation.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.basicinformation.gen.BasicInformation;
import com.ajwcc.epinurse.common.network.NurseLogin_;
import com.ajwcc.epinurse.common.network.ShineSender;
import com.ajwcc.epinurse.common.utils.BaseEpinurseSelectActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;

@EActivity(R.layout.activity_family_select)
public class FamilySelectActivity extends BaseEpinurseSelectActivity implements SearchView.OnQueryTextListener
{
    @ViewById
    SearchView search;

	@Extra
	public QueryFilter<BasicInformation> filter;

    @ViewById(R.id.recyclerView)
    public RecyclerView recyclerView;

    @ViewById
    public TextView available;


    public Realm realm;

    @AfterViews
    public void init()
    {
        realm = Realm.getDefaultInstance();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(createAdapter());
        
        if (filter!=null)
        {
        	available.setText(available.getText()+"("+filter.getName()+")");
        }
    }

    public void finish()
    {
        super.finish();
        realm.close();
    }

    public RealmResults<BasicInformation> getResults()
    {
        // load realm results
        RealmQuery<BasicInformation> query = realm.where(BasicInformation.class);

        query = query.isNotNull("familyID")
                    .isNotEmpty("familyID");


		if (filter!=null)
        {
            query = filter.adjust(query);
        }

        RealmResults<BasicInformation> results = query.findAll();

        results = results.sort("editing", Sort.DESCENDING, "createdAt", Sort.DESCENDING);

        //System.out.println(results);
        return results;
    }

    @Override
    public boolean getSelectMode() {
        return true;
    }

    @Override
    public void newRow() {

    }

    @Override
    public void openEditor(View view) {

    }

    public RealmRecyclerViewAdapter createAdapter()
    {
        // make adapter
        FamilySelectAdapter adapter = new FamilySelectAdapter(this, getResults(), true);
        return adapter;
    }

    public RealmRecyclerViewAdapter createAdapter(QueryFilter<BasicInformation> searchFilter)
    {
        // make adapter
        BasicInformationSelectAdapter adapter = new BasicInformationSelectAdapter(this, getResults(searchFilter), true);
        return adapter;
    }

    @AfterViews
    public void initSearchView()
    {
        search.setOnQueryTextListener(this);
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String s) {

        // reload query
        if (s.equals("")) {
            recyclerView.setAdapter(createAdapter());
        }
        else
        {
            recyclerView.setAdapter(createAdapter(new QueryFilter<BasicInformation>() {
                @Override
                public String getName() {
                    return "Filtered";
                }

                @Override
                public RealmQuery<BasicInformation> adjust(RealmQuery<BasicInformation> query) {

                    query = query.beginGroup()
                            .beginsWith("firstName", s, Case.INSENSITIVE)
                            .or()
                            .beginsWith("lastName", s, Case.INSENSITIVE)
                            .endGroup();

                    return query;
                }
            }));
        }
        return false;
    }


    public RealmResults<BasicInformation> getResults(QueryFilter<BasicInformation> searchFilter)
    {
        // load realm results
        RealmQuery<BasicInformation> query = realm.where(BasicInformation.class);

        query = query.isNotNull("familyID")
                     .isNotEmpty("familyID");

        if (filter!=null)
        {
            query = filter.adjust(query);  // the basic filter
        }

        if (searchFilter!=null)
        {
            query = searchFilter.adjust(query);  // the basic filter
        }

        RealmResults<BasicInformation> results = query.findAll();

        results = results.sort("editing", Sort.DESCENDING, "createdAt", Sort.DESCENDING);

        //System.out.println(results);
        return results;
    }
}
