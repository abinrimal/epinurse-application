package com.ajwcc.epinurse.nurseregistration.gen;

import android.support.v4.view.ViewPager;
import android.widget.SeekBar;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import com.ajwcc.epinurse.common.SaveFragment_;


@EActivity(R.layout.gen_activity_nurse_registration)
public class NurseRegistrationActivity extends BaseEpinurseFormActivity
{

    @Extra
    public String uuid;

    @ViewById(R.id.title)
    public TextView title;

    @ViewById(R.id.pager)
    public ViewPager viewPager;

    @ViewById(R.id.seekbar)
    public SeekBar seekBar;

	@ViewById(R.id.previous)
    public Button previous;

    @ViewById(R.id.next)
    public Button next;
    
    @AfterViews
    public void init()
    {
    	
    	//title.setText("Nurse Registration");
    	
    	title.setText(getResources().getString(R.string.nurse_registration));
    	
    
        // load existing for editing at this point, given a uuid
        initModel(NurseRegistration.class, uuid);


        initViewPagerAndSeekbar(createFragmentList(), viewPager, seekBar, previous, next);
        
        createToC();
    	setValidationHandler(getValidationHandler());
    }


	public ValidationHandler<NurseRegistration> getValidationHandler()
	{
		NurseRegistrationValidator validator = new NurseRegistrationValidator(this);
		validator.setModel((NurseRegistration)getModel());	
		return validator;
	}

    LinkedHashMap<String, Integer> pageMap = new LinkedHashMap<>();

    private void createToC()
    {
        List<String> toc = createTableOfContents();
        for (int i=0; i<toc.size(); i++)
        {
            pageMap.put(toc.get(i), i);
        }
    }

    public Integer getPage(String name)
    {
        return pageMap.get(name);
    }

    protected List<String> createTableOfContents()
    {
    	List<String> names = new ArrayList<>();
    	
		names.add("Registration");
		names.add("Temporary Address");
		names.add("Permanent Address");
		names.add("Professional Information");

    	
        return names;
    }


    protected List<LazyFragment> createFragmentList()
    {
        List<LazyFragment> list = new ArrayList<>();
        
		list.add(() -> RegistrationFragment_.builder().build());
		list.add(() -> TemporaryAddressFragment_.builder().build());
		list.add(() -> PermanentAddressFragment_.builder().build());
		list.add(() -> ProfessionalInformationFragment_.builder().build());


        list.add(() -> SaveFragment_.builder().build());


        return list;
    }

}
