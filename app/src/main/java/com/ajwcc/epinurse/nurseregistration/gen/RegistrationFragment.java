package com.ajwcc.epinurse.nurseregistration.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_nurse_registration_registration)
public class RegistrationFragment extends BaseEpinurseFragment {


    public RegistrationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText nanNumber;

	@ViewById
	@MapToModelField
	protected EditText firstName;

	@ViewById
	@MapToModelField
	protected EditText middleName;

	@ViewById
	@MapToModelField
	protected EditText lastName;

	@ViewById
	@MapToModelField
	protected RadioGroup sex;

	@ViewById
	@MapToModelField
	protected EditText citizenshipNumber;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInAd;

	@ViewById
	@MapToModelField
	protected EditText dateOfBirthInBs;

	@ViewById
	@MapToModelField
	protected EditText age;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
					PojoToViewMapper.setViewValue(nanNumber,pojo.getNanNumber());
					PojoToViewMapper.setViewValue(firstName,pojo.getFirstName());
					PojoToViewMapper.setViewValue(middleName,pojo.getMiddleName());
					PojoToViewMapper.setViewValue(lastName,pojo.getLastName());
					PojoToViewMapper.setViewValue(sex,pojo.getSex());
					PojoToViewMapper.setViewValue(citizenshipNumber,pojo.getCitizenshipNumber());
					PojoToViewMapper.setViewValue(dateOfBirthInAd,pojo.getDateOfBirthInAd());
					PojoToViewMapper.setViewValue(dateOfBirthInBs,pojo.getDateOfBirthInBs());
					PojoToViewMapper.setViewValue(age,pojo.getAge());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
                	
					pojo.setNanNumber((String) ViewToPojoMapper.getValueByView(nanNumber));
					pojo.setFirstName((String) ViewToPojoMapper.getValueByView(firstName));
					pojo.setMiddleName((String) ViewToPojoMapper.getValueByView(middleName));
					pojo.setLastName((String) ViewToPojoMapper.getValueByView(lastName));
					pojo.setSex((Integer) ViewToPojoMapper.getValueByView(sex));
					pojo.setCitizenshipNumber((String) ViewToPojoMapper.getValueByView(citizenshipNumber));
					pojo.setDateOfBirthInAd((String) ViewToPojoMapper.getValueByView(dateOfBirthInAd));
					pojo.setDateOfBirthInBs((String) ViewToPojoMapper.getValueByView(dateOfBirthInBs));
					pojo.setAge((Integer) ViewToPojoMapper.getValueByView(age));

					if (save)
					{
                		System.out.println("Save to realm: NurseRegistration");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			NurseRegistration model = (NurseRegistration) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(NurseRegistration mode, boolean update)
	{
		return update;
	}
	



}
