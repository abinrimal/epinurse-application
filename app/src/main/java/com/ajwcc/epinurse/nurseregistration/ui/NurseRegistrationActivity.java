package com.ajwcc.epinurse.nurseregistration.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.nurseregistration.gen.NurseRegistration;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFormActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

import org.androidannotations.annotations.EActivity;

import java.util.List;


@EActivity(R.layout.gen_activity_nurse_registration)
public class NurseRegistrationActivity extends com.ajwcc.epinurse.nurseregistration.gen.NurseRegistrationActivity
{
    public ValidationHandler<NurseRegistration> getValidationHandler()
    {
        NurseRegistrationValidator validator = new NurseRegistrationValidator(this);
        validator.setModel((NurseRegistration)getModel());
        return validator;
    }

    protected List<String> createTableOfContents()
    {
        List<String> names = super.createTableOfContents();

        names.add("Qualifications");
        names.add("Trainings");

        return names;
    }

    protected List<LazyFragment> createFragmentList() {


        List<LazyFragment> list = super.createFragmentList();

        list.set(0, () -> RegistrationFragment_.builder().build());

        // add qualification fragment
        // add training fragment

        list.add(list.size()-1, () -> QualificationsFragment_.builder().build());
        list.add(list.size()-1,() -> TrainingsFragment_.builder().build());



        return list;
    }
}
