package com.ajwcc.epinurse.nurseregistration.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.nurseregistration.gen.NurseRegistration;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.ViewToPojoMapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.realm.RealmList;


@EFragment(R.layout.fragment_nurse_registration_trainings)
public class TrainingsFragment extends BaseEpinurseFragment
{

    @ViewById
    LinearLayout trainings;


    @AfterViews
    public void init()
    {
    }

    public void deleteRow(View v)
    {
        trainings.removeView((View) v.getTag());
    }

    @Click(R.id.addEntry)
    public void newRow()
    {
        View view = createRow("","", "");

        // add to layout
        trainings.addView(view);
        trainings.invalidate();
    }

    public View createRow(String name, String organizedBy, String trainingDate)
    {
        final View view = (View) getActivity().getLayoutInflater().inflate(R.layout.row_training, null);

        // getViews
        EditText nameField = (EditText) view.findViewById(R.id.name);
        EditText organizedByField = (EditText) view.findViewById(R.id.organizedBy);
        EditText trainingDateField = (EditText) view.findViewById(R.id.date);

        Button button = view.findViewById(R.id.delete);
        button.setTag(view);       // this is the index, to help with delete

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRow(v);
            }
        });

        // fill rows
        nameField.setText(name);
        organizedByField.setText(organizedBy);
        trainingDateField.setText(trainingDate);

        return view;
    }

    public View createRowFromEntry(TrainingEntry entry)
    {
        return createRow(entry.getName(), entry.getOrganizedBy(), entry.getDate());
    }

    public void mapModelToViews()
    {
        NurseRegistration model = (NurseRegistration) getModel();
        RealmList<TrainingEntry> list = model.getTrainings();

        for (TrainingEntry entry : list)
        {
            View view = createRowFromEntry(entry);
            trainings.addView(view);
        }
        trainings.invalidate();

    }

    public void mapViewsToModel()
    {
        // validate the fields first to make sure they all have values


        try {

            NurseRegistration model = (NurseRegistration) getModel();
            RealmList<TrainingEntry> list = model.getTrainings();

            list.clear();

            // create TrainingEntry from rows
            for (int i = 0; i < trainings.getChildCount(); i++) {
                View view = trainings.getChildAt(i);

                // getViews
                EditText nameField = (EditText) view.findViewById(R.id.name);
                EditText organizedByField = (EditText) view.findViewById(R.id.organizedBy);
                EditText dateField = (EditText) view.findViewById(R.id.date);


                TrainingEntry entry = new TrainingEntry();
                entry.setName((String) ViewToPojoMapper.getValueByView(nameField));
                entry.setOrganizedBy((String) ViewToPojoMapper.getValueByView(organizedByField));
                entry.setDate((String) ViewToPojoMapper.getValueByView(dateField));

                list.add(entry);

                // force these for now
                // NOTE: this means no matter what happens this will be flagged as dirty
                //       even if you just swipe through
                model.setEditing(true);
                model.setSynced(false);
            }

            // save
            saveModel();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }







}
