package com.ajwcc.epinurse.nurseregistration.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;


@EFragment(R.layout.gen_fragment_nurse_registration_registration)
public class RegistrationFragment extends com.ajwcc.epinurse.nurseregistration.gen.RegistrationFragment {

	@AfterViews
	public void init()
	{
		age.setEnabled(false);
	}

	@AfterTextChange(R.id.dateOfBirthInAd)   // only need this one since it gets adjusted on both dates
	public void adjustAge()
	{
		age.setText(UiUtils.computeAge(dateOfBirthInAd.getText().toString()));
	}


	@Click(R.id.dateOfBirthInAdButton)
	public void openADCalendar()
	{
		UiUtils.openGregorianCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);
	}

	@Click(R.id.dateOfBirthInBsButton)
	public void openBSCalendar()
	{
		UiUtils.openNepaliCalendar(getActivity(), dateOfBirthInAd, dateOfBirthInBs);

	}
}
