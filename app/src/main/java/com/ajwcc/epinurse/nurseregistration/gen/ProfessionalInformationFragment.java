package com.ajwcc.epinurse.nurseregistration.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_nurse_registration_professional_information)
public class ProfessionalInformationFragment extends BaseEpinurseFragment {


    public ProfessionalInformationFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText professionalStatus;

	@ViewById
	@MapToModelField
	protected EditText designation;

	@ViewById
	@MapToModelField
	protected EditText office;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
					PojoToViewMapper.setViewValue(professionalStatus,pojo.getProfessionalStatus());
					PojoToViewMapper.setViewValue(designation,pojo.getDesignation());
					PojoToViewMapper.setViewValue(office,pojo.getOffice());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
                	
					pojo.setProfessionalStatus((String) ViewToPojoMapper.getValueByView(professionalStatus));
					pojo.setDesignation((String) ViewToPojoMapper.getValueByView(designation));
					pojo.setOffice((String) ViewToPojoMapper.getValueByView(office));

					if (save)
					{
                		System.out.println("Save to realm: NurseRegistration");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			NurseRegistration model = (NurseRegistration) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(NurseRegistration mode, boolean update)
	{
		return update;
	}
	



}
