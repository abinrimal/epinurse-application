package com.ajwcc.epinurse.nurseregistration.ui;

import io.realm.RealmObject;

public class QualificationEntry extends RealmObject
{
    private String degree;
    private String university;
    private String date;

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "QualificationEntry{" +
                "degree='" + degree + '\'' +
                ", university='" + university + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
