package com.ajwcc.epinurse.nurseregistration.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.nurseregistration.gen.NurseRegistration;
import com.ajwcc.epinurse.nurseregistration.gen.NurseRegistrationActivity;
import com.ajwcc.util.ui.validation.ValidationHandler;

public class NurseRegistrationValidator extends com.ajwcc.epinurse.nurseregistration.gen.NurseRegistrationValidator
{
    public NurseRegistrationValidator(NurseRegistrationActivity a)
    {
        super(a);
    }

    public void validateQualifications()
    {
        int page = activity.getPage("Qualifications");
        int viewId =  R.id.qualificationsContainer;

        for (QualificationEntry me : model.getQualifications())
        {
            if (me.getDegree().equals(""))
            {
//                throw new ValidationHandler.ValidationException("Degree must have a value", page, viewId);  // TODO: USE RES

                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_qualification_degree_diploma_certificate_fellowship)),
                                                                              page, viewId);  // TODO: USE RES

            }

            if (me.getUniversity()==null)
            {
 //               throw new ValidationHandler.ValidationException("University must have a value", page, viewId);  // TODO: USE RES

                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_qualification_university_institution)),
                                                                              page, viewId);  // TODO: USE RES

            }

            if (me.getDate()==null)
            {
 //               throw new ValidationHandler.ValidationException("Date must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                              context.getResources().getString(R.string.row_qualification_training_date)),
                                                                              page, viewId);  // TODO: USE RES
            }
        }

    }

    public void validateTrainings()
    {
        int page = activity.getPage("Trainings");
        int viewId =  R.id.trainingsContainer;

        for (TrainingEntry me : model.getTrainings())
        {
            if (me.getName().equals(""))
            {
//                throw new ValidationHandler.ValidationException("Name must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                            context.getResources().getString(R.string.row_training_name_of_training)),
                                                                            page, viewId);  // TODO: USE RES
            }

            if (me.getOrganizedBy()==null)
            {
//                throw new ValidationHandler.ValidationException("Organized By must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                                context.getResources().getString(R.string.row_training_organized_by)),
                                                                                page, viewId);  // TODO: USE RES
            }

            if (me.getDate()==null)
            {
//                throw new ValidationHandler.ValidationException("Date must have a value", page, viewId);  // TODO: USE RES
                throw new ValidationHandler.ValidationException(String.format(context.getResources().getString(R.string.error_field_must_have_value),
                                                                                context.getResources().getString(R.string.row_training_training_date)),
                                                                                page, viewId);  // TODO: USE RES
            }
        }

    }


}
