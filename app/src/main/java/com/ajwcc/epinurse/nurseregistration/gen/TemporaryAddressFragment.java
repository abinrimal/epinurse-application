package com.ajwcc.epinurse.nurseregistration.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_nurse_registration_temporary_address)
public class TemporaryAddressFragment extends BaseEpinurseFragment {


    public TemporaryAddressFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText tempHouseNumber;

	@ViewById
	@MapToModelField
	protected EditText tempWardNumber;

	@ViewById
	@MapToModelField
	protected EditText tempMunicipality;

	@ViewById
	@MapToModelField
	protected EditText tempDistrict;

	@ViewById
	@MapToModelField
	protected EditText tempCountry;

	@ViewById
	@MapToModelField
	protected EditText tempZipcode;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
					PojoToViewMapper.setViewValue(tempHouseNumber,pojo.getTempHouseNumber());
					PojoToViewMapper.setViewValue(tempWardNumber,pojo.getTempWardNumber());
					PojoToViewMapper.setViewValue(tempMunicipality,pojo.getTempMunicipality());
					PojoToViewMapper.setViewValue(tempDistrict,pojo.getTempDistrict());
					PojoToViewMapper.setViewValue(tempCountry,pojo.getTempCountry());
					PojoToViewMapper.setViewValue(tempZipcode,pojo.getTempZipcode());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
                	
					pojo.setTempHouseNumber((String) ViewToPojoMapper.getValueByView(tempHouseNumber));
					pojo.setTempWardNumber((String) ViewToPojoMapper.getValueByView(tempWardNumber));
					pojo.setTempMunicipality((String) ViewToPojoMapper.getValueByView(tempMunicipality));
					pojo.setTempDistrict((String) ViewToPojoMapper.getValueByView(tempDistrict));
					pojo.setTempCountry((String) ViewToPojoMapper.getValueByView(tempCountry));
					pojo.setTempZipcode((String) ViewToPojoMapper.getValueByView(tempZipcode));

					if (save)
					{
                		System.out.println("Save to realm: NurseRegistration");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			NurseRegistration model = (NurseRegistration) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(NurseRegistration mode, boolean update)
	{
		return update;
	}
	



}
