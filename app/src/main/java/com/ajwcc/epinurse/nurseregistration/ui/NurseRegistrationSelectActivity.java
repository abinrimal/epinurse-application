package com.ajwcc.epinurse.nurseregistration.ui;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.network.NurseLogin_;
import com.ajwcc.epinurse.common.network.ShineSender;

import org.androidannotations.annotations.EActivity;

import io.realm.RealmRecyclerViewAdapter;

@EActivity(R.layout.gen_activity_nurse_registration_select)
public class NurseRegistrationSelectActivity extends com.ajwcc.epinurse.nurseregistration.gen.NurseRegistrationSelectActivity
{
    public RealmRecyclerViewAdapter createAdapter()
    {
        // make adapter
        NurseRegistrationSelectAdapter adapter = new NurseRegistrationSelectAdapter(this, getResults(), true);
        return adapter;
    }
}
