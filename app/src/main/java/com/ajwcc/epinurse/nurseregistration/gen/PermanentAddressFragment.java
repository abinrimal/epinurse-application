package com.ajwcc.epinurse.nurseregistration.gen;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.TextChange;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.common.UiUtils;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.MapToModelField;

import android.view.View;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


@EFragment(R.layout.gen_fragment_nurse_registration_permanent_address)
public class PermanentAddressFragment extends BaseEpinurseFragment {


    public PermanentAddressFragment() {
        // Required empty public constructor
    }


	@ViewById
	@MapToModelField
	protected EditText permanentHouseNumber;

	@ViewById
	@MapToModelField
	protected EditText permanentWardNumber;

	@ViewById
	@MapToModelField
	protected EditText permanentMunicipality;

	@ViewById
	@MapToModelField
	protected EditText permanentDistrict;

	@ViewById
	@MapToModelField
	protected EditText permanentCountry;

	@ViewById
	@MapToModelField
	protected EditText permanentZipcode;


    



    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                	System.out.println("Init from NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
					PojoToViewMapper.setViewValue(permanentHouseNumber,pojo.getPermanentHouseNumber());
					PojoToViewMapper.setViewValue(permanentWardNumber,pojo.getPermanentWardNumber());
					PojoToViewMapper.setViewValue(permanentMunicipality,pojo.getPermanentMunicipality());
					PojoToViewMapper.setViewValue(permanentDistrict,pojo.getPermanentDistrict());
					PojoToViewMapper.setViewValue(permanentCountry,pojo.getPermanentCountry());
					PojoToViewMapper.setViewValue(permanentZipcode,pojo.getPermanentZipcode());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
    public void mapViewsToModel()
    {
    	mapViewsToModel(true);
    }
    
    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                	System.out.println("Updating pojo: NurseRegistration");
                	NurseRegistration pojo = (NurseRegistration) getModel();
                	
					pojo.setPermanentHouseNumber((String) ViewToPojoMapper.getValueByView(permanentHouseNumber));
					pojo.setPermanentWardNumber((String) ViewToPojoMapper.getValueByView(permanentWardNumber));
					pojo.setPermanentMunicipality((String) ViewToPojoMapper.getValueByView(permanentMunicipality));
					pojo.setPermanentDistrict((String) ViewToPojoMapper.getValueByView(permanentDistrict));
					pojo.setPermanentCountry((String) ViewToPojoMapper.getValueByView(permanentCountry));
					pojo.setPermanentZipcode((String) ViewToPojoMapper.getValueByView(permanentZipcode));

					if (save)
					{
                		System.out.println("Save to realm: NurseRegistration");
	                	saveModel();
	                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    


	public void onVisible()
	{
		checkDependencies();
	}


	boolean inCheckDependencies = false; // needed to suppress change events in widgets
	
	public void checkDependencies()
	{
		
		
		if (getModel()!=null)
		{
			NurseRegistration model = (NurseRegistration) getModel();
			boolean update = false;
	

			update = checkDependenciesMisc(model, update);

			if (update)
			{
				inCheckDependencies = true;
				mapModelToViews();
				inCheckDependencies = false;
			}
		}
		
	}
	
	// this is used to introduce extra dependency checks manually for special situations
	public boolean checkDependenciesMisc(NurseRegistration mode, boolean update)
	{
		return update;
	}
	



}
