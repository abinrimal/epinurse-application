package com.ajwcc.epinurse.nurseregistration.ui;

import io.realm.RealmObject;

public class TrainingEntry extends RealmObject {
    private String name;
    private String organizedBy;
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizedBy() {
        return organizedBy;
    }

    public void setOrganizedBy(String organizedBy) {
        this.organizedBy = organizedBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TrainingEntry{" +
                "name='" + name + '\'' +
                ", organizedBy='" + organizedBy + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
