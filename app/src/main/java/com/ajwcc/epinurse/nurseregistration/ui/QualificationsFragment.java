package com.ajwcc.epinurse.nurseregistration.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ajwcc.epinurse.R;
import com.ajwcc.epinurse.nurseregistration.gen.NurseRegistration;
import com.ajwcc.epinurse.common.utils.BaseEpinurseFragment;
import com.ajwcc.util.reflect.ViewToPojoMapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.realm.RealmList;


@EFragment(R.layout.fragment_nurse_registration_qualifications)
public class QualificationsFragment extends BaseEpinurseFragment
{

    @ViewById
    LinearLayout qualifications;


    @AfterViews
    public void init()
    {
    }

    public void deleteRow(View v)
    {
        qualifications.removeView((View) v.getTag());
        qualifications.invalidate();
    }

    @Click(R.id.addEntry)
    public void newRow()
    {
        View view = createRow("","", "");

        // add to layout
        qualifications.addView(view);
        qualifications.invalidate();
    }

    public View createRow(String degree, String university, String trainingDate)
    {
        final View view = (View) getActivity().getLayoutInflater().inflate(R.layout.row_qualification, null);

        // getViews
        EditText degreeField = (EditText) view.findViewById(R.id.degree);
        EditText universityField = (EditText) view.findViewById(R.id.university);
        EditText trainingDateField = (EditText) view.findViewById(R.id.date);

        Button button = view.findViewById(R.id.delete);
        button.setTag(view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRow(v );
            }
        });

        // fill rows
        degreeField.setText(degree);
        universityField.setText(university);
        trainingDateField.setText(trainingDate);

        return view;
    }

    public View createRowFromEntry(QualificationEntry entry)
    {
        return createRow(entry.getDegree(), entry.getUniversity(), entry.getDate());
    }

    public void mapModelToViews()
    {
        NurseRegistration model = (NurseRegistration) getModel();
        RealmList<QualificationEntry> list = model.getQualifications();

        for (QualificationEntry entry : list)
        {
            View view = createRowFromEntry(entry);
            qualifications.addView(view);
        }
        qualifications.invalidate();

    }

    public void mapViewsToModel()
    {
        // validate the fields first to make sure they all have values


        try {

            NurseRegistration model = (NurseRegistration) getModel();
            RealmList<QualificationEntry> list = model.getQualifications();

            list.clear();

            // create MedicineEntry from rows
            for (int i = 0; i < qualifications.getChildCount(); i++) {
                View view = qualifications.getChildAt(i);

                // getViews
                EditText degreeField = (EditText) view.findViewById(R.id.degree);
                EditText universityField = (EditText) view.findViewById(R.id.university);
                EditText dateField = (EditText) view.findViewById(R.id.date);


                QualificationEntry entry = new QualificationEntry();
                entry.setDegree((String) ViewToPojoMapper.getValueByView(degreeField));
                entry.setUniversity((String) ViewToPojoMapper.getValueByView(universityField));
                entry.setDate((String) ViewToPojoMapper.getValueByView(dateField));

                list.add(entry);

                // force these for now
                // NOTE: this means no matter what happens this will be flagged as dirty
                //       even if you just swipe through
                model.setEditing(true);
                model.setSynced(false);
            }

            // save
            saveModel();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }







}
