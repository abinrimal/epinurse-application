package com.ajwcc.util.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by logan on 26/03/2018.
 */

public class NetworkConnectionDetector
{
    Context context;
    NetworkConnectionListener listener;


    BroadcastReceiver receiver;


    public NetworkConnectionDetector(Context context, NetworkConnectionListener listener)
    {
        this.context = context;
        this.listener = listener;
    }





    public void onResume()
    {
        if (receiver==null)
        {
            // register receiver
            System.out.println("Registering Network Receiver");
            IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            receiver = new NetworkChecker();
            context.registerReceiver(receiver, intentFilter);
        }
    }

    public void onPause()
    {
        if (receiver!=null)
        {
            System.out.println("Unregistering Network Receiver");

            // unregister
            context.unregisterReceiver(receiver);

            // null this so it can be reavtivated later
            receiver = null;
        }
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo!=null && networkInfo.isConnected());
    }

    public interface NetworkConnectionListener
    {
        public void connectionChanged(boolean onlineState);
    }

    class NetworkChecker extends BroadcastReceiver
    {
        public void onReceive(Context context, Intent data)
        {
            if (listener!=null)
            {
                listener.connectionChanged(isOnline());
            }
        }
    }
}
