package com.ajwcc.util.reflect;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

public class RadioButtonManager {
    List<RadioButton> radios;

    public void setRadioButtons(ViewGroup parent) {
        final List<RadioButton> radios = getRadioButtons(parent);
        setRadioButtons(radios);
    }

    public RadioButton getSelectedRadio()
    {
        for (RadioButton r2:RadioButtonManager.this.radios) {
            if (r2.isChecked())
            {
                return r2;
            }
        }

        return null;
    }

    public int getSelectedRadioIndex()
    {
        int i = 0;
        for (RadioButton r2:RadioButtonManager.this.radios) {
            if (r2.isChecked())
            {
                break;
            }
            i++;
        }

        return i;
    }

    public void setSelectedRadioIndex(int index)
    {
        int i = 0;
        for (RadioButton r2:RadioButtonManager.this.radios) {
            if (i == index)
            {
                r2.setChecked(true);
            }
            else
            {
                r2.setChecked(false);
            }
            i++;
        }
    }


    public void clearCheck()
    {
        for (RadioButton r2:RadioButtonManager.this.radios) {
                r2.setChecked(false);
        }
    }


    public void setRadioButtons(final List<RadioButton> radios) {

        this.radios = radios;

        for (RadioButton radio: this.radios) {
            radio.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    RadioButton r = (RadioButton) v;
                    r.setChecked(true);
                    for (RadioButton r2:RadioButtonManager.this.radios) {
                        if (r2.getId() != r.getId()) {
                            r2.setChecked(false);
                        }
                    }

                }
            });
        }
    }

    private List<RadioButton> getRadioButtons(ViewGroup parent) {
        List<RadioButton> radios = new ArrayList<>();
        for (int i=0;i < parent.getChildCount(); i++) {
            View v = parent.getChildAt(i);
            if (v instanceof RadioButton) {
                radios.add((RadioButton) v);
            } else if (v instanceof ViewGroup) {
                List<RadioButton> nestedRadios = getRadioButtons((ViewGroup) v);
                radios.addAll(nestedRadios);
            }
        }
        return radios;
    }



}