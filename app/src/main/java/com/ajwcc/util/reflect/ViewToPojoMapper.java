package com.ajwcc.util.reflect;

import android.support.v4.app.Fragment;
import android.text.InputType;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * Created by logan on 09/12/2017.
 */

public class ViewToPojoMapper
{
    // this takes the values of the Views and puts them to the POJO/Model

    // this affects: Spinner, RadioGroup, CheckBox, RadioButtonManager
    public static boolean useLabelsAsValues = false;



    public Class getParamTypeByView(Field f, Object o) throws Exception
    {

        if (o instanceof EditText)
        {
            EditText et = (EditText) o;

            if ((et.getInputType() & InputType.TYPE_MASK_CLASS) == InputType.TYPE_CLASS_NUMBER)
            {
                if ((et.getInputType() & InputType.TYPE_MASK_FLAGS) == InputType.TYPE_NUMBER_FLAG_DECIMAL)
                {
                    return Double.class;
                }
                else
                {
                    return Integer.class;
                }

            }
            return String.class;
        }
        else if (o instanceof RadioGroup)
        {
            // NOTE: this depends if you need the value or label
            if (useLabelsAsValues) {
                return String.class;
            }
            else {
                return Integer.class;
            }
        }
        else if (o instanceof RadioButtonManager)
        {
            // NOTE: this depends if you need the value or label
            if (useLabelsAsValues) {
                return String.class;
            }
            else {
                return Integer.class;
            }
        }
        else if (o instanceof Spinner)
        {
            // NOTE: this depends if you need the value or label
            if (useLabelsAsValues) {
                return String.class;
            }
            else {
                return Integer.class;
            }
        }
        else if (o instanceof CheckBox)
        {
            // this should check for booleans too
            return Integer.class;
        }
        else if (o instanceof SeekBar)
        {
            return Integer.class;
        }
        throw new RuntimeException("No matching widget for "+f.getName());
    }



    public void mapFieldsToModel(Fragment fragment, Object model)
    {
        Field[] fieldList = fragment.getClass().getFields();


        // NOTE: this assumes an @EFragment
        if (fieldList.length==0)
        {
            // this is a temp fix for AS hiding the fields
            fieldList = fragment.getClass().getSuperclass().getDeclaredFields();
        }
        System.out.println(Arrays.asList(fieldList));


        for (Field f : fieldList) {

            try {
                f.setAccessible(true);

                if (f.isAnnotationPresent(MapToModelField.class)) {

                    MapToModelField fieldLocation = f.getAnnotation(MapToModelField.class);
                    String fieldName = fieldLocation.value();

                    if (fieldName.equals("")) {
                        fieldName = f.getName(); // use the view's name
                    }


                    // get the setter name needed
                    String setter = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

                    Object o = f.get(fragment); // this is the view instance

                    Class[] params = new Class[]{getParamTypeByView(f, o)};
                    Object[] args = new Object[]{getValueByView(o)};

                    System.out.println("calling " + setter + "(" + args[0] + ")");

                    // invoke method from model
                    Method m = model.getClass().getDeclaredMethod(setter, params);
                    m.invoke(model, args);
                }
            } catch (Exception e) {
                System.out.println("Problem with field " + f.getName());

                throw new RuntimeException(model.getClass().getName()+" "+e);
            }
        }

    }





    public static Object getValueByView(Object o) throws Exception {

        Object retVal = getValueByViewInternal(o);
        System.out.println("Getting value from " + o+ " -> " + retVal);
        return retVal;
    }

    private static Object getValueByViewInternal(Object o) throws Exception
    {

        if (o instanceof EditText)
        {
            EditText et = (EditText) o;
            String raw = et.getText().toString();

            if ((et.getInputType() & InputType.TYPE_MASK_CLASS) == InputType.TYPE_CLASS_NUMBER) {
                if ((et.getInputType() & InputType.TYPE_MASK_FLAGS) == InputType.TYPE_NUMBER_FLAG_DECIMAL) {
                    try {
                        return Double.parseDouble(raw);
                    }
                    catch(Exception e)
                    {
                        //e.printStackTrace();
                        return null;
                    }
                } else {
                    try {
                        return Integer.parseInt(raw);
                    }
                    catch(Exception e)
                    {
                        //e.printStackTrace();
                        return null;
                    }
                }
            }

            return raw;
        }
        else if (o instanceof RadioGroup)
        {
            // might need to go through the radio group to find the checked one
            RadioGroup rg = (RadioGroup) o;
            for (int i = 0; i<rg.getChildCount();i++)
            {
                RadioButton rb = (RadioButton) rg.getChildAt(i);
                if (rb.isChecked())
                {
                    if (useLabelsAsValues)
                    {
                        return rb.getText();
                    }
                    else {
                        return i;
                    }
                }
            }

            return null;
        }
        else if (o instanceof RadioButtonManager)
        {
            return ((RadioButtonManager) o).getSelectedRadioIndex();

            // what to do if useLabelsAsValues is set?


        }
        else if (o instanceof Spinner) {
            // NOTE: this depends if it returns the postion or label

            if (useLabelsAsValues) {
                return ((Spinner) o).getSelectedItem();
            } else {
                int pos = ((Spinner) o).getSelectedItemPosition();
                return pos;
            }
        }
        else if (o instanceof CheckBox)
        {
            return ((CheckBox) o).isChecked() ? 1 : 0;
        }
        else if (o instanceof SeekBar)
        {
            return ((SeekBar) o).getProgress();
        }
        throw new RuntimeException("No matching widget for "+o.getClass().getName());
    }


}
