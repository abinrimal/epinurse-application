package com.ajwcc.util.reflect;

import android.support.v4.app.Fragment;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * Created by logan on 09/12/2017.
 */

public class PojoToViewMapper
{
    // this takes the values of the POJO/Model and puts them to the Views


    public static void setViewValue(Object view, Object value) throws Exception
    {
        System.out.println("Setting value of "+view+" to "+value);

        if (view instanceof EditText)
        {
            if (value==null)
            {
                ((EditText) view).setText("");
            }
            else {
                ((EditText) view).setText(String.valueOf(value));
            }
        }
        else if (view instanceof RadioGroup)
        {
            // might need to go through the radio group to find the checked one
            RadioGroup rg = (RadioGroup) view;

            if (value!=null) {
                if (value instanceof Integer) {
                    Integer selected = (Integer) value;
                    int id = -1;
                    for (int i = 0; i < rg.getChildCount(); i++) {
                        if (i == selected) {
                            id = rg.getChildAt(i).getId();
                            break;
                        }
                    }
                    rg.check(id);
                }
                else if (value instanceof String)
                {
                    // look for the radio button with the right text value
                    for (int i = 0; i<rg.getChildCount(); i++)
                    {
                        RadioButton rb = (RadioButton) rg.getChildAt(i);
                        if (rb.getText().toString().equals(value))
                        {
                            rg.check(rb.getId());
                            return;
                        }
                    }
                    rg.clearCheck();  // no match
                }
            }
            else
            {
                rg.clearCheck();
            }
        }
        else if (view instanceof RadioButtonManager)
        {
            if (value instanceof Integer) {
                if (value != null) {
                    ((RadioButtonManager) view).setSelectedRadioIndex((Integer) value);
                } else {
                    ((RadioButtonManager) view).clearCheck();
                }
            }
            else if (value instanceof String)
            {
                // ??  need to search labels
            }
        }
        else if (view instanceof Spinner)
        {
            if (value instanceof Integer)
            {
                if (value!=null) {
                    ((Spinner) view).setSelection((Integer) value);
                }
                else
                {
                    ((Spinner) view).setSelection(0);
                }
            }
            else if (value instanceof String)
            {
                // look for the spinner value with the right text value
                if (value!=null) {
                    SpinnerAdapter adapter = (((Spinner) view).getAdapter());
                    int count = adapter.getCount();
                    for (int i=0; i<count; i++)
                    {
                        Object item = adapter.getItem(i);

                        if (value.equals(item))
                        {
                            ((Spinner) view).setSelection(i);
                            return;
                        }
                    }

                    ((Spinner) view).setSelection(0);
                }
                else
                {
                    ((Spinner) view).setSelection(0);
                }
            }
        }
        else if (view instanceof CheckBox)
        {
            // check for boolean, String true/false?, or Int 0/1
            if (value!=null) {
                ((CheckBox) view).setChecked(((Integer) value) == 1);
            }
            else
            {
                ((CheckBox) view).setChecked(false);
            }
        }
        else if (view instanceof SeekBar)
        {
            if (value!=null) {
                ((SeekBar) view).setProgress((Integer) value);
            }
            else
            {
                ((SeekBar) view).setProgress(0);
            }
        }

        // TODO: might need to apply special cases for like the dual text field?
            // alternative is to extract all the fields so each are treated individually
        else {
            throw new RuntimeException("No matching widget: "+view.getClass().getName());
        }
    }

    public void mapModelToFields(Fragment fragment, Object model)
    {
//        System.out.println(Arrays.toString(fragment.getClass().getFields()));

        Field[] fieldList = fragment.getClass().getFields();

        // NOTE: this assumes an @EFragment
        if (fieldList.length==0)
        {
            // this is a temp fix for AS hiding the fields
            fieldList = fragment.getClass().getSuperclass().getDeclaredFields();
        }
        //System.out.println(Arrays.asList(fieldList));

        try {
            for (Field f : fieldList) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(MapToModelField.class)) {

                    MapToModelField fieldLocation = f.getAnnotation(MapToModelField.class);
                    String fieldName = fieldLocation.value();

                    if (fieldName.equals("")) {
                        fieldName = f.getName(); // use the view's name
                    }

                    // get the setter name needed
                    String getter = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);


                    // invoke method from model
                    Method m =  model.getClass().getDeclaredMethod(getter);
                    Object returnValue = m.invoke(model);

                    Object o = f.get(fragment);  // this is the view instance

                    // place into view
                    setViewValue(o, returnValue);
                }
            }
        }
        catch(Exception e)
        {
            throw new RuntimeException(model.getClass().getName()+" "+e);
        }
    }



}
