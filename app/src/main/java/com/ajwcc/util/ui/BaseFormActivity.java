package com.ajwcc.util.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajwcc.util.ui.validation.ValidationHandler;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseFormActivity extends AppCompatActivity
{
    private SeekBar seekBar;
    protected CustomPagerAdapter adapter;
    private CustomViewPager viewPager;

    public void onCreate(Bundle b)
    {
        super.onCreate(b);
    }

    public CustomPagerAdapter getAdapter()
    {
        return adapter;
    }

    public void next()
    {
        if (seekBar.getProgress()<seekBar.getMax())
        {
            seekBar.setProgress(seekBar.getProgress()+1);
        }
    }

    public void previous()
    {
        if (seekBar.getProgress()>0)
        {
            seekBar.setProgress(seekBar.getProgress()-1);
        }
    }

    public void initViewPagerAndSeekbar(final List<LazyFragment> fragmentList, final ViewPager viewPager, final SeekBar seekBar, final Button previous, final Button next)
    {
        initViewPagerAndSeekbar(getSupportFragmentManager(), fragmentList,viewPager,seekBar,previous,next);
    }

    public void initViewPagerAndSeekbar(FragmentManager fm, final List<LazyFragment> fragmentList, final ViewPager viewPager, final SeekBar seekBar, final Button previous, final Button next)
    {
        if (previous!=null) {
            previous.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    previous();
                }
            });
        }

        if (next != null) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    next();
                }
            });
        }

        this.viewPager = (CustomViewPager) viewPager;
        adapter = createAdapter(fm, fragmentList);
        viewPager.setAdapter(adapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                seekBar.setProgress(position);
                if (previous != null) {
                    previous.setEnabled(position > 0);
                }
                if (next != null) {
                    next.setEnabled(position < fragmentList.size() - 1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    // save when about to move
                    saveCurrentFragmentContents();
                }
                else if (state == ViewPager.SCROLL_STATE_IDLE)
                {
                    fragmentOnVisible();
                }
            }
        });

        this.seekBar = seekBar;

        viewPager.setOffscreenPageLimit(1);  // this need to be done to prevent the views being deallocated before mapping
        seekBar.setMax(adapter.getCount() - 1);
        seekBar.setProgress(0);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // how do you prevent a seekbar from switching to new position on a validation error?
                // seek bar is already moved at this point
                System.out.println("Changed to: " + seekBar.getProgress());

                saveCurrentFragmentContents();
                viewPager.setCurrentItem(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                System.out.println("Start track at: " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public CustomPagerAdapter createAdapter(FragmentManager fm, List<LazyFragment> fragmentList)
    {
        return new CustomPagerAdapter(fm, fragmentList);
    }

    public void fragmentOnVisible()
    {
        try {
            // WARNING: this only works if the fragment is live
            // starting to move, save the current contents
            ((BaseInputFragment) adapter.getItem(viewPager.getCurrentItem())).onVisible();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void saveCurrentFragmentContents()
    {
        try {
            // WARNING: this only works if the fragment is live
            // starting to move, save the current contents
            ((BaseInputFragment) adapter.getItem(viewPager.getCurrentItem())).mapViewsToModel();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    public interface LazyFragment
    {
        public Fragment create();
    }

    protected class FragmentHolder
    {
        LazyFragment lf;
        Fragment fragment;

        public FragmentHolder(LazyFragment lf)
        {
            this.lf = lf;
        }

        public Fragment get()
        {
            if (fragment==null)
            {
                fragment = lf.create();
            }

            return fragment;
        }
    }

    protected class CustomPagerAdapter extends
                                    // FragmentStatePagerAdapter
                                     FragmentPagerAdapter
    {
        List<FragmentHolder> fragmentList = new ArrayList<>();

        public List<FragmentHolder> getFragmentList()
        {
            return fragmentList;
        }

        public CustomPagerAdapter(FragmentManager fragmentManager, List<LazyFragment> fragmentList)
        {
            super(fragmentManager);

 //           System.out.println(fragmentList);

            for (LazyFragment lf : fragmentList)
            {
                this.fragmentList.add(new FragmentHolder(lf));
            }

        }

        public int getCount()
        {
            return fragmentList.size();
        }

        public Fragment getItem(int position)
        {
            return fragmentList.get(position).get();
        }

        public CharSequence getPageTitle(int position)
        {
            return "P"+position;
        }
    }



    public void finish()
    {
        super.finish();
    }


    public abstract Object getModel();
    public abstract void saveModel();



    // VALIDATION UTILS

    // this will be used for jumping to pages with missing data during failed validation
    public void jumpToPage(int index)
    {
        seekBar.setProgress(index);
    }

    public void jumpToPage(int index, int id)
    {
        seekBar.setProgress(index);

        try {
            final ScrollView sv = ((ScrollView) ((Fragment) adapter.getItem(index)).getView());
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View v = sv.findViewById(id);

                    System.out.println(v);
                    System.out.println(v.getParent());

                    int[] pos = new int[2];
                    v.getLocationOnScreen(pos);

                    //v.requestFocus();
                    System.out.println(pos[1]);
                    System.out.println(v.getHeight());

                    sv.scrollTo(0, pos[1]-v.getHeight());
                }
            });
        }
        catch(RuntimeException e)
        {
            e.printStackTrace();
        }
    }

    public void toastMessage(String s)
    {
        Toast.makeText(this,
                s,
                Toast.LENGTH_LONG).show();
    }

    public boolean validateForm()
    {
        try {
            if (handler != null) {
                // this throws Validation exceptions
                handler.validateModel();
            }
            return true;
        }
        catch(ValidationHandler.ValidationException e)
        {
            // jump to page
            //toastMessage(e.getMessage()); // this should go to the top of screen instead

            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,30);
            toast.show();

            if (e.getPage()!=-1)
            {
                jumpToPage(e.getPage(), e.getViewId());
            }
        }
        catch(Exception e)
        {
            // just toast
            toastMessage(e.getMessage());
        }
        return false;
    }

    private ValidationHandler handler;

    public void setValidationHandler(ValidationHandler handler)
    {
        this.handler = handler;
    }



}
