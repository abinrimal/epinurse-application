package com.ajwcc.util.ui;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.ajwcc.epinurse.common.utils.TimePickerFragment;
import com.ajwcc.util.reflect.PojoToViewMapper;
import com.ajwcc.util.reflect.ViewToPojoMapper;


public abstract class BaseInputFragment extends Fragment {


    public BaseInputFragment()
    {
    }

    public void openTimeDialog(String title, final EditText field)
    {
        final TimePickerFragment dialog = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putString("format","HH:mm");
        args.putString("time", field.getText().toString());
        dialog.setArguments(args);
        dialog.setOnTimeSetListener(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                field.setText(dialog.getFormattedTimeString(hour, minute));
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), title);
    }

    public void openDateDialog(String title, final EditText field)
    {
        final DatePickerFragment dialog = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putString("format","yyyy-MM-dd");
        args.putString("date", field.getText().toString());
        dialog.setArguments(args);
        dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                field.setText(dialog.getFormattedDateString(year, month, day));
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), title);
    }


    public void onStart()
    {
        super.onStart();
        System.out.println("start "+getClass().getName());
        mapModelToViews();
    }
//
//    public void onPause()
//    {
//        super.onPause();
//        System.out.println("stop "+getClass().getName());
//        mapViewsToModel();
//    }


    // MODEL MANAGEMENT


    public Object getModel() {
        // get the model from the activity
        return ((BaseFormActivity) getActivity()).getModel();
    }


    public void saveModel() {
        // save the model from the activity
        ((BaseFormActivity) getActivity()).saveModel();
    }



    PojoToViewMapper mapper = new PojoToViewMapper();
    ViewToPojoMapper mapper2 = new ViewToPojoMapper();


    // MODEL TO UI MAPPING


    public void mapModelToViews()
    {
            try {
                if (getModel()!=null) {
                    System.out.println(getClass().getCanonicalName()+" update views from pojo");
                    mapper.mapModelToFields(this, getModel());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }


    public void mapViewsToModel()
    {
        mapViewsToModel(true);
    }


    public void mapViewsToModel(boolean save)
    {
        try {
            if (getModel()!=null) {
                System.out.println(getClass().getCanonicalName()+" update pojo from views");
                mapper2.mapFieldsToModel(this, getModel());

                System.out.println(getModel());

                if (save) {
                    saveModel();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean validateModel()
    {
        return ((BaseFormActivity) getActivity()).validateForm();
    }

    public void onVisible()
    {
    }

}
