package com.ajwcc.util.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatePickerFragment extends android.support.v4.app.DialogFragment  {


    DatePickerDialog.OnDateSetListener listener;

    public DatePickerFragment() {
        // Required empty public constructor
    }

    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener listener)
    {
        this.listener = listener;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker

        String format = getArguments().getString("format");
        dateFormatter = new SimpleDateFormat(format!=null ? format : "MM-dd-yyyy");

        String timeString = getArguments().getString("date");

        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        try {
            Date date = dateFormatter.parse(timeString);
            calendar.setTime(date);
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

        } catch (ParseException e) {
            e.printStackTrace();
        }



        // Create a new instance of TimePickerDialog and return it
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), listener, year, month, day);

        if (getArguments().get("maxdate")!=null) {
            dpd.getDatePicker().setMaxDate(((Date) getArguments().getSerializable("maxdate")).getTime());
        }
        if (getArguments().get("mindate")!=null) {
            dpd.getDatePicker().setMinDate(((Date) getArguments().getSerializable("mindate")).getTime());
        }

        return dpd;
    }

    SimpleDateFormat dateFormatter;

    public String getFormattedDateString(int year, int month, int day)
    {
        Calendar c =  Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        return dateFormatter.format(c.getTime());
    }
}