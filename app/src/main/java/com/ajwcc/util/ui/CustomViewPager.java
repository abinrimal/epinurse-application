package com.ajwcc.util.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

    float mStartDragX;
    AllowSwipeListener mListener;


    public CustomViewPager(Context context)
    {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAllowSwipeListener(AllowSwipeListener listener) {
        mListener = listener;
    }




    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

//        float x = ev.getX();
//
//
//
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                System.out.println("down");
//                break;
//            case MotionEvent.ACTION_MOVE:
//                System.out.println("move");
//                break;
//
//            case MotionEvent.ACTION_UP:
//                System.out.println("up");
//                break;
//        }

//        return false;  // setting to false will disable the swiping


        if (mListener!=null) {
            if (mListener.allowSwipe()) {
                return super.onInterceptTouchEvent(ev);
            } else {
                return false;
            }
        }
        else
        {
            return super.onInterceptTouchEvent(ev);
        }
    }

    public interface AllowSwipeListener {
        public boolean allowSwipe();
    }

}