package com.ajwcc.util.ui.validation;




public interface ValidationHandler<T>
{
    public void setModel(T model);
    public void validateModel();


    public static class ValidationException extends RuntimeException
    {
        int page = -1;
        int viewId = -1;

        public ValidationException(String message)
        {
            super(message);
        }

        public ValidationException(String message, int page, int viewId)
        {
            super(message);
            this.page = page;
            this.viewId = viewId;
        }

        public int getPage()
        {
            return page;
        }
        public int getViewId()
        {
            return viewId;
        }

    }
}